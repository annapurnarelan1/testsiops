//
//  UIFont+Extension.swift
//  sIOPs
//
//  Created by Annapurna on 30/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

extension UIFont
{
    func fontWithBoldSize(ofsize size : CGFloat) -> UIFont  {
        return UIFont.init(name:"JioType-Bold", size: size) ??  .systemFont(ofSize: size)
    }
    func fontWithLightSize(ofsize size : CGFloat) -> UIFont  {
        return UIFont.init(name:"JioType-Light", size: size) ?? .systemFont(ofSize: size)
    }
    func fontWithMediumSize(ofsize size : CGFloat) -> UIFont  {
        return UIFont.init(name:"JioType-Medium", size: size) ?? .systemFont(ofSize: size)
    }
    func fontWithRegularSize(ofsize size : CGFloat)  -> UIFont {
        return UIFont.init(name:"JioType-Regular", size: size) ?? .systemFont(ofSize: size)
    }
}
