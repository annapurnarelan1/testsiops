//
//  UIColor+Extension.swift
//  sIOPs
//
//  Created by Annapurna on 30/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

extension UIColor
{
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(CFloat(red) / 255.0),
            green: CGFloat(CFloat(green) / 255.0),
            blue: CGFloat(Float(blue) / 255.0),
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
    
    convenience init(hexString:String) {
        let hexString:String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner            = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }

    static let myBlue = UIColor(red:0.043, green:0.576 ,blue:0.588, alpha:1.00)
    static let lightGreyColor = UIColor.init(hexString: "F4F4F4")
    static let strongBlueColor =  UIColor.init(hexString: "0078C1")
    static let veryLightBlueColor =  UIColor.init(hexString: "89B5D7")
    static let darkBlueColor =  UIColor.init(hexString: "214796")
    static let darkYellowColor =  UIColor.init(hexString: "D8B600")
    static let orangeColor =  UIColor.init(hexString: "FF9E00")
    static let redColor =  UIColor.init(hexString: "FF3B30")
    static let limeGreenColor =  UIColor.init(hexString: "2ECC71")
    static let lightOrangeColor =  UIColor.init(hexString: "B58D3D")
    static let verylightGreyColor =  UIColor.init(hexString: "D9D9D9")
    
//    func darkGreyColor() -> UIColor {
//           return UIColor.init(hexString:"7B7B7B")
//    }
//    func darkblueColor() -> UIColor {
//           return UIColor.init(hexString:"0078C1")
//    }
//    func veryLightBlueColor() -> UIColor {
//           return UIColor.init(hexString:"89B5D7")
//    }
//    func darkBlueColor() -> UIColor {
//        return UIColor.init(hexString:"214796")
//    }
//    func darkYellowColor() -> UIColor {
//         return UIColor.init(hexString:"D8B600")
//     }
//    func orangeColor() -> UIColor {
//        return UIColor.init(hexString:"FF9E00")
//       }
//    func redColor() -> UIColor {
//        return UIColor.init(hexString:"FF3B30")
//       }
//    func limeGreenColor() -> UIColor {
//        return UIColor.init(hexString:"2ECC71")
//    }
//    func lightOrangeColor() -> UIColor {
//           return UIColor.init(hexString:"B58D3D")
//       }
//    func verylightGreyColor() -> UIColor {
//        return UIColor.init(hexString:"D9D9D9")
//    }
}
