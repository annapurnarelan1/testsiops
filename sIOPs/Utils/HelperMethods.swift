//
//  HelperMethods.swift
//  sIOPs
//
//  Created by Gunjan on 03/12/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import UIKit
import UtilitiesComponent
class HelperMethods: NSObject {
    
    
    
    
    func addNotificationButton(_ vc : UIViewController, clickable : Bool,backbutton:Bool){
        Constants.Notification.notificationButton = UIButton.init(frame: CGRect(x:0, y:0, width:40, height:30))
        Constants.Notification.notificationButton?.setImage(UIImage(named: "notificationIcon"), for: .normal)
        
        Constants.Notification.notificationButton?.isUserInteractionEnabled = clickable
        
        let logoutBtn : UIButton = UIButton.init(frame: CGRect(x:0, y:0, width:40, height:30))
        logoutBtn.setImage(UIImage(named: "logout"), for: .normal)
        
        logoutBtn.isUserInteractionEnabled = clickable
        logoutBtn.addTargetClosure { (sender) in
            
             BaseWireFrame.instantiateBase(fromView: vc)
             (vc as! BaseViewController).basePresenter?.logout()
           
        }
        
        
        Constants.Notification.notificationButton?.addTargetClosure { (sender) in
            
            NotificationWireFrame.presentNotificationModule(fromView: self)
        }
        
        
        let barButtonBackStr = " \(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "")\n \(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.ngoMaxRole ?? "")"
       
        let main_string =  barButtonBackStr
        let string_to_color = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? ""
        let roleString_to_color = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.ngoMaxRole ?? ""
        
        
        let range = (main_string as NSString).range(of: string_to_color)
        let range2 = (main_string as NSString).range(of: roleString_to_color)
        
        let attributedBarButtonBackStr = NSMutableAttributedString(string: barButtonBackStr )
        attributedBarButtonBackStr.addAttribute(NSAttributedString.Key.font,
                                                value: UIFont(
                                                    name: "JioType-Medium",
                                                    size: 14.0)!, range: range)
        attributedBarButtonBackStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: range)
        attributedBarButtonBackStr.addAttribute(NSAttributedString.Key.font,
                                                value: UIFont(
                                                    name: "JioType-Medium",
                                                    size: 14.0)!, range: range)
        attributedBarButtonBackStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(red: 239, green: 201, blue: 21) , range: range2)
        var width = 120
        if(!backbutton)
        {
            width = 150
        }
        let leftButtonView = UIView.init(frame: CGRect(x:-20, y: 0, width: width, height: 50))
        
        let leftButton = UIButton.init(type: .system)
        leftButton.backgroundColor = .clear
        leftButton.frame = leftButtonView.frame
        if(!backbutton)
        {
            leftButton.setImage(UIImage.init(imageLiteralResourceName: "backIcon"), for: .normal)
        }
        
        leftButton.titleLabel?.lineBreakMode = .byWordWrapping
        leftButton.titleLabel?.numberOfLines = 0
        leftButton.titleLabel?.sizeToFit()
        leftButton.setAttributedTitle(attributedBarButtonBackStr, for: .normal)
        
        leftButton.addTargetClosure { (sender) in
            
            vc.navigationController?.popViewController(animated: true)
        }
        
        
       
        leftButtonView.addSubview(leftButton)
        
        
        
        
        let newBackButton = UIBarButtonItem(customView: leftButtonView)
        vc.navigationItem.leftBarButtonItem = newBackButton
        
        vc.navigationItem.setRightBarButtonItems([UIBarButtonItem(customView: logoutBtn),UIBarButtonItem(customView:Constants.Notification.notificationButton!)], animated: true)
        
        
    }
    
    func addLogoOnNavBar(_ vc : UIViewController){
        let imageView = UIImageView(image:UIImage(named: "logo"))
        // imageView.contentMode = .scaleAspectFit
        vc.navigationItem.titleView = imageView
        vc.navigationItem.titleView?.isUserInteractionEnabled = true
        let btn = UIButton.init(frame: vc.navigationItem.titleView?.frame ?? CGRect.init(x: 0, y: 0, width: vc.view.frame.width, height: vc.view.frame.width))
        vc.navigationItem.titleView?.addSubview(btn)
        btn.addTargetClosure { (sender) in
            
           
            if let button = vc.navigationController?.navigationBar.viewWithTag(567) as? UIButton{
                button.sendActions(for: .touchUpInside)
            }
            BaseWireFrame.instantiateBase(fromView: vc)
            vc.view.endEditing(true)
            (vc as! BaseViewController).basePresenter?.popToRoot()
            // }
        }
    }
    
    
    func registerPushNotification(_ application: UIApplication){

               UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in

                   if granted {
                       print("Notification: Granted")

                   } else {
                       print("Notification: not granted")

                   }
               }

               application.registerForRemoteNotifications()
           }
    
    func addMenuButton(_ vc : UIViewController){
        let btn1 = UIButton()
        btn1.tag = 567
        btn1.setImage(UIImage(named: "notificationIcon"), for: .normal)
        btn1.frame = CGRect(x:0, y:0, width:20, height:30)
        
        
        let logoutBtn : UIButton = UIButton.init(frame: CGRect(x:0, y:0, width:40, height:30))
        logoutBtn.setImage(UIImage(named: "logout"), for: .normal)
        
        logoutBtn.isUserInteractionEnabled = true
       
        
        btn1.addTargetClosure { (sender) in
            
            NotificationWireFrame.presentNotificationModule(fromView: self)
        }
        
        logoutBtn.addTargetClosure { (sender) in
//            UserDefaults.standard.removeObject(forKey: "fcmToken")
//            LoginModel.sharedInstance.doLogout()
//            LoginWireFrame.presentLoginModule(fromView: self)
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "remoteNotif"), object: nil)
            BaseWireFrame.instantiateBase(fromView: vc)
            (vc as! BaseViewController).basePresenter?.logout()
        }
        
        
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: vc.navigationController, action: nil)
        vc.navigationItem.rightBarButtonItem = backButton
        // vc.navigationItem.setRightBarButton(UIBarButtonItem(customView: btn1), animated: true)
        vc.navigationItem.setRightBarButtonItems([UIBarButtonItem(customView: logoutBtn),UIBarButtonItem(customView:btn1)], animated: true)
        vc.navigationItem.setHidesBackButton(true, animated:true)
    }
    
    func addCustomBarButtons(_ vc : UIViewController, cartClickable:Bool,isMenu:Bool){
        HelperMethods().addNotificationButton(vc, clickable: cartClickable,backbutton:isMenu)
        HelperMethods().addLogoOnNavBar(vc)
        
        if(isMenu)
        {
            HelperMethods().addMenuButton(vc)
            vc.navigationItem.hidesBackButton = true
        }else{
            
        }
    }
    
    func removeCustomBarButtons(_ vc : UIViewController){
        vc.navigationItem.setLeftBarButton(nil, animated: false)
        vc.navigationItem.setRightBarButton(nil, animated: false)
        if let subviews = vc.navigationItem.titleView?.subviews{
            for view in subviews{
                view.removeFromSuperview()
            }
        }
    }
    
   
    func appendAttributedString(attrStr : (NSMutableAttributedString),arrayDictAttributes :[Dictionary<String,Any>]){
        for dictAttributes in arrayDictAttributes {
            let tempStr = NSMutableAttributedString.init(string: dictAttributes["string"] as? String ?? "", attributes: dictAttributes["attributes"] as? [NSAttributedString.Key : Any] ?? [:])
            attrStr.append(tempStr)
        }
    }
    
    func addTarget(_ vc : UIViewController, _ handler : @escaping UIButtonTargetClosure){
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        vc.navigationController!.navigationBar.addSubview(button)
        button.addTargetClosure(closure: handler)
        button.topAnchor.constraint(equalTo: (vc.navigationController!.navigationBar.topAnchor), constant: 0).isActive = true
        button.bottomAnchor.constraint(equalTo: (vc.navigationController!.navigationBar.bottomAnchor), constant: 0).isActive = true
        button.leadingAnchor.constraint(equalTo: (vc.navigationController!.navigationBar.leadingAnchor), constant: 0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 130).isActive = true
        vc.navigationController!.navigationBar.bringSubviewToFront(button)
    }
    
    func formatDate(date : String) -> String {
        
        var formattedDate : String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM"
        if let dateFrom = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: dateFrom))
            formattedDate = (dateFormatterPrint.string(from: dateFrom))
        } else {
            print("There was an error decoding the string")
        }
        return formattedDate
    }
    
    
    func formatDateString(date : String) -> String {
        
        var formattedDate : String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM"
        
        
        if let dateFrom = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: dateFrom))
            formattedDate = (dateFormatterPrint.string(from: dateFrom))
            
        } else {
            print("There was an error decoding the string")
        }
        
        return formattedDate
    }
    
    
    func formatDateNotif(date : String) -> String {
        
        var formattedDate : String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yy HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM hh:mm a"
        
        
        if let dateFrom = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: dateFrom))
            formattedDate = (dateFormatterPrint.string(from: dateFrom))
            
        } else {
            print("There was an error decoding the string")
        }
        
        return formattedDate
    }
    
    func formatDateJourney(date : String) -> String {
        
        var formattedDate : String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yy HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        
        if let dateFrom = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: dateFrom))
            formattedDate = (dateFormatterPrint.string(from: dateFrom))
            
        } else {
            print("There was an error decoding the string")
        }
        
        return formattedDate
    }
    
    func formatDateOrderJourney(date : String) -> String {
        
        var formattedDate : String = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy"
        
        
        if let dateFrom = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: dateFrom))
            formattedDate = (dateFormatterPrint.string(from: dateFrom))
            
        } else {
            print("There was an error decoding the string")
        }
        
        return formattedDate
    }
    
    func nuumberFormatting(value : String) -> String {
        guard let numberValue = Int(value) else { return "" }
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let formattedInt = String(format: "%.0f", locale:Locale.init(identifier: "en-US"),Double(numberValue))
        //guard let formattedNumber = numberFormatter.string(from: NSNumber(value:numberValue)) else { return "" }
        return formattedInt
    }
}

class BlockBarButtonItem : UIBarButtonItem {
    
    private var actionHandler: (() -> Void)?
    
    convenience init(image: UIImage?, actionHandler: (() -> Void)?) {
        self.init(image: image, style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.target = self
        self.action = #selector(BlockBarButtonItem.barButtonItemPressed(_:))
        self.actionHandler = actionHandler
    }
    
    convenience init(title: String?, actionHandler: (() -> Void)?) {
        self.init(title: title, style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.target = self
        self.action = #selector(BlockBarButtonItem.barButtonItemPressed(_:))
        self.actionHandler = actionHandler
    }
    
    convenience init(image: UIImage?, title: String?, actionHandler: (() -> Void)?) {
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 31))
        
        let button = UIButton(frame: CGRect.init(x: 0, y: 0, width: 80, height: 31))
        button.setImage(image, for: UIControl.State.normal)
        button.setImage(image, for: UIControl.State.highlighted)
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        button.tintColor = UINavigationBar.appearance().tintColor
        
        let label = UIButton(frame:CGRect.init(x: 20, y: 0, width: 80, height: 31))
        label.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label.setTitle(title, for: UIControl.State.normal)
        label.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        label.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        label.isUserInteractionEnabled = false
        label.setTitleColor(UINavigationBar.appearance().tintColor, for: UIControl.State.normal)
        
        button.addSubview(label)
        view.addSubview(button)
        
        self.init(customView: view)
        
        button.addTarget(self, action: #selector(BlockBarButtonItem.barButtonItemPressed(_:)), for: UIControl.Event.touchUpInside)
        self.actionHandler = actionHandler
    }
    
    @objc func barButtonItemPressed(_ sender: UIBarButtonItem) {
        if let actionHandler = self.actionHandler {
            actionHandler()
        }
    }
    
}

class CustomVC : NSObject{
    
    lazy var backButtonImage = UIImage(named: "backIcon")
    
    func navigationControllerPop(_ vc : UIViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
    func createEmptyButton() -> UIBarButtonItem {
        return UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
    }
    
    func createCustomBackButton(_ image: UIImage?, title: String?, leftSpaceCloseToDefault:Bool, backFn: @escaping (() -> ())) -> [UIBarButtonItem] {
        var arr: [UIBarButtonItem] = []
        if leftSpaceCloseToDefault {
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -8
            arr.append(negativeSpacer)
        }
        if let image = image,let title = title {
            let button = BlockBarButtonItem(image: image, title: title, actionHandler: backFn)
            arr.append(button)
        } else {
            if let image = image {
                let imageButton = BlockBarButtonItem(image: image, actionHandler: backFn)
                imageButton.title = title
                arr.append(imageButton)
            }
            if let title = title {
                let titleButton = BlockBarButtonItem(title: title, actionHandler: backFn)
                arr.append(titleButton)
            }
        }
        return arr
    }
    
    func createBackButtonGeneral(_ vc : UIViewController) -> [UIBarButtonItem] {
        return createCustomBackButton(backButtonImage, title: nil, leftSpaceCloseToDefault: true, backFn: {CustomVC().navigationControllerPop(vc)})
    }
    
    func createBackButtonWithFn(_ backFn: @escaping (() -> ()) ) -> [UIBarButtonItem] {
        return createCustomBackButton(backButtonImage, title: nil, leftSpaceCloseToDefault: true, backFn: backFn)
    }
    
    func updateNavigationBarUI(vc : UIViewController,leftItem : LeftItem) {
        vc.navigationItem.backBarButtonItem = createEmptyButton()
        vc.navigationItem.leftBarButtonItem = nil
        vc.navigationItem.leftBarButtonItems = nil
        vc.navigationItem.hidesBackButton = true
        vc.navigationController?.navigationBar.backgroundColor = Constants.COLOR.loginEnabled
        vc.navigationController?.navigationBar.barTintColor  = Constants.COLOR.loginEnabled
        switch leftItem {
        case .none:
            break;
        case .backSystemDefault:
            vc.navigationItem.backBarButtonItem = nil
            vc.navigationItem.hidesBackButton = false
        case .backFn(let visibilityFn, let backFn):
            if (visibilityFn()) {
                vc.navigationItem.leftBarButtonItems = createBackButtonWithFn(backFn)
            }
        case .backCustom(let image, let title, let leftSpaceCloseToDefault):
            if (vc.navigationController?.viewControllers.count ?? 0 > 1) {
                vc.navigationItem.leftBarButtonItems = createCustomBackButton(image, title: title, leftSpaceCloseToDefault: leftSpaceCloseToDefault, backFn: {CustomVC().navigationControllerPop(vc)})
            }
        case .backCustomFn(let image, let title, let leftSpaceCloseToDefault, let visibilityFn, let backFn):
            if (visibilityFn()) {
                vc.navigationItem.leftBarButtonItems = createCustomBackButton(image, title: title, leftSpaceCloseToDefault: leftSpaceCloseToDefault, backFn: backFn)
            }
        case .customView(let view):
            vc.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
        case .backGeneral:
            if (vc.navigationController?.viewControllers.count ?? 0 > 1) {
                vc.navigationItem.leftBarButtonItems = createBackButtonGeneral(vc)
            }
        }
    }
}

enum LeftItem {
    case none
    case backSystemDefault
    case backGeneral
    case backCustom(image: UIImage?, title: String?, leftSpaceCloseToDefault:Bool)
    case backFn(visibilityFn:(() -> Bool), backFn:(() -> ()))
    case backCustomFn(image: UIImage?, title: String?, leftSpaceCloseToDefault:Bool, visibilityFn:(() -> Bool), backFn:(() -> ()))
    case customView(view: UIView)
}



