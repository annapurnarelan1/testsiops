//
//  DoubleExtension.swift
//  MerchantApp
//
//  Created by Sebastian Jorquera on 10/9/17.
//  Copyright © 2017 Sebastian Jorquera. All rights reserved.
//

import Foundation
extension Double {
    
    /// Rounds the double to decimal places value
    ///
    /// - Parameter fractionDigits: Numbers of digits
    /// - Returns: A double with the decimals needed
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10.0, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func roundTwoDecimalPoints() -> String{
        return String.init(format: "%.2f", self)
    }
}
