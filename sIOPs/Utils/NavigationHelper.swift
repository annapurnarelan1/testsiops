//
//  NavigationHelper.swift
//  MerchantApp
//
//  Created by Sebastian Jorquera on 10/7/17.
//  Copyright © 2017 Sebastian Jorquera. All rights reserved.
//

import Foundation
import UIKit
import Dropdowns

/// Used to do allK the pushes and pops
class NavigationHelper{
    
   static var dropdown:DropdownController?
    /// Sets the root view controller
    ///
    /// - Parameter withViewController: View controller to set as root view controller
    static func setRootViewController(withViewController: UIViewController){
        let navigationController = UINavigationController(rootViewController: withViewController)
        navigationController.setNavigationBarHidden(true, animated: false)
        
        //let navigationController = UINavigationController(rootViewController: rootVC)
        
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
    /// Pushes a new view controller to the navigation controller
    ///
    /// - Parameter viewController: UIViewController to be pushed
    static func pushViewController(viewController: UIViewController){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.pushViewController(viewController, animated: true)
    }
    
    /// Pushes a new view controller to the navigation controller without navigation
    ///
    /// - Parameter viewController: UIViewController to be pushed
    static func pushViewControllerWithoutNavigation(viewController: UIViewController){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.pushViewController(viewController, animated: false)
    }
    
    /// Makes a pop of the current view, to go the previous screen
    ///
    /// - Parameter animated: animated flag
    static func singleBack(animated: Bool){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.popViewController(animated: animated)
    }
    
    /// Pops all views until the root view controller
    ///
    /// - Parameter animated: animation flag
    static func popToRootViewController(animated: Bool){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.popToRootViewController(animated: animated)
    }
    
    
    /// Presents a view controller using the top view controller as a presenter
    ///
    /// - Parameter viewController: UIViewController to be presented
    static func presentViewController(viewController: UIViewController){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.present(viewController, animated: true, completion: {})
    }
    
    /// Presents a view controller using the top view controller as a presenter
    ///
    /// - Parameter viewController: UIViewController to be presented
    static func presentDropDownMenu(viewController: UIViewController){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
//        let dropdownView = LMDropdownView()
//        dropdownView.show(from: navigationController, withContentView: viewController.view)
        if let drop = dropdown
        {
            
            
            drop.hide()
            
            dropdown = nil
        }
        
        dropdown = DropdownController(contentController: viewController, navigationController: navigationController)
        dropdown?.show()
        
    }
    /// Presents a view controller using the top view controller as a presenter
    ///
    /// - Parameter viewController: UIViewController to be presented
    static func hideDropDownMenu(){
       
        dropdown?.hide()
        dropdown = nil

        
    }
    
    static func backToViewController(vc: Any) {
        // iterate to find the type of vc
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        for element in navigationController.viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                navigationController.popToViewController(element, animated: true)
                break
            }
        }
    }
    
    /// pop back n viewcontroller
   static func popBack(_ nb: Int) {
    
    let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
    if let viewControllers: [UIViewController] = navigationController.viewControllers {
            guard viewControllers.count < nb else {
                navigationController.popToViewController(viewControllers[viewControllers.count - nb], animated: false)
                return
            }
        }
    }
    
    static func fetchVc(forClass : AnyClass)->UIViewController?{
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        if let viewControllers: [UIViewController] = navigationController.viewControllers {
            for viewController in viewControllers{
                if viewController.isKind(of: forClass){
                    return viewController
                }
        }
            return nil
    }
    }
    
    static func lastVc()->UIViewController?{
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        return navigationController.viewControllers.last
    }
    
    static func removeVCs(_ classes : [AnyClass]){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        if var viewControllers: [UIViewController] = navigationController.viewControllers {
            var i = 0
            for vc in viewControllers{
                for vcClass in  classes{
                    if vc.isKind(of: vcClass), !(navigationController.visibleViewController?.isKind(of: vcClass))!{
                        viewControllers.remove(at: i)
                    }
                }
                i += 1
            }
           navigationController.setViewControllers(viewControllers, animated: false)
        }
    }
    
    static func popToViewController(_ vc : UIViewController, animated: Bool){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.popToViewController(vc, animated: true)
    }
    
    static func dismissController(animated: Bool){
        let navigationController = UIApplication.shared.windows[0].rootViewController as! UINavigationController
        navigationController.dismiss(animated: animated, completion: nil)
    }
}






