//
//  ErrorViewController.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 18/10/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import UIKit
import UtilitiesComponent

class ErrorViewController: UIViewController {

    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var erorlabel: UILabel!
    @IBOutlet weak var errorDescLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var viewLine: UIView!
    
    var error : String = ""
    var descriptionStr : String = ""
    var imageStr : String = ""
    var update = false
    
    /// init view with error message , description and image
    ///
    /// - Parameters:
    ///   - errorString: String with the error
    ///   - description: String with the description
    ///   - errorImage: string with the error image
    convenience init(errorImage : String, errorString : String, description: String ) {
        
        self.init(nibName:"ErrorViewController", bundle:nil)
        self.imageStr = errorImage
        self.error = errorString
        self.descriptionStr = description
        
    }
    
   /// Overwritten method from UIVIewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isOpaque = false
        self.errorImage.image = UIImage(named: self.imageStr)
        
        if(self.imageStr == "info")
        {
            self.erorlabel.font = UIFont.systemFont(ofSize: 30.0)
             self.erorlabel.addTextWithImage(text: "", image: UIImage(named: error)!, imageBehindText: false, keepPreviousText: false)
            
        }
        else
        {
            self.erorlabel.text = self.error.uppercased()
        }
        
        if update
        {
            self.okButton.isHidden = true
            self.viewLine.isHidden = true
        }
        
        self.errorDescLabel.text = self.descriptionStr
        
        

        // Do any additional setup after loading the view.
    }
    
    /// Overwritten method from UIVIewController
    ///
    /// - Parameter animated: animation flag

    
    override func viewWillLayoutSubviews() {
        okButton.layer.cornerRadius = okButton.frame.size.height/2
        okButton.layer.masksToBounds = true
    }

    /// button action to dismiss the view
    @IBAction func okButtonAcn(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
