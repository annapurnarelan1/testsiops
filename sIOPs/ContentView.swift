//
//  ContentView.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 26/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
