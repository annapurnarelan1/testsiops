//
//  BaseProtocols.swift
//  sIOPs
//
//  Created by Gunjan on 29/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation
import UIKit

/// Method contract between PRESENTER -> VIEW
protocol BaseViewProtocol: class {
    var basePresenter: BasePresenterProtocol? { get set }
   
      
      func stopLoaderBase()
      func showFailErrorBase(error:String)
}

/// Method contract between PRESENTER -> WIREFRAME
protocol BaseWireFrameProtocol: class {
   
    
}

/// Method contract between VIEW -> PRESENTER
protocol BasePresenterProtocol: class {
    var baseView: BaseViewProtocol? { get set }
    var baseInteractor: BaseInteractorInputProtocol? { get set }
    var baseWireFrame: BaseWireFrameProtocol? { get set }
    
    func popToRoot()
    func filterNavStack()
    func logout()
}

/// Method contract between INTERACTOR -> PRESENTER
protocol BaseInteractorOutputProtocol: class {
   

      
       
       func stopLoaderBase()
       func showFailErrorBase(error:String)
}

/// Method contract between PRESENTER -> INTERACTOR
protocol BaseInteractorInputProtocol: class
{
    var basePresenter: BaseInteractorOutputProtocol? { get set }
    var baseAPIDataManager: BaseAPIDataManagerInputProtocol? { get set }
    
    /**
     * Add here your methods for communication
     */
    
    func popToRoot()
    func filterNavStack()
    func logout()
}

/// Method contract between INTERACTOR -> DATAMANAGER
protocol BaseDataManagerInputProtocol: class{
    
    
}

/// Method contract between INTERACTOR -> APIDATAMANAGER
protocol BaseAPIDataManagerInputProtocol: class{
   
}


