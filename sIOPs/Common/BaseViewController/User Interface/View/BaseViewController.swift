//
//  BaseViewController.swift
//  sIOPs
//
//  Created by Gunjan on 29/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Toast

/// Base view controller, used to use common methods
class BaseViewController: UIViewController ,NVActivityIndicatorViewable,BaseViewProtocol,UIGestureRecognizerDelegate{
    
    var basePresenter: BasePresenterProtocol?
    let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
    var iPadUI: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let leftItem: LeftItem = LeftItem.backGeneral
        CustomVC().updateNavigationBarUI(vc: self, leftItem: leftItem)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
        
        checkDevice()
        
       // NotificationCenter.default.addObserver(self, selector: #selector(localize), name: NSNotification.Name(rawValue: MCLocalizationLanguageDidChangeNotification), object: nil)
        
    }
    
    /// Overwritten method from UIVIewController, calls the presenter to get the required data
    ///
    /// - Parameter animated: animation flag
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        filterNavStack()
    }
    
   
    
    

//MARK:-  Copyright © 2017 Sebastian Jorquera. All rights reserved.

    /// Shows an alert on top of the current view controller
    ///
    /// - Parameters:
    ///   - message: String with the message
    ///   - title: String with the title
    ///   - handler: Block of code to execute when the user clicks on a
    func showErrorInAlert(message: String = "", title: String = "", handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    /// Shows an alert view on top of the current view controller
    ///
    /// - Parameters:
    ///   - error: String with the error
    ///   - description: String with the description
    ///   - image: string with the error image
    
    func showErrorView(image: String = "",error: String = "", description: String = ""){
        
        let errorController  = ErrorViewController(errorImage : image, errorString : error, description: description )
        errorController.modalPresentationStyle = .overCurrentContext
        //  self.present(errorController, animated: false, completion: nil)
        
        //let appDelegate = UIApplication.shared.delegate as! SceneDelegate
        UIApplication.shared.windows.first?.rootViewController!.present(errorController, animated: true, completion: nil)
        
        
    }
    
    class func showUpdateErrorView( description: String = ""){
        
        let errorController  = ErrorViewController(errorImage : "", errorString : "", description: description )
        errorController.modalPresentationStyle = .overCurrentContext
       
        
        
        errorController.update = true
       
        UIApplication.shared.windows.first?.rootViewController!.present(errorController, animated: true, completion: nil)
        
        
    }
    
    func senReminderPopUp(image: String = "",error: String = "", description: String = "",apkInstallationStatus:Int){
        
        let errorController  = ErrorViewController(errorImage : image, errorString : error, description: description )
        errorController.modalPresentationStyle = .overCurrentContext
        
        
        
        //  self.present(errorController, animated: false, completion: nil)
        
        //let appDelegate = UIApplication.shared.delegate as! SceneDelegate
        UIApplication.shared.windows.first?.rootViewController!.present(errorController, animated: true, completion: nil)
        if(apkInstallationStatus == 0)
        {
            errorController.errorDescLabel.textColor = UIColor.red
        }
        
        
    }
    
    /// Shows an toast message on current view controller
    ///
    /// - Parameters:
    ///   - error: String with the error
    
    
    func showToastMsg(error: String = ""){
        
        //self.view.makeToast(error, duration: 2.0, position: CSToastPositionCenter)
        UIApplication.shared.keyWindow?.makeToast(error, duration: 2.0, position: CSToastPositionCenter)
        
    }
    
    
    
    
    func reloadData(){
        
    }
    
    /// Sets a message in a label if the collection view has no items to show
    ///
    /// - Parameters:
    ///   - collectionView: Collectionview where the items are going to be shown
    ///   - array: array of items to be shown
    ///   - emptyMessage: String with the empty message
    func setupEmptyMessageIfNeeded(collectionView: UICollectionView, array: [Any], emptyMessage: String!) {
        collectionView.backgroundView = nil
        if (!(array.count > 0)){
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
            noDataLabel.text          = emptyMessage
            noDataLabel.numberOfLines = 0
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            collectionView.backgroundView  = noDataLabel
        }
    }
    
    /// Sets a message in a label if the collection view has no items to show
    ///
    /// - Parameters:
    ///   - tableView: TableView where the items are going to be shown
    ///   - array: array of items to be shown
    ///   - emptyMessage: String with the empty message
    func setupEmptyMessageIfNeededForTable(tableView: UITableView, array: [Any], emptyMessage: String!) {
        tableView.backgroundView = nil
        if (!(array.count > 0)){
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = emptyMessage
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
        }
    }
    
    /// Animates the passed view
    ///
    /// - Parameters:
    ///   - view: View to animate
    ///   - duration: duration of the animation
    func animateView(view:UIView, duration: TimeInterval){
        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: .beginFromCurrentState,
            animations: { () -> Void in
                view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }) { (completed:Bool) -> Void in
            UIView.animate(
                withDuration: duration,
                delay: 0,
                options: .beginFromCurrentState,
                animations: { () -> Void in
                    view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }) { (completed:Bool) -> Void in
            }
        }
    }

    func filterNavStack(){
//        if NavigationHelper.fetchVc(forClass: ChooseUserViewController.self) != nil , let vc = NavigationHelper.lastVc(), !vc.isKind(of: ChooseUserViewController.self), !vc.isKind(of: RegisterPopUpViewController.self){
//            NavigationHelper.removeVCs([ChooseUserViewController.self,RegisterPopUpViewController.self])
//
//        }
    }
    
    
    func stopLoaderBase() {
          stopAnimating()
      }
      
      func showFailErrorBase(error:String)
      {
          stopAnimating()
          self.showToastMsg(error: error)
      }
      
      
     
    @objc func localize()
    {
        // Override this method to localize your components
    }
    
    func checkDevice()
    {
        switch (deviceIdiom) {
            
        case .pad:
            self.iPadUI = true
        case .phone:
            self.iPadUI = false
        case .tv:
            print("User Interface 💖 Not supported")
        default:
            print("User Interface 💖 Not supported")
        }
    }
}
