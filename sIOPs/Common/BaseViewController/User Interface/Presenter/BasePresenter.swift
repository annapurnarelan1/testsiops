//
//  BasePresenter.swift
//  sIOPs
//
//  Created by Gunjan on 29/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation
import UIKit

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class BasePresenter: BasePresenterProtocol, BaseInteractorOutputProtocol {
    
    
    //MARK:-  Copyright © 2017 Sebastian Jorquera. All rights reserved.
    /// Gets a string from the given table and key
    ///
    /// - Parameters:
    ///   - key: key to get the string
    ///   - table: table from where to get the string
    /// - Returns: String with the message
    func localizedString(forKey key: String, fromTable table:String) -> String {
        return Bundle.main.localizedString(forKey: key, value: nil, table: table)
    }
    
    // MARK: Variables
    weak var baseView: BaseViewProtocol?
    var baseInteractor: BaseInteractorInputProtocol?
    var baseWireFrame: BaseWireFrameProtocol?
    
    // MARK: BasePresenterProtocol
    
   
   
    
    func popToRoot() {
        self.baseInteractor?.popToRoot()
    }
    
    
   
    func filterNavStack() {
        self.baseInteractor?.filterNavStack()
    }
    
    
    func logout()
    {
         self.baseInteractor?.logout()
    }
    
    
    /// Called to show error popup
      
      
        func stopLoaderBase()
        {
            self.baseView?.stopLoaderBase()
        }
    
    func showFailErrorBase(error:String)
       {
           self.baseView?.showFailErrorBase(error:error)
       }
    
   
    
}
