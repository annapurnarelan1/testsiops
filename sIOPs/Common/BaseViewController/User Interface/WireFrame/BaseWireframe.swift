//
//  BaseWireframe.swift
//  sIOPs
//
//  Created by Gunjan on 29/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation


/// Wireframe that handles all routing between views
class BaseWireFrame: BaseWireFrameProtocol {
    
    // MARK: BaseWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func instantiateBase(fromView:AnyObject) {
        
        // Generating module components
        let view: BaseViewProtocol = fromView as! BaseViewProtocol
        let presenter: BasePresenterProtocol & BaseInteractorOutputProtocol = BasePresenter()
        let interactor: BaseInteractorInputProtocol = BaseInteractor()
        let APIDataManager: BaseAPIDataManagerInputProtocol = BaseAPIDataManager()
        let wireFrame: BaseWireFrameProtocol = BaseWireFrame()
        
        // Connecting
        view.basePresenter = presenter
        presenter.baseView = view
        presenter.baseWireFrame = wireFrame
        presenter.baseInteractor = interactor
        interactor.basePresenter = presenter
        interactor.baseAPIDataManager = APIDataManager
        
    }
    
    
    
}
