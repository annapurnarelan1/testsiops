//
//  BaseInteractor.swift
//  sIOPs
//
//  Created by Gunjan on 29/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation
import UIKit

/// BaseInteractor implements BaseInteractorInputProtocol protocol, handles the interaction
class BaseInteractor: BaseInteractorInputProtocol {
    
    // MARK: Variables
    weak var basePresenter: BaseInteractorOutputProtocol?
    var baseAPIDataManager: BaseAPIDataManagerInputProtocol?
    
    
    
    func popToRoot() {
        NavigationHelper.popToRootViewController(animated: false)
    }
    
    
    
    
    func filterNavStack(){
        // if NavigationHelper.fetchVc(forClass: ChooseUserViewController.self) != nil , let vc = NavigationHelper.lastVc(), !vc.isKind(of: ChooseUserViewController.self){
        //            NavigationHelper.removeVCs([ChooseUserViewController.self,RegisterPopUpViewController.self])
        
        //  }
    }
    
    
    
    func logout()
    {
        
        
        var Timestamp: String {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        /// UserDefaults.standard.string(forKey: "buildNumber") ?? ""
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        
        let str =  UserDefaults.standard.object(forKey: "fcmToken")
        
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "type": "userInfo",
            
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? ""
        ]
        
        let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        
        let requestList: [String: Any] = [
            "busiCode": Constants.BusiCode.Logout,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["message"] as? String ?? "") == Constants.status.FAILURE ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                     UserDefaults.standard.removeObject(forKey: "fcmToken")
                              LoginModel.sharedInstance.doLogout()
                              LoginWireFrame.presentLoginModule(fromView: self)
                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "remoteNotif"), object: nil)
                    
                    //self.presenter?.loginDone(loginRes:loginRes)
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.basePresenter?.showFailErrorBase(error:message)
                default:
                    print("error")
                   self.basePresenter?.showFailErrorBase(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                
                
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.basePresenter?.showFailErrorBase(error:APIManager.NetworkError.internetError.rawValue)
                 print("error")
                case .authorizationError( _):
                    self.basePresenter?.showFailErrorBase(error: APIManager.NetworkError.unKnownError.rawValue)
                    print("error")
                case .serverError:
                    self.basePresenter?.showFailErrorBase(error: APIManager.NetworkError.unKnownError.rawValue)
                    print("error")
                case .newUpdate:
                    print("error")
                    self.basePresenter?.stopLoaderBase()
                default:
                    print("error")
                    self.basePresenter?.showFailErrorBase(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
    }
}
