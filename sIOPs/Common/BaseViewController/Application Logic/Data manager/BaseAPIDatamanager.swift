//
//  BaseAPIDatamanager.swift
//  sIOPs
//
//  Created by Gunjan on 29/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation

import Alamofire
/// BaseAPIDataManager implements the BaseAPIDataManagerInputProtocol protocol, if data needs to be saved/retrieved from the server, all the implentation should be done here
class BaseAPIDataManager: BaseAPIDataManagerInputProtocol {
    
    // MARK: Initializers
    /// Base initializer
    init() {}
    
    /// Fetches all orders from OneServer
    ///
    /// - Parameter completionHandler: Block of code that will be executed after the request to get all orders
    
}
