//
//  Network.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 26/11/19.
//  Copyright © 2019 sIOPs. All rights reserved.
//
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON
import Alamofire


class APIManager {
    
    
    static let baseUrl = Constants.server.baseURL
    
    typealias parameters = [String:Any]
    
    enum ApiResult {
        case success(Any)
        case failure(RequestError)
    }
    enum HTTPMethod: String {
        case options = "OPTIONS"
        case get     = "GET"
        case head    = "HEAD"
        case post    = "POST"
        case put     = "PUT"
        case patch   = "PATCH"
        case delete  = "DELETE"
        case trace   = "TRACE"
        case connect = "CONNECT"
    }
    enum RequestError: Error {
        case unknownError
        case connectionError
        case authorizationError(NSDictionary)
        case invalidRequest
        case notFound
        case invalidResponse
        case serverError
        case serverUnavailable
        case newUpdate
    }
    
    
    enum StatusCode : String
    {
        case versionUpdate = "30002"
        case sessionExpire = "80000"
    }
    
    enum NetworkError : String
    {
        case versionUpdateError = "The application version you are currently running has been marked obsolete by your administer. Kindly upgrade to the latest version to continue using the application."
        case internetError = "Check your Internet connection."
        case unKnownError = "Something went wrong"
    }
    
    
    static func rSAPubkey() -> String
    {
        let uniqueKey =  UUID().uuidString
        let PublicKeyTag = uniqueKey
        let PrivateKeyTag = "privateTag"
        
        let publicKeyAttr: [NSString: Any] = [
            kSecAttrIsPermanent: NSNumber(value: true),
            kSecAttrApplicationTag: PublicKeyTag
        ]
        let privateKeyAttr: [NSString: Any] = [
            kSecAttrIsPermanent: NSNumber(value: true),
            kSecAttrApplicationTag: PrivateKeyTag
        ]
        
        let keyPairAttr: [NSString: Any] = [
            kSecAttrKeyType: kSecAttrKeyTypeRSA,
            kSecAttrKeySizeInBits: 1024 as NSObject,
            kSecPublicKeyAttrs: publicKeyAttr,
            kSecPrivateKeyAttrs: privateKeyAttr
        ]
        
        var publicKey: SecKey?
        var privateKey: SecKey?
        var statusCode: OSStatus
        statusCode = SecKeyGeneratePair(keyPairAttr as CFDictionary, &publicKey, &privateKey)
        
        if statusCode == noErr && publicKey != nil && privateKey != nil {
            print(publicKey!)
            print(privateKey!)
        } else {
            print("Error generating key pair: \(statusCode)")
        }
        
        var dataPtr: AnyObject?
        let query: [NSString: Any] = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: PrivateKeyTag,
            kSecReturnData: NSNumber(value: true)
        ]
        statusCode = SecItemCopyMatching(query as CFDictionary, &dataPtr)
        
        var datapub: AnyObject?
        let queryPub: [NSString: Any] = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: PublicKeyTag,
            kSecReturnData: NSNumber(value: true)
        ]
        statusCode = SecItemCopyMatching(queryPub as CFDictionary, &datapub)
        
        let privateKeyData = dataPtr as! Data
        let privateKeyString = privateKeyData.base64EncodedString(options: [])
        print(privateKeyString)
        
        let publicKeyData = datapub as! Data
        let publicKeyString = publicKeyData.base64EncodedString(options: [])
        print(publicKeyString)
        return publicKeyString
    }
    
    
    static func requestData(url:String,method:Alamofire.HTTPMethod,parameters:parameters?,completion: @escaping (ApiResult)->Void) {
        
        let header =  ["Content-Type": "application/json"]
        
        var urlRequest = URLRequest(url: URL(string: baseUrl+url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.allHTTPHeaderFields = header
        urlRequest.httpMethod = method.rawValue
        if let parameters = parameters {
            let parameterData = parameters.reduce("") { (result, param) -> String in
                return result + "&\(param.key)=\(param.value)"
            }.data(using: .utf8)
            urlRequest.httpBody = parameterData
        }
        let encoding = JSONEncoding() as ParameterEncoding
        
        let req = Alamofire.request(URL(string: baseUrl+url)!, method: method, parameters:parameters, encoding: encoding, headers: header)
        
        
        req.validate(statusCode: 200..<600).responseJSON { response in
            switch response.result
            {
            case .failure(let error):
                print(error)
                completion(ApiResult.failure(.connectionError))
            case .success(let value):
                
                let responseJson = value
                print("responseCode : \(response.response?.statusCode ?? 200)")
                print("responseJSON : \(responseJson)")
                if let status = response.response?.statusCode {
                    switch status {
                    case 200:
                        completion(ApiResult.success(responseJson))
                    case 400...499:
                        
                        if(((responseJson as? [String:Any])?["respData"] as? [Any])?.count ?? 0 > 0)
                        {
                            let code = (((responseJson as? [String:Any])?["respData"] as? [Any])?[0] as? [String:Any])?["code"]
                            if code as? String == StatusCode.versionUpdate.rawValue
                            {
                                BaseViewController.showUpdateErrorView(description:  NetworkError.versionUpdateError.rawValue)
                                completion(ApiResult.failure(.newUpdate))
                            }
                            if code as? String == StatusCode.sessionExpire.rawValue
                            {
                                LoginModel.sharedInstance.doLogout()
                                LoginWireFrame.presentLoginModule(fromView: self)
                                completion(ApiResult.failure(.newUpdate))
                            }
                            else
                            {
                                completion(ApiResult.failure(.authorizationError(responseJson as? NSDictionary ?? [:] )))
                            }
                        }
                        else
                        {
                            completion(ApiResult.failure(.authorizationError(responseJson as? NSDictionary ?? [:] )))
                        }
                        
                    case 500...599:
                        completion(ApiResult.failure(.serverError))
                    default:
                        completion(ApiResult.failure(.unknownError))
                        break
                    }
                }
                
            }
            
        }
        
    }
    
}

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}
