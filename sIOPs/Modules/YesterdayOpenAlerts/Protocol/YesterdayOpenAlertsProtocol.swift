//
//  OpenAlertsProtocol.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol YesterdayOpenAlertsViewProtocol: class {
    var presenter: YesterdayOpenAlertsPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol YesterdayOpenAlertsWireFrameProtocol: class {
    static func presentYesterdayOpenAlertsModule(fromView:AnyObject,type:String,selected:String,response:OpenAlertYesterdayModel,date:String)
    
    func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
  // func presentYesterdayOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
}

/// Method contract between VIEW -> PRESENTER
protocol YesterdayOpenAlertsPresenterProtocol: class {
    var view: YesterdayOpenAlertsViewProtocol? { get set }
    var interactor: YesterdayOpenAlertsInteractorInputProtocol? { get set }
    var wireFrame: YesterdayOpenAlertsWireFrameProtocol? { get set }
    func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
   // func presentYesterdayOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
    func requestData(date:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol YesterdayOpenAlertsInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    
   func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol YesterdayOpenAlertsInteractorInputProtocol: class
{
    var presenter: YesterdayOpenAlertsInteractorOutputProtocol? { get set }
    func requestData(date:String)
    
}
