//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class YesterdayOpenAlertsVC: BaseViewController,YesterdayOpenAlertsViewProtocol, FSCalendarDataSource, FSCalendarDelegate
{
    func reloadData(yesterdayOpenAlertsResponse: [OpenAlertYesterdayApplication]) {
        
    }
    
    
    var presenter: YesterdayOpenAlertsPresenterProtocol?
    
//    @IBOutlet weak var descriptionThirdLbl: UILabel!
//    @IBOutlet weak var thirdBtn: UIButton!
//    @IBOutlet weak var thirdLbl: UILabel!
//    @IBOutlet weak var descriptionSecondLbl: UILabel!
//    @IBOutlet weak var secondBtn: UIButton!
//    @IBOutlet weak var secondlbl: UILabel!
//    @IBOutlet weak var firstBtn: UIButton!
//    @IBOutlet weak var descriptionFirstLbl: UILabel!
//    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
//    @IBOutlet weak var filteBtn: UIButton!
//    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var YesterdayOpenAlertsCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
   // @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var type:String?
    var selected:String?
    var NGOResponse:OpenAlertYesterdayModel?
    var selectedArray:[NGOApplication]?
    
    let date = Date()
    var selectedDate:String?
    let formatter = DateFormatter()
   //  @IBOutlet weak var calendarView: FSCalendar!
   // @IBOutlet weak var cnstHeightCalender: NSLayoutConstraint!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> YesterdayOpenAlertsViewProtocol{
        return UIStoryboard(name: "YesterdayOpenAlerts", bundle: nil).instantiateViewController(withIdentifier: "YesterdayOpenAlerts") as! YesterdayOpenAlertsVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatter.dateFormat = "yyyy-MM-dd"
//        if UIDevice.current.model.hasPrefix("iPad") {
//            self.cnstHeightCalender.constant = 400
//        }
//
//        self.calendarView.select(Date())
//        self.calendarView.scope = .week
//
//        // For UITest
//        self.calendarView.accessibilityIdentifier = "calendar"
        dateLabel.text = HelperMethods().formatDateOrderJourney(date: selectedDate ?? "")

        YesterdayOpenAlertsCollectionView?.register(UINib(nibName: "ThreeButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ThreeButtonCollectionView")
        YesterdayOpenAlertsCollectionView?.register(UINib(nibName: "TwoButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoButtonCollectionViewCell")
        YesterdayOpenAlertsCollectionView?.register(UINib(nibName: "TwoButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoButtonCollectionViewCell")

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated);
        
       HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       self.navigationController?.setNavigationBarHidden(false, animated: false)
       self.navigationItem.setHidesBackButton(false, animated:true)
        selectedDate = formatter.string(from: date)
        let titleLabel = String(UserDefaults.standard.string(forKey: "openAlertType") ?? "")
        self.titleLbl.text = "Service Glance - \(titleLabel)  Alerts"
    }
    
     
    
     func stopLoader() {
        stopAnimating()
    }
    
    func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel) {
        stopAnimating()
        NGOResponse = yesterdayOpenAlertsResponse
        self.YesterdayOpenAlertsCollectionView.reloadData()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    

//}

    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        //self.cnstHeightCalender.constant = bounds.height
        self.view.layoutIfNeeded()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedDate = "\(self.dateFormatter.string(from: date))"
        print("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
            
        }
        startAnimating()
        self.presenter?.requestData(date: selectedDate ?? "")
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        let fromDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        return self.dateFormatter.date(from: self.dateFormatter.string(from: fromDate!))!
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return self.dateFormatter.date(from: self.dateFormatter.string(from:Date()))!
    }
}

extension YesterdayOpenAlertsVC :UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section
        {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            
            let normalText = "Alerts "
            let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            attributedString.append(boldString)
            cell.titleLbl.attributedText = attributedString
            
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                cell.firstLabel.text = "\(NGOResponse?.openAlerts[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = NGOResponse?.openAlerts[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.openAlerts[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = NGOResponse?.openAlerts[1].featureName ?? ""
                cell.thirdLbl.text = "\(NGOResponse?.openAlerts[2].outlierCount ?? 0)"
                cell.descriptionThirdLbl.text = NGOResponse?.openAlerts[2].featureName ?? ""
                cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                cell.thirdBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 3
                                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                                cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                                cell.thirdBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
            }
            
            return cell
        
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            let normalText = "Status "
            let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            
            attributedString.append(boldString)
            cell.titleLbl.attributedText = attributedString
            
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                cell.firstLabel.text = "\(NGOResponse?.status[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = NGOResponse?.status[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.status[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = NGOResponse?.status[1].featureName ?? ""
               cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                if cell.descriptionSecondLbl.text == "Open" {
                    cell.secondlbl.textColor = UIColor.red
                }
                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
    
            }
            return cell
            
        case 2:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
        
        let normalText = "Acknowledgement "
        let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
        let attributedString = NSMutableAttributedString(string:normalText)
        
        let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
        let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
        attributedString.append(boldString)
        cell.titleLbl.attributedText = attributedString
        
        if((NGOResponse?.openAlerts.count ?? 0) > 0)
        {
            cell.firstLabel.text = "\(NGOResponse?.acknowledement[0].outlierCount ?? 0)"
            cell.descriptionFirstLbl.text = NGOResponse?.acknowledement[0].featureName ?? ""
            cell.secondlbl.text = "\(NGOResponse?.acknowledement[1].outlierCount ?? 0)"
            cell.descriptionSecondLbl.text = NGOResponse?.acknowledement[1].featureName ?? ""
            cell.thirdLbl.text = "\(NGOResponse?.acknowledement[2].outlierCount ?? 0)"
            cell.descriptionThirdLbl.text = NGOResponse?.acknowledement[2].featureName ?? ""
            if  cell.descriptionThirdLbl.text  == "Unacknoledged" {
                cell.thirdLbl.textColor = UIColor.red
            }
             cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
            cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
            cell.thirdBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 3
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
            cell.thirdBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
        }
        
        return cell
            
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            let normalText = "Acknowledgement SLA "
            let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            
            attributedString.append(boldString)
            cell.titleLbl.attributedText = attributedString
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                
                
                cell.firstLabel.text = "\(NGOResponse?.acknowledementSLA[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = NGOResponse?.acknowledementSLA[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.acknowledementSLA[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = NGOResponse?.acknowledementSLA[1].featureName ?? ""
                cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                if cell.descriptionSecondLbl.text == "Missed"{
                    cell.secondlbl.textColor = UIColor.red
                }
                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
            }
            return cell
            
            case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            let normalText = "Average Time "
            let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            
            attributedString.append(boldString)
            cell.titleLbl.attributedText = attributedString
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                
                
                cell.firstLabel.text = "\(NGOResponse?.averageTime[0].value ?? "0")"
                cell.descriptionFirstLbl.text = NGOResponse?.averageTime[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.averageTime[1].value ?? "0")"
                cell.descriptionSecondLbl.text = NGOResponse?.averageTime[1].featureName ?? ""
                cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                //                cell.firstBtn.addTarget(self, action: #selector(ngoDetailDeliquency), for: .touchUpInside)
                //                cell.secondBtn.addTarget(self, action: #selector(ngoDetailDeliquency), for: .touchUpInside)
            }
            return cell
        default:
            print("In defualt")
            //return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(NGOResponse != nil)
        {
            return 5
        }
        else
        {
            return 0
        }
    }
    
    @objc func ngoDetailOpenAlerts(sender:Any)
    {
        //let cell =  self.OpenAlertsCollectionView.cellForItem(at: self.selectedIndexPath ?? IndexPath(row: 0, section: 0))
        var selectedDetailType = ""
        var count = "0"
        let btn = (sender as? UIButton)!
        let section = btn.tag/100
        let index = btn.tag%100
        let cell =  self.YesterdayOpenAlertsCollectionView.cellForItem(at: IndexPath(row: 0, section: section))
        if let selectedCell = cell as? ThreeButtonCollectionViewCell {

            switch index {
            case 1:
                selectedDetailType =   selectedCell.descriptionFirstLbl.text ?? ""
                count =  selectedCell.firstLabel.text ?? ""
                break
            case 2:
                selectedDetailType =   selectedCell.descriptionSecondLbl.text ?? ""
                count =  selectedCell.secondlbl.text ?? ""
                break
             case 3:
                selectedDetailType = selectedCell.descriptionThirdLbl.text ?? ""
                count = selectedCell.thirdLbl.text ?? ""
                 break
            default:
                print("in default")
            }
        }
        
        if let selectedCell = cell as? TwoButtonCollectionViewCell
        {
            switch index {
            case 1:
             selectedDetailType =  selectedCell.descriptionFirstLbl.text ?? ""
             count = selectedCell.firstLabel.text ?? ""
                break
            case 2:
                selectedDetailType = selectedCell.descriptionSecondLbl.text ?? ""
                count = selectedCell.secondlbl.text ?? ""
                break
            default:
                print("in default")
            }
        }
                UserDefaults.standard.set(selected, forKey: "openAlertType")
        
        if let response = self.NGOResponse {
                    self.presenter?.presentYesterdayOpenAlertsDetailModule(selectedHistoryType: selectedDetailType, type: selected ?? "", date: selectedDate ?? "", ngoResponse: response, count: count)
        }
        

                
//        self.presenter?.presentYesterdayOpenAlertsDetailModule(selectedHistoryType: selectedDetailType, date: selectedDate ?? "" , count: count, response: self.NGOResponse )
                   
    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
//
//
//        if(indexPath.section == 0)
//        {
//            let myString = NSMutableAttributedString(string: "HISTORY")
//            headerView?.alarmLbl.attributedText = myString
//            headerView?.headerTitleLbl.text = "Technology"
//            headerView?.alarmLbl.isHidden = false
//            headerView?.alarmBtn.addTarget(self, action: #selector(makeHistoryCall), for: .touchUpInside)
//        }
//        else
//        {
//            headerView?.alarmLbl.isHidden = true
//            headerView?.headerTitleLbl.text = "Time Clocking"
//        }
//
//
//
//
//
//        return headerView ?? UICollectionReusableView()
//    }
    
}

extension YesterdayOpenAlertsVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        itemsPerRow = 1
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 1
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        return CGSize(width: widthPerItem, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height:30)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //heightConstraint.constant = self.OpenAlertsCollectionView.contentSize.height + 40
        self.YesterdayOpenAlertsCollectionView.setNeedsLayout()
    }
}

extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }

    func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }

    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}

