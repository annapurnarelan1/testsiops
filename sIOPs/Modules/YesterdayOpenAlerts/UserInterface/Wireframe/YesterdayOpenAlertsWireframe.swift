//
//  OpenAlertsWireframe.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class YesterdayOpenAlertsWireFrame: YesterdayOpenAlertsWireFrameProtocol {
    
    
    
    
    
    // MARK: NGODetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentYesterdayOpenAlertsModule(fromView:AnyObject,type:String,selected:String,response:OpenAlertYesterdayModel,date:String) {

        // Generating module components
        let view: YesterdayOpenAlertsViewProtocol = YesterdayOpenAlertsVC.instantiate()
        let presenter: YesterdayOpenAlertsPresenterProtocol & YesterdayOpenAlertsInteractorOutputProtocol = YesterdayOpenAlertsPresenter()
        let interactor: YesterdayOpenAlertsInteractorInputProtocol = YesterdayOpenAlertsInteractor()
       
        let wireFrame: YesterdayOpenAlertsWireFrameProtocol = YesterdayOpenAlertsWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! YesterdayOpenAlertsVC
        viewController.type = type
        viewController.selected = selected
        viewController.selectedDate = date
        viewController.NGOResponse = response
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String, type: String, date: String, ngoResponse: OpenAlertYesterdayModel, count: String)
   {
       OpenAlertsHistoryWireFrame.presentOpenAlertsHistoryModule(fromView:self,type:selectedHistoryType,date: date, count: count, isFromYesterday: true, response: ngoResponse as Any)
            
      // OpenAlertsDetailWireFrame.presentOpenAlertsDetailModule(fromView:self,type:type,ngoResponse:ngoResponse,count:count)
    }

}
