//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SiteDownModel : NSObject, NSCoding{

	var list : [DownList]!
	var sitesList : [SitesList]!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		list = [DownList]()
		if let listArray = dictionary["list"] as? [[String:Any]]{
			for dic in listArray{
				let value = DownList(fromDictionary: dic)
				list.append(value)
			}
		}
		sitesList = [SitesList]()
		if let sitesListArray = dictionary["sitesList"] as? [[String:Any]]{
			for dic in sitesListArray{
				let value = SitesList(fromDictionary: dic)
				sitesList.append(value)
			}
		}
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if sitesList != nil{
			var dictionaryElements = [[String:Any]]()
			for sitesListElement in sitesList {
				dictionaryElements.append(sitesListElement.toDictionary())
			}
			dictionary["sitesList"] = dictionaryElements
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey :"list") as? [DownList]
         sitesList = aDecoder.decodeObject(forKey :"sitesList") as? [SitesList]
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if sitesList != nil{
			aCoder.encode(sitesList, forKey: "sitesList")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
