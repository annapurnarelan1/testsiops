//
//	InfraModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class InfraModel : NSObject, NSCoding{

	var alarmCount : Int!
	var cellImpacted : [InfraCellImpacted]!
	var cellImpactedCount : Int!
	var complaints : AnyObject!
	var consumption : [AnyObject]!
	var projects : [AnyObject]!
	var sitesDown : [InfraCellImpacted]!
	var sitesDownCount : Int!
	var throughput : [ThroughPutModel]!
	var voice : voiceModel!
    var performanceLastUpdate :String!
    var siteDownLastUpdate :String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		alarmCount = dictionary["alarmCount"] as? Int
		cellImpacted = [InfraCellImpacted]()
		if let cellImpactedArray = dictionary["cellImpacted"] as? [[String:Any]]{
			for dic in cellImpactedArray{
				let value = InfraCellImpacted(fromDictionary: dic)
				cellImpacted.append(value)
			}
		}
		cellImpactedCount = dictionary["cellImpactedCount"] as? Int
		complaints = dictionary["complaints"] as? AnyObject
		consumption = dictionary["consumption"] as? [AnyObject]
		projects = dictionary["projects"] as? [AnyObject]
		sitesDown = [InfraCellImpacted]()
		if let sitesDownArray = dictionary["sitesDown"] as? [[String:Any]]{
			for dic in sitesDownArray{
				let value = InfraCellImpacted(fromDictionary: dic)
				sitesDown.append(value)
			}
		}
		sitesDownCount = dictionary["sitesDownCount"] as? Int
		throughput = [ThroughPutModel]()
        if let throughputArray = dictionary["throughput"] as? [[String:Any]]{
            for dic in throughputArray{
                let value = ThroughPutModel(fromDictionary: dic)
                throughput.append(value)
            }
        }
        performanceLastUpdate = dictionary["performanceLastUpdate"] as? String
        siteDownLastUpdate = dictionary["siteDownLastUpdate"] as? String
		if let voiceData = dictionary["voice"] as? [String:Any]{
			voice = voiceModel(fromDictionary: voiceData)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if alarmCount != nil{
			dictionary["alarmCount"] = alarmCount
		}
		if cellImpacted != nil{
			var dictionaryElements = [[String:Any]]()
			for cellImpactedElement in cellImpacted {
				dictionaryElements.append(cellImpactedElement.toDictionary())
			}
			dictionary["cellImpacted"] = dictionaryElements
		}
		if cellImpactedCount != nil{
			dictionary["cellImpactedCount"] = cellImpactedCount
		}
		if complaints != nil{
			dictionary["complaints"] = complaints
		}
		if consumption != nil{
			dictionary["consumption"] = consumption
		}
		if projects != nil{
			dictionary["projects"] = projects
		}
		if sitesDown != nil{
			var dictionaryElements = [[String:Any]]()
			for sitesDownElement in sitesDown {
				dictionaryElements.append(sitesDownElement.toDictionary())
			}
			dictionary["sitesDown"] = dictionaryElements
		}
		if sitesDownCount != nil{
			dictionary["sitesDownCount"] = sitesDownCount
		}
        
        if throughput != nil{
            var dictionaryElements = [[String:Any]]()
            for sitesDownElement in throughput {
                dictionaryElements.append(sitesDownElement.toDictionary())
            }
            dictionary["throughput"] = dictionaryElements
        }
//		if throughput != nil{
//			dictionary["throughput"] = throughput
//		}
		if voice != nil{
			dictionary["voice"] = voice.toDictionary()
		}
        if performanceLastUpdate != nil{
            dictionary["performanceLastUpdate"] = performanceLastUpdate
        }
        if siteDownLastUpdate != nil{
            dictionary["siteDownLastUpdate"] = siteDownLastUpdate
        }
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         alarmCount = aDecoder.decodeObject(forKey: "alarmCount") as? Int
         cellImpacted = aDecoder.decodeObject(forKey :"cellImpacted") as? [InfraCellImpacted]
         cellImpactedCount = aDecoder.decodeObject(forKey: "cellImpactedCount") as? Int
         complaints = aDecoder.decodeObject(forKey: "complaints") as? AnyObject
         consumption = aDecoder.decodeObject(forKey: "consumption") as? [AnyObject]
         projects = aDecoder.decodeObject(forKey: "projects") as? [AnyObject]
         sitesDown = aDecoder.decodeObject(forKey :"sitesDown") as? [InfraCellImpacted]
         sitesDownCount = aDecoder.decodeObject(forKey: "sitesDownCount") as? Int
         throughput = aDecoder.decodeObject(forKey: "throughput") as? [ThroughPutModel]
         voice = aDecoder.decodeObject(forKey: "voice") as? voiceModel
        performanceLastUpdate = aDecoder.decodeObject(forKey: "performanceLastUpdate") as? String
        siteDownLastUpdate = aDecoder.decodeObject(forKey: "siteDownLastUpdate") as? String
        

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if alarmCount != nil{
			aCoder.encode(alarmCount, forKey: "alarmCount")
		}
		if cellImpacted != nil{
			aCoder.encode(cellImpacted, forKey: "cellImpacted")
		}
		if cellImpactedCount != nil{
			aCoder.encode(cellImpactedCount, forKey: "cellImpactedCount")
		}
		if complaints != nil{
			aCoder.encode(complaints, forKey: "complaints")
		}
		if consumption != nil{
			aCoder.encode(consumption, forKey: "consumption")
		}
		if projects != nil{
			aCoder.encode(projects, forKey: "projects")
		}
		if sitesDown != nil{
			aCoder.encode(sitesDown, forKey: "sitesDown")
		}
		if sitesDownCount != nil{
			aCoder.encode(sitesDownCount, forKey: "sitesDownCount")
		}
		if throughput != nil{
			aCoder.encode(throughput, forKey: "throughput")
		}
		if voice != nil{
			aCoder.encode(voice, forKey: "voice")
		}
        if performanceLastUpdate != nil{
            aCoder.encode(performanceLastUpdate, forKey: "performanceLastUpdate")
        }
        if siteDownLastUpdate != nil{
            aCoder.encode(siteDownLastUpdate, forKey: "siteDownLastUpdate")
        }

	}

}
