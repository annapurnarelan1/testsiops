//
//	CellImpacted.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class voiceModel : NSObject, NSCoding{

	var featureId : String!
	var featureName : String!
	var featureMsg : AnyObject!
	var iconUri : String!
	var outlierCount : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		featureId = dictionary["featureId"] as? String
		featureName = dictionary["featureName"] as? String
		featureMsg = dictionary["feature_msg"] as? AnyObject
		iconUri = dictionary["icon_uri"] as? String
		outlierCount = dictionary["outlierCount"] as? Double
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if featureId != nil{
			dictionary["featureId"] = featureId
		}
		if featureName != nil{
			dictionary["featureName"] = featureName
		}
		if featureMsg != nil{
			dictionary["feature_msg"] = featureMsg
		}
		if iconUri != nil{
			dictionary["icon_uri"] = iconUri
		}
		if outlierCount != nil{
			dictionary["outlierCount"] = outlierCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         featureId = aDecoder.decodeObject(forKey: "featureId") as? String
         featureName = aDecoder.decodeObject(forKey: "featureName") as? String
         featureMsg = aDecoder.decodeObject(forKey: "feature_msg") as? AnyObject
         iconUri = aDecoder.decodeObject(forKey: "icon_uri") as? String
         outlierCount = aDecoder.decodeObject(forKey: "outlierCount") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if featureId != nil{
			aCoder.encode(featureId, forKey: "featureId")
		}
		if featureName != nil{
			aCoder.encode(featureName, forKey: "featureName")
		}
		if featureMsg != nil{
			aCoder.encode(featureMsg, forKey: "feature_msg")
		}
		if iconUri != nil{
			aCoder.encode(iconUri, forKey: "icon_uri")
		}
		if outlierCount != nil{
			aCoder.encode(outlierCount, forKey: "outlierCount")
		}

	}

}
