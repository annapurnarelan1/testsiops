//
//  InfraProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol InfraViewProtocol: class {
    var presenter: InfraPresenterProtocol? { get set }
    //func InfraDone(InfraRes :InfraModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadData(infraModelResponse:InfraModel)
    func reloadAlarmData(alarmResponse:AlarmModel)
    func reloadSiteDownData(response:SiteDownModel)

}

/// Method contract between PRESENTER -> WIREFRAME
protocol InfraWireFrameProtocol: class {
    static func presentInfraModule(fromView:AnyObject,baseVC:Any) -> BaseViewController
    func presentAlarmDetailModule(type:String,selected:String,infraModelResponse infraResponse:Any,applicationDic:LoginApplication)
    func presentInfraDetailModule(type:String,selected:String,response:Any,applicationDic:LoginApplication,selectedSite:InfraCellImpacted)

}

/// Method contract between VIEW -> PRESENTER
protocol InfraPresenterProtocol: class {
    var view: InfraViewProtocol? { get set }
    var interactor: InfraInteractorInputProtocol? { get set }
    var wireFrame: InfraWireFrameProtocol? { get set } 
    
    func presentAlarmDetailModule(type:String,selected:String,infraModelResponse infraResponse:Any,applicationDic:LoginApplication)
    func presentInfraDetailModule(type:String,selected:String,response:Any,applicationDic:LoginApplication,selectedSite:InfraCellImpacted)
    
    func requestData()
    func requestAlarmData(applicationDic:LoginApplication)
    func requestSiteDown(response:InfraCellImpacted)
    
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol InfraInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(Infraresponse:InfraModel)
    func reloadAlarmData(alarmResponse:AlarmModel)
    func reloadSiteDownData(response:SiteDownModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol InfraInteractorInputProtocol: class
{
    var presenter: InfraInteractorOutputProtocol? { get set }
    func requestData()
    func requestAlarmData(applicationDic:LoginApplication)
    func requestSiteDown(response:InfraCellImpacted)
    
}
