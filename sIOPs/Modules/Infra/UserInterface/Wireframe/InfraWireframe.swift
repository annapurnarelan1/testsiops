//
//  InfraWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class InfraWireFrame: InfraWireFrameProtocol {
    
    
    
    // MARK: InfraWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentInfraModule(fromView:AnyObject,baseVC:Any) -> BaseViewController {

        // Generating module components
        let view: InfraViewProtocol = InfraViewController.instantiate()
        let presenter: InfraPresenterProtocol & InfraInteractorOutputProtocol = InfraPresenter()
        let interactor: InfraInteractorInputProtocol = InfraInteractor()
       
        let wireFrame: InfraWireFrameProtocol = InfraWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! InfraViewController
        viewController.delegate = baseVC as? DashboardVC
        return viewController
    }
    
    func presentAlarmDetailModule(type:String,selected:String,infraModelResponse infraResponse:Any,applicationDic:LoginApplication)
    {
        InfraDetailWireFrame.presentAlarmDetailModule(fromView: self, type: type, selected: selected, response: infraResponse,applicationDic:applicationDic)
    }
    
    
    func presentInfraDetailModule(type:String,selected:String,response:Any,applicationDic:LoginApplication,selectedSite:InfraCellImpacted)
    {
        InfraDetailWireFrame.presentInfraDetailModule(fromView:self,type:type,selected:selected,response:response,applicationDic:applicationDic,selectedSite:selectedSite)
    }
   

}
