//
//  InfraPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class InfraPresenter:BasePresenter, InfraPresenterProtocol, InfraInteractorOutputProtocol {
   
    
    

    // MARK: Variables
    weak var view: InfraViewProtocol?
    var interactor: InfraInteractorInputProtocol?
    var wireFrame: InfraWireFrameProtocol?
    let stringsTableName = "Infra"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
    func requestData()
    {
        self.interactor?.requestData()
    }
    
    func reloadData(Infraresponse:InfraModel)
    {
        self.view?.reloadData(infraModelResponse:Infraresponse)
    }
    
    func presentAlarmDetailModule(type:String,selected:String,infraModelResponse infraResponse:Any,applicationDic:LoginApplication)
    {
        self.wireFrame?.presentAlarmDetailModule(type: type, selected: selected, infraModelResponse: infraResponse,applicationDic:applicationDic)
    }
    
    func requestAlarmData(applicationDic:LoginApplication)
    {
        self.interactor?.requestAlarmData(applicationDic:applicationDic)
    }
    
    
    func reloadAlarmData(alarmResponse:AlarmModel)
    {
        self.view?.reloadAlarmData(alarmResponse:alarmResponse)
    }
    
    
     func requestSiteDown(response:InfraCellImpacted)
     {
        self.interactor?.requestSiteDown(response:response)
    }
    
    func reloadSiteDownData(response:SiteDownModel)
    {
         self.view?.reloadSiteDownData(response:response)
    }
    
    func presentInfraDetailModule(type:String,selected:String,response:Any,applicationDic:LoginApplication,selectedSite:InfraCellImpacted)
    {
         self.wireFrame?.presentInfraDetailModule(type: type, selected: selected, response: response,applicationDic:applicationDic,selectedSite:selectedSite)
    }
}
