//
//  InfraViewController.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

protocol InfraDelegate:NSObjectProtocol {
    func reloadInfradata()
    func errorInfra()
    func viewDetailLoad()
}


class InfraViewController: BaseViewController,InfraViewProtocol{
    
    var presenter: InfraPresenterProtocol?
    @IBOutlet weak var InfraCollectionView: UICollectionView!
    
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var infraResponse :InfraModel?
    var section1Header = 1
    var applicationDic : LoginApplication?
    var siteDownbtn:Int?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
     weak var delegate: InfraDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        InfraCollectionView?.register(UINib(nibName: "TwoButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoButtonCollectionViewCell")
        InfraCollectionView?.register(UINib(nibName: "ThreeButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ThreeButtonCollectionView")
        InfraCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        InfraCollectionView.register(UINib(nibName: "FooterCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterCollection")
        InfraCollectionView?.register(UINib(nibName: "SingleButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"SingleButtonCollectionViewCell")
        InfraCollectionView?.register(UINib(nibName: "ProjectCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ProjectCollectionViewCell")
        
        let applicationArray = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.applications
        let arr = applicationArray?.filter { $0.applicationUCode == "F_RAN" }
        if(arr?.count ?? 0 > 0)
        {
            applicationDic = arr?[0]
        }
        
        
        if(!(DashboardVC.isToggle ?? false))
        {
            startAnimating()
        }
        self.presenter?.requestData()
        
        // Do any additional setup after loading the view.
    }
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> InfraViewProtocol{
        return UIStoryboard(name: "Infra", bundle: nil).instantiateViewController(withIdentifier: "Infra") as! InfraViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        
    }
    
    func requestData()
    {
         startAnimating()
         self.presenter?.requestData()
    }
   
    
    override func viewDidLayoutSubviews()
    {
        heightConstraint.constant = self.InfraCollectionView.contentSize.height + 40
        InfraCollectionView?.setNeedsLayout()
       //  InfraCollectionView.collectionViewLayout.invalidateLayout()
    }
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.delegate?.errorInfra()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(infraModelResponse:InfraModel)
    {
        stopAnimating()
        DashboardVC.addedToggleView?.removeFromSuperview()
         self.delegate?.reloadInfradata()
        infraResponse = infraModelResponse
        InfraCollectionView.reloadData()
        
    }
    
    func reloadAlarmData(alarmResponse:AlarmModel)
    {
        stopAnimating()
        self.presenter?.presentAlarmDetailModule(type: "Alarms", selected: "Alarms", infraModelResponse: alarmResponse,applicationDic:applicationDic!)
    }
    
    func reloadSiteDownData(response:SiteDownModel)
    {
        stopAnimating()
        self.presenter?.presentInfraDetailModule(type: "Sites down", selected: infraResponse?.sitesDown[siteDownbtn ?? 0].featureName ?? "", response: response,applicationDic:applicationDic!,selectedSite:(infraResponse?.sitesDown[siteDownbtn ?? 0])!)
    }
    
    @objc func infraDetailSiteDown(sender:Any)
    {
        
        let btn = (sender as? UIButton)!
        siteDownbtn = btn.tag
        
        if(btn.tag <= 3)
        {
            startAnimating()
            self.presenter?.requestSiteDown(response:(infraResponse?.sitesDown[btn.tag])!)
        }
        
        
    }
    
    @objc func infracellImpactedAction(sender:Any)
    {
        let btn = (sender as? UIButton)!
        if(btn.tag > 3)
        {
            self.showFailError(error: "Coming Soon")
        }
        
        
    }
    
    
    @objc func makeAlarmCall()
    {
        startAnimating()
        self.presenter?.requestAlarmData(applicationDic:applicationDic!)
    }
    
    
    @objc func viewDetailAction(_ sender:UIButton)
    {
         
        if(sender.isSelected)
        {
            sender.backgroundColor = UIColor.init(hexString: "#D9D9D9")
            sender.setTitle("view details", for: .normal)
            sender.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
            sender.isSelected = false
            section1Header = 1
            self.InfraCollectionView.reloadData()
        }
        else
        {
            sender.isSelected = true
            sender.backgroundColor = UIColor.init(hexString: "#0078C1")
            sender.setTitle("hide details", for: .normal)
            sender.setTitleColor(UIColor.white, for: .normal)
            
            section1Header = 3
            self.InfraCollectionView.reloadData()
        }
        
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    
}

extension InfraViewController :UICollectionViewDataSource
{
    
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            cell.separatorLine.isHidden = false
            cell.titleLbl.isHidden = false
            let normalText = "Sites down "
            let boldText  = "(" + "\(infraResponse?.sitesDownCount ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)

             let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
             let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)

             attributedString.append(boldString)
            cell.titleLbl.attributedText = attributedString
            
            if( infraResponse?.sitesDown.count ?? 0 > 0)
            {
                cell.firstLabel.text = "\(infraResponse?.sitesDown[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = infraResponse?.sitesDown[0].featureName ?? ""
                cell.secondlbl.text = "\(infraResponse?.sitesDown[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = infraResponse?.sitesDown[1].featureName ?? ""
                cell.thirdLbl.text = "\(infraResponse?.sitesDown[2].outlierCount ?? 0)"
                cell.descriptionThirdLbl.text = infraResponse?.sitesDown[2].featureName ?? ""
                cell.firstBtn.tag = 0
                cell.secondBtn.tag = 1
                cell.thirdBtn.tag = 2
                cell.descriptionFirstLbl.backgroundColor = UIColor.clear
                cell.descriptionSecondLbl.backgroundColor = UIColor.clear
                cell.descriptionThirdLbl.backgroundColor = UIColor.clear
                cell.descriptionFirstLbl.textColor = UIColor.init(hexString: "7B7B7B")
                cell.descriptionSecondLbl.textColor = UIColor.init(hexString: "7B7B7B")
                cell.descriptionThirdLbl.textColor = UIColor.init(hexString: "7B7B7B")
                cell.firstBtn.addTarget(self, action: #selector(infraDetailSiteDown), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action: #selector(infraDetailSiteDown), for: .touchUpInside)
                cell.thirdBtn.addTarget(self, action: #selector(infraDetailSiteDown), for: .touchUpInside)
            }
            
            
            
            
            return cell
            
        case 1:
            switch indexPath.row {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
                cell.separatorLine.isHidden = false
                cell.titleLbl.isHidden = false
                
                let normalText = "Cells impacted "
                let boldText  = "(" + "\(infraResponse?.cellImpactedCount ?? 0)" + ")"
                let attributedString = NSMutableAttributedString(string:normalText)
                
                let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
                
                attributedString.append(boldString)
                cell.titleLbl.attributedText = attributedString
                
                
                let normalText1 = "\(infraResponse?.cellImpacted[0].formattedCount ?? 0.00)"
                               let boldText1  = infraResponse?.cellImpacted[0].unit ?? ""
                               let attributedString1 = NSMutableAttributedString(string:normalText1)
                               
                               let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                               let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                               
                               attributedString1.append(boldString1)
                               cell.firstLabel.attributedText = attributedString1
                
                let normalText2 = "\(infraResponse?.cellImpacted[1].formattedCount ?? 0.00)"
                               let boldText2  = infraResponse?.cellImpacted[1].unit ?? ""
                               let attributedString2 = NSMutableAttributedString(string:normalText2)
                               
                               let attrs2 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                               let boldString2 = NSMutableAttributedString(string: boldText2, attributes:attrs2)
                               
                               attributedString2.append(boldString2)
                               cell.secondlbl.attributedText = attributedString2
                
                let normalText3 = "\(infraResponse?.cellImpacted[2].formattedCount ?? 0.00)"
                               let boldText3  = infraResponse?.cellImpacted[2].unit ?? ""
                               let attributedString3 = NSMutableAttributedString(string:normalText3)
                               
                               let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                               let boldString3 = NSMutableAttributedString(string: boldText3, attributes:attrs3)
                               
                               attributedString3.append(boldString3)
                               cell.thirdLbl.attributedText = attributedString3
                
                if( infraResponse?.cellImpacted.count ?? 0 > 0)
                           {
               // cell.firstLabel.text = "\(infraResponse?.cellImpacted[0].formattedCount ?? "")"
                cell.descriptionFirstLbl.text = infraResponse?.cellImpacted[0].featureName ?? ""
              //  cell.secondlbl.text = "\(infraResponse?.cellImpacted[1].formattedCount ?? "")"
                cell.descriptionSecondLbl.text = infraResponse?.cellImpacted[1].featureName ?? ""
               // cell.thirdLbl.text = "\(infraResponse?.cellImpacted[2].formattedCount ?? "")"
                cell.descriptionThirdLbl.text = infraResponse?.cellImpacted[2].featureName ?? ""
                cell.firstBtn.tag = 4
                cell.secondBtn.tag = 5
                cell.thirdBtn.tag = 6
                cell.firstBtn.addTarget(self, action: #selector(infracellImpactedAction), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action: #selector(infracellImpactedAction), for: .touchUpInside)
                cell.thirdBtn.addTarget(self, action: #selector(infracellImpactedAction), for: .touchUpInside)
                cell.descriptionFirstLbl.backgroundColor = UIColor.init(red: 249, green: 106, blue: 76)
                cell.descriptionSecondLbl.backgroundColor = UIColor.init(red: 255, green: 158, blue: 0)
                cell.descriptionThirdLbl.backgroundColor = UIColor.init(red: 222, green: 187, blue:0 )
                cell.descriptionFirstLbl.textColor = UIColor.white
                cell.descriptionSecondLbl.textColor = UIColor.white
                cell.descriptionThirdLbl.textColor = UIColor.white
                }
                return cell
                
            case 1:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleButtonCollectionViewCell", for: indexPath) as! SingleButtonCollectionViewCell
                
                if(infraResponse?.throughput.count ?? 0 > 0)
                {
                    cell.titleLbl.text = "IP throughput"
                    cell.firstLabel.text = "\(infraResponse?.throughput[0].outlierCount ?? 0.0)" + "mbps"
                    cell.descriptionFirstLbl.text = infraResponse?.throughput[0].featureName ?? ""
                }
                else{
                    cell.titleLbl.text = "Throughput"
                    cell.firstLabel.text = "-"
                    cell.descriptionFirstLbl.text = "IP throughput"
                }
               
//                cell.secondlbl.text = "-"
//                cell.descriptionSecondLbl.text = "Cell effective throughput"
                return cell
//            case 2:
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
//
//                cell.titleLbl.text = "Consumption"
//                cell.firstLabel.text = "-"
//                cell.descriptionFirstLbl.text = "GB / day"
//                cell.secondlbl.text = "-"
//                cell.descriptionSecondLbl.text = "Min. / Day"
//                return cell
            case 2:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleButtonCollectionViewCell", for: indexPath) as! SingleButtonCollectionViewCell
                cell.titleLbl.text = "Voice"
                cell.firstLabel.text = "\(infraResponse?.voice.outlierCount ?? 0.0)" + "%"
                cell.descriptionFirstLbl.text = infraResponse?.voice.featureName ?? ""
                
                return cell
                
//            case 3:
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleButtonCollectionViewCell", for: indexPath) as! SingleButtonCollectionViewCell
//                cell.titleLbl.text = "Complaints"
//                cell.firstLabel.text = "-"
//                cell.descriptionFirstLbl.text = "Complaints"
//                return cell
                
            default:
                return UICollectionViewCell()
            }
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectCollectionViewCell", for: indexPath) as! ProjectCollectionViewCell
            
            if( infraResponse?.cellImpacted.count ?? 0 > 0)
                                      {
            cell.firstLabel.text = "\(infraResponse?.cellImpacted[0].outlierCount ?? 0)"
            cell.descriptionFirstLbl.text = infraResponse?.cellImpacted[0].featureName ?? ""
            cell.secondlbl.text = "\(infraResponse?.cellImpacted[1].outlierCount ?? 0)"
            cell.descriptionSecondLbl.text = infraResponse?.cellImpacted[1].featureName ?? ""
            cell.thirdLbl.text = "\(infraResponse?.cellImpacted[2].outlierCount ?? 0)"
            cell.descriptionThirdLbl.text = infraResponse?.cellImpacted[2].featureName ?? ""
            cell.firstBtn.tag = 0
            cell.secondBtn.tag = 1
            cell.thirdBtn.tag = 2
            cell.firstLabel.text = "25"
            cell.descriptionFirstLbl.text = "Surveys"
            cell.secondlbl.text = "45"
            cell.descriptionSecondLbl.text = "ATP"
            cell.thirdLbl.text = "01"
            cell.descriptionThirdLbl.text = "Supervisory"
            }
            
            
            return cell
            
        default:
            return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return section1Header
        }
        
        return 1
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if(infraResponse != nil)
        {
            return 2
        }
        else
        {
            return 0
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
            
            headerView?.alarmLbl.isHidden = true
            
            if(indexPath.section == 0)
            {
                let attachment = NSTextAttachment()
                attachment.image = UIImage(named: "alarm")
                let attachmentString = NSAttributedString(attachment: attachment)
                let myString = NSMutableAttributedString(string: "Alarms", attributes:
                [.underlineStyle: NSUnderlineStyle.single.rawValue])
                
                let atrstr = NSMutableAttributedString(string: " (\(infraResponse?.alarmCount ?? 0))")
                myString.append(atrstr)
//                let myString = NSMutableAttributedString(string: " Alarms(\(infraResponse?.alarmCount ?? 0))")
                myString.insert(attachmentString, at: 0)
                headerView?.alarmLbl.attributedText = myString
                
                
                let normalText1 = "Maintenance "
                let boldText1  = "( \(infraResponse?.siteDownLastUpdate ?? "") )"
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 10.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#7B7B7B")]
                let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                
                attributedString1.append(boldString1)
                headerView?.headerTitleLbl.attributedText = attributedString1
                
                
                //headerView?.headerTitleLbl.text = "Maintenance"
                headerView?.alarmLbl.isHidden = false
                headerView?.alarmBtn.addTarget(self, action: #selector(makeAlarmCall), for: .touchUpInside)
                
            }
            else if (indexPath.section == 1)
            {
                
                let normalText1 = "Performance "
                let boldText1  = "( \(infraResponse?.performanceLastUpdate ?? "") )"
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 10.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#7B7B7B")]
                let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                
                attributedString1.append(boldString1)
                headerView?.headerTitleLbl.attributedText = attributedString1
               
                
            }
                
            else
            {
                headerView?.headerTitleLbl.text = "Projects"
                
            }
            
            
            
            
            return headerView ?? UICollectionReusableView()
            
        case UICollectionView.elementKindSectionFooter:
            
            if(indexPath.section == 1)
            {
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollection", for: indexPath) as? FooterCollectionReusableView
                footerView?.viewDetailBtn.addTarget(self, action: #selector(viewDetailAction(_:)), for: .touchUpInside)
                
                // footerView?.backgroundColor = UIColor.green
                return footerView ?? UICollectionReusableView()
            }
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "cell", for: indexPath)
            
            footerView.backgroundColor = UIColor.green
            return footerView
            // return UICollectionReusableView()
            
            
            
            
        default:
            
            assert(false, "Unexpected element kind")
        }
        
        return UICollectionReusableView()
    }
    
    
    
}

extension InfraViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        itemsPerRow = 1
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 1
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        
        if indexPath.section == 1{
            
            if(indexPath.item == 0 )
            {
                return CGSize(width: widthPerItem, height: 100)
            }
            
            else
            {
                itemsPerRow = 2
                 let sectionInsets = UIEdgeInsets(top: 10.0, left: 5.0, bottom: 10.0, right: 5.0)
                let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
                let availableWidth = InfraCollectionView.frame.width - paddingSpace
                var widthPerItem: CGFloat
                if(self.iPadUI!){
                    itemsPerRow = 1
                    widthPerItem = availableWidth / itemsPerRow
                }else{
                    widthPerItem = availableWidth / itemsPerRow
                }
                return CGSize(width: widthPerItem - 10 , height: 100)
                
            }
            
            
        }
        else {
            
            return CGSize(width: widthPerItem, height: 100)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        
        return CGSize(width: collectionView.frame.width, height:30)
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if(section == 1)
        {
            return CGSize(width: collectionView.frame.width, height:40)
        }
        
        return CGSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        heightConstraint.constant = self.InfraCollectionView.contentSize.height + 40
        self.InfraCollectionView.setNeedsLayout()
         self.delegate?.viewDetailLoad()
    }
}
