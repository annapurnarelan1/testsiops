//
//  DCBInUseVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class DCBInUseVC: BaseViewController,DCBInUseViewProtocol{
    
    var presenter: DCBInUsePresenterProtocol?
    @IBOutlet private weak var DCBInUseCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var dcbInUseResponse :DCBInUseModel?
    private let spacing:CGFloat = 10.0
    
    var application:LoginApplication?
    var dcbDetails:DCBDetailListModel?
    var itemsInGranite: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DCBInUseCollectionView?.register(UINib(nibName: "CompletionBoardSummartTableViewCell", bundle: nil), forCellWithReuseIdentifier:"CompletionBoardSummaryTableViewCell")
        DCBInUseCollectionView?.register(UINib(nibName: "SingleStatusCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"SingleStatusCollectionViewCell")
        DCBInUseCollectionView?.register(UINib(nibName: "TwoStatusCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoStatusCollectionViewCell")
        
        
        DCBInUseCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        startAnimating()
        
        self.presenter?.requestData(application:application! ,dcbDetails:dcbDetails! )
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.DCBInUseCollectionView?.collectionViewLayout = layout
        // Do any additional setup after loading the view.
    }
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> DCBInUseViewProtocol{
        return UIStoryboard(name: "DCBInUse", bundle: nil).instantiateViewController(withIdentifier: "DCBInUse") as! DCBInUseVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(DCBInUseResponse:DCBInUseModel)
    {
        stopAnimating()
        dcbInUseResponse = DCBInUseResponse
        DCBInUseCollectionView.reloadData()
        //        dcbInUseResponse?.controls.count
        
        if(DCBInUseResponse.jcp.count != 0){
            itemsInGranite += 1
        }
        if(DCBInUseResponse.everest.count != 0){
            itemsInGranite += 1
        }
        if(DCBInUseResponse.mcom.count != 0){
            itemsInGranite += 1
        }
        if(DCBInUseResponse.teMIP.count != 0){
            itemsInGranite += 1
        }
        
        
        
    }
    
    func stopLoader()
    {
        stopAnimating()
    }
    
}

extension DCBInUseVC :UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var largest = 0 , first = 0 , second = 0 , third = 0
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompletionBoardSummaryTableViewCell", for: indexPath) as! CompletionBoardSummartTableViewCell
            
                        cell.descriptionFirstSectionTitleLbl.text = "\(dcbInUseResponse?.totalCount ?? 0)"
            
            first  = dcbInUseResponse?.countBar[0].cnt ?? 0
            second = dcbInUseResponse?.countBar[1].cnt ?? 0
            third  = dcbInUseResponse?.countBar[2].cnt ?? 0
            
            largest = max(max(first, second), third)
            print("\(largest)")
            
            let  textValue1 : (Int) -> Float = { value in
                
                return Float(value)/Float(largest)
            }
            
            print(("\(textValue1(first))") , "\(textValue1(second))" , "\(textValue1(third))")
            cell.firstLabel.text = "\(dcbInUseResponse?.countBar[0].sites ?? "-")"
            cell.descriptionFirstLbl.text = "\(first)"
            cell.secondlbl.text = "\(dcbInUseResponse?.countBar[1].sites ?? "-")"
            cell.descriptionSecondLbl.text = "\(second)"
            cell.thirdLbl.text = "\(dcbInUseResponse?.countBar[2].sites ?? "-")"
            cell.descriptionThirdLbl.text = "\(third)"
            cell.titleLbl.text = "Total IN USE"
            // let firstValue = Float(dcbResponse?.countBar[0].cnt ?? 0) / Float(dcbResponse?.totalTowerSites ?? 0)
            
            cell.firstProgressView .setProgress(textValue1(first), animated: false)
            //  let secondValue = Float(dcbResponse?.countBar[1].cnt ?? 0) / Float(dcbResponse?.totalTowerSites ?? 0)
            cell.secondProgressView .setProgress(textValue1(second), animated: false)
            
            // let thirdValue = Float(dcbResponse?.countBar[2].cnt ?? 0) / Float(dcbResponse?.totalTowerSites ?? 0)
            cell.thirdProgressView .setProgress(textValue1(third), animated: false)
            
            cell.contentView.layer.cornerRadius = 4.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = false
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
            cell.layer.shadowRadius = 4.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            return cell
        case 1:
            if ( dcbInUseResponse?.controls.count != 0) {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleStatusCollectionViewCell", for: indexPath) as! SingleStatusCollectionViewCell
                cell.firstLabel.text = "\(dcbInUseResponse?.controls?[ indexPath.row ].value ?? 0)"
                cell.descriptionFirstLbl.text = dcbInUseResponse?.controls[  indexPath.row ].key
                //            cell.firstLabel.textColor = dcbResponse?.list[indexPath.row].color == 1 ? UIColor.red : UIColor.init(hexString: "0078C1")
                return cell
            }
            else {
                return UICollectionViewCell()
                
            }
            
            
        case 2:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoStatusCollectionViewCell", for: indexPath) as! TwoStatusCollectionViewCell
            
            if(dcbInUseResponse?.teMIP.count != 0){
                cell.headertitleLabel.text = "temip"
                
                
                cell.titlefirstLabel.text = "\( dcbInUseResponse?.teMIP?[0].value ?? 0)"
                cell.descriptionFirstLabel.text = "\( dcbInUseResponse?.teMIP?[0].key ?? "")"
                
                if( dcbInUseResponse?.teMIP.indices.contains(1) ?? false) {
                    cell.secondView.isHidden = false
                    cell.titleSecondLabel.text = "\( dcbInUseResponse?.teMIP?[1].value ?? 0)"
                    cell.descriptionSecondLabel.text = dcbInUseResponse?.teMIP[1].key
                }
                    
                else {
                    
                    cell.secondView.isHidden = true
                    
                }
            }
            
            return cell
            
        case 3:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoStatusCollectionViewCell", for: indexPath) as! TwoStatusCollectionViewCell
            if(dcbInUseResponse?.jcp.count != 0){
                cell.headertitleLabel.text = "JCP"
                
                
                cell.titlefirstLabel.text = "\( dcbInUseResponse?.jcp?[0].value ?? 0)"
                cell.descriptionFirstLabel.text = "\( dcbInUseResponse?.jcp?[0].key ?? "")"
                
                if( dcbInUseResponse?.jcp.indices.contains(1) ?? false) {
                    cell.secondView.isHidden = false
                    cell.titleSecondLabel.text = "\( dcbInUseResponse?.jcp?[1].value ?? 0)"
                    cell.descriptionSecondLabel.text = dcbInUseResponse?.jcp[1].key
                }
                    
                else {
                    
                    cell.secondView.isHidden = true
                    
                }
                
            }
            return cell
            
        case 4:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoStatusCollectionViewCell", for: indexPath) as! TwoStatusCollectionViewCell
            
            if(dcbInUseResponse?.everest.count != 0){
                cell.headertitleLabel.text = "Everest"
                
                
                cell.titlefirstLabel.text = "\( dcbInUseResponse?.everest?[0].value ?? 0)"
                cell.descriptionFirstLabel.text = "\( dcbInUseResponse?.everest?[0].key ?? "")"
                
                if( dcbInUseResponse?.everest.indices.contains(1) ?? false) {
                    cell.secondView.isHidden = false
                    cell.titleSecondLabel.text = "\( dcbInUseResponse?.everest?[1].value ?? 0)"
                    cell.descriptionSecondLabel.text = dcbInUseResponse?.everest[1].key
                }
                    
                else {
                    
                    cell.secondView.isHidden = true
//                    cell.firstViewXContraint.multiplier = 1.0
                    
                }
                
            }
            return cell
            
        case 5:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoStatusCollectionViewCell", for: indexPath) as! TwoStatusCollectionViewCell
            
            if(dcbInUseResponse?.mcom.count != 0){
                cell.headertitleLabel.text = "MyCOM"
                
                
                cell.titlefirstLabel.text = "\( dcbInUseResponse?.mcom?[0].value ?? 0)"
                cell.descriptionFirstLabel.text = dcbInUseResponse?.mcom?[0].key ??  "-"
                
                if( dcbInUseResponse?.mcom.indices.contains(1) ?? false) {
                    cell.secondView.isHidden = false
                    cell.titleSecondLabel.text = "\( dcbInUseResponse?.mcom?[1].value ?? 0)"
                    cell.descriptionSecondLabel.text = dcbInUseResponse?.mcom?[1].key ?? "-"
                }
                    
                else {
                    
                    cell.secondView.isHidden = true
                    
                }
                
            }
            
            return cell
            
        default:
            return  UICollectionViewCell()
        }
        
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1
        {
            return dcbInUseResponse?.controls.count ?? 0
            
        }else
        {
            
            return 1
            
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if(dcbInUseResponse != nil)
        {
            return 2 + itemsInGranite
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
        
        switch indexPath.section {
        case 0:
            headerView?.headerTitleLbl.text = "Granite in service"
        case 1:
            if ( dcbInUseResponse?.controls.count != 0) {
                headerView?.headerTitleLbl.text = "Controls" + " (" + "\(dcbInUseResponse?.controlCount ?? 0)" + ")"
            }
            else {
                headerView?.headerTitleLbl.text = ""
            }
        case 2:
            headerView?.headerTitleLbl.text = "Missing in granite production state"
            
        default:
            headerView?.headerTitleLbl.text = ""
            
        }
        
        return headerView ?? UICollectionReusableView()
        
        
        
    }
}
extension DCBInUseVC : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var numberOfItemsPerRow:CGFloat = 1
        var spacingBetweenCells:CGFloat = 15
        
        if indexPath.section == 1 {
            numberOfItemsPerRow = 3
            spacingBetweenCells = 15
        }
        else {
            
            
            _ = 1
            _ = 15
        }
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        if let collection = self.DCBInUseCollectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: 120)
        }else{
            
            return CGSize(width: 0, height: 0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        
        switch section {
        case 0:
            return   CGSize(width: collectionView.frame.width, height:40)
        case 1:
            
            
            if (dcbInUseResponse?.controls.count != 0) {
                return CGSize(width: collectionView.frame.width, height:40)
                
            }
            else {
                return CGSize(width: collectionView.frame.width, height:0)
            }
            
        case 2:
            return CGSize(width: collectionView.frame.width, height:40)
            
        default:
            return CGSize(width: collectionView.frame.width, height:0)
            
        }
    }
    
}
extension DCBInUseVC : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if (indexPath.section == 1) {
        self.presenter?.presentDCBCommissionedModule(fromView: self, dcbDetails:"\( dcbInUseResponse?.controls[ indexPath.row].key ??  "-")", application: application!,outLinerID:"\( dcbInUseResponse?.controls[ indexPath.row].id ??  "-")"
)
        }
    }
    
}
