//
//  DCBWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class DCBInUseWireFrame: DCBInUseWireFrameProtocol {
    
    
    
    class func presentDCBInUseModule(fromView: AnyObject, dcbDetails: DCBDetailListModel,application:LoginApplication) {
        // Generating module components
        let view: DCBInUseViewProtocol = DCBInUseVC.instantiate()
        let presenter: DCBInUsePresenterProtocol & DCBInUseInteractorOutputProtocol = DCBInUsePresenter()
        let interactor: DCBInUseInteractorInputProtocol = DCBInUseInteractor()
        
        let wireFrame: DCBInUseWireFrameProtocol = DCBInUseWireFrame()
        
        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! DCBInUseVC
        viewController.dcbDetails = dcbDetails
        viewController.application = application
        
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
    
    
    
    // MARK: DCBInUseWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)     {
        DCBCommissionedWireframe.presentDCBCommissionedModule(fromView: self, dcbDetails: dcbDetails,application:application,outLinerID: outLinerID)
    }
    
    func presentDCBInUseModule(fromView: AnyObject, dcbDetails: DCBDetailListModel,application:LoginApplication) {
        DCBInUseWireFrame.presentDCBInUseModule(fromView: self, dcbDetails: dcbDetails,application: application)
        
    }
    
    
}
