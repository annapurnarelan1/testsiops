//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DCBInUseModel : NSObject, NSCoding{

	var controlCount : Int!
	var controls : [DCBInUseControlModel]!
	var countBar : [DCBInUseCountBarModel]!
	var everest : [DCBInUseControlModel]!
	var jcp : [DCBInUseControlModel]!
	var mcom : [DCBInUseControlModel]!
	var teMIP : [DCBInUseControlModel]!
	var totalCount : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		controlCount = dictionary["controlCount"] as? Int
		controls = [DCBInUseControlModel]()
		if let controlsArray = dictionary["controls"] as? [[String:Any]]{
			for dic in controlsArray{
				let value = DCBInUseControlModel(fromDictionary: dic)
				controls.append(value)
			}
		}
		countBar = [DCBInUseCountBarModel]()
		if let countBarArray = dictionary["countBar"] as? [[String:Any]]{
			for dic in countBarArray{
				let value = DCBInUseCountBarModel(fromDictionary: dic)
				countBar.append(value)
			}
		}
		everest = [DCBInUseControlModel]()
		if let everestArray = dictionary["everest"] as? [[String:Any]]{
			for dic in everestArray{
				let value = DCBInUseControlModel(fromDictionary: dic)
				everest.append(value)
			}
		}
		jcp = [DCBInUseControlModel]()
		if let jcpArray = dictionary["jcp"] as? [[String:Any]]{
			for dic in jcpArray{
				let value = DCBInUseControlModel(fromDictionary: dic)
				jcp.append(value)
			}
		}
        mcom = [DCBInUseControlModel]()
        if let mcomArray = dictionary["mcom"] as? [[String:Any]]{
            for dic in mcomArray{
                let value = DCBInUseControlModel(fromDictionary: dic)
                mcom.append(value)
            }
        }
        		teMIP = [DCBInUseControlModel]()
		if let teMIPArray = dictionary["teMIP"] as? [[String:Any]]{
			for dic in teMIPArray{
				let value = DCBInUseControlModel(fromDictionary: dic)
				teMIP.append(value)
			}
		}
		totalCount = dictionary["totalCount"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if controlCount != nil{
			dictionary["controlCount"] = controlCount
		}
		if controls != nil{
			var dictionaryElements = [[String:Any]]()
			for controlsElement in controls {
				dictionaryElements.append(controlsElement.toDictionary())
			}
			dictionary["controls"] = dictionaryElements
		}
		if countBar != nil{
			var dictionaryElements = [[String:Any]]()
			for countBarElement in countBar {
				dictionaryElements.append(countBarElement.toDictionary())
			}
			dictionary["countBar"] = dictionaryElements
		}
		if everest != nil{
			var dictionaryElements = [[String:Any]]()
			for everestElement in everest {
				dictionaryElements.append(everestElement.toDictionary())
			}
			dictionary["everest"] = dictionaryElements
		}
		if jcp != nil{
			var dictionaryElements = [[String:Any]]()
			for jcpElement in jcp {
				dictionaryElements.append(jcpElement.toDictionary())
			}
			dictionary["jcp"] = dictionaryElements
		}
        if mcom != nil{
            var dictionaryElements = [[String:Any]]()
            for mcomElement in mcom {
                dictionaryElements.append(mcomElement.toDictionary())
            }
            dictionary["mcom"] = dictionaryElements
        }
		if teMIP != nil{
			var dictionaryElements = [[String:Any]]()
			for teMIPElement in teMIP {
				dictionaryElements.append(teMIPElement.toDictionary())
			}
			dictionary["teMIP"] = dictionaryElements
		}
		if totalCount != nil{
			dictionary["totalCount"] = totalCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         controlCount = aDecoder.decodeObject(forKey: "controlCount") as? Int
         controls = aDecoder.decodeObject(forKey :"controls") as? [DCBInUseControlModel]
         countBar = aDecoder.decodeObject(forKey :"countBar") as? [DCBInUseCountBarModel]
         everest = aDecoder.decodeObject(forKey :"everest") as? [DCBInUseControlModel]
         jcp = aDecoder.decodeObject(forKey :"jcp") as? [DCBInUseControlModel]
         mcom = aDecoder.decodeObject(forKey: "mcom") as? [DCBInUseControlModel]
         teMIP = aDecoder.decodeObject(forKey :"teMIP") as? [DCBInUseControlModel]
         totalCount = aDecoder.decodeObject(forKey: "totalCount") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if controlCount != nil{
			aCoder.encode(controlCount, forKey: "controlCount")
		}
		if controls != nil{
			aCoder.encode(controls, forKey: "controls")
		}
		if countBar != nil{
			aCoder.encode(countBar, forKey: "countBar")
		}
		if everest != nil{
			aCoder.encode(everest, forKey: "everest")
		}
		if jcp != nil{
			aCoder.encode(jcp, forKey: "jcp")
		}
		if mcom != nil{
			aCoder.encode(mcom, forKey: "mcom")
		}
		if teMIP != nil{
			aCoder.encode(teMIP, forKey: "teMIP")
		}
		if totalCount != nil{
			aCoder.encode(totalCount, forKey: "totalCount")
		}

	}

}
