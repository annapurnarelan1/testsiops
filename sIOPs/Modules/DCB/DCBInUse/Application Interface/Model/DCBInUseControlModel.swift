//
//	Control.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DCBInUseControlModel : NSObject, NSCoding{

	var color : Int!
	var id : String!
	var key : String!
	var onclick : Int!
	var value : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		color = dictionary["color"] as? Int
		id = dictionary["id"] as? String
		key = dictionary["key"] as? String
		onclick = dictionary["onclick"] as? Int
		value = dictionary["value"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if color != nil{
			dictionary["color"] = color
		}
		if id != nil{
			dictionary["id"] = id
		}
		if key != nil{
			dictionary["key"] = key
		}
		if onclick != nil{
			dictionary["onclick"] = onclick
		}
		if value != nil{
			dictionary["value"] = value
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         color = aDecoder.decodeObject(forKey: "color") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         key = aDecoder.decodeObject(forKey: "key") as? String
         onclick = aDecoder.decodeObject(forKey: "onclick") as? Int
         value = aDecoder.decodeObject(forKey: "value") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if color != nil{
			aCoder.encode(color, forKey: "color")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if key != nil{
			aCoder.encode(key, forKey: "key")
		}
		if onclick != nil{
			aCoder.encode(onclick, forKey: "onclick")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}
