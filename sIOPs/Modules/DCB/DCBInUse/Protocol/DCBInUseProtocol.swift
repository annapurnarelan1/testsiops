//
//  DCBInUseProtocol.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol DCBInUseViewProtocol: class {
    var presenter: DCBInUsePresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func showFailError(error:String)
    func reloadData(DCBInUseResponse:DCBInUseModel)
     func stopLoader()

  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol DCBInUseWireFrameProtocol: class {
   static func presentDCBInUseModule(fromView:AnyObject, dcbDetails:DCBDetailListModel,application:LoginApplication)
    func presentDCBInUseModule(fromView: AnyObject, dcbDetails: DCBDetailListModel,application:LoginApplication)
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)
}

/// Method contract between VIEW -> PRESENTER
protocol DCBInUsePresenterProtocol: class {
    var view: DCBInUseViewProtocol? { get set }
    var interactor: DCBInUseInteractorInputProtocol? { get set }
    var wireFrame: DCBInUseWireFrameProtocol? { get set }
    
   //func presentDCBCommissionedModule()
    func presentDCBInUseModule(fromView:AnyObject, dcbDetails:DCBDetailListModel,application:LoginApplication)
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String) 
    func requestData(application:LoginApplication ,dcbDetails:DCBDetailListModel )

  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol DCBInUseInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func showFailError(error:String)
    func reloadData(dcbInuseResponse:DCBInUseModel)
     func stopLoader()


}

/// Method contract between PRESENTER -> INTERACTOR
protocol DCBInUseInteractorInputProtocol: class
{
    var presenter: DCBInUseInteractorOutputProtocol? { get set }
    func requestData(application:LoginApplication ,dcbDetails:DCBDetailListModel )

    
}
