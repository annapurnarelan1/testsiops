//
//  DCBWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class DCBWireFrame: DCBWireFrameProtocol {
   
    
  
    
 
    
  
    
    
    
    // MARK: DCBWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
  class func presentDCBModule(fromView:AnyObject,application:LoginApplication)  {

        // Generating module components
        let view: DCBViewProtocol = DCBViewController.instantiate()
        let presenter: DCBPresenterProtocol & DCBInteractorOutputProtocol = DCBPresenter()
        let interactor: DCBInteractorInputProtocol = DCBInteractor()
       
        let wireFrame: DCBWireFrameProtocol = DCBWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! DCBViewController
        viewController.application = application
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)       {
        DCBCommissionedWireframe.presentDCBCommissionedModule(fromView: self, dcbDetails: dcbDetails,application:application,outLinerID: outLinerID)
       }
   
    func presentDCBInUseModule(fromView: AnyObject, dcbDetails :DCBDetailListModel,application:LoginApplication){
        DCBInUseWireFrame.presentDCBInUseModule(fromView: self, dcbDetails: dcbDetails,application:application)

    }
}
