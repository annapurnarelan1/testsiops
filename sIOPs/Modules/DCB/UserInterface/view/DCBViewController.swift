//
//  DCBViewController.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class DCBViewController: BaseViewController,DCBViewProtocol{
    
    var presenter: DCBPresenterProtocol?
    @IBOutlet private weak var DCBCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var dcbResponse :DCBModel?
    private let spacing:CGFloat = 10.0
    
    var application:LoginApplication?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DCBCollectionView?.register(UINib(nibName: "CompletionBoardSummartTableViewCell", bundle: nil), forCellWithReuseIdentifier:"CompletionBoardSummaryTableViewCell")
        DCBCollectionView?.register(UINib(nibName: "SingleStatusCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"SingleStatusCollectionViewCell")
        DCBCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        DCBCollectionView?.register(UINib(nibName: "TwoStatusCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoStatusCollectionViewCell")
        DCBCollectionView?.register(UINib(nibName: "ThreeButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ThreeButtonCollectionView")
        
        startAnimating()
        self.presenter?.requestData(application:application!)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.DCBCollectionView?.collectionViewLayout = layout
        // Do any additional setup after loading the view.
    }
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> DCBViewProtocol{
        return UIStoryboard(name: "DCB", bundle: nil).instantiateViewController(withIdentifier: "DCB") as! DCBViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(DCBResponse:DCBModel)
    {
        stopAnimating()
        dcbResponse = DCBResponse
        DCBCollectionView.reloadData()
        
    }
}
    extension DCBViewController :UICollectionViewDataSource
    {
        func collectionView(_ collectionView: UICollectionView,
                            cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            var largest = 0 , first = 0 , second = 0 , third = 0
            
            switch indexPath.section {
            case 0:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompletionBoardSummaryTableViewCell", for: indexPath) as! CompletionBoardSummartTableViewCell
                
                cell.descriptionFirstSectionTitleLbl.text = "\(dcbResponse?.totalTowerSites ?? 0)"
                
                first  = dcbResponse?.countBar[0].cnt ?? 0
                second = dcbResponse?.countBar[1].cnt ?? 0
                third  = dcbResponse?.countBar[2].cnt ?? 0
                
                largest = max(max(first, second), third)
                print("\(largest)")
                
                let  textValue1 : (Int) -> Float = { value in
                    
                    return Float(value)/Float(largest)
                }
                
                print(("\(textValue1(first))") , "\(textValue1(second))" , "\(textValue1(third))")
                cell.firstLabel.text = "\(dcbResponse?.countBar[0].sites ?? "-")"
                cell.descriptionFirstLbl.text = "\(first)"
                cell.secondlbl.text = "\(dcbResponse?.countBar[1].sites ?? "-")"
                cell.descriptionSecondLbl.text = "\(second)"
                cell.thirdLbl.text = "\(dcbResponse?.countBar[2].sites ?? "-")"
                cell.descriptionThirdLbl.text = "\(third)"
                
                // let firstValue = Float(dcbResponse?.countBar[0].cnt ?? 0) / Float(dcbResponse?.totalTowerSites ?? 0)
                
                cell.firstProgressView .setProgress(textValue1(first), animated: false)
                //  let secondValue = Float(dcbResponse?.countBar[1].cnt ?? 0) / Float(dcbResponse?.totalTowerSites ?? 0)
                cell.secondProgressView .setProgress(textValue1(second), animated: false)
                
                // let thirdValue = Float(dcbResponse?.countBar[2].cnt ?? 0) / Float(dcbResponse?.totalTowerSites ?? 0)
                cell.thirdProgressView .setProgress(textValue1(third), animated: false)
                
                cell.contentView.layer.cornerRadius = 4.0
                cell.contentView.layer.borderWidth = 1.0
                cell.contentView.layer.borderColor = UIColor.clear.cgColor
                cell.contentView.layer.masksToBounds = false
                cell.layer.shadowColor = UIColor.gray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
                cell.layer.shadowRadius = 4.0
                cell.layer.shadowOpacity = 1.0
                cell.layer.masksToBounds = false
                cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
                return cell
            case 1:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleStatusCollectionViewCell", for: indexPath) as! SingleStatusCollectionViewCell
                cell.firstLabel.text = "\(dcbResponse?.list?[ indexPath.row ].value ?? 0)"
                cell.descriptionFirstLbl.text = dcbResponse?.list[  indexPath.row ].key
                cell.firstLabel.textColor = dcbResponse?.list[indexPath.row].color == 1 ? UIColor.red : UIColor.init(hexString: "0078C1")
                return cell
            case 2:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
                cell.titleLbl.text = "Missing expected site count"
                cell.descriptionFirstLbl.text = "Current sites"
                cell.firstLabel.text = "\(dcbResponse?.totalTowerSites ?? 0)"
                cell.descriptionSecondLbl.text = "Missing sites"
                cell.secondlbl.text = "\(dcbResponse?.missing ?? 0)"
                cell.descriptionThirdLbl.text = "Expected sites"
                cell.thirdLbl.text = "\(dcbResponse?.expected ?? 0)"
                
                
                return cell
                
            default:
                return  UICollectionViewCell()
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if section == 0 {
                return 1
            }
            else if section == 1
            {
                return dcbResponse?.list.count ?? 0
                
            }else
            {
                
                return 1
                
            }
            
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            
            if(dcbResponse != nil)
            {
                return 3
            }
            else
            {
                return 0
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
            
            
            switch indexPath.section {
            case 0:
                headerView?.headerTitleLbl.text = "Data Completeness Board"
            case 1:
                if ( dcbResponse?.list.count != 0) {
                    headerView?.headerTitleLbl.text = "Tower site status" + " (" + "\(dcbResponse?.totalTowerSites ?? 0)" + ")"
                }
                else {
                    headerView?.headerTitleLbl.text = ""
                }
            case 2:
                headerView?.headerTitleLbl.text = ""
                
            default:
                headerView?.headerTitleLbl.text = ""
                
            }
            
            return headerView ?? UICollectionReusableView()
            
            
        }
    }
    
    extension DCBViewController : UICollectionViewDelegateFlowLayout {
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            var numberOfItemsPerRow:CGFloat = 1
            var spacingBetweenCells:CGFloat = 15
            
            if indexPath.section == 0 {
                _ = 1
                _ = 15
            }
                else  if indexPath.section == 2 {
                               _ = 1
                               _ = 15
                           }
            else {
                numberOfItemsPerRow = 3
                spacingBetweenCells = 15
            }
            let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
            
            if let collection = self.DCBCollectionView{
                let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
                return CGSize(width: width, height: 105)
            }else{
                return CGSize(width: 0, height: 0)
            }
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
        {
            
            switch section {
            case 0:
                return   CGSize(width: collectionView.frame.width, height:40)
            case 1:
                
                
                if (dcbResponse?.list.count != 0) {
                    return CGSize(width: collectionView.frame.width, height:40)
                    
                }
                else {
                    return CGSize(width: collectionView.frame.width, height:0)
                }
                
            case 2:
                return CGSize(width: collectionView.frame.width, height:40)
                
            default:
                return CGSize(width: collectionView.frame.width, height:0)
                
            }
        }
        
    }
    extension DCBViewController : UICollectionViewDelegate
    {
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
        {
            if (indexPath.section == 1) {
                
                //let model = CommissionedModel()
                if (dcbResponse?.list[  indexPath.row ].keyCode == "IN_USE"){
                    self.presenter?.presentDCBInUseModule(fromView: self, dcbDetails:(dcbResponse?.list[indexPath.row ] ?? nil)!, application: application!)
                    
                }
                else {
                    
                    self.presenter?.presentDCBCommissionedModule(fromView: self, dcbDetails:(dcbResponse?.list[ indexPath.row ].key)!, application: application!, outLinerID: "")
                    
                }
            }
        }
        
}
