//
//  DCBPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class DCBPresenter:BasePresenter, DCBPresenterProtocol, DCBInteractorOutputProtocol {
    func presentDCBInUseModule(fromView: AnyObject, dcbDetails: DCBDetailListModel,application:LoginApplication) {
        self.wireFrame?.presentDCBInUseModule(fromView: self, dcbDetails: dcbDetails,application:application)

    }
    
   
  
    
    func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String) {         self.wireFrame?.presentDCBCommissionedModule(fromView: self, dcbDetails: dcbDetails,application:application, outLinerID: outLinerID)
    }
    
    // MARK: Variables
    weak var view: DCBViewProtocol?
    var interactor: DCBInteractorInputProtocol?
    var wireFrame: DCBWireFrameProtocol?
    let stringsTableName = "DCB"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
     func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func requestData(application:LoginApplication)
    {
        self.interactor?.requestData(application:application)
    }
    
    func reloadData(dcbResponse:DCBModel)
    {
        self.view?.reloadData(DCBResponse:dcbResponse)
    }
    
   
   
}
