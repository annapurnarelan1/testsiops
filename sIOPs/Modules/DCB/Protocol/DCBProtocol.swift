//
//  DCBProtocol.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol DCBViewProtocol: class {
    var presenter: DCBPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(DCBResponse:DCBModel)

  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol DCBWireFrameProtocol: class {
 static   func presentDCBModule(fromView:AnyObject,application:LoginApplication)
    
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)
     func presentDCBInUseModule(fromView: AnyObject, dcbDetails: DCBDetailListModel,application:LoginApplication)

}

/// Method contract between VIEW -> PRESENTER
protocol DCBPresenterProtocol: class {
    var view: DCBViewProtocol? { get set }
    var interactor: DCBInteractorInputProtocol? { get set }
    var wireFrame: DCBWireFrameProtocol? { get set }
    
   //func presentDCBCommissionedModule()
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)
    func presentDCBInUseModule(fromView: AnyObject, dcbDetails: DCBDetailListModel,application:LoginApplication)
    
    func requestData(application:LoginApplication)

  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol DCBInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(dcbResponse:DCBModel)


}

/// Method contract between PRESENTER -> INTERACTOR
protocol DCBInteractorInputProtocol: class
{
    var presenter: DCBInteractorOutputProtocol? { get set }
    func requestData(application:LoginApplication)

    
}
