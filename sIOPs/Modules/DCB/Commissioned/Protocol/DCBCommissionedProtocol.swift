//
//  DCBProtocol.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol DCBCommissionedViewProtocol: class {
    var presenter: DCBCommisionedPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(DCBResponse:ResponsePayload)

  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol DCBCommissionedWireFrameProtocol: class {
func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)

}

/// Method contract between VIEW -> PRESENTER
protocol DCBCommisionedPresenterProtocol: class {
    var view: DCBCommissionedViewProtocol? { get set }
    var interactor: DCBCommissionedInteractorInputProtocol? { get set }
    var wireFrame: DCBCommissionedWireFrameProtocol? { get set }
    
   
func requestData(application:LoginApplication ,dcbDetails:String ,outlinerID: String)


  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol DCBCommissionedInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(dcbResponse:ResponsePayload)


}

/// Method contract between PRESENTER -> INTERACTOR
protocol DCBCommissionedInteractorInputProtocol: class
{
    var presenter: DCBCommissionedInteractorOutputProtocol? { get set }
    func requestData(application:LoginApplication ,dcbDetails:String ,outlinerID: String)

}
