// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let commissionedModel = try CommissionedModel(json)

import Foundation

// MARK: - CommissionedModel
@objcMembers class CommissionedModel: NSObject, Codable {
    let respInfo: RespInfo?
    let respData: [RespDatum]?

    init(respInfo: RespInfo?, respData: [RespDatum]?) {
        self.respInfo = respInfo
        self.respData = respData
    }
}

// MARK: CommissionedModel convenience initializers and mutators

extension CommissionedModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(CommissionedModel.self, from: data)
        self.init(respInfo: me.respInfo, respData: me.respData)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        respInfo: RespInfo?? = nil,
        respData: [RespDatum]?? = nil
    ) -> CommissionedModel {
        return CommissionedModel(
            respInfo: respInfo ?? self.respInfo,
            respData: respData ?? self.respData
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespDatum
@objcMembers class RespDatum: NSObject, Codable {
    let busiCode, transactionID, code, message: String?
    let isEncrypt: Bool?
    let respMsg: RespMsg?

    enum CodingKeys: String, CodingKey {
        case busiCode
        case transactionID
        case code, message, isEncrypt, respMsg
    }

    init(busiCode: String?, transactionID: String?, code: String?, message: String?, isEncrypt: Bool?, respMsg: RespMsg?) {
        self.busiCode = busiCode
        self.transactionID = transactionID
        self.code = code
        self.message = message
        self.isEncrypt = isEncrypt
        self.respMsg = respMsg
    }
}

// MARK: RespDatum convenience initializers and mutators

extension RespDatum {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(RespDatum.self, from: data)
        self.init(busiCode: me.busiCode, transactionID: me.transactionID, code: me.code, message: me.message, isEncrypt: me.isEncrypt, respMsg: me.respMsg)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        busiCode: String?? = nil,
        transactionID: String?? = nil,
        code: String?? = nil,
        message: String?? = nil,
        isEncrypt: Bool?? = nil,
        respMsg: RespMsg?? = nil
    ) -> RespDatum {
        return RespDatum(
            busiCode: busiCode ?? self.busiCode,
            transactionID: transactionID ?? self.transactionID,
            code: code ?? self.code,
            message: message ?? self.message,
            isEncrypt: isEncrypt ?? self.isEncrypt,
            respMsg: respMsg ?? self.respMsg
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespMsg
@objcMembers class RespMsg: NSObject, Codable {
    let responseHeader: ResponseHeader?
    let responsePayload: ResponsePayload?

    init(responseHeader: ResponseHeader?, responsePayload: ResponsePayload?) {
        self.responseHeader = responseHeader
        self.responsePayload = responsePayload
    }
}

// MARK: RespMsg convenience initializers and mutators

extension RespMsg {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(RespMsg.self, from: data)
        self.init(responseHeader: me.responseHeader, responsePayload: me.responsePayload)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        responseHeader: ResponseHeader?? = nil,
        responsePayload: ResponsePayload?? = nil
    ) -> RespMsg {
        return RespMsg(
            responseHeader: responseHeader ?? self.responseHeader,
            responsePayload: responsePayload ?? self.responsePayload
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponseHeader
@objcMembers class ResponseHeader: NSObject, Codable {
    let timestamp, message: String?
    let status: Int?
    let title, path: String?

    init(timestamp: String?, message: String?, status: Int?, title: String?, path: String?) {
        self.timestamp = timestamp
        self.message = message
        self.status = status
        self.title = title
        self.path = path
    }
}

// MARK: ResponseHeader convenience initializers and mutators

extension ResponseHeader {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ResponseHeader.self, from: data)
        self.init(timestamp: me.timestamp, message: me.message, status: me.status, title: me.title, path: me.path)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        timestamp: String?? = nil,
        message: String?? = nil,
        status: Int?? = nil,
        title: String?? = nil,
        path: String?? = nil
    ) -> ResponseHeader {
        return ResponseHeader(
            timestamp: timestamp ?? self.timestamp,
            message: message ?? self.message,
            status: status ?? self.status,
            title: title ?? self.title,
            path: path ?? self.path
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponsePayload
@objcMembers class ResponsePayload: NSObject, Codable {
    let totalCount: Int?
    let countBar: [CountBar]?
    let all: [All]?
    let p1, rp1, ipcolo: [JSONAny]?

    init(totalCount: Int?, countBar: [CountBar]?, all: [All]?, p1: [JSONAny]?, rp1: [JSONAny]?, ipcolo: [JSONAny]?) {
        self.totalCount = totalCount
        self.countBar = countBar
        self.all = all
        self.p1 = p1
        self.rp1 = rp1
        self.ipcolo = ipcolo
    }
}

// MARK: ResponsePayload convenience initializers and mutators

extension ResponsePayload {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ResponsePayload.self, from: data)
        self.init(totalCount: me.totalCount, countBar: me.countBar, all: me.all, p1: me.p1, rp1: me.rp1, ipcolo: me.ipcolo)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        totalCount: Int?? = nil,
        countBar: [CountBar]?? = nil,
        all: [All]?? = nil,
        p1: [JSONAny]?? = nil,
        rp1: [JSONAny]?? = nil,
        ipcolo: [JSONAny]?? = nil
    ) -> ResponsePayload {
        return ResponsePayload(
            totalCount: totalCount ?? self.totalCount,
            countBar: countBar ?? self.countBar,
            all: all ?? self.all,
            p1: p1 ?? self.p1,
            rp1: rp1 ?? self.rp1,
            ipcolo: ipcolo ?? self.ipcolo
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - All
@objcMembers class All: NSObject, Codable {
    let sapID: String?
    let status: String?
    let sites: String?

    enum CodingKeys: String, CodingKey {
        case sapID = "sap_ID"
        case status, sites
    }

    init(sapID: String?, status: String?, sites: String?) {
        self.sapID = sapID
        self.status = status
        self.sites = sites
    }
}

// MARK: All convenience initializers and mutators

extension All {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(All.self, from: data)
        self.init(sapID: me.sapID, status: me.status, sites: me.sites)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        sapID: String?? = nil,
        status: String?? = nil,
        sites: String?? = nil
    ) -> All {
        return All(
            sapID: sapID ?? self.sapID,
            status: status ?? self.status,
            sites: sites ?? self.sites
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}


//enum Status: String, Codable {
//    case inUseHold = "IN USE-HOLD"
//}

// MARK: - CountBar
@objcMembers class CountBar: NSObject, Codable {
    let sites: String?
    let cnt: Int?

    init(sites: String?, cnt: Int?) {
        self.sites = sites
        self.cnt = cnt
    }
}

// MARK: CountBar convenience initializers and mutators

extension CountBar {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(CountBar.self, from: data)
        self.init(sites: me.sites, cnt: me.cnt)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        sites: String?? = nil,
        cnt: Int?? = nil
    ) -> CountBar {
        return CountBar(
            sites: sites ?? self.sites,
            cnt: cnt ?? self.cnt
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespInfo
@objcMembers class RespInfo: NSObject, Codable {
    let code, message: String?

    init(code: String?, message: String?) {
        self.code = code
        self.message = message
    }
}

// MARK: RespInfo convenience initializers and mutators

extension RespInfo {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(RespInfo.self, from: data)
        self.init(code: me.code, message: me.message)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        message: String?? = nil
    ) -> RespInfo {
        return RespInfo(
            code: code ?? self.code,
            message: message ?? self.message
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Encode/decode helpers

@objcMembers class JSONNull: NSObject, Codable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    override public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

@objcMembers class JSONAny: NSObject, Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
