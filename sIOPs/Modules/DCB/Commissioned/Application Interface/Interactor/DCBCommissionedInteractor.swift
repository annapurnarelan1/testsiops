//
//  DCBInteractor.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation
import UIKit
class DCBCommissionedInteractor: DCBCommissionedInteractorInputProtocol {
   

public enum DCBError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: DCBCommissionedInteractorOutputProtocol?
    
    func requestData(application:LoginApplication ,dcbDetails:String, outlinerID: String ) {
           
        let pubInfo: [String : Any] =
            ["timestamp": "20191218131548",
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any]
        if (outlinerID.isEmpty ) {
             requestBody = [
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "appRoleCode": application.applicationCode ?? "",
                       "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                       "type": "userInfo",
                       "statusType" : dcbDetails
                       
                       
                   ]
        }
        else {
             requestBody = [
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                       "appRoleCode": application.applicationCode ?? "",
                       "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                       "type": "userInfo",
                       "statusType" : dcbDetails,
                       "outlierId" : outlinerID
            ]

        }
//        let requestBody:[String : Any] = [
//            "appRoleCode": application.applicationCode,
//            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
//            "type": "userInfo",
//            "statusType" : dcbDetails ,
//            "outlierId" : outlinerID
//
//
//        ]
        
       let busiParams: [String : Any] = [
            
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        var busicode = ""
//        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
//        {
        busicode = Constants.BusiCode.DataCompleteOnClick
      //  }
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001576654405065",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                
               
               
                
//                guard let car = try? JSONDecoder().decode(CommissionedModel.self, from: reponsePayload as! Data) else {
//                    print("Error: Couldn't decode data into car")
//                    return
//                }

                //let jsonData = (reponsePayload).data(encoding: .utf8)!
               // let decoder = JSONDecoder()
               // let beer = try! decoder.decode(CommissionedModel.self, for: jsonData)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    
                    //let commissionedModel =
                       
                         
                         let jsonDict =  returnJson as? [String:Any]
                         var respMsg : ResponsePayload?
                         var model : CommissionedModel?
                         do {

                                                                                        
                            let json = try JSONSerialization.data(withJSONObject: jsonDict as Any)
                            model = try CommissionedModel(data: json)
                            let arrRespData  = model?.respData
                            if let arrayResponse = arrRespData  {
                                for respsumObj in arrayResponse  {
                                    respMsg = ((respsumObj.respMsg?.responsePayload!)!)
                                }
                            }
                                            print(model!)
                                            self.presenter?.reloadData(dcbResponse:respMsg!)
                                        } catch {
                                            self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                                            print(error)
                                            self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                                        }
                         
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
//                let loginRes: LoginModel = LoginModel.sharedInstance
//
//                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
//                LoginModel.sharedInstance.saveUser()
                
               // self.presenter?.loginDone(loginRes:loginRes)
                
               
           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })

    }

    

    
}
