//
//  DCBCommissionedController.swift
//  sIOPs
//
//  Created by Annapurna on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class DCBCommissionedController: BaseViewController,DCBCommissionedViewProtocol {
    
    
     
    var presenter: DCBCommisionedPresenterProtocol?
    @IBOutlet weak var collectionVWComm: UICollectionView!
    var itemsPerRow: CGFloat = 1
    private let spacing:CGFloat = 10.0
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var dcbResponse :ResponsePayload?
    var application:LoginApplication?
    var dcbDetails:String?
    var outlinerId: String?

    override func viewDidLoad() {
        super.viewDidLoad()
         collectionVWComm?.register(UINib(nibName: "CompletionBoardSummartTableViewCell", bundle: nil), forCellWithReuseIdentifier:"CompletionBoardSummaryTableViewCell")
          collectionVWComm?.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
         collectionVWComm?.register(UINib(nibName: "CommissionedCell", bundle: nil), forCellWithReuseIdentifier:"CommissionedCell")
        startAnimating()
        self.presenter?.requestData(application:application! ,dcbDetails:dcbDetails!, outlinerID : outlinerId! )

        let layout = UICollectionViewFlowLayout()
               layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
               layout.minimumLineSpacing = spacing
               layout.minimumInteritemSpacing = spacing
               self.collectionVWComm?.collectionViewLayout = layout
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HelperMethods().removeCustomBarButtons(self)
         HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    func stopLoader() {
        stopAnimating()
    }
    func reloadData(DCBResponse: ResponsePayload) {
        stopAnimating()
        dcbResponse = DCBResponse
        self.collectionVWComm?.reloadData()
    }
    
    func show(image: String, error: String, description: String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String, description: String) {
         stopAnimating()
    }
    
    func showFailError(error: String) {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
   static func instantiate() -> DCBCommissionedViewProtocol{
       return UIStoryboard(name: "DCBCommissionedController", bundle: nil).instantiateViewController(withIdentifier: "DCBCommissionedVC") as! DCBCommissionedController
   }
    
}

extension DCBCommissionedController :UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var largest = 0 , first = 0 , second = 0 , third = 0
                
        if(indexPath.section == 0)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompletionBoardSummaryTableViewCell", for: indexPath) as! CompletionBoardSummartTableViewCell
            
            cell.descriptionFirstSectionTitleLbl.text = "\(dcbResponse?.totalCount ?? 0)"
            
            cell.titleLbl.text = "Total " + dcbDetails!
            first  = dcbResponse?.countBar?[0].cnt ?? 0
            second = dcbResponse?.countBar?[1].cnt ?? 0
            third  = dcbResponse?.countBar?[2].cnt ?? 0
            
            largest = max(max(first, second), third)
            print("\(largest)")
        
            let  textValue1 : (Int) -> Float = { value in

                 return Float(value)/Float(largest)
            }
            
            print(("\(textValue1(first))") , "\(textValue1(second))" , "\(textValue1(third))")
            cell.firstLabel.text = "\(dcbResponse?.countBar?[0].sites ?? "-")"
            cell.descriptionFirstLbl.text = "\(first)"
            cell.secondlbl.text = "\(dcbResponse?.countBar?[1].sites ?? "-")"
            cell.descriptionSecondLbl.text = "\(second)"
            cell.thirdLbl.text = "\(dcbResponse?.countBar?[2].sites ?? "-")"
            cell.descriptionThirdLbl.text = "\(third)"
            
            cell.firstProgressView.tintColor = UIColor.init(hexString: "89B5D7")
            cell.firstProgressView .setProgress(textValue1(first), animated: false)
            
             cell.secondProgressView.tintColor = UIColor.init(hexString: "89B5D7")
            cell.secondProgressView .setProgress(textValue1(second), animated: false)
            
            cell.thirdProgressView .setProgress(textValue1(third), animated: false)
            cell.thirdProgressView.tintColor = UIColor.init(hexString: "89B5D7")
            
            cell.contentView.layer.cornerRadius = 4.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = false
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
            cell.layer.shadowRadius = 4.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommissionedCell", for: indexPath) as! CommissionedCell
            cell.lblSiteID.text = dcbResponse?.all?[indexPath.row].sapID
            cell.lblGeography.text = dcbResponse?.all?[indexPath.row].sites
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else
        {
            return dcbResponse?.all?.count ?? 0
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       if(dcbResponse != nil)
       {
           return 2
       }
       else
       {
           return 0
       }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
        
        headerView?.btnFilter.isHidden = true
        
        if(indexPath.section == 0)
        {
            let normalText1 = "DCB / "
            let boldText1  = dcbDetails!
            let attributedString1 = NSMutableAttributedString(string:normalText1)
            let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 16.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#0078C1")]
            let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1 as [NSAttributedString.Key : Any])
            attributedString1.append(boldString1)
            headerView?.headerTitleLbl.attributedText = attributedString1
        }
        else
        {
            headerView?.headerTitleLbl.text = "Sites listing"
            headerView?.headerTitleLbl.textColor = UIColor.init(hexString: "#7B7B7B")
            headerView?.btnFilter.isHidden = true
        }
        return headerView ?? UICollectionReusableView()
    }
}

extension DCBCommissionedController : UICollectionViewDelegateFlowLayout {

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var numberOfItemsPerRow:CGFloat = 1
               var spacingBetweenCells:CGFloat = 5
    
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        if indexPath.section == 0 {
         
            if let collection = self.collectionVWComm{
                let width = (collection.bounds.width - totalSpacing)
                return CGSize(width: width, height: 105)
            }else{
                return CGSize(width: 0, height: 0)
            }
        }
        else {
         numberOfItemsPerRow = 1
        spacingBetweenCells = 10
        }
        
        if let collection = self.collectionVWComm{
            let width = (collection.bounds.width - totalSpacing)
            return CGSize(width: width, height: 60)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height:55)
    }
}
