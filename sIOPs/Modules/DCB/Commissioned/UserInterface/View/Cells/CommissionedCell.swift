//
//  CommissionedCell.swift
//  sIOPs
//
//  Created by Annapurna on 19/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class CommissionedCell: UICollectionViewCell {

    @IBOutlet weak var lblSiteID: UILabel!
    @IBOutlet weak var lblGeography: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()
        // Initialization code
    }

}
