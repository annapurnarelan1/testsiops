//
//  DCBPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class DCBCommissionedPresenter:BasePresenter, DCBCommisionedPresenterProtocol, DCBCommissionedInteractorOutputProtocol {
   
    
  
    
    
     
    // MARK: Variables
    weak var view: DCBCommissionedViewProtocol?
    var interactor: DCBCommissionedInteractorInputProtocol?
    var wireFrame: DCBCommissionedWireFrameProtocol?
    let stringsTableName = "DCBCommissionedVC"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func requestData(application:LoginApplication ,dcbDetails:String ,outlinerID:String)    {
        self.interactor?.requestData(application:application, dcbDetails: dcbDetails, outlinerID:outlinerID )
    }
    
    func reloadData(dcbResponse: ResponsePayload)
    {
        self.view?.reloadData(DCBResponse:dcbResponse)
    }
    
   
   
}
