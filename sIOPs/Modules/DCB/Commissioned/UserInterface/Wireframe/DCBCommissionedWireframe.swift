//
//  DCBWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class DCBCommissionedWireframe: DCBCommissionedWireFrameProtocol {
    func presentDCBCommissionedModule(fromView: AnyObject, dcbDetails: String, application: LoginApplication, outLinerID: String) {
        DCBCommissionedWireframe.presentDCBCommissionedModule(fromView: self, dcbDetails: dcbDetails,application:application,outLinerID: outLinerID)

    }
    
 
    
   
    
    // MARK: DCBWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentDCBCommissionedModule(fromView:AnyObject, dcbDetails: String,application:LoginApplication,outLinerID: String)  {

        // Generating module components
        let view: DCBCommissionedViewProtocol = DCBCommissionedController.instantiate()
        let presenter: DCBCommisionedPresenterProtocol & DCBCommissionedInteractorOutputProtocol = DCBCommissionedPresenter()
        let interactor: DCBCommissionedInteractorInputProtocol = DCBCommissionedInteractor()
       
        let wireFrame: DCBCommissionedWireFrameProtocol = DCBCommissionedWireframe()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! DCBCommissionedController
        viewController.application = application
        viewController.dcbDetails = dcbDetails
        viewController.outlinerId = outLinerID
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    

   

}
