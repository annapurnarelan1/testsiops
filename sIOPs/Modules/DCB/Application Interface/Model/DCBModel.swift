//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DCBModel : NSObject, NSCoding{

	var countBar : [DCBSummaryModel]!
	var list : [DCBDetailListModel]!
	var totalTowerSites : Int!
    var missing : Int!
    var expected : Int!
	var userName : String!
  var everest : [DCBInUseControlModel]!
    var jcp : [DCBInUseControlModel]!
    var mcom : [DCBInUseControlModel]!
    var teMIP : [DCBInUseControlModel]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		countBar = [DCBSummaryModel]()
		if let countBarArray = dictionary["countBar"] as? [[String:Any]]{
			for dic in countBarArray{
				let value = DCBSummaryModel(fromDictionary: dic)
				countBar.append(value)
			}
		}
		list = [DCBDetailListModel]()
		if let listArray = dictionary["list"] as? [[String:Any]]{
			for dic in listArray{
				let value = DCBDetailListModel(fromDictionary: dic)
				list.append(value)
			}
		}
		totalTowerSites = dictionary["totalTowerSites"] as? Int
        missing = dictionary["missing"] as? Int
        expected = dictionary["expected"] as? Int
		userName = dictionary["userName"] as? String
       everest = [DCBInUseControlModel]()
        if let everestArray = dictionary["everest"] as? [[String:Any]]{
            for dic in everestArray{
                let value = DCBInUseControlModel(fromDictionary: dic)
                everest.append(value)
            }
        }
        jcp = [DCBInUseControlModel]()
        if let jcpArray = dictionary["jcp"] as? [[String:Any]]{
            for dic in jcpArray{
                let value = DCBInUseControlModel(fromDictionary: dic)
                jcp.append(value)
            }
        }
        mcom = [DCBInUseControlModel]()
        if let mcomArray = dictionary["mcom"] as? [[String:Any]]{
            for dic in mcomArray{
                let value = DCBInUseControlModel(fromDictionary: dic)
                mcom.append(value)
            }
        }
                teMIP = [DCBInUseControlModel]()
        if let teMIPArray = dictionary["teMIP"] as? [[String:Any]]{
            for dic in teMIPArray{
                let value = DCBInUseControlModel(fromDictionary: dic)
                teMIP.append(value)
            }
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if countBar != nil{
			var dictionaryElements = [[String:Any]]()
			for countBarElement in countBar {
				dictionaryElements.append(countBarElement.toDictionary())
			}
			dictionary["countBar"] = dictionaryElements
		}
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if totalTowerSites != nil{
			dictionary["totalTowerSites"] = totalTowerSites
		}
        if missing != nil{
            dictionary["missing"] = missing
        }
        if expected != nil{
            dictionary["expected"] = expected
        }
		if userName != nil{
			dictionary["userName"] = userName
		}
        if everest != nil{
            var dictionaryElements = [[String:Any]]()
            for everestElement in everest {
                dictionaryElements.append(everestElement.toDictionary())
            }
            dictionary["everest"] = dictionaryElements
        }
        if jcp != nil{
            var dictionaryElements = [[String:Any]]()
            for jcpElement in jcp {
                dictionaryElements.append(jcpElement.toDictionary())
            }
            dictionary["jcp"] = dictionaryElements
        }
        if mcom != nil{
            var dictionaryElements = [[String:Any]]()
            for mcomElement in mcom {
                dictionaryElements.append(mcomElement.toDictionary())
            }
            dictionary["mcom"] = dictionaryElements
        }
        if teMIP != nil{
            var dictionaryElements = [[String:Any]]()
            for teMIPElement in teMIP {
                dictionaryElements.append(teMIPElement.toDictionary())
            }
            dictionary["teMIP"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countBar = aDecoder.decodeObject(forKey :"countBar") as? [DCBSummaryModel]
         list = aDecoder.decodeObject(forKey :"list") as? [DCBDetailListModel]
         totalTowerSites = aDecoder.decodeObject(forKey: "totalTowerSites") as? Int
        missing = aDecoder.decodeObject(forKey: "missing") as? Int
        expected = aDecoder.decodeObject(forKey: "expected") as? Int
         userName = aDecoder.decodeObject(forKey: "userName") as? String
        everest = aDecoder.decodeObject(forKey :"everest") as? [DCBInUseControlModel]
                jcp = aDecoder.decodeObject(forKey :"jcp") as? [DCBInUseControlModel]
                mcom = aDecoder.decodeObject(forKey: "mcom") as? [DCBInUseControlModel]
                teMIP = aDecoder.decodeObject(forKey :"teMIP") as? [DCBInUseControlModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if countBar != nil{
			aCoder.encode(countBar, forKey: "countBar")
		}
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if totalTowerSites != nil{
			aCoder.encode(totalTowerSites, forKey: "totalTowerSites")
		}
        if expected != nil{
            aCoder.encode(expected, forKey: "expected")
        }
        if missing != nil{
            aCoder.encode(missing, forKey: "missing")
        }
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}
        if everest != nil{
            aCoder.encode(everest, forKey: "everest")
        }
        if jcp != nil{
            aCoder.encode(jcp, forKey: "jcp")
        }
        if mcom != nil{
            aCoder.encode(mcom, forKey: "mcom")
        }
        if teMIP != nil{
            aCoder.encode(teMIP, forKey: "teMIP")
        }

	}

}
