//
//	CountBar.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DCBSummaryModel : NSObject, NSCoding{

	var cnt : Int!
	var sites : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		cnt = dictionary["cnt"] as? Int
		sites = dictionary["sites"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cnt != nil{
			dictionary["cnt"] = cnt
		}
		if sites != nil{
			dictionary["sites"] = sites
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cnt = aDecoder.decodeObject(forKey: "cnt") as? Int
         sites = aDecoder.decodeObject(forKey: "sites") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cnt != nil{
			aCoder.encode(cnt, forKey: "cnt")
		}
		if sites != nil{
			aCoder.encode(sites, forKey: "sites")
		}

	}

}
