//
//  LoginProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol LoginViewProtocol: class {
    var presenter: LoginPresenterProtocol? { get set }
    func loginDone(loginRes :LoginModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func showFailError(error:String)
    func OTPREsent(loginRes :LoginModel)
    func stopLoader()
}

/// Method contract between PRESENTER -> WIREFRAME
protocol LoginWireFrameProtocol: class {
    static func presentLoginModule(fromView:AnyObject)
    func presentDashboardScreen(loginRes :LoginModel)
    func presentNGOBusinessDashboard(loginRes :LoginModel)
}

/// Method contract between VIEW -> PRESENTER
protocol LoginPresenterProtocol: class {
    var view: LoginViewProtocol? { get set }
    var interactor: LoginInteractorInputProtocol? { get set }
    var wireFrame: LoginWireFrameProtocol? { get set }
    func requestData(loginParams:Dictionary<String,Any>)
    func goToDashboard(loginRes :LoginModel)
    func requestResendOTP(loginParams:Dictionary<String,Any>)
    func requestValidateOTP(loginParams:Dictionary<String,Any>,OTP:String)
    func presentNGOBusinessDashboard(loginRes :LoginModel)
    func requestSessionKey()
}

/// Method contract between INTERACTOR -> PRESENTER
protocol LoginInteractorOutputProtocol: class {
    
    func loginDone(loginRes :LoginModel)
    func show(error: String,description:String)
    func showFailError(error:String)
    func stopLoader()
    func OTPREsent(loginRes :LoginModel)
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol LoginInteractorInputProtocol: class
{
    var presenter: LoginInteractorOutputProtocol? { get set }
    func requestData(loginParams:Dictionary<String,Any>)
    func requestResendOTP(loginParams:Dictionary<String,Any>)
    func requestValidateOTP(loginParams:Dictionary<String,Any>,OTP:String)
    func requestSessionKey()
    
}




