//
//  HomeViewModel.swift
//  Storm
//
//  Created by Mohammad Zakizadeh on 7/17/18.
//  Copyright © 2018 Storm. All rights reserved.
//

import Foundation
import FirebaseMessaging
import UtilitiesComponent


class LoginInteractor: LoginInteractorInputProtocol {
    
    public enum LoginError {
        case internetError(String)
        case serverMessage(String)
    }
    
    // MARK: Variables
    weak var presenter: LoginInteractorOutputProtocol?
    
    func requestSessionKey(){
        
        var Timestamp: String {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId":"",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let _:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let _:[String : Any] = [
            
            "isNewDesign":"1",
            "channelTypeVer": "1.0",
            "fcmId": "",
            "channel": "APP",
            "channelType": "IOS",
            "deviceId": UIDevice.current.identifierForVendor!.uuidString
        ]
        
       let busiParams: [String : Any] = [
            "deviceInfo": [
                "platform": "ios",
                "device": "",
                "host": "100.99.30.140",
                "xandroidId": "315a22700b12cd9",
                "imei": "",
                "version": "7.0",
                "mac": "",
                "serial": "ZY32224LBN",
                "manufacturer": "apple",
                "cpuAbi": "qcom",
                "type": "GSM",
                "imsi": "",
                "product": "cedric_amzin",
                "model": UIDevice().screenType.rawValue
            ],
            "key":"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmQ30NCKPWitjCTDJZa2s5iY1ylPeUR48rqT6c\nFByE5BeavYagQoZOoXq+UA32hWe80I+Y0qWTJzCtOZQ/TIE+n/i4nfS1XGRZWD8cib2+dWsOg+1T\n5giKPnmkFOFiKtWQyBZ6QfTxx+3gvRj0mlM053EHzp5nhh3RtKniGknY1wIDAQAB\n",
            "type": "0"
        ]
        
        
        let requestList: [String: Any] = [
            "busiCode": Constants.BusiCode.getTransKey,
            "isEncrypt": false, 
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["message"] as? String ?? "") == Constants.status.FAILURE ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    
                    
                    let sessionKey =   (((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["sessionId"] ?? ""
                    
                    UserDefaults.standard.set(sessionKey, forKey: "sessionId")
                    UserDefaults.standard.synchronize()
                    print(returnJson)
                    
                case Constants.status.NOK:
                    _ =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    print(APIManager.NetworkError.unKnownError.rawValue)
                default:
                    print(APIManager.NetworkError.unKnownError.rawValue)
                }
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError( _):
                    print(APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    print(APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    print(APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestData(loginParams:Dictionary<String,Any>){
        
        var Timestamp: String {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        /// UserDefaults.standard.string(forKey: "buildNumber") ?? ""
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        
        let str =  UserDefaults.standard.object(forKey: "fcmToken")
        
        
        let requestBody:[String : Any] = [
            
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "password": loginParams["password"] ?? "",
            "userName": loginParams["domainId"] ?? "",
            "type": "userInfo",
            "isNewDesign":"1",
            "channelTypeVer": UserDefaults.standard.string(forKey: "buildNumber") ?? "",
            "fcmId": str ?? "",
            "channel": "APP",
            "channelType": "IOS",
            "deviceId": UIDevice.current.identifierForVendor!.uuidString
        ]

       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        
        let requestList: [String: Any] = [
            "busiCode": Constants.BusiCode.login,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["message"] as? String ?? "") == Constants.status.FAILURE ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let loginRes: LoginModel = LoginModel.sharedInstance
                    loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
                    LoginModel.sharedInstance.saveUser()
                    
                    self.presenter?.loginDone(loginRes:loginRes)
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                
                
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError( _):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestResendOTP(loginParams:Dictionary<String,Any>){
        
        var Timestamp: String {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            
            // "password": loginParams["password"] ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "userName": loginParams["domainId"] ?? "",
            "type": "userInfo",
            "isNewDesign":"1",
            "channelTypeVer": UserDefaults.standard.string(forKey: Constants.UserDefaultStr.appVersion) ?? "",
            "fcmId": "",
            "channel": "APP",
            "channelType": "IOS",
            "deviceId": UIDevice.current.identifierForVendor!.uuidString
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        
        let requestList: [String: Any] = [
            "busiCode": Constants.BusiCode.resendOTP,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["message"] as? String ?? "") == Constants.status.FAILURE ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    
                    let loginRes: LoginModel = LoginModel.sharedInstance
                    loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
                    LoginModel.sharedInstance.saveUser()
                    self.presenter?.OTPREsent(loginRes: loginRes)
                    //let loginRes: LoginModel = LoginModel.sharedInstance
                    
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                
                
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError( _):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestValidateOTP(loginParams:Dictionary<String,Any>,OTP:String){
        
        var Timestamp: String {
            return "\(NSDate().timeIntervalSince1970 * 1000)"
        }
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            // "password": loginParams["password"] ?? "",
             
            "userName": loginParams["domainId"] ?? "",
            "type": "userInfo",
            "isNewDesign":"1",
            "channelTypeVer": UserDefaults.standard.string(forKey: Constants.UserDefaultStr.appVersion) ?? "",
            "fcmId": "",
            "channel": "APP",
            "channelType": "IOS",
            "deviceId": UIDevice.current.identifierForVendor!.uuidString,
            "otp":OTP
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        
        let requestList: [String: Any] = [
            "busiCode": Constants.BusiCode.validateOTP,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["message"] as? String ?? "") == Constants.status.FAILURE ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let loginRes: LoginModel = LoginModel.sharedInstance
                    loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
                    LoginModel.sharedInstance.saveUser()
                    
                    self.presenter?.loginDone(loginRes:loginRes)
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                
                
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError( _):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    
    
    
    
    // Convert from JSON to nsdata
    func jsonToNSData(json: AnyObject) -> Data?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
    
}
