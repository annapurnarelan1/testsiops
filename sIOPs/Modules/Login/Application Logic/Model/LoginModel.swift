//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginModel : NSObject, NSCoding{

	var respData : [LoginRespData]!
	var respInfo : LoginRespInfo!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	func initData(fromDictionary dictionary: [String:Any]){
		respData = [LoginRespData]()
		if let respDataArray = dictionary["respData"] as? [[String:Any]]{
			for dic in respDataArray{
				let value = LoginRespData(fromDictionary: dic)
				respData.append(value)
			}
		}
		if let respInfoData = dictionary["respInfo"] as? [String:Any]{
			respInfo = LoginRespInfo(fromDictionary: respInfoData)
		}
	}
    
    
     private override init() {
               super.init()
           }
        
        /// Singleton instance
          static let sharedInstance : LoginModel = {
              if let instance = DataPersisterManager.sharedInstance.getLoginResponse() {
                  return instance
              }
            return LoginModel()
          }()
        
        // MARK: Methods
        /// Saves the Login
        func saveUser(){
            DataPersisterManager.sharedInstance.saveLoginResponse()
        }
        
        /// Validates if the user is logged in or not
        ///
        /// - Returns: Boolean indicating if the user is logged in
    //    func isLoggedIn() -> Bool {
    //        return !(self.userType ?? "").isEmpty
    //    }
        
        
        /// Deletes all user properties
        func doLogout() {
            //self.respData = nil
            //self.respInfo = nil
            DataPersisterManager.sharedInstance.deleteLoginResponse()
        }


	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if respData != nil{
			var dictionaryElements = [[String:Any]]()
			for respDataElement in respData {
				dictionaryElements.append(respDataElement.toDictionary())
			}
			dictionary["respData"] = dictionaryElements
		}
		if respInfo != nil{
			dictionary["respInfo"] = respInfo.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         respData = aDecoder.decodeObject(forKey :"respData") as? [LoginRespData]
         respInfo = aDecoder.decodeObject(forKey: "respInfo") as? LoginRespInfo

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if respData != nil{
			aCoder.encode(respData, forKey: "respData")
		}
		if respInfo != nil{
			aCoder.encode(respInfo, forKey: "respInfo")
		}

	}

}
