//
//	ResponsePayload.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginResponsePayload : NSObject, NSCoding{

	var allowOverApp : AnyObject!
	var allowOverPwa : AnyObject!
	var allowOverWeb : AnyObject!
	var allowToggle : Bool!
	var allowToggleView : String!
	var applications : [LoginApplication]!
	var authenticatedFrom : AnyObject!
	var channel : AnyObject!
	var channelType : AnyObject!
	var channelTypeVer : AnyObject!
	var chatToken : AnyObject!
	var chatUserId : AnyObject!
	var dashboardrurl : AnyObject!
	var demoUserName : AnyObject!
	var deviceId : AnyObject!
	var endPageNo : Int!
	var fcmId : AnyObject!
	var functionList : AnyObject!
	var functionMap : AnyObject!
	var guestMessage : String!
	var idmMobile : AnyObject!
	var idmRILEmail : AnyObject!
	var isActive : AnyObject!
	var isNewDesign : Int!
	var isTopsFunction : AnyObject!
	var isTopsUser : Int!
	var jobRole : AnyObject!
	var jobRoleLavel : AnyObject!
	var jwtToken : AnyObject!
	var k2status : AnyObject!
	var lastLoginTimeStr : AnyObject!
	var maxAppJobRole : AnyObject!
	var message : AnyObject!
	var ngoMaxRole : String!
	var notificationCount : Int!
	var otp : AnyObject!
	var otpAuthEnabled : AnyObject!
	var otpAuthExpiry : AnyObject!
	var otpEmail : AnyObject!
	var otpMobile : AnyObject!
	var otpOutcome : AnyObject!
	var otpTemp : AnyObject!
	var password : AnyObject!
	var remark : AnyObject!
	var resourceName : AnyObject!
	var role : String!
	var sessionExpTime : AnyObject!
	var sessionId : AnyObject!
	var startPageNo : Int!
	var title : AnyObject!
	var topsUserFlag : String!
	var transactionId : AnyObject!
	var userDeviceInfo : AnyObject!
	var userDomain : AnyObject!
	var userGivenName : AnyObject!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		allowOverApp = dictionary["allowOverApp"] as? AnyObject
		allowOverPwa = dictionary["allowOverPwa"] as? AnyObject
		allowOverWeb = dictionary["allowOverWeb"] as? AnyObject
		allowToggle = dictionary["allowToggle"] as? Bool
		allowToggleView = dictionary["allowToggleView"] as? String
		applications = [LoginApplication]()
		if let applicationsArray = dictionary["applications"] as? [[String:Any]]{
			for dic in applicationsArray{
				let value = LoginApplication(fromDictionary: dic)
				applications.append(value)
			}
		}
		authenticatedFrom = dictionary["authenticatedFrom"] as? AnyObject
		channel = dictionary["channel"] as? AnyObject
		channelType = dictionary["channelType"] as? AnyObject
		channelTypeVer = dictionary["channelTypeVer"] as? AnyObject
		chatToken = dictionary["chatToken"] as? AnyObject
		chatUserId = dictionary["chatUserId"] as? AnyObject
		dashboardrurl = dictionary["dashboardrurl"] as? AnyObject
		demoUserName = dictionary["demoUserName"] as? AnyObject
		deviceId = dictionary["deviceId"] as? AnyObject
		endPageNo = dictionary["endPageNo"] as? Int
		fcmId = dictionary["fcmId"] as? AnyObject
		functionList = dictionary["functionList"] as? AnyObject
		functionMap = dictionary["functionMap"] as? AnyObject
		guestMessage = dictionary["guestMessage"] as? String
		idmMobile = dictionary["idmMobile"] as? AnyObject
		idmRILEmail = dictionary["idmRILEmail"] as? AnyObject
		isActive = dictionary["isActive"] as? AnyObject
		isNewDesign = dictionary["isNewDesign"] as? Int
		isTopsFunction = dictionary["isTopsFunction"] as? AnyObject
		isTopsUser = dictionary["isTopsUser"] as? Int
		jobRole = dictionary["jobRole"] as? AnyObject
		jobRoleLavel = dictionary["jobRoleLavel"] as? AnyObject
		jwtToken = dictionary["jwtToken"] as? AnyObject
		k2status = dictionary["k2status"] as? AnyObject
		lastLoginTimeStr = dictionary["lastLoginTimeStr"] as? AnyObject
		maxAppJobRole = dictionary["maxAppJobRole"] as? AnyObject
		message = dictionary["message"] as? AnyObject
		ngoMaxRole = dictionary["ngoMaxRole"] as? String
		notificationCount = dictionary["notificationCount"] as? Int
		otp = dictionary["otp"] as? AnyObject
		otpAuthEnabled = dictionary["otpAuthEnabled"] as? AnyObject
		otpAuthExpiry = dictionary["otpAuthExpiry"] as? AnyObject
		otpEmail = dictionary["otpEmail"] as? AnyObject
		otpMobile = dictionary["otpMobile"] as? AnyObject
		otpOutcome = dictionary["otpOutcome"] as? AnyObject
		otpTemp = dictionary["otpTemp"] as? AnyObject
		password = dictionary["password"] as? AnyObject
		remark = dictionary["remark"] as? AnyObject
		resourceName = dictionary["resourceName"] as? AnyObject
		role = dictionary["role"] as? String
		sessionExpTime = dictionary["sessionExpTime"] as? AnyObject
		sessionId = dictionary["sessionId"] as? AnyObject
		startPageNo = dictionary["startPageNo"] as? Int
		title = dictionary["title"] as? AnyObject
		topsUserFlag = dictionary["topsUserFlag"] as? String
		transactionId = dictionary["transactionId"] as? AnyObject
		userDeviceInfo = dictionary["userDeviceInfo"] as? AnyObject
		userDomain = dictionary["userDomain"] as? AnyObject
		userGivenName = dictionary["userGivenName"] as? AnyObject
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if allowOverApp != nil{
			dictionary["allowOverApp"] = allowOverApp
		}
		if allowOverPwa != nil{
			dictionary["allowOverPwa"] = allowOverPwa
		}
		if allowOverWeb != nil{
			dictionary["allowOverWeb"] = allowOverWeb
		}
		if allowToggle != nil{
			dictionary["allowToggle"] = allowToggle
		}
		if allowToggleView != nil{
			dictionary["allowToggleView"] = allowToggleView
		}
		if applications != nil{
			var dictionaryElements = [[String:Any]]()
			for applicationsElement in applications {
				dictionaryElements.append(applicationsElement.toDictionary())
			}
			dictionary["applications"] = dictionaryElements
		}
		if authenticatedFrom != nil{
			dictionary["authenticatedFrom"] = authenticatedFrom
		}
		if channel != nil{
			dictionary["channel"] = channel
		}
		if channelType != nil{
			dictionary["channelType"] = channelType
		}
		if channelTypeVer != nil{
			dictionary["channelTypeVer"] = channelTypeVer
		}
		if chatToken != nil{
			dictionary["chatToken"] = chatToken
		}
		if chatUserId != nil{
			dictionary["chatUserId"] = chatUserId
		}
		if dashboardrurl != nil{
			dictionary["dashboardrurl"] = dashboardrurl
		}
		if demoUserName != nil{
			dictionary["demoUserName"] = demoUserName
		}
		if deviceId != nil{
			dictionary["deviceId"] = deviceId
		}
		if endPageNo != nil{
			dictionary["endPageNo"] = endPageNo
		}
		if fcmId != nil{
			dictionary["fcmId"] = fcmId
		}
		if functionList != nil{
			dictionary["functionList"] = functionList
		}
		if functionMap != nil{
			dictionary["functionMap"] = functionMap
		}
		if guestMessage != nil{
			dictionary["guestMessage"] = guestMessage
		}
		if idmMobile != nil{
			dictionary["idmMobile"] = idmMobile
		}
		if idmRILEmail != nil{
			dictionary["idmRILEmail"] = idmRILEmail
		}
		if isActive != nil{
			dictionary["isActive"] = isActive
		}
		if isNewDesign != nil{
			dictionary["isNewDesign"] = isNewDesign
		}
		if isTopsFunction != nil{
			dictionary["isTopsFunction"] = isTopsFunction
		}
		if isTopsUser != nil{
			dictionary["isTopsUser"] = isTopsUser
		}
		if jobRole != nil{
			dictionary["jobRole"] = jobRole
		}
		if jobRoleLavel != nil{
			dictionary["jobRoleLavel"] = jobRoleLavel
		}
		if jwtToken != nil{
			dictionary["jwtToken"] = jwtToken
		}
		if k2status != nil{
			dictionary["k2status"] = k2status
		}
		if lastLoginTimeStr != nil{
			dictionary["lastLoginTimeStr"] = lastLoginTimeStr
		}
		if maxAppJobRole != nil{
			dictionary["maxAppJobRole"] = maxAppJobRole
		}
		if message != nil{
			dictionary["message"] = message
		}
		if ngoMaxRole != nil{
			dictionary["ngoMaxRole"] = ngoMaxRole
		}
		if notificationCount != nil{
			dictionary["notificationCount"] = notificationCount
		}
		if otp != nil{
			dictionary["otp"] = otp
		}
		if otpAuthEnabled != nil{
			dictionary["otpAuthEnabled"] = otpAuthEnabled
		}
		if otpAuthExpiry != nil{
			dictionary["otpAuthExpiry"] = otpAuthExpiry
		}
		if otpEmail != nil{
			dictionary["otpEmail"] = otpEmail
		}
		if otpMobile != nil{
			dictionary["otpMobile"] = otpMobile
		}
		if otpOutcome != nil{
			dictionary["otpOutcome"] = otpOutcome
		}
		if otpTemp != nil{
			dictionary["otpTemp"] = otpTemp
		}
		if password != nil{
			dictionary["password"] = password
		}
		if remark != nil{
			dictionary["remark"] = remark
		}
		if resourceName != nil{
			dictionary["resourceName"] = resourceName
		}
		if role != nil{
			dictionary["role"] = role
		}
		if sessionExpTime != nil{
			dictionary["sessionExpTime"] = sessionExpTime
		}
		if sessionId != nil{
			dictionary["sessionId"] = sessionId
		}
		if startPageNo != nil{
			dictionary["startPageNo"] = startPageNo
		}
		if title != nil{
			dictionary["title"] = title
		}
		if topsUserFlag != nil{
			dictionary["topsUserFlag"] = topsUserFlag
		}
		if transactionId != nil{
			dictionary["transactionId"] = transactionId
		}
		if userDeviceInfo != nil{
			dictionary["userDeviceInfo"] = userDeviceInfo
		}
		if userDomain != nil{
			dictionary["userDomain"] = userDomain
		}
		if userGivenName != nil{
			dictionary["userGivenName"] = userGivenName
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         allowOverApp = aDecoder.decodeObject(forKey: "allowOverApp") as? AnyObject
         allowOverPwa = aDecoder.decodeObject(forKey: "allowOverPwa") as? AnyObject
         allowOverWeb = aDecoder.decodeObject(forKey: "allowOverWeb") as? AnyObject
         allowToggle = aDecoder.decodeObject(forKey: "allowToggle") as? Bool
         allowToggleView = aDecoder.decodeObject(forKey: "allowToggleView") as? String
         applications = aDecoder.decodeObject(forKey :"applications") as? [LoginApplication]
         authenticatedFrom = aDecoder.decodeObject(forKey: "authenticatedFrom") as? AnyObject
         channel = aDecoder.decodeObject(forKey: "channel") as? AnyObject
         channelType = aDecoder.decodeObject(forKey: "channelType") as? AnyObject
         channelTypeVer = aDecoder.decodeObject(forKey: "channelTypeVer") as? AnyObject
         chatToken = aDecoder.decodeObject(forKey: "chatToken") as? AnyObject
         chatUserId = aDecoder.decodeObject(forKey: "chatUserId") as? AnyObject
         dashboardrurl = aDecoder.decodeObject(forKey: "dashboardrurl") as? AnyObject
         demoUserName = aDecoder.decodeObject(forKey: "demoUserName") as? AnyObject
         deviceId = aDecoder.decodeObject(forKey: "deviceId") as? AnyObject
         endPageNo = aDecoder.decodeObject(forKey: "endPageNo") as? Int
         fcmId = aDecoder.decodeObject(forKey: "fcmId") as? AnyObject
         functionList = aDecoder.decodeObject(forKey: "functionList") as? AnyObject
         functionMap = aDecoder.decodeObject(forKey: "functionMap") as? AnyObject
         guestMessage = aDecoder.decodeObject(forKey: "guestMessage") as? String
         idmMobile = aDecoder.decodeObject(forKey: "idmMobile") as? AnyObject
         idmRILEmail = aDecoder.decodeObject(forKey: "idmRILEmail") as? AnyObject
         isActive = aDecoder.decodeObject(forKey: "isActive") as? AnyObject
         isNewDesign = aDecoder.decodeObject(forKey: "isNewDesign") as? Int
         isTopsFunction = aDecoder.decodeObject(forKey: "isTopsFunction") as? AnyObject
         isTopsUser = aDecoder.decodeObject(forKey: "isTopsUser") as? Int
         jobRole = aDecoder.decodeObject(forKey: "jobRole") as? AnyObject
         jobRoleLavel = aDecoder.decodeObject(forKey: "jobRoleLavel") as? AnyObject
         jwtToken = aDecoder.decodeObject(forKey: "jwtToken") as? AnyObject
         k2status = aDecoder.decodeObject(forKey: "k2status") as? AnyObject
         lastLoginTimeStr = aDecoder.decodeObject(forKey: "lastLoginTimeStr") as? AnyObject
         maxAppJobRole = aDecoder.decodeObject(forKey: "maxAppJobRole") as? AnyObject
         message = aDecoder.decodeObject(forKey: "message") as? AnyObject
         ngoMaxRole = aDecoder.decodeObject(forKey: "ngoMaxRole") as? String
         notificationCount = aDecoder.decodeObject(forKey: "notificationCount") as? Int
         otp = aDecoder.decodeObject(forKey: "otp") as? AnyObject
         otpAuthEnabled = aDecoder.decodeObject(forKey: "otpAuthEnabled") as? AnyObject
         otpAuthExpiry = aDecoder.decodeObject(forKey: "otpAuthExpiry") as? AnyObject
         otpEmail = aDecoder.decodeObject(forKey: "otpEmail") as? AnyObject
         otpMobile = aDecoder.decodeObject(forKey: "otpMobile") as? AnyObject
         otpOutcome = aDecoder.decodeObject(forKey: "otpOutcome") as? AnyObject
         otpTemp = aDecoder.decodeObject(forKey: "otpTemp") as? AnyObject
         password = aDecoder.decodeObject(forKey: "password") as? AnyObject
         remark = aDecoder.decodeObject(forKey: "remark") as? AnyObject
         resourceName = aDecoder.decodeObject(forKey: "resourceName") as? AnyObject
         role = aDecoder.decodeObject(forKey: "role") as? String
         sessionExpTime = aDecoder.decodeObject(forKey: "sessionExpTime") as? AnyObject
         sessionId = aDecoder.decodeObject(forKey: "sessionId") as? AnyObject
         startPageNo = aDecoder.decodeObject(forKey: "startPageNo") as? Int
         title = aDecoder.decodeObject(forKey: "title") as? AnyObject
         topsUserFlag = aDecoder.decodeObject(forKey: "topsUserFlag") as? String
         transactionId = aDecoder.decodeObject(forKey: "transactionId") as? AnyObject
         userDeviceInfo = aDecoder.decodeObject(forKey: "userDeviceInfo") as? AnyObject
         userDomain = aDecoder.decodeObject(forKey: "userDomain") as? AnyObject
         userGivenName = aDecoder.decodeObject(forKey: "userGivenName") as? AnyObject
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if allowOverApp != nil{
			aCoder.encode(allowOverApp, forKey: "allowOverApp")
		}
		if allowOverPwa != nil{
			aCoder.encode(allowOverPwa, forKey: "allowOverPwa")
		}
		if allowOverWeb != nil{
			aCoder.encode(allowOverWeb, forKey: "allowOverWeb")
		}
		if allowToggle != nil{
			aCoder.encode(allowToggle, forKey: "allowToggle")
		}
		if allowToggleView != nil{
			aCoder.encode(allowToggleView, forKey: "allowToggleView")
		}
		if applications != nil{
			aCoder.encode(applications, forKey: "applications")
		}
		if authenticatedFrom != nil{
			aCoder.encode(authenticatedFrom, forKey: "authenticatedFrom")
		}
		if channel != nil{
			aCoder.encode(channel, forKey: "channel")
		}
		if channelType != nil{
			aCoder.encode(channelType, forKey: "channelType")
		}
		if channelTypeVer != nil{
			aCoder.encode(channelTypeVer, forKey: "channelTypeVer")
		}
		if chatToken != nil{
			aCoder.encode(chatToken, forKey: "chatToken")
		}
		if chatUserId != nil{
			aCoder.encode(chatUserId, forKey: "chatUserId")
		}
		if dashboardrurl != nil{
			aCoder.encode(dashboardrurl, forKey: "dashboardrurl")
		}
		if demoUserName != nil{
			aCoder.encode(demoUserName, forKey: "demoUserName")
		}
		if deviceId != nil{
			aCoder.encode(deviceId, forKey: "deviceId")
		}
		if endPageNo != nil{
			aCoder.encode(endPageNo, forKey: "endPageNo")
		}
		if fcmId != nil{
			aCoder.encode(fcmId, forKey: "fcmId")
		}
		if functionList != nil{
			aCoder.encode(functionList, forKey: "functionList")
		}
		if functionMap != nil{
			aCoder.encode(functionMap, forKey: "functionMap")
		}
		if guestMessage != nil{
			aCoder.encode(guestMessage, forKey: "guestMessage")
		}
		if idmMobile != nil{
			aCoder.encode(idmMobile, forKey: "idmMobile")
		}
		if idmRILEmail != nil{
			aCoder.encode(idmRILEmail, forKey: "idmRILEmail")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "isActive")
		}
		if isNewDesign != nil{
			aCoder.encode(isNewDesign, forKey: "isNewDesign")
		}
		if isTopsFunction != nil{
			aCoder.encode(isTopsFunction, forKey: "isTopsFunction")
		}
		if isTopsUser != nil{
			aCoder.encode(isTopsUser, forKey: "isTopsUser")
		}
		if jobRole != nil{
			aCoder.encode(jobRole, forKey: "jobRole")
		}
		if jobRoleLavel != nil{
			aCoder.encode(jobRoleLavel, forKey: "jobRoleLavel")
		}
		if jwtToken != nil{
			aCoder.encode(jwtToken, forKey: "jwtToken")
		}
		if k2status != nil{
			aCoder.encode(k2status, forKey: "k2status")
		}
		if lastLoginTimeStr != nil{
			aCoder.encode(lastLoginTimeStr, forKey: "lastLoginTimeStr")
		}
		if maxAppJobRole != nil{
			aCoder.encode(maxAppJobRole, forKey: "maxAppJobRole")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if ngoMaxRole != nil{
			aCoder.encode(ngoMaxRole, forKey: "ngoMaxRole")
		}
		if notificationCount != nil{
			aCoder.encode(notificationCount, forKey: "notificationCount")
		}
		if otp != nil{
			aCoder.encode(otp, forKey: "otp")
		}
		if otpAuthEnabled != nil{
			aCoder.encode(otpAuthEnabled, forKey: "otpAuthEnabled")
		}
		if otpAuthExpiry != nil{
			aCoder.encode(otpAuthExpiry, forKey: "otpAuthExpiry")
		}
		if otpEmail != nil{
			aCoder.encode(otpEmail, forKey: "otpEmail")
		}
		if otpMobile != nil{
			aCoder.encode(otpMobile, forKey: "otpMobile")
		}
		if otpOutcome != nil{
			aCoder.encode(otpOutcome, forKey: "otpOutcome")
		}
		if otpTemp != nil{
			aCoder.encode(otpTemp, forKey: "otpTemp")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if remark != nil{
			aCoder.encode(remark, forKey: "remark")
		}
		if resourceName != nil{
			aCoder.encode(resourceName, forKey: "resourceName")
		}
		if role != nil{
			aCoder.encode(role, forKey: "role")
		}
		if sessionExpTime != nil{
			aCoder.encode(sessionExpTime, forKey: "sessionExpTime")
		}
		if sessionId != nil{
			aCoder.encode(sessionId, forKey: "sessionId")
		}
		if startPageNo != nil{
			aCoder.encode(startPageNo, forKey: "startPageNo")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if topsUserFlag != nil{
			aCoder.encode(topsUserFlag, forKey: "topsUserFlag")
		}
		if transactionId != nil{
			aCoder.encode(transactionId, forKey: "transactionId")
		}
		if userDeviceInfo != nil{
			aCoder.encode(userDeviceInfo, forKey: "userDeviceInfo")
		}
		if userDomain != nil{
			aCoder.encode(userDomain, forKey: "userDomain")
		}
		if userGivenName != nil{
			aCoder.encode(userGivenName, forKey: "userGivenName")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
