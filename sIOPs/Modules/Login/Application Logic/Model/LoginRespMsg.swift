//
//	RespMsg.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginRespMsg : NSObject, NSCoding{

	var responseHeader : LoginResponseHeader!
	var responsePayload : LoginResponsePayload!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let responseHeaderData = dictionary["responseHeader"] as? [String:Any]{
			responseHeader = LoginResponseHeader(fromDictionary: responseHeaderData)
		}
		if let responsePayloadData = dictionary["responsePayload"] as? [String:Any]{
			responsePayload = LoginResponsePayload(fromDictionary: responsePayloadData)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if responseHeader != nil{
			dictionary["responseHeader"] = responseHeader.toDictionary()
		}
		if responsePayload != nil{
			dictionary["responsePayload"] = responsePayload.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         responseHeader = aDecoder.decodeObject(forKey: "responseHeader") as? LoginResponseHeader
         responsePayload = aDecoder.decodeObject(forKey: "responsePayload") as? LoginResponsePayload

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if responseHeader != nil{
			aCoder.encode(responseHeader, forKey: "responseHeader")
		}
		if responsePayload != nil{
			aCoder.encode(responsePayload, forKey: "responsePayload")
		}

	}

}
