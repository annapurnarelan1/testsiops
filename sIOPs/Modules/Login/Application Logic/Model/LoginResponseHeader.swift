//
//	ResponseHeader.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginResponseHeader : NSObject, NSCoding{

	var message : String!
	var path : String!
	var status : Int!
	var timestamp : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		message = dictionary["message"] as? String
		path = dictionary["path"] as? String
		status = dictionary["status"] as? Int
		timestamp = dictionary["timestamp"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if message != nil{
			dictionary["message"] = message
		}
		if path != nil{
			dictionary["path"] = path
		}
		if status != nil{
			dictionary["status"] = status
		}
		if timestamp != nil{
			dictionary["timestamp"] = timestamp
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "message") as? String
         path = aDecoder.decodeObject(forKey: "path") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         timestamp = aDecoder.decodeObject(forKey: "timestamp") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if path != nil{
			aCoder.encode(path, forKey: "path")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if timestamp != nil{
			aCoder.encode(timestamp, forKey: "timestamp")
		}

	}

}
