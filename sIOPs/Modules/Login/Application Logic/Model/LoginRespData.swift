//
//	RespData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginRespData : NSObject, NSCoding{

	var busiCode : String!
	var code : String!
	var isEncrypt : Bool!
	var message : String!
	var respMsg : LoginRespMsg!
	var transactionId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		busiCode = dictionary["busiCode"] as? String
		code = dictionary["code"] as? String
		isEncrypt = dictionary["isEncrypt"] as? Bool
		message = dictionary["message"] as? String
		if let respMsgData = dictionary["respMsg"] as? [String:Any]{
			respMsg = LoginRespMsg(fromDictionary: respMsgData)
		}
		transactionId = dictionary["transactionId"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if busiCode != nil{
			dictionary["busiCode"] = busiCode
		}
		if code != nil{
			dictionary["code"] = code
		}
		if isEncrypt != nil{
			dictionary["isEncrypt"] = isEncrypt
		}
		if message != nil{
			dictionary["message"] = message
		}
		if respMsg != nil{
			dictionary["respMsg"] = respMsg.toDictionary()
		}
		if transactionId != nil{
			dictionary["transactionId"] = transactionId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         busiCode = aDecoder.decodeObject(forKey: "busiCode") as? String
         code = aDecoder.decodeObject(forKey: "code") as? String
         isEncrypt = aDecoder.decodeObject(forKey: "isEncrypt") as? Bool
         message = aDecoder.decodeObject(forKey: "message") as? String
         respMsg = aDecoder.decodeObject(forKey: "respMsg") as? LoginRespMsg
         transactionId = aDecoder.decodeObject(forKey: "transactionId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if busiCode != nil{
			aCoder.encode(busiCode, forKey: "busiCode")
		}
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if isEncrypt != nil{
			aCoder.encode(isEncrypt, forKey: "isEncrypt")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if respMsg != nil{
			aCoder.encode(respMsg, forKey: "respMsg")
		}
		if transactionId != nil{
			aCoder.encode(transactionId, forKey: "transactionId")
		}

	}

}
