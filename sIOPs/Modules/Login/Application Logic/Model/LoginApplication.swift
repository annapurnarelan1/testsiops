//
//	Application.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LoginApplication : NSObject, NSCoding{

	var allowOverApp : AnyObject!
	var allowOverPwa : AnyObject!
	var allowOverWeb : AnyObject!
	var appLead : AnyObject!
	var appRole : String!
	var applicability : String!
	var applicationCode : String!
	var applicationIcon : String!
	var applicationName : String!
	var applicationUCode : String!
	var colourCodeForApp : String!
	var colourCodeForPwa : String!
	var domainLead : AnyObject!
	var isTopsFunction : String!
	var platformLead : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		allowOverApp = dictionary["allowOverApp"] as? AnyObject
		allowOverPwa = dictionary["allowOverPwa"] as? AnyObject
		allowOverWeb = dictionary["allowOverWeb"] as? AnyObject
		appLead = dictionary["appLead"] as? AnyObject
		appRole = dictionary["appRole"] as? String
		applicability = dictionary["applicability"] as? String
		applicationCode = dictionary["applicationCode"] as? String
		applicationIcon = dictionary["applicationIcon"] as? String
		applicationName = dictionary["applicationName"] as? String
		applicationUCode = dictionary["applicationUCode"] as? String
		colourCodeForApp = dictionary["colourCodeForApp"] as? String
		colourCodeForPwa = dictionary["colourCodeForPwa"] as? String
		domainLead = dictionary["domainLead"] as? AnyObject
		isTopsFunction = dictionary["isTopsFunction"] as? String
		platformLead = dictionary["platformLead"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if allowOverApp != nil{
			dictionary["allowOverApp"] = allowOverApp
		}
		if allowOverPwa != nil{
			dictionary["allowOverPwa"] = allowOverPwa
		}
		if allowOverWeb != nil{
			dictionary["allowOverWeb"] = allowOverWeb
		}
		if appLead != nil{
			dictionary["appLead"] = appLead
		}
		if appRole != nil{
			dictionary["appRole"] = appRole
		}
		if applicability != nil{
			dictionary["applicability"] = applicability
		}
		if applicationCode != nil{
			dictionary["applicationCode"] = applicationCode
		}
		if applicationIcon != nil{
			dictionary["applicationIcon"] = applicationIcon
		}
		if applicationName != nil{
			dictionary["applicationName"] = applicationName
		}
		if applicationUCode != nil{
			dictionary["applicationUCode"] = applicationUCode
		}
		if colourCodeForApp != nil{
			dictionary["colourCodeForApp"] = colourCodeForApp
		}
		if colourCodeForPwa != nil{
			dictionary["colourCodeForPwa"] = colourCodeForPwa
		}
		if domainLead != nil{
			dictionary["domainLead"] = domainLead
		}
		if isTopsFunction != nil{
			dictionary["isTopsFunction"] = isTopsFunction
		}
		if platformLead != nil{
			dictionary["platformLead"] = platformLead
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         allowOverApp = aDecoder.decodeObject(forKey: "allowOverApp") as? AnyObject
         allowOverPwa = aDecoder.decodeObject(forKey: "allowOverPwa") as? AnyObject
         allowOverWeb = aDecoder.decodeObject(forKey: "allowOverWeb") as? AnyObject
         appLead = aDecoder.decodeObject(forKey: "appLead") as? AnyObject
         appRole = aDecoder.decodeObject(forKey: "appRole") as? String
         applicability = aDecoder.decodeObject(forKey: "applicability") as? String
         applicationCode = aDecoder.decodeObject(forKey: "applicationCode") as? String
         applicationIcon = aDecoder.decodeObject(forKey: "applicationIcon") as? String
         applicationName = aDecoder.decodeObject(forKey: "applicationName") as? String
         applicationUCode = aDecoder.decodeObject(forKey: "applicationUCode") as? String
         colourCodeForApp = aDecoder.decodeObject(forKey: "colourCodeForApp") as? String
         colourCodeForPwa = aDecoder.decodeObject(forKey: "colourCodeForPwa") as? String
         domainLead = aDecoder.decodeObject(forKey: "domainLead") as? AnyObject
         isTopsFunction = aDecoder.decodeObject(forKey: "isTopsFunction") as? String
         platformLead = aDecoder.decodeObject(forKey: "platformLead") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if allowOverApp != nil{
			aCoder.encode(allowOverApp, forKey: "allowOverApp")
		}
		if allowOverPwa != nil{
			aCoder.encode(allowOverPwa, forKey: "allowOverPwa")
		}
		if allowOverWeb != nil{
			aCoder.encode(allowOverWeb, forKey: "allowOverWeb")
		}
		if appLead != nil{
			aCoder.encode(appLead, forKey: "appLead")
		}
		if appRole != nil{
			aCoder.encode(appRole, forKey: "appRole")
		}
		if applicability != nil{
			aCoder.encode(applicability, forKey: "applicability")
		}
		if applicationCode != nil{
			aCoder.encode(applicationCode, forKey: "applicationCode")
		}
		if applicationIcon != nil{
			aCoder.encode(applicationIcon, forKey: "applicationIcon")
		}
		if applicationName != nil{
			aCoder.encode(applicationName, forKey: "applicationName")
		}
		if applicationUCode != nil{
			aCoder.encode(applicationUCode, forKey: "applicationUCode")
		}
		if colourCodeForApp != nil{
			aCoder.encode(colourCodeForApp, forKey: "colourCodeForApp")
		}
		if colourCodeForPwa != nil{
			aCoder.encode(colourCodeForPwa, forKey: "colourCodeForPwa")
		}
		if domainLead != nil{
			aCoder.encode(domainLead, forKey: "domainLead")
		}
		if isTopsFunction != nil{
			aCoder.encode(isTopsFunction, forKey: "isTopsFunction")
		}
		if platformLead != nil{
			aCoder.encode(platformLead, forKey: "platformLead")
		}

	}

}
