//
//  HomeVC.swift
//  MVVMRx
//
//  Created by Gunjan Goyal on 11/26/19.
//  Copyright © 2019 ril. All rights reserved.
//

import UIKit

// MARK: - Constant Strings
enum StaticStrings:String {
    case Version = "Version"
    case resendOTP = "Resend OTP"
    case betaStr = "(Beta)"
    case Submit = "Submit"
    case domainId = "domainId"
    case password = "password"
    case Login = "Login"
    case MaximumLimitOTP = "Reached maximum limit of OTPs"
    case yes = "YES"
    case tickMark = "tickMark"
    case OTPResent = "OTP RESENT"
    case OTPRegisterd = "OTP has been sent to registered mobile number (+91xxxxxx"
    case emailIdStr = "and email id"
    case OTPExpires = "OTP Expire in"
    case GUEST = "GUEST"
    case GuestUser = "GUEST USER"
}

// MARK: - Login ViewController
class LoginVC: BaseViewController, LoginViewProtocol, UITextFieldDelegate {
    
    // MARK: - SubViews
    @IBOutlet weak var domainid_txt: UITextField!
    @IBOutlet weak var password_txt: UITextField!
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var otpHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var otpTextView: UITextField!
    @IBOutlet weak var OTPView: UIView!
    @IBOutlet weak var login_btn: UIButton!
    
    
    // MARK: - Variables
    var presenter: LoginPresenterProtocol?
    var timer: Timer?
    var otpCount = 0
    
    
    // MARK: - View Initiation
    /**
    Initializes a Login View with the provided parts and specifications.
    - Returns:StoryBoard of LoginViewController
    */
    static func instantiate() -> LoginViewProtocol{
        return UIStoryboard(name: StaticStrings.Login.rawValue, bundle: nil).instantiateViewController(withIdentifier: StaticStrings.Login.rawValue) as! LoginVC
    }
    
    // MARK: - View's Cycle
    /// Overwritten method from UIVIewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    /// Overwritten method from UIVIewController, it calls a method to subscribe to keyboard events
    ///
    /// - Parameter animated: animation flag
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.viewWillAppearSetUp()
    }
    
    /// Overwritten method from UIVIewController, it calls a method to subscribe to keyboard events
    ///
    /// - Parameter animated: animation flag
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    // MARK: - Initial View Setup
    
    /// Setup initial loginView
    func setupView() {
        if(UserDefaults.standard.object(forKey: Constants.rememberUsername.isUsername) as? Bool == true)
        {
            if let username = UserDefaults.standard.object(forKey: Constants.rememberUsername.username) as? String
            {
                domainid_txt.text = username
            }
            checkBtn.isSelected = true
        }
        else
        {
            checkBtn.isSelected = false
        }
        versionLbl.text = "\(StaticStrings.Version) \(UserDefaults.standard.string(forKey: Constants.UserDefaultStr.appVersion) ?? "") (Beta)"
        let resendStr = self.setAttributedStr(inputStr: StaticStrings.resendOTP.rawValue)
        resendOTPBtn.setAttributedTitle(resendStr, for: .normal)
        otpHeightConstraint.constant = 0
        OTPView.isHidden = true
        timeLabel.isHidden = true
        otpTextView.textContentType = .oneTimeCode
    }
    
    /**
    Creates a Login View when view appears
     
     ## Unordered Lists
     
     - Requesting Authorization session Key
     - Hiding navigation bar
     - Disabling or enabling Login button by checking condition, Username and password should not be blanck
    */
    func viewWillAppearSetUp()
    {
        self.presenter?.requestSessionKey()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if (self.domainid_txt.text?.count ?? 0  > 0 && self.password_txt.text?.count ?? 0 > 0) {
            self.login_btn.backgroundColor = Constants.COLOR.loginEnabled
            self.login_btn.isEnabled = true
        } else {
            self.login_btn.backgroundColor = Constants.COLOR.loginDisabled
            self.login_btn.isEnabled = false
        }
    }
    
    /**
     Setting attributed string color and underline style
     - Parameter inputStr: String which need to be change
     - Returns: A new attributed string with color and style
    */
    func setAttributedStr(inputStr:String) -> NSAttributedString
    {
        let attrs = [
            NSAttributedString.Key.foregroundColor : UIColor.link,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        let buttonTitleStr = NSMutableAttributedString(string:inputStr, attributes:attrs)
        let attributedString = NSMutableAttributedString(string:"")
        attributedString.append(buttonTitleStr)
        return attributedString
    }
    
    
    /**
     Hide OTP View
     ## Unordered Lists
     - OTP Insert timer invalidate
     - OTP view is hidden
     - Time label showing OTP time is hidden
     - Submit button title change to login
     - Username and password textfield interaction enabled
    */
    func hideOTPView()
    {
        self.timer?.invalidate()
        self.OTPView.isHidden = true
        self.timeLabel.isHidden = true
        self.otpHeightConstraint.constant = 0
        self.login_btn.setTitle(StaticStrings.Login.rawValue, for: .normal)
        self.domainid_txt.isUserInteractionEnabled  = true
        self.password_txt.isUserInteractionEnabled  = true
    }
    
    /**
     Show OTP View when click on login button
     ## Unordered Lists
     - OTP view will be shown
     - Time label will be shown
     - login button title change to Submit
     - Username and password textfield interaction disabled
    */
    func showOTPView()
    {
        login_btn.setTitle(StaticStrings.Submit.rawValue, for: .normal)
        self.OTPView.isHidden = false
        self.timeLabel.isHidden = false
        self.otpHeightConstraint.constant = 44
        self.domainid_txt.isUserInteractionEnabled  = false
        self.password_txt.isUserInteractionEnabled  = false
    }
    
    //MARK: IBAction
    
    /**
    Login or submit action called from the view depend on button title
     
     - Parameter sender: Any object
     
     ## Unordered Lists
     - Saved username in default preferences on the basis remember username selection
     - Request to Validate OTP when button title is submit
     - Request to make network call for login
     - OTP count is 0 means no OTP resent again
    */
    @IBAction func onLoginClicked(_ sender: Any) {
        UserDefaults.standard.set(domainid_txt.text, forKey: Constants.rememberUsername.username)
        UserDefaults.standard.synchronize()
        if(login_btn.title(for: .normal) == StaticStrings.Submit.rawValue)
        {
            var dictParams = [String : String] ()
            dictParams[StaticStrings.domainId.rawValue] = self.domainid_txt.text
            dictParams[StaticStrings.password.rawValue] = password_txt.text
            startAnimating()
            self.presenter?.requestValidateOTP(loginParams: dictParams, OTP: otpTextView.text ?? "")
        }
        else
        {
            otpCount = 0
            self.view.endEditing(true)
            // make Network call to login
            self.makeNetworkCall()
        }
    }
    
    /**
   Resend OTP called from the view
     
     - Parameter sender: Any object
     
     ## Unordered Lists
     -  OTP count should be less than 2 to request for another OTP
     - OTP count if greater than 2 , OTP view hide
     - Error message should be shown that OTP resend maximum limit reached
    */
    @IBAction func resendOTP(_ sender: Any) {
        var dictParams = [String : String] ()
        dictParams[StaticStrings.domainId.rawValue] = self.domainid_txt.text
        dictParams[StaticStrings.password.rawValue] = password_txt.text
        
        if(otpCount < 2) {
            startAnimating()
            self.presenter?.requestResendOTP(loginParams: dictParams)
        }
        else {
            self.hideOTPView()
            self.show(image: "", error: "", description: StaticStrings.MaximumLimitOTP.rawValue)
        }
        
    }
    
    /**
    Remember username called from the view
      
      - Parameter sender: Any object
      
      ## Unordered Lists
      - Button got deselect when already selected and remember username will set no.
      - Button got select when already deselected and remember username will set true.
     */
    @IBAction func onCheckBoxClicked(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                UserDefaults.standard.set(false, forKey: Constants.rememberUsername.isUsername)
                UserDefaults.standard.synchronize()
                button.isSelected = false
            } else {
                // set selected
                UserDefaults.standard.set(true, forKey: Constants.rememberUsername.isUsername)
                UserDefaults.standard.synchronize()
                button.isSelected = true
            }
        }
    }
    
    // MARK: Network call
    
    /**
      Network call for Login
     */
    func makeNetworkCall() {
        
        var dictParams = [String : String] ()
        dictParams[StaticStrings.domainId.rawValue] = self.domainid_txt.text
        dictParams[StaticStrings.password.rawValue] = password_txt.text
        startAnimating()
        self.presenter?.requestData(loginParams:dictParams)
    }
    
    
    // MARK:- UITextField Delegate
    
    /**
     Textfield delegate to check whitespaces and enabling and disabling login button
     
     ## Unordered Lists
    
     - Disabling or enabling Login button by checking condition, Username and password should not be blanck
    */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let domainname: NSString = self.domainid_txt?.text as NSString? ?? ""
        let password: NSString = self.password_txt?.text as NSString? ?? ""
        var newDomain = self.domainid_txt.text ?? ""
        var newPassword = self.password_txt.text ?? ""
        if textField == self.domainid_txt {
            newDomain = domainname.replacingCharacters(in: range, with: string) as String
        }else {
            newPassword = password.replacingCharacters(in: range, with: string) as String
        }
        if (newDomain.count  > 0 && newPassword.count > 0) {
            self.login_btn.backgroundColor = Constants.COLOR.loginEnabled
            self.login_btn.isEnabled = true
        } else {
            self.login_btn.backgroundColor = Constants.COLOR.loginDisabled
            self.login_btn.isEnabled = false
        }
        return true
    }
    
    
    /// Testfield end editing
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    /**
    View setup after  OTP sent
      
      - Parameter loginRes: Login response object
      
      ## Unordered Lists
      - Set Popup message having partial hidden mobile number and email
      - if timer reach to 0 , View is hidden
     */
    func OTPSendSetup(loginRes :LoginModel)
    {
        if let responseData = loginRes.respData {
            let stringRange = (((responseData[0].respMsg as LoginRespMsg).responsePayload.idmRILEmail as? String)?.count ?? 0) - 10
            var xStr = ""
            for _ in 0..<stringRange
            {
                xStr = xStr + "x"
            }
            self.show(image: StaticStrings.tickMark.rawValue, error:StaticStrings.OTPResent.rawValue, description: "\(StaticStrings.OTPRegisterd.rawValue)\(((responseData[0].respMsg as LoginRespMsg).responsePayload.idmMobile as? String)?.suffix(4) ?? "")) \(StaticStrings.emailIdStr) (\(((responseData[0].respMsg as LoginRespMsg).responsePayload.idmRILEmail as? String)?.prefix(2) ?? "")\(xStr)\(((responseData[0].respMsg as LoginRespMsg).responsePayload.idmRILEmail as? String)?.suffix(8) ?? ""))")
            
            var timerValue = 120
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: {_ in
                
                timerValue = timerValue - 1
                var value = "\(timerValue%60)"
                if(timerValue%60 < 10)
                {
                    value = "0\(timerValue%60)"
                }
                self.timeLabel.text = "\(StaticStrings.OTPExpires) \(timerValue/60):\(value)"
                if(timerValue == 0)
                {
                    self.hideOTPView()
                }
                
            })
            self.timer?.fire()
        }
    }
    
    // MARK:- LoginViewProtocol Methods
    
    /**
     OTP has been sent
      
      - Parameter loginRes: Login response object
      
      ## Unordered Lists
      - Invalidate OTP timer
      - OTP Count increase to 1
      - OTP View setup
     */
    func OTPREsent(loginRes :LoginModel) {
        self.timer?.invalidate()
        stopAnimating()
        otpCount = otpCount + 1
        self.OTPSendSetup(loginRes: loginRes)
    }
    
    /**
    Login  action response in the view
     
     - Parameter loginRes: Login response object
     
     ## Unordered Lists
     - Check NgMaxRole, If Guest message has been shown otherwise Login is done
     - OTP setup called
     - Timer invalidate and present dashboard
    */
    func loginDone(loginRes :LoginModel) {
        stopAnimating()
        if let responseData = loginRes.respData {
            if(((responseData[0].respMsg as LoginRespMsg).responsePayload.ngoMaxRole) == StaticStrings.GUEST.rawValue)
            {
                self.show(image: "", error: StaticStrings.GuestUser.rawValue, description: "\(((responseData[0].respMsg as LoginRespMsg).responsePayload.guestMessage ?? ""))")
            }
            else
            {
                if(((responseData[0].respMsg as LoginRespMsg).responsePayload.otpAuthEnabled as? String) == StaticStrings.yes.rawValue)
                {
                    self.showOTPView()
                    self.OTPSendSetup(loginRes: loginRes)
                }
                else {
                    self.timer?.invalidate()
                    if(((responseData[0].respMsg as LoginRespMsg).responsePayload.allowToggleView) == Constants.toggle.NGOBuisnessToggle)
                    {
                        self.presenter?.presentNGOBusinessDashboard(loginRes: loginRes)
                    }
                    else
                    {
                        self.presenter?.goToDashboard(loginRes :loginRes)
                    }
                    
                }
            }
        }
        
    }
    
    
    // MARK:- Error Message Handler
    
    
    /**
    StopLoader called when update error is thrown
    */
    func stopLoader() {
        stopAnimating()
    }
    /**
    showFailError called when error thrown by server
     - Parameter error: error thrown by server
    */
    
    func showFailError(error:String) {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    /**
    show called when error shown in popup
     - Parameter image: image icon in popup
     - Parameter error: error Title to be shown
     - Parameter description: description Of error
    */
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    
    /**
    show called when error shown in popup
     - Parameter error: error Title to be shown
     - Parameter description: description Of error
    */
    func showError(error: String,description:String) {
        stopAnimating()
    }
}



