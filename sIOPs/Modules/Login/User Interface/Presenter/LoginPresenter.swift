//
//  LoginPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation
/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class LoginPresenter:BasePresenter, LoginPresenterProtocol, LoginInteractorOutputProtocol {

    // MARK: Variables
    weak var view: LoginViewProtocol?
    var interactor: LoginInteractorInputProtocol?
    var wireFrame: LoginWireFrameProtocol?
    let stringsTableName = "Login"

    // MARK: LoginPresenterProtocol
    
    func requestData(loginParams:Dictionary<String,Any>) {
        self.interactor?.requestData(loginParams: loginParams)
    }
    
    func goToDashboard(loginRes :LoginModel)
    {
        self.wireFrame?.presentDashboardScreen(loginRes :loginRes)
    }
    func requestResendOTP(loginParams:Dictionary<String,Any>)
    {
        self.interactor?.requestResendOTP(loginParams:loginParams)
    }
    func requestValidateOTP(loginParams:Dictionary<String,Any>,OTP:String)
    {
        self.interactor?.requestValidateOTP(loginParams:loginParams,OTP:OTP)
    }
    func presentNGOBusinessDashboard(loginRes :LoginModel)
    {
        self.wireFrame?.presentNGOBusinessDashboard(loginRes :loginRes)
    }
    
    // MARK: LoginInteractorOutputProtocol
    
    func loginDone(loginRes :LoginModel) {
        self.view?.loginDone(loginRes :loginRes)
    }
    
    func requestSessionKey()
    {
        self.interactor?.requestSessionKey()
    }
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    func OTPREsent(loginRes :LoginModel)
    {
        self.view?.OTPREsent(loginRes :loginRes)
    }
    func stopLoader()
    {
        self.view?.stopLoader()
    }
   
}
