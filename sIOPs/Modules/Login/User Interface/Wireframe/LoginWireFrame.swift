//
//  LoginWireFrame.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Wireframe that handles all routing between views
class LoginWireFrame: LoginWireFrameProtocol {
    // MARK: LoginWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentLoginModule(fromView:AnyObject) {

        // Generating module components
        let view: LoginViewProtocol = LoginVC.instantiate()
        let presenter: LoginPresenterProtocol & LoginInteractorOutputProtocol = LoginPresenter()
        let interactor: LoginInteractorInputProtocol = LoginInteractor()
       
        let wireFrame: LoginWireFrameProtocol = LoginWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        let viewController = view as! LoginVC
        NavigationHelper.setRootViewController(withViewController: viewController);
    }
    
    // MARK: Present Dashborad Screen
    func presentDashboardScreen(loginRes :LoginModel) {
        DashboardWireFrame.presentDashboardModule(fromView: self, loginRes: loginRes)
    }
    
    // MARK: Present NGO Buisness Board
    func presentNGOBusinessDashboard(loginRes :LoginModel)
    {
      // NGOBusinessServiceWireFrame.presentNGOBusinessServiceModule(fromView: self,loginRes :loginRes)
        
        NGOLeadershipServiceWireFrame.presentNGOLeadershipServiceModule(fromView: self, loginRes: loginRes)
    }

}
