//
//  SiteDownDetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class SiteDownDetailPresenter:BasePresenter, SiteDownDetailPresenterProtocol, SiteDownDetailInteractorOutputProtocol {
   
    
    

    // MARK: Variables
    weak var view: SiteDownDetailViewProtocol?
    var interactor: SiteDownDetailInteractorInputProtocol?
    var wireFrame: SiteDownDetailWireFrameProtocol?
    let stringsTableName = "SiteDownDetail"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
     func requestData(category:String,id:String,reason:String,filterSelected:[String:Any])
    {
        self.interactor?.requestData(category:category,id:id,reason:reason,filterSelected:filterSelected)
    }
    func reloadData(detail:SiteDetailModel)
    {
        self.view?.reloadData(detail:detail)
    }
    
    func requestSiteDownHistory(sapId:String,id:String)
    {
        self.interactor?.requestSiteDownHistory(sapId:sapId,id:id)
    }
    
    func reloadHistoryData(detail:SiteDownHistoryModel)
    {
        self.view?.reloadHistoryData(detail:detail)
    }
    
}
