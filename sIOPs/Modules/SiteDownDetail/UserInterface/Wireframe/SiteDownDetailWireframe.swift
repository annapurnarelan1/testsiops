//
//  SiteDownDetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class SiteDownDetailWireFrame: SiteDownDetailWireFrameProtocol {
    
    
    
    // MARK: SiteDownDetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentSiteDownDetailModule(fromView:AnyObject,type:String,selected:String,response:DownList,selectedSite:InfraCellImpacted,filterSelected:[String:Any]) {

        // Generating module components
        let view: SiteDownDetailViewProtocol = SiteDownDetailVC.instantiate()
        let presenter: SiteDownDetailPresenterProtocol & SiteDownDetailInteractorOutputProtocol = SiteDownDetailPresenter()
        let interactor: SiteDownDetailInteractorInputProtocol = SiteDownDetailInteractor()
       
        let wireFrame: SiteDownDetailWireFrameProtocol = SiteDownDetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! SiteDownDetailVC
           viewController.type = type
            viewController.selected = selected
            viewController.response =  response
            viewController.cellImpacted = selectedSite
        viewController.selectedFilter = filterSelected
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
