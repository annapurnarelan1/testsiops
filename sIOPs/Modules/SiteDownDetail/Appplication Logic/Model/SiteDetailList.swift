//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SiteDetailList : NSObject, NSCoding{

	var ageing : Double!
	var category : String!
	var count : AnyObject!
	var iconURI : AnyObject!
	var id : AnyObject!
	var impactedCustomers : String!
	var insertDate : String!
	var jobOwner : String!
	var kpiName : AnyObject!
	var kpiValue : AnyObject!
	var kpiDate : String!
	var message : AnyObject!
	var mobileNumber : AnyObject!
	var name : AnyObject!
	var page : Int!
	var priority : Int!
	var query : AnyObject!
	var queryIndex : Int!
	var sapId : String!
	var workOrder : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ageing = dictionary["ageing"] as? Double
		category = dictionary["category"] as? String
		count = dictionary["count"] as? AnyObject
		iconURI = dictionary["iconURI"] as? AnyObject
		id = dictionary["id"] as? AnyObject
		impactedCustomers = dictionary["impactedCustomers"] as? String
		insertDate = dictionary["insertDate"] as? String
		jobOwner = dictionary["jobOwner"] as? String
		kpiName = dictionary["kpiName"] as? AnyObject
		kpiValue = dictionary["kpiValue"] as? AnyObject
		kpiDate = dictionary["kpi_date"] as? String
		message = dictionary["message"] as? AnyObject
		mobileNumber = dictionary["mobileNumber"] as? AnyObject
		name = dictionary["name"] as? AnyObject
		page = dictionary["page"] as? Int
		priority = dictionary["priority"] as? Int
		query = dictionary["query"] as? AnyObject
		queryIndex = dictionary["queryIndex"] as? Int
		sapId = dictionary["sapId"] as? String
		workOrder = dictionary["workOrder"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ageing != nil{
			dictionary["ageing"] = ageing
		}
		if category != nil{
			dictionary["category"] = category
		}
		if count != nil{
			dictionary["count"] = count
		}
		if iconURI != nil{
			dictionary["iconURI"] = iconURI
		}
		if id != nil{
			dictionary["id"] = id
		}
		if impactedCustomers != nil{
			dictionary["impactedCustomers"] = impactedCustomers
		}
		if insertDate != nil{
			dictionary["insertDate"] = insertDate
		}
		if jobOwner != nil{
			dictionary["jobOwner"] = jobOwner
		}
		if kpiName != nil{
			dictionary["kpiName"] = kpiName
		}
		if kpiValue != nil{
			dictionary["kpiValue"] = kpiValue
		}
		if kpiDate != nil{
			dictionary["kpi_date"] = kpiDate
		}
		if message != nil{
			dictionary["message"] = message
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if page != nil{
			dictionary["page"] = page
		}
		if priority != nil{
			dictionary["priority"] = priority
		}
		if query != nil{
			dictionary["query"] = query
		}
		if queryIndex != nil{
			dictionary["queryIndex"] = queryIndex
		}
		if sapId != nil{
			dictionary["sapId"] = sapId
		}
		if workOrder != nil{
			dictionary["workOrder"] = workOrder
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ageing = aDecoder.decodeObject(forKey: "ageing") as? Double
         category = aDecoder.decodeObject(forKey: "category") as? String
         count = aDecoder.decodeObject(forKey: "count") as? AnyObject
         iconURI = aDecoder.decodeObject(forKey: "iconURI") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? AnyObject
         impactedCustomers = aDecoder.decodeObject(forKey: "impactedCustomers") as? String
         insertDate = aDecoder.decodeObject(forKey: "insertDate") as? String
         jobOwner = aDecoder.decodeObject(forKey: "jobOwner") as? String
         kpiName = aDecoder.decodeObject(forKey: "kpiName") as? AnyObject
         kpiValue = aDecoder.decodeObject(forKey: "kpiValue") as? AnyObject
         kpiDate = aDecoder.decodeObject(forKey: "kpi_date") as? String
         message = aDecoder.decodeObject(forKey: "message") as? AnyObject
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? AnyObject
         name = aDecoder.decodeObject(forKey: "name") as? AnyObject
         page = aDecoder.decodeObject(forKey: "page") as? Int
         priority = aDecoder.decodeObject(forKey: "priority") as? Int
         query = aDecoder.decodeObject(forKey: "query") as? AnyObject
         queryIndex = aDecoder.decodeObject(forKey: "queryIndex") as? Int
         sapId = aDecoder.decodeObject(forKey: "sapId") as? String
         workOrder = aDecoder.decodeObject(forKey: "workOrder") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ageing != nil{
			aCoder.encode(ageing, forKey: "ageing")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if iconURI != nil{
			aCoder.encode(iconURI, forKey: "iconURI")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if impactedCustomers != nil{
			aCoder.encode(impactedCustomers, forKey: "impactedCustomers")
		}
		if insertDate != nil{
			aCoder.encode(insertDate, forKey: "insertDate")
		}
		if jobOwner != nil{
			aCoder.encode(jobOwner, forKey: "jobOwner")
		}
		if kpiName != nil{
			aCoder.encode(kpiName, forKey: "kpiName")
		}
		if kpiValue != nil{
			aCoder.encode(kpiValue, forKey: "kpiValue")
		}
		if kpiDate != nil{
			aCoder.encode(kpiDate, forKey: "kpi_date")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if page != nil{
			aCoder.encode(page, forKey: "page")
		}
		if priority != nil{
			aCoder.encode(priority, forKey: "priority")
		}
		if query != nil{
			aCoder.encode(query, forKey: "query")
		}
		if queryIndex != nil{
			aCoder.encode(queryIndex, forKey: "queryIndex")
		}
		if sapId != nil{
			aCoder.encode(sapId, forKey: "sapId")
		}
		if workOrder != nil{
			aCoder.encode(workOrder, forKey: "workOrder")
		}

	}

}
