//
//  SiteDownDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol SiteDownDetailViewProtocol: class {
    var presenter: SiteDownDetailPresenterProtocol? { get set }
    //func SiteDownDetailDone(SiteDownDetailRes :SiteDownDetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadData(detail:SiteDetailModel)
    func reloadHistoryData(detail:SiteDownHistoryModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol SiteDownDetailWireFrameProtocol: class {
    static func presentSiteDownDetailModule(fromView:AnyObject,type:String,selected:String,response:DownList,selectedSite:InfraCellImpacted,filterSelected:[String:Any])

}

/// Method contract between VIEW -> PRESENTER
protocol SiteDownDetailPresenterProtocol: class {
    var view: SiteDownDetailViewProtocol? { get set }
    var interactor: SiteDownDetailInteractorInputProtocol? { get set }
    var wireFrame: SiteDownDetailWireFrameProtocol? { get set }
    
    func requestData(category:String,id:String,reason:String,filterSelected:[String:Any])
    func requestSiteDownHistory(sapId:String,id:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol SiteDownDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:SiteDetailModel)
    func reloadHistoryData(detail:SiteDownHistoryModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol SiteDownDetailInteractorInputProtocol: class
{
    var presenter: SiteDownDetailInteractorOutputProtocol? { get set }
   func requestData(category:String,id:String,reason:String,filterSelected:[String:Any])
    func requestSiteDownHistory(sapId:String,id:String)
    
}
