//
//  OpenChangesWireframe.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenChangesWireFrame: OpenChangesWireFrameProtocol {
    
    
    
    // MARK: NGODetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenChangesModule(fromView:AnyObject,type:String,selected:String,response:OpenChangeModel) {

        // Generating module components
        let view: OpenChangesViewProtocol = OpenChangesVC.instantiate()
        let presenter: OpenChangesPresenterProtocol & OpenChangesInteractorOutputProtocol = OpenChangesPresenter()
        let interactor: OpenChangesInteractorInputProtocol = OpenChangesInteractor()
       
        let wireFrame: OpenChangesWireFrameProtocol = OpenChangesWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OpenChangesVC
        viewController.type = type
        viewController.selected = selected
        viewController.changeModelResponse = response
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
