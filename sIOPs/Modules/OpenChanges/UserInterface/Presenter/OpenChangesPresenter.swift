//
//  OpenChangesPresenter.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OpenChangesPresenter:BasePresenter, OpenChangesPresenterProtocol, OpenChangesInteractorOutputProtocol {
    
    
    
    
    // MARK: Variables
    weak var view: OpenChangesViewProtocol?
    var interactor: OpenChangesInteractorInputProtocol?
    var wireFrame: OpenChangesWireFrameProtocol?
    let stringsTableName = "NGODetail"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func requestData()
    {
        self.interactor?.requestData()
    }
    
    
}
