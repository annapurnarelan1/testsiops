//
//  OpenChangesProtocol.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenChangesViewProtocol: class {
    var presenter: OpenChangesPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenChangesWireFrameProtocol: class {
static func presentOpenChangesModule(fromView:AnyObject,type:String,selected:String,response:OpenChangeModel)
  
}

/// Method contract between VIEW -> PRESENTER
protocol OpenChangesPresenterProtocol: class {
    var view: OpenChangesViewProtocol? { get set }
    var interactor: OpenChangesInteractorInputProtocol? { get set }
    var wireFrame: OpenChangesWireFrameProtocol? { get set }

  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenChangesInteractorOutputProtocol: class {

    func show(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    

}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenChangesInteractorInputProtocol: class
{
    var presenter: OpenChangesInteractorOutputProtocol? { get set }
    func requestData()
    
}
