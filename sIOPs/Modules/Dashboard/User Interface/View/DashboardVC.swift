//
//  DashboardVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit
import SwiftGifOrigin

// MARK: - Constant Strings
enum DashboardStaticStrings:String {
    case DashboardVC = "DashboardVC"
    case resendOTP = "Resend OTP"
    case contentSize = "contentSize"
    case Useful_Links = "Useful Links"
    case Toggle = "Toggle"
    case doubleSideArrow = "doubleSideArrow"
    case faviconForever = "faviconForever"
    case login = "login"
    case DCB = "DCB"
    case Energy = "Energy"
    case Business = "Business"
    case Radio = "Radio"
    case Timesheet = "Timesheet"
    
}

class DashboardVC: BaseViewController, DashboardViewProtocol {
    
    // MARK: - Variables
    var presenter: DashboardPresenterProtocol?
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var loginRes:LoginModel?
    var NGO : NGOViewController?
    var Infra:InfraViewController?
    var currentView:String?
    var applicationArray = [Any]()
    var allowToggle:Bool?
    static var addedToggleView:UIView?
    static var isToggle:Bool?
    var refresher:UIRefreshControl!
    var ngoResponse : NGO?
    
    // MARK: - SubViews
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet private weak var usefullLinksCollectionView: UICollectionView!
    @IBOutlet weak var userLinkheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerVWheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dashContainerView: UIView!
    @IBOutlet  var loadToggleView:UIView?
    @IBOutlet weak var toggleImage:UIImageView?
 
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> DashboardViewProtocol{
        return UIStoryboard(name: DashboardStaticStrings.DashboardVC.rawValue, bundle: nil).instantiateViewController(withIdentifier: DashboardStaticStrings.DashboardVC.rawValue) as! DashboardVC
    }
    
    // MARK: - View's Cycle
    /// Overwritten method from UIVIewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpDashBoardView()
    }
    
    /// Overwritten method from UIVIewController, it calls a method to subscribe to keyboard events
    ///
    /// - Parameter animated: animation flag
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:true)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    
    
    ///Overwritten method from UIVIewController
    override func viewDidLayoutSubviews() {
         super.updateViewConstraints()
        userLinkheightConstraint.constant = self.usefullLinksCollectionView.contentSize.height + 40
        NGO?.view.frame = CGRect(x: 0, y: 0, width: self.dashContainerView.frame.size.width , height: self.dashContainerView.frame.size.height)
        NGO?.view.setNeedsLayout()
        Infra?.view.frame = CGRect(x: 0, y: 0, width: self.dashContainerView.frame.size.width , height: self.dashContainerView.frame.size.height)
        Infra?.view.setNeedsLayout()
        self.containerVWheightConstraint.constant = (currentView == Constants.toggle.INFRA) ? Infra?.heightConstraint.constant ?? 361 : NGO?.heightConstraint.constant ?? 361
        self.view.setNeedsLayout()
        
    }
    
    // MARK: - Initial View Setup
    
    /// Setup initial DashboardView
    func setUpDashBoardView() {
        self.registerSubViewNib()
        self.addPullToRefresh()
        DashboardVC.isToggle = false
        let allowToggleView = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView
        (allowToggleView == Constants.toggle.NGOBuisnessToggle || allowToggleView == Constants.toggle.NGO) ? self.initiateNGO() : self.initiateInfra()
        self.allowToggle = loginRes?.respData[0].respMsg.responsePayload.allowToggle
    }
    
    /// Registered CollectionView Nibs
    func registerSubViewNib()
    {
        usefullLinksCollectionView?.register(UINib(nibName: Constants.collectIonViewCell.UserfullLinksCollectionViewCell, bundle: nil), forCellWithReuseIdentifier:Constants.collectIonViewCell.UserfullLinksCollectionViewCell)
        usefullLinksCollectionView.register(UINib(nibName: Constants.collectIonViewCell.HeaderCollectionReusableView, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:  Constants.collectIonViewCell.HeaderCollectionReusableView)
    }
    
    /**
    Initiated NGO View in container
     
     ## Unordered Lists
     
     - Set currentView to NGO
     - Set ToggleView As NGO
     - Initiate NGOViewController
     - Added NGO collectionView Observer
     - Get UsefullLinks array for NGO from login Response
    */
    func initiateNGO() {
        currentView = Constants.toggle.NGO
        Constants.toggle.toggleView = Constants.toggle.NGO
        NGO = NGOWireFrame.presentNGOModule(fromView: self as AnyObject,baseVC:self) as? NGOViewController
        self.dashContainerView.addSubview(NGO?.view ?? UIView())
        NGO?.NGOTableView.addObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue, options: NSKeyValueObservingOptions.old, context: nil)
        usefullLinkArrayForNGO()
    }
    
    /**
    Initiated INFRA View in container
     
     ## Unordered Lists
     
     - Set currentView to INFRA
     - Set ToggleView As INFRA
     - Initiate InfraViewController
     - Added INFRA collectionView Observer
     - Get UsefullLinks array for INFRA from login Response
    */
    func initiateInfra() {
        currentView = Constants.toggle.INFRA
        Constants.toggle.toggleView = Constants.toggle.INFRA
        Infra = InfraWireFrame.presentInfraModule(fromView: self as AnyObject,baseVC:self) as? InfraViewController
        self.dashContainerView.addSubview(Infra?.view ?? UIView())
        Infra?.InfraCollectionView.addObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue, options: NSKeyValueObservingOptions.old, context: nil)
        usefullLinkArrayForInfra()
    }
    
    /// Added Pull to refresh to collectionView
    func addPullToRefresh() {
        self.refresher = UIRefreshControl()
        self.scrollVw.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.init(red: 44, green: 88, blue: 156)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.scrollVw.addSubview(refresher)
    }
    
    /// stopRefresher to stop pull to refresh indicator
    func stopRefresher() {
        refresher.endRefreshing()
    }
    
     /// Called when Pull to refresh and request data according to currentView
    @objc func loadData() {
        stopRefresher()
        currentView == Constants.toggle.INFRA ? Infra?.requestData() : NGO?.requestData()
    }
    
    /**
    Find Usefull links of NGO from Login Response
     
     ## Unordered Lists
     
     - Removed all items from applicationArray
     - Filter login application array on the basis NGO and Both
     - Removed NGO and INFRA from application array
    */
    func usefullLinkArrayForNGO() {
        applicationArray.removeAll()
        let arr = loginRes?.respData[0].respMsg.responsePayload.applications
        let ngoArray = arr?.filter { ($0).applicability == Constants.toggle.NGO } ?? []
        let bothAppArray = arr?.filter { ($0).applicability == Constants.toggle.BOTH } ?? []
        applicationArray.append(contentsOf: ngoArray)
        applicationArray.append(contentsOf: bothAppArray)
        applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != Constants.ApplicationCode.F_NGO_NextGenOPS }
        applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != Constants.ApplicationCode.F_RAN }
        
    }
    
    /**
    Find Usefull links of INFRA from Login Response
     
     ## Unordered Lists
     
     - Removed all items from applicationArray
     - Filter login application array on the basis INFRA and Both
     - Removed NGO and INFRA from application array
    */
    func usefullLinkArrayForInfra() {
        applicationArray.removeAll()
        let arr = loginRes?.respData[0].respMsg.responsePayload.applications
        let infraArray = arr?.filter { ($0).applicability == Constants.toggle.INFRA } ?? []
        let bothAppArray = arr?.filter { ($0).applicability == Constants.toggle.BOTH } ?? []
        applicationArray.append(contentsOf: infraArray)
        applicationArray.append(contentsOf: bothAppArray)
        applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != Constants.ApplicationCode.F_NGO_NextGenOPS }
        applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != Constants.ApplicationCode.F_RAN }
    }
    // MARK:- Error Message Handler

    /**
    StopLoader called when update error is thrown
    */
    func stopLoader() {
        stopAnimating()
    }
    /**
    showFailError called when error thrown by server
     - Parameter error: error thrown by server
    */
    
    func showFailError(error:String) {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    /**
    show called when error shown in popup
     - Parameter image: image icon in popup
     - Parameter error: error Title to be shown
     - Parameter description: description Of error
    */
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    
    /**
    show called when error shown in popup
     - Parameter error: error Title to be shown
     - Parameter description: description Of error
    */
    func showError(error: String,description:String) {
        stopAnimating()
    }
    
    // MARK: - CollectionView Observer Setup
    func infraUsefullLinksObservableSetup(){
        self.loadToggleView?.removeFromSuperview()
        if  NGO?.NGOTableView.observationInfo != nil {
            NGO?.NGOTableView.removeObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue)
        }
        self.dashContainerView.willRemoveSubview(NGO?.view ?? UIView())
        self.dashContainerView.addSubview(Infra?.view ?? UIView())
        Infra?.InfraCollectionView.addObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue, options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    func ngoUsefullLinksObservableSetup(){
        self.loadToggleView?.removeFromSuperview()
        if  Infra?.InfraCollectionView.observationInfo != nil {
            Infra?.InfraCollectionView.removeObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue)
        }
        self.dashContainerView.willRemoveSubview(Infra?.view ?? UIView())
        self.dashContainerView.addSubview(NGO?.view ?? UIView())
        NGO?.NGOTableView.addObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue, options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    func usefullLinksObservableSetup(){
        if  usefullLinksCollectionView.observationInfo != nil {
            usefullLinksCollectionView.removeObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue)
        }
        userLinkheightConstraint.constant = self.usefullLinksCollectionView.contentSize.height + 40
        usefullLinksCollectionView.setNeedsLayout()
    }
    
    func infraObservableSetup(){
        Infra?.view.frame = CGRect(x: 0, y: 0, width: self.dashContainerView.frame.size.width , height: self.containerVWheightConstraint.constant + 10)
        self.containerVWheightConstraint.constant = Infra?.heightConstraint.constant ?? 361
        Infra?.view.setNeedsLayout()
        self.dashContainerView.setNeedsLayout()
        self.view.setNeedsLayout()
    }
    func ngoObservableSetup(){
        
        NGO?.view.frame = CGRect(x: 0, y: 0, width: self.dashContainerView.frame.size.width , height: self.containerVWheightConstraint.constant)
        self.containerVWheightConstraint.constant = NGO?.heightConstraint.constant ?? 361
        NGO?.view.setNeedsLayout()
        self.dashContainerView.setNeedsLayout()
        self.view.setNeedsLayout()
    }
    
    // MARK: - Initiate Toggle Loader Screen
    func toggleLoaderScreen(image:UIImage)
    {
        DashboardVC.isToggle = true
        let window =  UIWindow(frame: UIScreen.main.bounds)
        loadToggleView?.frame = CGRect(x:window.bounds.origin.x, y: window.bounds.origin.y, width: window.bounds.width, height:  window.bounds.height)
        loadToggleView?.tag = 120
        toggleImage?.image = image
        DashboardVC.addedToggleView = loadToggleView
        UIApplication.shared.windows.first?.rootViewController!.view.addSubview(DashboardVC.addedToggleView ?? UIView())
    }
    
    // MARK: - Usefull Application selection
    func selectApplication(item:Int)
    {
        if((self.applicationArray[item] as? LoginApplication)?.applicationName.contains(DashboardStaticStrings.DCB.rawValue) ?? false)
        {
            self.presenter?.presentDCBModule(applicationRes: self.applicationArray[item] as! LoginApplication)
        }
        else if((self.applicationArray[item] as? LoginApplication)?.applicationName.contains(DashboardStaticStrings.Energy.rawValue) ?? false)
        {
             self.presenter?.presentViewControllerModule(applicationRes: self.applicationArray[item] as! LoginApplication)
            
           // self.presenter?.presentEnergyModule(applicationRes: self.applicationArray[item] as! LoginApplication)
        }
        else if((self.applicationArray[item] as? LoginApplication)?.applicationName.contains(DashboardStaticStrings.Business.rawValue) ?? false)
        {
            self.presenter?.presentBuisnessBoard(loginRes:loginRes!)
        }
        else if((self.applicationArray[item] as? LoginApplication)?.applicationName.contains(DashboardStaticStrings.Radio.rawValue) ?? false)
        {
            self.presenter?.presentRadioNetworkModule(applicationRes: self.applicationArray[item] as! LoginApplication)
        }
        else if((self.applicationArray[item] as? LoginApplication)?.applicationName.contains(DashboardStaticStrings.Timesheet.rawValue) ?? false)
               {
                guard self.ngoResponse != nil
                    else {
                    return
                }
                   self.presenter?.ngoDeliquency(type: "Delinquency", selected: "Delinquent", applicationRes: self.applicationArray[item] as! LoginApplication, ngoResponse: self.ngoResponse!)
               }
    }
    
    
    // MARK: - CollectionView Observer Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let observedObject = object as? UICollectionView, observedObject == usefullLinksCollectionView {
            currentView == Constants.toggle.INFRA ? self.infraUsefullLinksObservableSetup() : self.ngoUsefullLinksObservableSetup()
            let point = CGPoint(x: 0, y: 0)
            scrollVw.contentOffset = point
            self.usefullLinksObservableSetup()
            
        }
        else
        {
            if let observedObject = object as? UICollectionView
            {
                observedObject == Infra?.InfraCollectionView ? self.infraObservableSetup() : self.ngoObservableSetup()
            }
            else if let observedObject = object as? UITableView
            {
                observedObject == NGO?.NGOTableView ?self.ngoObservableSetup() : self.infraObservableSetup()
            }
        }
    }
}

// MARK: - CollectionView Datasource Methods
extension DashboardVC :UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constants.collectIonViewCell.HeaderCollectionReusableView, for: indexPath) as? HeaderCollectionReusableView
        headerView?.headerTitleLbl.layoutIfNeeded()
        headerView?.headerTitleLbl.text = DashboardStaticStrings.Useful_Links.rawValue
        return headerView ?? UICollectionReusableView()
       }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.collectIonViewCell.UserfullLinksCollectionViewCell, for: indexPath) as! UserfullLinksCollectionViewCell
        
        if(indexPath.item == applicationArray.count)
        {
            cell.titleLabel.text = DashboardStaticStrings.Toggle.rawValue
            cell.backgroundVw.backgroundColor = UIColor.lightOrangeColor
            cell.iconImageVw.image = UIImage(named:DashboardStaticStrings.doubleSideArrow.rawValue)
        }
        else
        {
            cell.titleLabel.text = (self.applicationArray[indexPath.item] as? LoginApplication)?.applicationName ?? ""
            let color2 = (self.applicationArray[indexPath.item] as? LoginApplication)?.colourCodeForApp ?? ""
            cell.backgroundVw.backgroundColor = UIColor.init(hexString : color2)
            let iconImage = (self.applicationArray[indexPath.item] as? LoginApplication)?.applicationIcon ?? ""
            cell.iconImageVw.image = UIImage(named:iconImage)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.allowToggle == true ? (applicationArray.count )  + 1 : applicationArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.allowToggle ?? false)
        {
            if indexPath.item == applicationArray.count
            {
                if currentView == Constants.toggle.NGO
                {
                    Infra = InfraWireFrame.presentInfraModule(fromView: self, baseVC: self) as? InfraViewController
                    self.dashContainerView.addSubview(Infra?.view ?? UIView())
                    self.toggleLoaderScreen(image: UIImage(named: DashboardStaticStrings.login.rawValue)!)
                }
                else
                {
                    NGO = NGOWireFrame.presentNGOModule(fromView: self as AnyObject,baseVC:self) as? NGOViewController
                    self.dashContainerView.addSubview(NGO?.view ?? UIView())
                    self.toggleLoaderScreen(image: UIImage.gif(name: DashboardStaticStrings.faviconForever.rawValue)!)
                }
                let point = CGPoint(x: 0, y: 0)
                scrollVw.contentOffset = point
            }
            else {
                self.selectApplication(item: indexPath.item)
            }
            
        }
        else
        {
             self.selectApplication(item: indexPath.item)
        }
    }
    
}

// MARK: - CollectionView FlowLayout Delegate
extension DashboardVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        itemsPerRow = 3
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 1
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        return CGSize(width: widthPerItem, height: 130)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        if collectionView == usefullLinksCollectionView
        {
            return CGSize(width: collectionView.frame.width, height:50)
        }
        return CGSize()
        
    }
    
}

// MARK: - NGOViewController Delegate
extension DashboardVC:NGODelegate
{
    
    func ngoResponseData(response: NGO) {
           self.ngoResponse = response
       }
    func ngoControllerResponseLoad() {
        currentView = Constants.toggle.NGO
        Constants.toggle.toggleView = Constants.toggle.NGO
        self.loadToggleView?.removeFromSuperview()
        usefullLinksCollectionView.addObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue, options: NSKeyValueObservingOptions.old, context: nil)
        usefullLinkArrayForNGO()
        usefullLinksCollectionView.reloadData()
    }
    func reloadNGOdata()
    {
        self.ngoControllerResponseLoad()
        self.view.setNeedsLayout()
    }
    
    func error()
    {
        self.containerVWheightConstraint.constant =  361
       self.ngoControllerResponseLoad()
    }
    func viewDetailLoadNGO()
       {
           NGO?.view.setNeedsLayout()
           self.containerVWheightConstraint.constant = NGO?.heightConstraint.constant ?? 361
           self.dashContainerView.setNeedsLayout()
           self.view.setNeedsLayout()
       }
}

// MARK: - InfraViewController Delegate
extension DashboardVC:InfraDelegate
{
    func infraControllerResponseLoad() {
        currentView = Constants.toggle.INFRA
        Constants.toggle.toggleView = Constants.toggle.INFRA
        self.loadToggleView?.removeFromSuperview()
        usefullLinksCollectionView.addObserver(self, forKeyPath: DashboardStaticStrings.contentSize.rawValue, options: NSKeyValueObservingOptions.old, context: nil)
        usefullLinkArrayForInfra()
        usefullLinksCollectionView.reloadData()
    }
    func reloadInfradata()
    {
        self.infraControllerResponseLoad()
        self.view.setNeedsLayout()
    }
    
    func errorInfra()
    {
        self.containerVWheightConstraint.constant =  361
        self.infraControllerResponseLoad()
    }
    
    func viewDetailLoad()
    {
        Infra?.view.setNeedsLayout()
        self.containerVWheightConstraint.constant = Infra?.heightConstraint.constant ?? 361
        self.dashContainerView.setNeedsLayout()
        self.view.setNeedsLayout()
    }
}
