//
//  DashboardPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class DashboardPresenter:BasePresenter, DashboardPresenterProtocol, DashboardInteractorOutputProtocol {
 
    // MARK: Variables
    weak var view: DashboardViewProtocol?
    var interactor: DashboardInteractorInputProtocol?
    var wireFrame: DashboardWireFrameProtocol?
    let stringsTableName = "Dashboard"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
    func requestData()
    {
        self.interactor?.requestData()
    }
    
    
    func presentDCBModule(applicationRes:LoginApplication)
    {
        self.wireFrame?.presentDCBModule(applicationRes:applicationRes)
    }
    
    func presentBuisnessBoard(loginRes:LoginModel)
    {
        self.wireFrame?.presentBuisnessBoard(loginRes:loginRes)
    }
    func presentEnergyModule(applicationRes:LoginApplication)
    {
         self.wireFrame?.presentEnergyModule(applicationRes:applicationRes)
    }
    
    func presentViewControllerModule(applicationRes:LoginApplication)
       {
            self.wireFrame?.presentViewControllerModule(applicationRes:applicationRes)
       }
    
    func presentRadioNetworkModule(applicationRes:LoginApplication)
    {
        self.wireFrame?.presentRadioNetworkModule(applicationRes:applicationRes)
    }
    func ngoDeliquency(type:String,selected:String,applicationRes:LoginApplication , ngoResponse: NGO) {
        self.wireFrame?.ngoDeliquency(type: type, selected: selected , applicationRes: applicationRes, ngoResponse: ngoResponse)
     }
}
