//
//  DashboardWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class DashboardWireFrame: DashboardWireFrameProtocol {
    
    
    
    // MARK: DashboardWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentDashboardModule(fromView:AnyObject,loginRes:LoginModel) {
        
        // Generating module components
        let view: DashboardViewProtocol = DashboardVC.instantiate()
        let presenter: DashboardPresenterProtocol & DashboardInteractorOutputProtocol = DashboardPresenter()
        let interactor: DashboardInteractorInputProtocol = DashboardInteractor()
        
        let wireFrame: DashboardWireFrameProtocol = DashboardWireFrame()
        
        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! DashboardVC
        viewController.loginRes = loginRes
        NavigationHelper.setRootViewController(withViewController: viewController);
    }
    
    func presentDCBModule(applicationRes:LoginApplication)
    {
        DCBWireFrame.presentDCBModule(fromView: self, application:applicationRes)
    }
    
    func presentBuisnessBoard(loginRes:LoginModel)
    {
       // NGOBusinessServiceWireFrame.presentNGOBusinessServiceModule(fromView: self, loginRes: loginRes)
        NGOLeadershipServiceWireFrame.presentNGOLeadershipServiceModule(fromView: self, loginRes: loginRes)
    }
    func presentEnergyModule(applicationRes:LoginApplication)
    {
        EnergyWireFrame.presentEnergyModule(fromView: self, application: applicationRes)
    }
    
  func presentViewControllerModule(applicationRes:LoginApplication)
          {
            ViewControllerWireFrame.presentViewControllerModule(fromView: self, application:applicationRes)
          }
    
    
    func presentRadioNetworkModule(applicationRes:LoginApplication)
    {
        RANWireFrame.presentRANModule(fromView: self, application: applicationRes)
    }
    
    func ngoDeliquency(type ttype:String,selected:String,applicationRes:LoginApplication , ngoResponse: NGO)
    {
        DeliquencyWireFrame.presentDeliquencyModule(fromView:self,type:ttype, ngoResponse: ngoResponse,selected:selected )
    }
    
}
