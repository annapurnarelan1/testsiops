//
//  DashboardProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation
/// Method contract between PRESENTER -> VIEW
protocol DashboardViewProtocol: class {
    var presenter: DashboardPresenterProtocol? { get set }
    
    func show(image:String,error: String,description:String)
    
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol DashboardWireFrameProtocol: class {
    static func presentDashboardModule(fromView:AnyObject,loginRes:LoginModel)
    func presentDCBModule(applicationRes:LoginApplication)
    
    func presentBuisnessBoard(loginRes:LoginModel)
    func presentEnergyModule(applicationRes:LoginApplication)
    func presentViewControllerModule(applicationRes:LoginApplication)
    func presentRadioNetworkModule(applicationRes:LoginApplication)
    func ngoDeliquency(type:String,selected:String,applicationRes:LoginApplication , ngoResponse: NGO)
}

/// Method contract between VIEW -> PRESENTER
protocol DashboardPresenterProtocol: class {
    var view: DashboardViewProtocol? { get set }
    var interactor: DashboardInteractorInputProtocol? { get set }
    var wireFrame: DashboardWireFrameProtocol? { get set }
    
    func requestData()
    func presentDCBModule(applicationRes:LoginApplication)
    func presentBuisnessBoard(loginRes:LoginModel)
    func presentEnergyModule(applicationRes:LoginApplication)
    func presentViewControllerModule(applicationRes:LoginApplication)
    func presentRadioNetworkModule(applicationRes:LoginApplication)
    func ngoDeliquency(type:String,selected:String,applicationRes:LoginApplication , ngoResponse : NGO)
    
    
}

/// Method contract between INTERACTOR -> PRESENTER
protocol DashboardInteractorOutputProtocol: class {
    
    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol DashboardInteractorInputProtocol: class
{
    var presenter: DashboardInteractorOutputProtocol? { get set }
    func requestData()
    /**
     * Add here your methods for communication
     */
    
}
