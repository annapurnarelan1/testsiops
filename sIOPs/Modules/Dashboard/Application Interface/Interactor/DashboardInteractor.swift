//
//  DashboardInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class DashboardInteractor: DashboardInteractorInputProtocol {

public enum DashboardError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: DashboardInteractorOutputProtocol?
    
    
    func requestData(){
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
           // "appRoleCode": NGOParams.applicationCode ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        var busicode = ""
        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
        {
            busicode = Constants.BusiCode.NGOSummary
        }
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
//                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
//
//                               switch status {
//                               case Constants.status.OK:
//                                        let NGOdata = NGO.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//                                                      self.presenter?.reloadData(NGOResponse:NGOdata)
//                                 case Constants.status.NOK:
//                                        let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
//                                       self.presenter?.showFailError(error:message)
//                               default:
//                                   self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                               }
                
//                let loginRes: LoginModel = LoginModel.sharedInstance
//                
//                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
//                LoginModel.sharedInstance.saveUser()
                
               // self.presenter?.loginDone(loginRes:loginRes)
                
               
           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
}
