//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenChangeModel : NSObject, NSCoding{

	var emergency : Int!
	var emergencyList : [EmergencyList]!
	var macD : Int!
	var macDList : [MacDList]!
	var normal : Int!
	var normalList : [EmergencyList]!
	var pendingClosure : Int!
	var pendingClosureList : [MacDList]!
	var pendingImplementaion : Int!
	var pendingImplementaionList : [MacDList]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		emergency = dictionary["emergency"] as? Int
		emergencyList = [EmergencyList]()
		if let emergencyListArray = dictionary["emergencyList"] as? [[String:Any]]{
			for dic in emergencyListArray{
				let value = EmergencyList(fromDictionary: dic)
				emergencyList.append(value)
			}
		}
		macD = dictionary["macD"] as? Int
		macDList = [MacDList]()
		if let macDListArray = dictionary["macDList"] as? [[String:Any]]{
			for dic in macDListArray{
				let value = MacDList(fromDictionary: dic)
				macDList.append(value)
			}
		}
		normal = dictionary["normal"] as? Int
		normalList = [EmergencyList]()
		if let normalListArray = dictionary["normalList"] as? [[String:Any]]{
			for dic in normalListArray{
				let value = EmergencyList(fromDictionary: dic)
				normalList.append(value)
			}
		}
		pendingClosure = dictionary["pendingClosure"] as? Int
		pendingClosureList = [MacDList]()
		if let pendingClosureListArray = dictionary["pendingClosureList"] as? [[String:Any]]{
			for dic in pendingClosureListArray{
				let value = MacDList(fromDictionary: dic)
				pendingClosureList.append(value)
			}
		}
		pendingImplementaion = dictionary["pendingImplementaion"] as? Int
		pendingImplementaionList = [MacDList]()
		if let pendingImplementaionListArray = dictionary["pendingImplementaionList"] as? [[String:Any]]{
			for dic in pendingImplementaionListArray{
				let value = MacDList(fromDictionary: dic)
				pendingImplementaionList.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if emergency != nil{
			dictionary["emergency"] = emergency
		}
		if emergencyList != nil{
			var dictionaryElements = [[String:Any]]()
			for emergencyListElement in emergencyList {
				dictionaryElements.append(emergencyListElement.toDictionary())
			}
			dictionary["emergencyList"] = dictionaryElements
		}
		if macD != nil{
			dictionary["macD"] = macD
		}
		if macDList != nil{
			var dictionaryElements = [[String:Any]]()
			for macDListElement in macDList {
				dictionaryElements.append(macDListElement.toDictionary())
			}
			dictionary["macDList"] = dictionaryElements
		}
		if normal != nil{
			dictionary["normal"] = normal
		}
		if normalList != nil{
			var dictionaryElements = [[String:Any]]()
			for normalListElement in normalList {
				dictionaryElements.append(normalListElement.toDictionary())
			}
			dictionary["normalList"] = dictionaryElements
		}
		if pendingClosure != nil{
			dictionary["pendingClosure"] = pendingClosure
		}
		if pendingClosureList != nil{
			var dictionaryElements = [[String:Any]]()
			for pendingClosureListElement in pendingClosureList {
				dictionaryElements.append(pendingClosureListElement.toDictionary())
			}
			dictionary["pendingClosureList"] = dictionaryElements
		}
		if pendingImplementaion != nil{
			dictionary["pendingImplementaion"] = pendingImplementaion
		}
		if pendingImplementaionList != nil{
			var dictionaryElements = [[String:Any]]()
			for pendingImplementaionListElement in pendingImplementaionList {
				dictionaryElements.append(pendingImplementaionListElement.toDictionary())
			}
			dictionary["pendingImplementaionList"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         emergency = aDecoder.decodeObject(forKey: "emergency") as? Int
         emergencyList = aDecoder.decodeObject(forKey :"emergencyList") as? [EmergencyList]
         macD = aDecoder.decodeObject(forKey: "macD") as? Int
         macDList = aDecoder.decodeObject(forKey :"macDList") as? [MacDList]
         normal = aDecoder.decodeObject(forKey: "normal") as? Int
         normalList = aDecoder.decodeObject(forKey :"normalList") as? [EmergencyList]
         pendingClosure = aDecoder.decodeObject(forKey: "pendingClosure") as? Int
         pendingClosureList = aDecoder.decodeObject(forKey :"pendingClosureList") as? [MacDList]
         pendingImplementaion = aDecoder.decodeObject(forKey: "pendingImplementaion") as? Int
         pendingImplementaionList = aDecoder.decodeObject(forKey :"pendingImplementaionList") as? [MacDList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if emergency != nil{
			aCoder.encode(emergency, forKey: "emergency")
		}
		if emergencyList != nil{
			aCoder.encode(emergencyList, forKey: "emergencyList")
		}
		if macD != nil{
			aCoder.encode(macD, forKey: "macD")
		}
		if macDList != nil{
			aCoder.encode(macDList, forKey: "macDList")
		}
		if normal != nil{
			aCoder.encode(normal, forKey: "normal")
		}
		if normalList != nil{
			aCoder.encode(normalList, forKey: "normalList")
		}
		if pendingClosure != nil{
			aCoder.encode(pendingClosure, forKey: "pendingClosure")
		}
		if pendingClosureList != nil{
			aCoder.encode(pendingClosureList, forKey: "pendingClosureList")
		}
		if pendingImplementaion != nil{
			aCoder.encode(pendingImplementaion, forKey: "pendingImplementaion")
		}
		if pendingImplementaionList != nil{
			aCoder.encode(pendingImplementaionList, forKey: "pendingImplementaionList")
		}

	}

}
