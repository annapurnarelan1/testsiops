//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenIncidentModel : NSObject, NSCoding{

	var busiOI : [BusiOI]!
	var busiS1 : [NonBusiS2]!
	var busiS2 : [NonBusiS2]!
	var businessImpactingCount : Int!
	var nonBusiOI : [BusiOI]!
	var nonBusiS1 : [NonBusiS2]!
	var nonBusiS2 : [NonBusiS2]!
	var nonBusiS3 : [NonBusiS3]!
	var nonBusiS4 : [NonBusiS3]!
	var nonBusinessImpactingCount : Int!
    var s1Count : Int!
    var s2Count : Int!
    var s3Count : Int!
    var s4Count : Int!



	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		busiOI = [BusiOI]()
		if let busiOIArray = dictionary["busiOI"] as? [[String:Any]]{
			for dic in busiOIArray{
				let value = BusiOI(fromDictionary: dic)
				busiOI.append(value)
			}
		}
        
        
        busiS1 = [NonBusiS2]()
               if let nonBusiS1Array = dictionary["busiS1"] as? [[String:Any]]{
                   for dic in nonBusiS1Array{
                       let value = NonBusiS2(fromDictionary: dic)
                       busiS1.append(value)
                   }
               }
        busiS2 = [NonBusiS2]()
                      if let nonBusiS1Array = dictionary["busiS2"] as? [[String:Any]]{
                          for dic in nonBusiS1Array{
                              let value = NonBusiS2(fromDictionary: dic)
                              busiS2.append(value)
                          }
                      }
		businessImpactingCount = dictionary["businessImpactingCount"] as? Int
		nonBusiOI = [BusiOI]()
		if let nonBusiOIArray = dictionary["nonBusiOI"] as? [[String:Any]]{
			for dic in nonBusiOIArray{
				let value = BusiOI(fromDictionary: dic)
				nonBusiOI.append(value)
			}
		}
        nonBusiS1 = [NonBusiS2]()
        if let nonBusiS1Array = dictionary["nonBusiS1"] as? [[String:Any]]{
            for dic in nonBusiS1Array{
                let value = NonBusiS2(fromDictionary: dic)
                nonBusiS1.append(value)
            }
        }
		nonBusiS2 = [NonBusiS2]()
		if let nonBusiS2Array = dictionary["nonBusiS2"] as? [[String:Any]]{
			for dic in nonBusiS2Array{
				let value = NonBusiS2(fromDictionary: dic)
				nonBusiS2.append(value)
			}
		}
		nonBusiS3 = [NonBusiS3]()
		if let nonBusiS3Array = dictionary["nonBusiS3"] as? [[String:Any]]{
			for dic in nonBusiS3Array{
				let value = NonBusiS3(fromDictionary: dic)
				nonBusiS3.append(value)
			}
		}
		nonBusiS4 = [NonBusiS3]()
		if let nonBusiS4Array = dictionary["nonBusiS4"] as? [[String:Any]]{
			for dic in nonBusiS4Array{
				let value = NonBusiS3(fromDictionary: dic)
				nonBusiS4.append(value)
			}
		}
		nonBusinessImpactingCount = dictionary["nonBusinessImpactingCount"] as? Int
        
        s1Count = dictionary["s1Count"] as? Int
        s2Count = dictionary["s2Count"] as? Int
        s3Count = dictionary["s3Count"] as? Int
        s4Count = dictionary["s4Count"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if busiOI != nil{
			var dictionaryElements = [[String:Any]]()
			for busiOIElement in busiOI {
				dictionaryElements.append(busiOIElement.toDictionary())
			}
			dictionary["busiOI"] = dictionaryElements
		}
		if busiS1 != nil{
			dictionary["busiS1"] = busiS1
		}
		if busiS2 != nil{
			dictionary["busiS2"] = busiS2
		}
		if businessImpactingCount != nil{
			dictionary["businessImpactingCount"] = businessImpactingCount
		}
        if s1Count != nil{
            dictionary["s1Count"] = s1Count
        }
        if s2Count != nil{
            dictionary["s2Count"] = s2Count
        }
        if s3Count != nil{
            dictionary["s3Count"] = s3Count
        }
        if s4Count != nil{
            dictionary["s4Count"] = s4Count
        }
		if nonBusiOI != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiOIElement in nonBusiOI {
				dictionaryElements.append(nonBusiOIElement.toDictionary())
			}
			dictionary["nonBusiOI"] = dictionaryElements
		}
		
        if nonBusiS1 != nil{
            var dictionaryElements = [[String:Any]]()
            for nonBusiS1Element in nonBusiS1 {
                dictionaryElements.append(nonBusiS1Element.toDictionary())
            }
            dictionary["nonBusiS1"] = dictionaryElements
        }
		if nonBusiS2 != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiS2Element in nonBusiS2 {
				dictionaryElements.append(nonBusiS2Element.toDictionary())
			}
			dictionary["nonBusiS2"] = dictionaryElements
		}
		if nonBusiS3 != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiS3Element in nonBusiS3 {
				dictionaryElements.append(nonBusiS3Element.toDictionary())
			}
			dictionary["nonBusiS3"] = dictionaryElements
		}
		if nonBusiS4 != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiS4Element in nonBusiS4 {
				dictionaryElements.append(nonBusiS4Element.toDictionary())
			}
			dictionary["nonBusiS4"] = dictionaryElements
		}
		if nonBusinessImpactingCount != nil{
			dictionary["nonBusinessImpactingCount"] = nonBusinessImpactingCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         busiOI = aDecoder.decodeObject(forKey :"busiOI") as? [BusiOI]
         busiS1 = aDecoder.decodeObject(forKey: "busiS1") as? [NonBusiS2]
         busiS2 = aDecoder.decodeObject(forKey: "busiS2") as? [NonBusiS2]
         businessImpactingCount = aDecoder.decodeObject(forKey: "businessImpactingCount") as? Int
         nonBusiOI = aDecoder.decodeObject(forKey :"nonBusiOI") as? [BusiOI]
         nonBusiS1 = aDecoder.decodeObject(forKey: "nonBusiS1") as? [NonBusiS2]
         nonBusiS2 = aDecoder.decodeObject(forKey :"nonBusiS2") as? [NonBusiS2]
         nonBusiS3 = aDecoder.decodeObject(forKey :"nonBusiS3") as? [NonBusiS3]
         nonBusiS4 = aDecoder.decodeObject(forKey :"nonBusiS4") as? [NonBusiS3]
         nonBusinessImpactingCount = aDecoder.decodeObject(forKey: "nonBusinessImpactingCount") as? Int
        s1Count = aDecoder.decodeObject(forKey: "s1Count") as? Int
        s2Count = aDecoder.decodeObject(forKey: "s2Count") as? Int
        s3Count = aDecoder.decodeObject(forKey: "s3Count") as? Int
        s4Count = aDecoder.decodeObject(forKey: "s4Count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if busiOI != nil{
			aCoder.encode(busiOI, forKey: "busiOI")
		}
		if busiS1 != nil{
			aCoder.encode(busiS1, forKey: "busiS1")
		}
		if busiS2 != nil{
			aCoder.encode(busiS2, forKey: "busiS2")
		}
		if businessImpactingCount != nil{
			aCoder.encode(businessImpactingCount, forKey: "businessImpactingCount")
		}
		if nonBusiOI != nil{
			aCoder.encode(nonBusiOI, forKey: "nonBusiOI")
		}
		if nonBusiS1 != nil{
			aCoder.encode(nonBusiS1, forKey: "nonBusiS1")
		}
		if nonBusiS2 != nil{
			aCoder.encode(nonBusiS2, forKey: "nonBusiS2")
		}
		if nonBusiS3 != nil{
			aCoder.encode(nonBusiS3, forKey: "nonBusiS3")
		}
		if nonBusiS4 != nil{
			aCoder.encode(nonBusiS4, forKey: "nonBusiS4")
		}
		if s1Count != nil{
			aCoder.encode(s1Count, forKey: "s1Count")
		}
        if s2Count != nil{
            aCoder.encode(s2Count, forKey: "s2Count")
        }
        if s3Count != nil{
            aCoder.encode(s3Count, forKey: "s3Count")
        }
        if s4Count != nil{
            aCoder.encode(s4Count, forKey: "s4Count")
        }
        if nonBusinessImpactingCount != nil{
            aCoder.encode(nonBusinessImpactingCount, forKey: "nonBusinessImpactingCount")
        }

	}

}
