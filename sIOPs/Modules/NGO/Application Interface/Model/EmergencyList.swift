//
//	EmergencyList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EmergencyList : NSObject, NSCoding{

	var actualENDDATE : String!
	var actualSTARTDATE : String!
	var applicationimpacted : String!
	var changeTYPES : String!
	var changecategory : String!
	var changeexecutorgroup : String!
	var changeid : String!
	var crstatus : String!
	var crtitle : String!
	var downtimeend : String!
	var downtimestart : String!
	var reasonforchange : String!
	var requestedapplication : String!
	var testedenvironment : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		actualENDDATE = dictionary["actual_ENDDATE"] as? String
		actualSTARTDATE = dictionary["actual_STARTDATE"] as? String
		applicationimpacted = dictionary["applicationimpacted"] as? String
		changeTYPES = dictionary["change_TYPES"] as? String
		changecategory = dictionary["changecategory"] as? String
		changeexecutorgroup = dictionary["changeexecutorgroup"] as? String
		changeid = dictionary["changeid"] as? String
		crstatus = dictionary["crstatus"] as? String
		crtitle = dictionary["crtitle"] as? String
		downtimeend = dictionary["downtimeend"] as? String
		downtimestart = dictionary["downtimestart"] as? String
		reasonforchange = dictionary["reasonforchange"] as? String
		requestedapplication = dictionary["requestedapplication"] as? String
		testedenvironment = dictionary["testedenvironment"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if actualENDDATE != nil{
			dictionary["actual_ENDDATE"] = actualENDDATE
		}
		if actualSTARTDATE != nil{
			dictionary["actual_STARTDATE"] = actualSTARTDATE
		}
		if applicationimpacted != nil{
			dictionary["applicationimpacted"] = applicationimpacted
		}
		if changeTYPES != nil{
			dictionary["change_TYPES"] = changeTYPES
		}
		if changecategory != nil{
			dictionary["changecategory"] = changecategory
		}
		if changeexecutorgroup != nil{
			dictionary["changeexecutorgroup"] = changeexecutorgroup
		}
		if changeid != nil{
			dictionary["changeid"] = changeid
		}
		if crstatus != nil{
			dictionary["crstatus"] = crstatus
		}
		if crtitle != nil{
			dictionary["crtitle"] = crtitle
		}
		if downtimeend != nil{
			dictionary["downtimeend"] = downtimeend
		}
		if downtimestart != nil{
			dictionary["downtimestart"] = downtimestart
		}
		if reasonforchange != nil{
			dictionary["reasonforchange"] = reasonforchange
		}
		if requestedapplication != nil{
			dictionary["requestedapplication"] = requestedapplication
		}
		if testedenvironment != nil{
			dictionary["testedenvironment"] = testedenvironment
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         actualENDDATE = aDecoder.decodeObject(forKey: "actual_ENDDATE") as? String
         actualSTARTDATE = aDecoder.decodeObject(forKey: "actual_STARTDATE") as? String
         applicationimpacted = aDecoder.decodeObject(forKey: "applicationimpacted") as? String
         changeTYPES = aDecoder.decodeObject(forKey: "change_TYPES") as? String
         changecategory = aDecoder.decodeObject(forKey: "changecategory") as? String
         changeexecutorgroup = aDecoder.decodeObject(forKey: "changeexecutorgroup") as? String
         changeid = aDecoder.decodeObject(forKey: "changeid") as? String
         crstatus = aDecoder.decodeObject(forKey: "crstatus") as? String
         crtitle = aDecoder.decodeObject(forKey: "crtitle") as? String
         downtimeend = aDecoder.decodeObject(forKey: "downtimeend") as? String
         downtimestart = aDecoder.decodeObject(forKey: "downtimestart") as? String
         reasonforchange = aDecoder.decodeObject(forKey: "reasonforchange") as? String
         requestedapplication = aDecoder.decodeObject(forKey: "requestedapplication") as? String
         testedenvironment = aDecoder.decodeObject(forKey: "testedenvironment") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if actualENDDATE != nil{
			aCoder.encode(actualENDDATE, forKey: "actual_ENDDATE")
		}
		if actualSTARTDATE != nil{
			aCoder.encode(actualSTARTDATE, forKey: "actual_STARTDATE")
		}
		if applicationimpacted != nil{
			aCoder.encode(applicationimpacted, forKey: "applicationimpacted")
		}
		if changeTYPES != nil{
			aCoder.encode(changeTYPES, forKey: "change_TYPES")
		}
		if changecategory != nil{
			aCoder.encode(changecategory, forKey: "changecategory")
		}
		if changeexecutorgroup != nil{
			aCoder.encode(changeexecutorgroup, forKey: "changeexecutorgroup")
		}
		if changeid != nil{
			aCoder.encode(changeid, forKey: "changeid")
		}
		if crstatus != nil{
			aCoder.encode(crstatus, forKey: "crstatus")
		}
		if crtitle != nil{
			aCoder.encode(crtitle, forKey: "crtitle")
		}
		if downtimeend != nil{
			aCoder.encode(downtimeend, forKey: "downtimeend")
		}
		if downtimestart != nil{
			aCoder.encode(downtimestart, forKey: "downtimestart")
		}
		if reasonforchange != nil{
			aCoder.encode(reasonforchange, forKey: "reasonforchange")
		}
		if requestedapplication != nil{
			aCoder.encode(requestedapplication, forKey: "requestedapplication")
		}
		if testedenvironment != nil{
			aCoder.encode(testedenvironment, forKey: "testedenvironment")
		}

	}

}