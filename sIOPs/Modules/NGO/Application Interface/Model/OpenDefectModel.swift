//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenDefectModel : NSObject, NSCoding{

	var defect : Int!
	var defectList : [DefectList]!
	var hotFix : Int!
	var hotFixList : [DefectList]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		defect = dictionary["defect"] as? Int
		defectList = [DefectList]()
		if let defectListArray = dictionary["defectList"] as? [[String:Any]]{
			for dic in defectListArray{
				let value = DefectList(fromDictionary: dic)
				defectList.append(value)
			}
		}
		hotFix = dictionary["hotFix"] as? Int
		hotFixList = [DefectList]()
		if let hotFixListArray = dictionary["hotFixList"] as? [[String:Any]]{
			for dic in hotFixListArray{
				let value = DefectList(fromDictionary: dic)
				hotFixList.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if defect != nil{
			dictionary["defect"] = defect
		}
		if defectList != nil{
			var dictionaryElements = [[String:Any]]()
			for defectListElement in defectList {
				dictionaryElements.append(defectListElement.toDictionary())
			}
			dictionary["defectList"] = dictionaryElements
		}
		if hotFix != nil{
			dictionary["hotFix"] = hotFix
		}
		if hotFixList != nil{
			var dictionaryElements = [[String:Any]]()
			for hotFixListElement in hotFixList {
				dictionaryElements.append(hotFixListElement.toDictionary())
			}
			dictionary["hotFixList"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         defect = aDecoder.decodeObject(forKey: "defect") as? Int
         defectList = aDecoder.decodeObject(forKey :"defectList") as? [DefectList]
         hotFix = aDecoder.decodeObject(forKey: "hotFix") as? Int
         hotFixList = aDecoder.decodeObject(forKey :"hotFixList") as? [DefectList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if defect != nil{
			aCoder.encode(defect, forKey: "defect")
		}
		if defectList != nil{
			aCoder.encode(defectList, forKey: "defectList")
		}
		if hotFix != nil{
			aCoder.encode(hotFix, forKey: "hotFix")
		}
		if hotFixList != nil{
			aCoder.encode(hotFixList, forKey: "hotFixList")
		}

	}

}
