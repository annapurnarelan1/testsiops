//
//	MainOutlier.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOMainOutlier : NSObject, NSCoding{

	var ackByMobile : AnyObject!
	var ackBY : AnyObject!
	var ackON : AnyObject!
	var ackStatus : AnyObject!
	var agingSinceInsert : AnyObject!
	var appName : AnyObject!
	var appOwner : AnyObject!
	var classType : String!
	var cname : AnyObject!
	var count : Int!
	var createTime : AnyObject!
	var delayInAcknowledge : AnyObject!
	var delayInCreation : AnyObject!
	var eventKPI : AnyObject!
	var eventMessage : AnyObject!
	var eventStatus : AnyObject!
	var eventTime : AnyObject!
	var hostName : AnyObject!
	var ipAddress : AnyObject!
	var openDuration : AnyObject!
	var outlierName : AnyObject!
	var platformOwner : AnyObject!
	var remaingTimeToAck : AnyObject!
	var state : AnyObject!
	var subType : AnyObject!
	var type : String!
	var unAckCount : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ackByMobile = dictionary["ackByMobile"] as? AnyObject
		ackBY = dictionary["ack_BY"] as? AnyObject
		ackON = dictionary["ack_ON"] as? AnyObject
		ackStatus = dictionary["ack_status"] as? AnyObject
		agingSinceInsert = dictionary["aging_since_insert"] as? AnyObject
		appName = dictionary["appName"] as? AnyObject
		appOwner = dictionary["appOwner"] as? AnyObject
		classType = dictionary["classType"] as? String
		cname = dictionary["cname"] as? AnyObject
		count = dictionary["count"] as? Int
		createTime = dictionary["createTime"] as? AnyObject
		delayInAcknowledge = dictionary["delayInAcknowledge"] as? AnyObject
		delayInCreation = dictionary["delayInCreation"] as? AnyObject
		eventKPI = dictionary["eventKPI"] as? AnyObject
		eventMessage = dictionary["eventMessage"] as? AnyObject
		eventStatus = dictionary["eventStatus"] as? AnyObject
		eventTime = dictionary["eventTime"] as? AnyObject
		hostName = dictionary["hostName"] as? AnyObject
		ipAddress = dictionary["ipAddress"] as? AnyObject
		openDuration = dictionary["openDuration"] as? AnyObject
		outlierName = dictionary["outlierName"] as? AnyObject
		platformOwner = dictionary["platformOwner"] as? AnyObject
		remaingTimeToAck = dictionary["remaing_time_to_ack"] as? AnyObject
		state = dictionary["state"] as? AnyObject
		subType = dictionary["subType"] as? AnyObject
		type = dictionary["type"] as? String
		unAckCount = dictionary["unAckCount"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ackByMobile != nil{
			dictionary["ackByMobile"] = ackByMobile
		}
		if ackBY != nil{
			dictionary["ack_BY"] = ackBY
		}
		if ackON != nil{
			dictionary["ack_ON"] = ackON
		}
		if ackStatus != nil{
			dictionary["ack_status"] = ackStatus
		}
		if agingSinceInsert != nil{
			dictionary["aging_since_insert"] = agingSinceInsert
		}
		if appName != nil{
			dictionary["appName"] = appName
		}
		if appOwner != nil{
			dictionary["appOwner"] = appOwner
		}
		if classType != nil{
			dictionary["classType"] = classType
		}
		if cname != nil{
			dictionary["cname"] = cname
		}
		if count != nil{
			dictionary["count"] = count
		}
		if createTime != nil{
			dictionary["createTime"] = createTime
		}
		if delayInAcknowledge != nil{
			dictionary["delayInAcknowledge"] = delayInAcknowledge
		}
		if delayInCreation != nil{
			dictionary["delayInCreation"] = delayInCreation
		}
		if eventKPI != nil{
			dictionary["eventKPI"] = eventKPI
		}
		if eventMessage != nil{
			dictionary["eventMessage"] = eventMessage
		}
		if eventStatus != nil{
			dictionary["eventStatus"] = eventStatus
		}
		if eventTime != nil{
			dictionary["eventTime"] = eventTime
		}
		if hostName != nil{
			dictionary["hostName"] = hostName
		}
		if ipAddress != nil{
			dictionary["ipAddress"] = ipAddress
		}
		if openDuration != nil{
			dictionary["openDuration"] = openDuration
		}
		if outlierName != nil{
			dictionary["outlierName"] = outlierName
		}
		if platformOwner != nil{
			dictionary["platformOwner"] = platformOwner
		}
		if remaingTimeToAck != nil{
			dictionary["remaing_time_to_ack"] = remaingTimeToAck
		}
		if state != nil{
			dictionary["state"] = state
		}
		if subType != nil{
			dictionary["subType"] = subType
		}
		if type != nil{
			dictionary["type"] = type
		}
		if unAckCount != nil{
			dictionary["unAckCount"] = unAckCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ackByMobile = aDecoder.decodeObject(forKey: "ackByMobile") as? AnyObject
         ackBY = aDecoder.decodeObject(forKey: "ack_BY") as? AnyObject
         ackON = aDecoder.decodeObject(forKey: "ack_ON") as? AnyObject
         ackStatus = aDecoder.decodeObject(forKey: "ack_status") as? AnyObject
         agingSinceInsert = aDecoder.decodeObject(forKey: "aging_since_insert") as? AnyObject
         appName = aDecoder.decodeObject(forKey: "appName") as? AnyObject
         appOwner = aDecoder.decodeObject(forKey: "appOwner") as? AnyObject
         classType = aDecoder.decodeObject(forKey: "classType") as? String
         cname = aDecoder.decodeObject(forKey: "cname") as? AnyObject
         count = aDecoder.decodeObject(forKey: "count") as? Int
         createTime = aDecoder.decodeObject(forKey: "createTime") as? AnyObject
         delayInAcknowledge = aDecoder.decodeObject(forKey: "delayInAcknowledge") as? AnyObject
         delayInCreation = aDecoder.decodeObject(forKey: "delayInCreation") as? AnyObject
         eventKPI = aDecoder.decodeObject(forKey: "eventKPI") as? AnyObject
         eventMessage = aDecoder.decodeObject(forKey: "eventMessage") as? AnyObject
         eventStatus = aDecoder.decodeObject(forKey: "eventStatus") as? AnyObject
         eventTime = aDecoder.decodeObject(forKey: "eventTime") as? AnyObject
         hostName = aDecoder.decodeObject(forKey: "hostName") as? AnyObject
         ipAddress = aDecoder.decodeObject(forKey: "ipAddress") as? AnyObject
         openDuration = aDecoder.decodeObject(forKey: "openDuration") as? AnyObject
         outlierName = aDecoder.decodeObject(forKey: "outlierName") as? AnyObject
         platformOwner = aDecoder.decodeObject(forKey: "platformOwner") as? AnyObject
         remaingTimeToAck = aDecoder.decodeObject(forKey: "remaing_time_to_ack") as? AnyObject
         state = aDecoder.decodeObject(forKey: "state") as? AnyObject
         subType = aDecoder.decodeObject(forKey: "subType") as? AnyObject
         type = aDecoder.decodeObject(forKey: "type") as? String
         unAckCount = aDecoder.decodeObject(forKey: "unAckCount") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ackByMobile != nil{
			aCoder.encode(ackByMobile, forKey: "ackByMobile")
		}
		if ackBY != nil{
			aCoder.encode(ackBY, forKey: "ack_BY")
		}
		if ackON != nil{
			aCoder.encode(ackON, forKey: "ack_ON")
		}
		if ackStatus != nil{
			aCoder.encode(ackStatus, forKey: "ack_status")
		}
		if agingSinceInsert != nil{
			aCoder.encode(agingSinceInsert, forKey: "aging_since_insert")
		}
		if appName != nil{
			aCoder.encode(appName, forKey: "appName")
		}
		if appOwner != nil{
			aCoder.encode(appOwner, forKey: "appOwner")
		}
		if classType != nil{
			aCoder.encode(classType, forKey: "classType")
		}
		if cname != nil{
			aCoder.encode(cname, forKey: "cname")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if createTime != nil{
			aCoder.encode(createTime, forKey: "createTime")
		}
		if delayInAcknowledge != nil{
			aCoder.encode(delayInAcknowledge, forKey: "delayInAcknowledge")
		}
		if delayInCreation != nil{
			aCoder.encode(delayInCreation, forKey: "delayInCreation")
		}
		if eventKPI != nil{
			aCoder.encode(eventKPI, forKey: "eventKPI")
		}
		if eventMessage != nil{
			aCoder.encode(eventMessage, forKey: "eventMessage")
		}
		if eventStatus != nil{
			aCoder.encode(eventStatus, forKey: "eventStatus")
		}
		if eventTime != nil{
			aCoder.encode(eventTime, forKey: "eventTime")
		}
		if hostName != nil{
			aCoder.encode(hostName, forKey: "hostName")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ipAddress")
		}
		if openDuration != nil{
			aCoder.encode(openDuration, forKey: "openDuration")
		}
		if outlierName != nil{
			aCoder.encode(outlierName, forKey: "outlierName")
		}
		if platformOwner != nil{
			aCoder.encode(platformOwner, forKey: "platformOwner")
		}
		if remaingTimeToAck != nil{
			aCoder.encode(remaingTimeToAck, forKey: "remaing_time_to_ack")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if subType != nil{
			aCoder.encode(subType, forKey: "subType")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if unAckCount != nil{
			aCoder.encode(unAckCount, forKey: "unAckCount")
		}

	}

}
