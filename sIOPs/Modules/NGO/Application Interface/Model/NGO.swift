//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGO : NSObject, NSCoding{

	var application : [NGOApplication]!
	var delinquency : [NGOApplication]!
	var delinquencyCount : Int!
	var infra : [NGOApplication]!
	var mainOutlier : [NGOMainOutlier]!
	var openAlerts : [NGOApplication]!
	var openAlertsCount : Int!
	var tools : [NGOApplication]!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		application = [NGOApplication]()
		if let applicationArray = dictionary["application"] as? [[String:Any]]{
			for dic in applicationArray{
				let value = NGOApplication(fromDictionary: dic)
				application.append(value)
			}
		}
		delinquency = [NGOApplication]()
		if let delinquencyArray = dictionary["delinquency"] as? [[String:Any]]{
			for dic in delinquencyArray{
				let value = NGOApplication(fromDictionary: dic)
				delinquency.append(value)
			}
		}
		delinquencyCount = dictionary["delinquencyCount"] as? Int
		infra = [NGOApplication]()
		if let infraArray = dictionary["infra"] as? [[String:Any]]{
			for dic in infraArray{
				let value = NGOApplication(fromDictionary: dic)
				infra.append(value)
			}
		}
		mainOutlier = [NGOMainOutlier]()
		if let mainOutlierArray = dictionary["mainOutlier"] as? [[String:Any]]{
			for dic in mainOutlierArray{
				let value = NGOMainOutlier(fromDictionary: dic)
				mainOutlier.append(value)
			}
		}
		openAlerts = [NGOApplication]()
		if let openAlertsArray = dictionary["openAlerts"] as? [[String:Any]]{
			for dic in openAlertsArray{
				let value = NGOApplication(fromDictionary: dic)
				openAlerts.append(value)
			}
		}
		openAlertsCount = dictionary["openAlertsCount"] as? Int
		tools = [NGOApplication]()
		if let toolsArray = dictionary["tools"] as? [[String:Any]]{
			for dic in toolsArray{
				let value = NGOApplication(fromDictionary: dic)
				tools.append(value)
			}
		}
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if application != nil{
			var dictionaryElements = [[String:Any]]()
			for applicationElement in application {
				dictionaryElements.append(applicationElement.toDictionary())
			}
			dictionary["application"] = dictionaryElements
		}
		if delinquency != nil{
			var dictionaryElements = [[String:Any]]()
			for delinquencyElement in delinquency {
				dictionaryElements.append(delinquencyElement.toDictionary())
			}
			dictionary["delinquency"] = dictionaryElements
		}
		if delinquencyCount != nil{
			dictionary["delinquencyCount"] = delinquencyCount
		}
		if infra != nil{
			var dictionaryElements = [[String:Any]]()
			for infraElement in infra {
				dictionaryElements.append(infraElement.toDictionary())
			}
			dictionary["infra"] = dictionaryElements
		}
		if mainOutlier != nil{
			var dictionaryElements = [[String:Any]]()
			for mainOutlierElement in mainOutlier {
				dictionaryElements.append(mainOutlierElement.toDictionary())
			}
			dictionary["mainOutlier"] = dictionaryElements
		}
		if openAlerts != nil{
			var dictionaryElements = [[String:Any]]()
			for openAlertsElement in openAlerts {
				dictionaryElements.append(openAlertsElement.toDictionary())
			}
			dictionary["openAlerts"] = dictionaryElements
		}
		if openAlertsCount != nil{
			dictionary["openAlertsCount"] = openAlertsCount
		}
		if tools != nil{
			var dictionaryElements = [[String:Any]]()
			for toolsElement in tools {
				dictionaryElements.append(toolsElement.toDictionary())
			}
			dictionary["tools"] = dictionaryElements
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         application = aDecoder.decodeObject(forKey :"application") as? [NGOApplication]
         delinquency = aDecoder.decodeObject(forKey :"delinquency") as? [NGOApplication]
         delinquencyCount = aDecoder.decodeObject(forKey: "delinquencyCount") as? Int
         infra = aDecoder.decodeObject(forKey :"infra") as? [NGOApplication]
         mainOutlier = aDecoder.decodeObject(forKey :"mainOutlier") as? [NGOMainOutlier]
         openAlerts = aDecoder.decodeObject(forKey :"openAlerts") as? [NGOApplication]
         openAlertsCount = aDecoder.decodeObject(forKey: "openAlertsCount") as? Int
         tools = aDecoder.decodeObject(forKey :"tools") as? [NGOApplication]
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if application != nil{
			aCoder.encode(application, forKey: "application")
		}
		if delinquency != nil{
			aCoder.encode(delinquency, forKey: "delinquency")
		}
		if delinquencyCount != nil{
			aCoder.encode(delinquencyCount, forKey: "delinquencyCount")
		}
		if infra != nil{
			aCoder.encode(infra, forKey: "infra")
		}
		if mainOutlier != nil{
			aCoder.encode(mainOutlier, forKey: "mainOutlier")
		}
		if openAlerts != nil{
			aCoder.encode(openAlerts, forKey: "openAlerts")
		}
		if openAlertsCount != nil{
			aCoder.encode(openAlertsCount, forKey: "openAlertsCount")
		}
		if tools != nil{
			aCoder.encode(tools, forKey: "tools")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
