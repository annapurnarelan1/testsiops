//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOOpenALertsModel : NSObject, NSCoding{

	var acknowledement : [Acknowledement]!
	var acknowledementSLA : [Acknowledement]!
	var acknowledged : [Acknowledged]!
	var application : [Acknowledged]!
	var critical : Int!
	var fatal : Int!
	var infrastructure : [Acknowledged]!
	var met : [Acknowledged]!
	var missed : [Acknowledged]!
	var openAlertCount : Int!
	var openAlerts : [Acknowledement]!
	var tools : [Acknowledged]!
	var unAcknowledged : [Acknowledged]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		acknowledement = [Acknowledement]()
		if let acknowledementArray = dictionary["acknowledement"] as? [[String:Any]]{
			for dic in acknowledementArray{
				let value = Acknowledement(fromDictionary: dic)
				acknowledement.append(value)
			}
		}
		acknowledementSLA = [Acknowledement]()
		if let acknowledementSLAArray = dictionary["acknowledementSLA"] as? [[String:Any]]{
			for dic in acknowledementSLAArray{
				let value = Acknowledement(fromDictionary: dic)
				acknowledementSLA.append(value)
			}
		}
		acknowledged = [Acknowledged]()
		if let acknowledgedArray = dictionary["acknowledged"] as? [[String:Any]]{
			for dic in acknowledgedArray{
				let value = Acknowledged(fromDictionary: dic)
				acknowledged.append(value)
			}
		}
		application = [Acknowledged]()
		if let applicationArray = dictionary["application"] as? [[String:Any]]{
			for dic in applicationArray{
				let value = Acknowledged(fromDictionary: dic)
				application.append(value)
			}
		}
		critical = dictionary["critical"] as? Int
		fatal = dictionary["fatal"] as? Int
		infrastructure = [Acknowledged]()
		if let infrastructureArray = dictionary["infrastructure"] as? [[String:Any]]{
			for dic in infrastructureArray{
				let value = Acknowledged(fromDictionary: dic)
				infrastructure.append(value)
			}
		}
		met = [Acknowledged]()
		if let metArray = dictionary["met"] as? [[String:Any]]{
			for dic in metArray{
				let value = Acknowledged(fromDictionary: dic)
				met.append(value)
			}
		}
		missed = [Acknowledged]()
		if let missedArray = dictionary["missed"] as? [[String:Any]]{
			for dic in missedArray{
				let value = Acknowledged(fromDictionary: dic)
				missed.append(value)
			}
		}
		openAlertCount = dictionary["openAlertCount"] as? Int
		openAlerts = [Acknowledement]()
		if let openAlertsArray = dictionary["openAlerts"] as? [[String:Any]]{
			for dic in openAlertsArray{
				let value = Acknowledement(fromDictionary: dic)
				openAlerts.append(value)
			}
		}
		tools = [Acknowledged]()
        if let toolsArray = dictionary["tools"] as? [[String:Any]]{
                for dic in toolsArray{
                    let value = Acknowledged(fromDictionary: dic)
                    tools.append(value)
                }
            }
		unAcknowledged = [Acknowledged]()
        if let unackArray = dictionary["unAcknowledged"] as? [[String:Any]]{
                    for dic in unackArray{
                        let value = Acknowledged(fromDictionary: dic)
                        unAcknowledged.append(value)
                    }
                }
    }

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if acknowledement != nil{
			var dictionaryElements = [[String:Any]]()
			for acknowledementElement in acknowledement {
				dictionaryElements.append(acknowledementElement.toDictionary())
			}
			dictionary["acknowledement"] = dictionaryElements
		}
		if acknowledementSLA != nil{
			var dictionaryElements = [[String:Any]]()
			for acknowledementSLAElement in acknowledementSLA {
				dictionaryElements.append(acknowledementSLAElement.toDictionary())
			}
			dictionary["acknowledementSLA"] = dictionaryElements
		}
		if acknowledged != nil{
			var dictionaryElements = [[String:Any]]()
			for acknowledgedElement in acknowledged {
				dictionaryElements.append(acknowledgedElement.toDictionary())
			}
			dictionary["acknowledged"] = dictionaryElements
		}
		if application != nil{
			var dictionaryElements = [[String:Any]]()
			for applicationElement in application {
				dictionaryElements.append(applicationElement.toDictionary())
			}
			dictionary["application"] = dictionaryElements
		}
		if critical != nil{
			dictionary["critical"] = critical
		}
		if fatal != nil{
			dictionary["fatal"] = fatal
		}
		if infrastructure != nil{
			var dictionaryElements = [[String:Any]]()
			for infrastructureElement in infrastructure {
				dictionaryElements.append(infrastructureElement.toDictionary())
			}
			dictionary["infrastructure"] = dictionaryElements
		}
		if met != nil{
			var dictionaryElements = [[String:Any]]()
			for metElement in met {
				dictionaryElements.append(metElement.toDictionary())
			}
			dictionary["met"] = dictionaryElements
		}
		if missed != nil{
			var dictionaryElements = [[String:Any]]()
			for missedElement in missed {
				dictionaryElements.append(missedElement.toDictionary())
			}
			dictionary["missed"] = dictionaryElements
		}
		if openAlertCount != nil{
			dictionary["openAlertCount"] = openAlertCount
		}
		if openAlerts != nil{
			var dictionaryElements = [[String:Any]]()
			for openAlertsElement in openAlerts {
				dictionaryElements.append(openAlertsElement.toDictionary())
			}
			dictionary["openAlerts"] = dictionaryElements
		}
		if tools != nil{
			dictionary["tools"] = tools
		}
		if unAcknowledged != nil{
			dictionary["unAcknowledged"] = unAcknowledged
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         acknowledement = aDecoder.decodeObject(forKey :"acknowledement") as? [Acknowledement]
         acknowledementSLA = aDecoder.decodeObject(forKey :"acknowledementSLA") as? [Acknowledement]
         acknowledged = aDecoder.decodeObject(forKey :"acknowledged") as? [Acknowledged]
         application = aDecoder.decodeObject(forKey :"application") as? [Acknowledged]
         critical = aDecoder.decodeObject(forKey: "critical") as? Int
         fatal = aDecoder.decodeObject(forKey: "fatal") as? Int
         infrastructure = aDecoder.decodeObject(forKey :"infrastructure") as? [Acknowledged]
         met = aDecoder.decodeObject(forKey :"met") as? [Acknowledged]
         missed = aDecoder.decodeObject(forKey :"missed") as? [Acknowledged]
         openAlertCount = aDecoder.decodeObject(forKey: "openAlertCount") as? Int
         openAlerts = aDecoder.decodeObject(forKey :"openAlerts") as? [Acknowledement]
         tools = aDecoder.decodeObject(forKey: "tools") as? [Acknowledged]
         unAcknowledged = aDecoder.decodeObject(forKey: "unAcknowledged") as? [Acknowledged]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if acknowledement != nil{
			aCoder.encode(acknowledement, forKey: "acknowledement")
		}
		if acknowledementSLA != nil{
			aCoder.encode(acknowledementSLA, forKey: "acknowledementSLA")
		}
		if acknowledged != nil{
			aCoder.encode(acknowledged, forKey: "acknowledged")
		}
		if application != nil{
			aCoder.encode(application, forKey: "application")
		}
		if critical != nil{
			aCoder.encode(critical, forKey: "critical")
		}
		if fatal != nil{
			aCoder.encode(fatal, forKey: "fatal")
		}
		if infrastructure != nil{
			aCoder.encode(infrastructure, forKey: "infrastructure")
		}
		if met != nil{
			aCoder.encode(met, forKey: "met")
		}
		if missed != nil{
			aCoder.encode(missed, forKey: "missed")
		}
		if openAlertCount != nil{
			aCoder.encode(openAlertCount, forKey: "openAlertCount")
		}
		if openAlerts != nil{
			aCoder.encode(openAlerts, forKey: "openAlerts")
		}
		if tools != nil{
			aCoder.encode(tools, forKey: "tools")
		}
		if unAcknowledged != nil{
			aCoder.encode(unAcknowledged, forKey: "unAcknowledged")
		}

	}

}
