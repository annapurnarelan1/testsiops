//
//  NGOInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class NGOInteractor: NGOInteractorInputProtocol {

public enum NGOError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: NGOInteractorOutputProtocol?
    
    
    func requestData(){
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
           // "appRoleCode": NGOParams.applicationCode ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        var busicode = ""
//        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
//        {
        busicode = Constants.BusiCode.NGOSummary
      //  }
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
               
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                         let NGOdata = NGO.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                       self.presenter?.reloadData(NGOResponse:NGOdata)
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                

                
               
           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    
    func requestHistoryData(){
            
            
            let pubInfo: [String : Any] =
                ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "appRoleCode":  "726",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                
                
            ]
            
           let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            var busicode = ""
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
        busicode = Constants.BusiCode.NGOHistorySummary
          //  }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    
                   
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                             let NGOdata = NGO.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                           self.presenter?.reloadHistoryData(NGOResponse:NGOdata)
                      case Constants.status.NOK:
                             let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                            self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
    //                let loginRes: LoginModel = LoginModel.sharedInstance
    //
    //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
    //                LoginModel.sharedInstance.saveUser()
                    
                   // self.presenter?.loginDone(loginRes:loginRes)
                    
                   
                case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
                }
            })
            
        }
    
    
    
    
    
    func requestNGOOpenAlerts(){
            
            
            let pubInfo: [String : Any] =
                ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "appRoleCode": "726",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                
                
            ]
            
           let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            var busicode = ""
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
            busicode = Constants.BusiCode.NGOOpenAlerts
          //  }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    
                   
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                             let detail = NGOOpenALertsModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                           self.presenter?.reloadOpenALerts(response:detail)
                      case Constants.status.NOK:
                             let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                            self.presenter?.showAlertError(error:message)
                    default:
                        self.presenter?.showAlertError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
                   
               case .failure(let failure) :
                    switch failure {
                    case .connectionError:
                        self.presenter?.showAlertError(error:APIManager.NetworkError.internetError.rawValue)
                        
                    case .authorizationError(let errorJson):
                        self.presenter?.showAlertError(error: APIManager.NetworkError.unKnownError.rawValue)
                    case .serverError:
                        self.presenter?.showAlertError(error: APIManager.NetworkError.unKnownError.rawValue)
                    case .newUpdate:
                        self.presenter?.stopLoader()
                    default:
                        self.presenter?.showAlertError(error:APIManager.NetworkError.unKnownError.rawValue)
                        
                    }
                }
            })
            
        }
    
    
    func requestNGOOpenIncidents()
    {
       let pubInfo: [String : Any] =
                    ["timestamp": String(Date().ticks),
                    "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                    "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "osType": "ios",
                    "lang": "en_US",
                    "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
                
                let requestHeader:[String :Any] = [
                    "serviceName": "userInfo"
                ]
                
                let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "appRoleCode": "726",
                    "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                    "type": "userInfo",
                    
                    
                ]
                
               let busiParams: [String : Any] = [
                    "requestHeader": requestHeader,
                    "requestBody": requestBody,
                ]
               
                var busicode = ""
        //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
        //        {
                busicode = Constants.BusiCode.NGOOpenIncident
              //  }
                
                let requestList: [String: Any] = [
                    "busiCode": busicode,
                    "isEncrypt": false,
                    "transactionId": "0001574162779054",
                    "busiParams":busiParams
                ]
                
                let jsonParameter = [
                    "pubInfo" : pubInfo,
                    "requestList" : [requestList]
                    ] as [String : Any]
                
                APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                    switch result {
                    case .success(let returnJson) :
                        print(returnJson)
                        
                       
                        let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                        
                        switch status {
                        case Constants.status.OK:
                                 let detail = OpenIncidentModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                 self.presenter?.reloadOpenIncidents(response:detail)
                          case Constants.status.NOK:
                                 let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                self.presenter?.showIncidentError(error:message)
                        default:
                            self.presenter?.showIncidentError(error: APIManager.NetworkError.unKnownError.rawValue)
                        }
                        
                       
                   case .failure(let failure) :
                        switch failure {
                        case .connectionError:
                            self.presenter?.showIncidentError(error:APIManager.NetworkError.internetError.rawValue)
                            
                        case .authorizationError(let errorJson):
                            self.presenter?.showIncidentError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .serverError:
                            self.presenter?.showIncidentError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .newUpdate:
                            self.presenter?.stopLoader()
                        default:
                            self.presenter?.showIncidentError(error:APIManager.NetworkError.unKnownError.rawValue)
                            
                        }
                    }
                })
    }
    
    
    
    func requestNGOOpenChange()
    {
       let pubInfo: [String : Any] =
                    ["timestamp": String(Date().ticks),
                    "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                    "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "osType": "ios",
                    "lang": "en_US",
                    "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
                
                let requestHeader:[String :Any] = [
                    "serviceName": "userInfo"
                ]
                
                let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "appRoleCode": "726",
                    "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                    "type": "userInfo",
                    
                    
                ]
                
               let busiParams: [String : Any] = [
                    "requestHeader": requestHeader,
                    "requestBody": requestBody,
                ]
               
                var busicode = ""
        //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
        //        {
                busicode = Constants.BusiCode.NGOOpenChange
              //  }
                
                let requestList: [String: Any] = [
                    "busiCode": busicode,
                    "isEncrypt": false,
                    "transactionId": "0001574162779054",
                    "busiParams":busiParams
                ]
                
                let jsonParameter = [
                    "pubInfo" : pubInfo,
                    "requestList" : [requestList]
                    ] as [String : Any]
                
                APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                    switch result {
                    case .success(let returnJson) :
                        print(returnJson)
                        
                       
                        let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                        
                        switch status {
                        case Constants.status.OK:
                                 let detail = OpenChangeModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                 self.presenter?.reloadOpenChanges(response:detail)
                          case Constants.status.NOK:
                                 let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                self.presenter?.showChangesError(error:message)
                        default:
                            self.presenter?.showChangesError(error: APIManager.NetworkError.unKnownError.rawValue)
                        }
                        
                       
                   case .failure(let failure) :
                        switch failure {
                        case .connectionError:
                            self.presenter?.showChangesError(error:APIManager.NetworkError.internetError.rawValue)
                            
                        case .authorizationError(let errorJson):
                            self.presenter?.showChangesError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .serverError:
                            self.presenter?.showChangesError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .newUpdate:
                            self.presenter?.stopLoader()
                        default:
                            self.presenter?.showChangesError(error:APIManager.NetworkError.unKnownError.rawValue)
                            
                        }
                    }
                })
    }
    
    
    
    func requestNGOOpenDefects()
    {
        let pubInfo: [String : Any] =
                    ["timestamp": String(Date().ticks),
                    "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                    "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "osType": "ios",
                    "lang": "en_US",
                    "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
                
                let requestHeader:[String :Any] = [
                    "serviceName": "userInfo"
                ]
                
                let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "appRoleCode": "726",
                    "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                    "type": "userInfo",
                    
                    
                ]
                
               let busiParams: [String : Any] = [
                    "requestHeader": requestHeader,
                    "requestBody": requestBody,
                ]
               
                var busicode = ""
        //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
        //        {
                busicode = Constants.BusiCode.NGOOpenDefect
              //  }
                
                let requestList: [String: Any] = [
                    "busiCode": busicode,
                    "isEncrypt": false,
                    "transactionId": "0001574162779054",
                    "busiParams":busiParams
                ]
                
                let jsonParameter = [
                    "pubInfo" : pubInfo,
                    "requestList" : [requestList]
                    ] as [String : Any]
                
                APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                    switch result {
                    case .success(let returnJson) :
                        print("check open defects",returnJson)
                        
                       
                        let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                        
                        switch status {
                        case Constants.status.OK:
                                 let detail = OpenDefectModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                 self.presenter?.reloadOpenDefects(response:detail)
                          case Constants.status.NOK:
                                 let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                self.presenter?.showDefectsError(error:message)
                        default:
                            self.presenter?.showDefectsError(error: APIManager.NetworkError.unKnownError.rawValue)
                        }
                        
                       
                   case .failure(let failure) :
                        switch failure {
                        case .connectionError:
                            self.presenter?.showDefectsError(error:APIManager.NetworkError.internetError.rawValue)
                            
                        case .authorizationError(let errorJson):
                            self.presenter?.showDefectsError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .serverError:
                            self.presenter?.showDefectsError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .newUpdate:
                            self.presenter?.stopLoader()
                        default:
                            self.presenter?.showDefectsError(error:APIManager.NetworkError.unKnownError.rawValue)
                            
                        }
                    }
                })
    }
    
    
    
    func requestProblemTicket()
    {
        
        
        let pubInfo: [String : Any] =
                    ["timestamp": String(Date().ticks),
                    "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                    "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "osType": "ios",
                    "lang": "en_US",
                    "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
                
                let requestHeader:[String :Any] = [
                    "serviceName": "userInfo"
                ]
                
                let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                    "appRoleCode": "726",
                    "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                    "type": "userInfo",
                    
                    
                ]
                
               let busiParams: [String : Any] = [
                    "requestHeader": requestHeader,
                    "requestBody": requestBody,
                ]
               
                var busicode = ""
        //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
        //        {
                busicode = Constants.BusiCode.NGOProblemTicket
              //  }
                
                let requestList: [String: Any] = [
                    "busiCode": busicode,
                    "isEncrypt": false,
                    "transactionId": "0001574162779054",
                    "busiParams":busiParams
                ]
                
                let jsonParameter = [
                    "pubInfo" : pubInfo,
                    "requestList" : [requestList]
                    ] as [String : Any]
                
                APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                    switch result {
                    case .success(let returnJson) :
                        print(returnJson)
                        
                       
                        let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                        
                        switch status {
                        case Constants.status.OK:
                                 let detail = ProblemTickenModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                 
                                 self.presenter?.reloadProblemTickets(response:detail)
                          case Constants.status.NOK:
                                 let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                self.presenter?.showProblemError(error:message)
                        default:
                            self.presenter?.showProblemError(error: APIManager.NetworkError.unKnownError.rawValue)
                        }
                        
                       
                   case .failure(let failure) :
                        switch failure {
                        case .connectionError:
                            self.presenter?.showProblemError(error:APIManager.NetworkError.internetError.rawValue)
                            
                        case .authorizationError(let errorJson):
                            self.presenter?.showProblemError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .serverError:
                            self.presenter?.showProblemError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .newUpdate:
                            self.presenter?.stopLoader()
                        default:
                            self.presenter?.showProblemError(error:APIManager.NetworkError.unKnownError.rawValue)
                            
                        }
                    }
                })
    }
    
    
    
    func requestOpenAction()
       {
           let pubInfo: [String : Any] =
                       ["timestamp": String(Date().ticks),
                       "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                       "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                       "osType": "ios",
                       "lang": "en_US",
                       "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
                   
                   let requestHeader:[String :Any] = [
                       "serviceName": "userInfo"
                   ]
                   
                   let requestBody:[String : Any] = [
               "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                       "appRoleCode": "726",
                       "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                       "type": "userInfo",
                       
                       
                   ]
                   
                  let busiParams: [String : Any] = [
                       "requestHeader": requestHeader,
                       "requestBody": requestBody,
                   ]
                  
                   var busicode = ""
           //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
           //        {
                   busicode = Constants.BusiCode.NGOOpenAction
                 //  }
                   
                   let requestList: [String: Any] = [
                       "busiCode": busicode,
                       "isEncrypt": false,
                       "transactionId": "0001574162779054",
                       "busiParams":busiParams
                   ]
                   
                   let jsonParameter = [
                       "pubInfo" : pubInfo,
                       "requestList" : [requestList]
                       ] as [String : Any]
                   
                   APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                       switch result {
                       case .success(let returnJson) :
                           print(returnJson)
                           
                          
                           let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                           
                           switch status {
                           case Constants.status.OK:
                                    let detail = OpenActionModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                    
                                    self.presenter?.reloadOpenActions(response: detail)
                             case Constants.status.NOK:
                                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                   self.presenter?.showActionError(error:message)
                           default:
                               self.presenter?.showActionError(error: APIManager.NetworkError.unKnownError.rawValue)
                           }
                           
                          
                      case .failure(let failure) :
                           switch failure {
                           case .connectionError:
                               self.presenter?.showActionError(error:APIManager.NetworkError.internetError.rawValue)
                               
                           case .authorizationError(let errorJson):
                               self.presenter?.showActionError(error: APIManager.NetworkError.unKnownError.rawValue)
                           case .serverError:
                               self.presenter?.showActionError(error: APIManager.NetworkError.unKnownError.rawValue)
                           case .newUpdate:
                               self.presenter?.stopLoader()
                           default:
                               self.presenter?.showActionError(error:APIManager.NetworkError.unKnownError.rawValue)
                               
                           }
                       }
                   })
       }
    
}
