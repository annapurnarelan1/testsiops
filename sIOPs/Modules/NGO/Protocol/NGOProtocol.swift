//
//  NGOProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol NGOViewProtocol: class {
    var presenter: NGOPresenterProtocol? { get set }
    //func NGODone(NGORes :NGOModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(NGOResponse:NGO)
    func reloadHistoryData(NGOResponse:NGO)
    func reloadOpenALerts(response:NGOOpenALertsModel)
    func reloadOpenIncidents(response:OpenIncidentModel)
    func reloadOpenChanges(response:OpenChangeModel)
    func reloadOpenDefects(response:OpenDefectModel)
    func reloadProblemTickets(response:ProblemTickenModel)
    func reloadOpenActions(response:OpenActionModel)
  
    
    func showAlertError(error:String)
       func showIncidentError(error:String)
       func showChangesError(error:String)
       func showDefectsError(error:String)
       func showProblemError(error:String)
       func showActionError(error:String)
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGOWireFrameProtocol: class {
    static func presentNGOModule(fromView:AnyObject,baseVC:Any) -> BaseViewController
    func ngoDetailOpenAlerts(type:String,selected:String,response:NGOOpenALertsModel)
    func ngoDeliquency(type:String,selected:String,ngoResponse:NGO)
    func ngoDetailOpenIncidents(type:String,selected:String,response:OpenIncidentModel)
    func ngoDetailOpenChanges(type:String,selected:String,response:OpenChangeModel)
    func ngoDetailOpendefects(type:String,selected:String,response:OpenDefectModel)
    func ngoDetailOpenActions(type:String,selected:String,response:OpenActionModel)
    func ngoDetailProblemTickets(type:String,selected:String,response:ProblemTickenModel)

}

/// Method contract between VIEW -> PRESENTER
protocol NGOPresenterProtocol: class {
    var view: NGOViewProtocol? { get set }
    var interactor: NGOInteractorInputProtocol? { get set }
    var wireFrame: NGOWireFrameProtocol? { get set }
    
     func ngoDetailOpenAlerts(type:String,selected:String,response:NGOOpenALertsModel)
    func ngoDeliquency(type:String,selected:String,ngoResponse:NGO)
    
    
    func ngoDetailOpenIncidents(type:String,selected:String,response:OpenIncidentModel)
    func ngoDetailOpenChanges(type:String,selected:String,response:OpenChangeModel)
    func ngoDetailOpenDefects(type:String,selected:String,response:OpenDefectModel)
    func ngoDetailOpenActions(type:String,selected:String,response:OpenActionModel)
    func ngoDetailProblemTickets(type:String,selected:String,response:ProblemTickenModel)
    
    func requestData()
    func requestHistoryData()
    func requestNGOOpenAlerts()
    func requestNGOOpenIncidents()
    func requestNGOOpenChange()
    func requestNGOOpenDefects()
    func requestProblemTicket()
    func requestOpenAction()
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGOInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(NGOResponse:NGO)
    func reloadHistoryData(NGOResponse:NGO)
    func reloadOpenALerts(response:NGOOpenALertsModel)
    func reloadOpenIncidents(response:OpenIncidentModel)
    func reloadOpenChanges(response:OpenChangeModel)
    func reloadOpenDefects(response:OpenDefectModel)
    
    func showAlertError(error:String)
    func showIncidentError(error:String)
    func showChangesError(error:String)
    func showDefectsError(error:String)
    func showProblemError(error:String)
    func showActionError(error:String)
    func reloadProblemTickets(response:ProblemTickenModel)
    func reloadOpenActions(response:OpenActionModel)
    

}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGOInteractorInputProtocol: class
{
    var presenter: NGOInteractorOutputProtocol? { get set }
    func requestData()
    func requestHistoryData()
    func requestNGOOpenAlerts()
    func requestNGOOpenIncidents()
    func requestNGOOpenChange()
    func requestNGOOpenDefects()
    func requestProblemTicket()
    func requestOpenAction()
    
}
