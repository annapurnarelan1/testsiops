//
//  NGOViewController.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit



protocol NGODelegate:NSObjectProtocol {
    func reloadNGOdata()
    func error()
    func viewDetailLoadNGO()
    func ngoResponseData(response: NGO)
    
}


class NGOViewController: BaseViewController,NGOViewProtocol{
    
    var presenter: NGOPresenterProtocol?
    @IBOutlet weak var NGOCollectionView: UICollectionView!
    @IBOutlet weak var NGOTableView: UITableView!
    var NGOresponse :NGO?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var btn = UIButton()
    var buttonTapped:Bool = false
    weak var delegate: NGODelegate?
    var senderSelectedIndex:Int?
    var ngoOpenAlertsResponse:NGOOpenALertsModel?
    var OpenIncidenetsResponse:OpenIncidentModel?
    var openChangeResponse:OpenChangeModel?
    var opendefectsResponse:OpenDefectModel?
    var problemTicketResponse:ProblemTickenModel?
    var openActionResponse:OpenActionModel?
    
    var isFirstLoading = true
    var isSecondLoading = true
    var isThirdLoading = true
    var isFourthLoading = true
    var isFiveLoading = true
    var isSixthLoading = true
    var alertError = ""
    var incidentError = ""
    var changeError = ""
    var defectError = ""
    var problemError = ""
    var actionError = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonTapped = false
        
        self.NGOTableView.register(UINib(nibName: "OrderJourneyDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderJourneyDetailTableViewCell")
        
        
        //        if(!(DashboardVC.isToggle ?? false))
        //        {
        //            startAnimating()
        //        }
        
        self.presenter?.requestNGOOpenAlerts()
        self.presenter?.requestNGOOpenIncidents()
        self.presenter?.requestNGOOpenChange()
        self.presenter?.requestNGOOpenDefects()
        self.presenter?.requestProblemTicket()
        self.presenter?.requestOpenAction()
        self.presenter?.requestData()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func requestData()
    {
        //         startAnimating()
        alertError = ""
        incidentError = ""
        changeError = ""
        defectError = ""
        problemError = ""
        actionError = ""
        
        isFirstLoading = true
        isSecondLoading = true
        isThirdLoading = true
        isFourthLoading = true
        isFiveLoading = true
        isSixthLoading = true
        NGOTableView.reloadData()
        self.presenter?.requestNGOOpenAlerts()
        self.presenter?.requestNGOOpenIncidents()
        self.presenter?.requestNGOOpenChange()
        self.presenter?.requestNGOOpenDefects()
        self.presenter?.requestProblemTicket()
         self.presenter?.requestOpenAction()
    }
    
    
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> NGOViewProtocol{
        return UIStoryboard(name: "NGO", bundle: nil).instantiateViewController(withIdentifier: "NGO") as! NGOViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.delegate?.error()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    
    
    
    func showAlertError(error:String)
    {
        isFirstLoading = false
        alertError = error
        NGOTableView.reloadData()
    }
    func showIncidentError(error:String)
    {
        isSecondLoading = false
        incidentError = error
        NGOTableView.reloadData()
    }
    func showChangesError(error:String)
    {
        isThirdLoading = false
        changeError = error
        NGOTableView.reloadData()
    }
    func showDefectsError(error:String)
    {
        isFourthLoading = false
        defectError = error
        NGOTableView.reloadData()
    }
    func showProblemError(error:String)
    {
        isFiveLoading = false
        problemError = error
        NGOTableView.reloadData()
    }
    func showActionError(error:String)
    {
        isSixthLoading = false
        actionError = error
        NGOTableView.reloadData()
    }
    
    
    func reloadOpenChanges(response:OpenChangeModel)
    {
        isThirdLoading = false
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.openChangeResponse = response
        self.delegate?.reloadNGOdata()
        NGOTableView.reloadData()
    }
    
    func reloadOpenALerts(response:NGOOpenALertsModel)
    {
        isFirstLoading = false
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.ngoOpenAlertsResponse = response
        self.delegate?.reloadNGOdata()
        NGOTableView.reloadData()
    }
    
    func reloadOpenIncidents(response:OpenIncidentModel)
    {
        isSecondLoading = false
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.OpenIncidenetsResponse = response
        self.delegate?.reloadNGOdata()
        NGOTableView.reloadData()
    }
    
    func reloadOpenActions(response: OpenActionModel) {
        isSixthLoading = false
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.openActionResponse = response
        self.delegate?.reloadNGOdata()
        NGOTableView.reloadData()
    }
    
    func reloadData(NGOResponse:NGO)
    {
        //stopAnimating()
        // DashboardVC.addedToggleView?.removeFromSuperview()
        NGOresponse = NGOResponse
        
        self.delegate?.reloadNGOdata()
        self.delegate?.ngoResponseData(response: NGOresponse!)
       
        
        
        
    }
    
    func reloadOpenDefects(response:OpenDefectModel)
    {
        isFourthLoading = false
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.opendefectsResponse = response
        self.delegate?.reloadNGOdata()
        NGOTableView.reloadData()
    }
    
    
    func reloadProblemTickets(response:ProblemTickenModel)
    {
        isFiveLoading = false
        DashboardVC.addedToggleView?.removeFromSuperview()
        self.problemTicketResponse = response
        self.delegate?.reloadNGOdata()
        NGOTableView.reloadData()
    }
    
    
    
    func reloadHistoryData(NGOResponse:NGO) {
        stopAnimating()
        NGOresponse = NGOResponse
        // self.presenter?.ngoDetailOpenAlerts(type:"AlertsHistory",selected:Constants.openAlerts.infra,ngoResponse:NGOresponse!)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.updateViewConstraints()
        heightConstraint.constant = self.NGOTableView.contentSize.height + 40
        NGOTableView?.setNeedsLayout()
    }
    
    @objc func makeHistoryCall()
    {
        startAnimating()
        self.presenter?.requestHistoryData()
    }
    
    
    @objc func ngoDetailOpenAlerts(sender:Any)
    {
        btn = (sender as? UIButton)!
        let section = btn.tag/100
        let index = btn.tag%100
       // let row = 0
        buttonTapped = true
       
        let cell = self.NGOTableView.cellForRow(at: IndexPath(row:0 , section: section))
        if let outlinerCell = cell as? OrderJourneyDetailTableViewCell {
            if(buttonTapped)
            {
                switch index {
                case 1:
                    if outlinerCell.firstOutlIerCount.text != "0" {
                        self.presenter?.ngoDetailOpenAlerts(type:"Open Alerts",selected:"Fatal",response:ngoOpenAlertsResponse!)
                    }
                case 2:
                    if outlinerCell.secondOutlIerCount.text != "0" {
                         self.presenter?.ngoDetailOpenAlerts(type:"Open Alerts",selected:"Critical",response:ngoOpenAlertsResponse!)
                    }
                default:
                    print("nothing selected")
                }
            }
            buttonTapped = false
        }
    }
    
    @objc func ngoDetailOpenDefects(sender:Any)
       {
           btn = (sender as? UIButton)!
           let section = btn.tag/100
            let index = btn.tag%100
           buttonTapped = true
          
           let cell = self.NGOTableView.cellForRow(at: IndexPath(row:0 , section: section))
        if let outlinerCell = cell as? OrderJourneyDetailTableViewCell {
            if(buttonTapped)
                 {
                     switch index {
                     case 10:
                        if outlinerCell.firstOutlIerCount?.text != "0" {
                               self.presenter?.ngoDetailOpenDefects(type: "Open Defects", selected: "Hot Fix", response: opendefectsResponse!)
                        }
                     case 11:
                        if outlinerCell.secondOutlIerCount?.text != "0"  {
                         self.presenter?.ngoDetailOpenDefects(type:"Open Defects",selected:"Defects",response:opendefectsResponse!)
                        }
                    
                      default:
                         print("nothing selected")
                     }
                 }
                 buttonTapped = false
        }
}
    
    
    
    @objc func ngoDetailProblemTickets(sender:Any)
    {
        btn = (sender as? UIButton)!
        let section = btn.tag/100
        let index = btn.tag%100
        buttonTapped = true
      
        let cell = self.NGOTableView.cellForRow(at: IndexPath(row:0 , section: section))
        if let outlinerCell = cell as? OrderJourneyDetailTableViewCell {
            if(buttonTapped)
            {
                switch index {
                case 12:
                    if outlinerCell.firstOutlIerCount?.text != "0" {
                        self.presenter?.ngoDetailProblemTickets(type: "Problem Tickets", selected: Constants.ProblemTicketsType.pendingRCA, response: problemTicketResponse!)
                    }
              
                case 13:
                    if outlinerCell.secondOutlIerCount?.text != "0" {
                            self.presenter?.ngoDetailProblemTickets(type:"Problem Tickets",selected:Constants.ProblemTicketsType.RCAActionItem,response:problemTicketResponse!)
                    }
                default:
                    print("nothing selected")
                }
            }
            buttonTapped = false
        }

    }
    
    @objc func ngoDetailOpenIncidents(sender:Any)
    {
        btn = (sender as? UIButton)!
        let section = btn.tag/100
        let index = btn.tag%100
        let cell = self.NGOTableView.cellForRow(at: IndexPath(row:0 , section: section))
        if let outlinerCell = cell as? OrderJourneyDetailTableViewCell {
            switch index {
                case 3:
                    if outlinerCell.firstOutlIerCount.text != "0" {
                        self.presenter?.ngoDetailOpenIncidents(type:"Open Incidents",selected:Constants.OpenIncidentType.buisnessImpacting,response:OpenIncidenetsResponse!)
                    }
                    
                case 4:
                    if outlinerCell.secondOutlIerCount.text != "0" {
                            self.presenter?.ngoDetailOpenIncidents(type:"Open Incidents",selected:Constants.OpenIncidentType.nonBuisnessImpacting,response:OpenIncidenetsResponse!)
                    }
                    default:
                    print("nothing selected")
                }
        }
    
        
        
    }
    
    
    @objc func ngoDetailOpenActions(sender:Any)
    {
        btn = (sender as? UIButton)!
        var cell = self.NGOTableView.cellForRow(at: )
        let section = btn.tag/100
        let index = btn.tag%100
        let cellNGO = self.NGOTableView.cellForRow(at: IndexPath(row:0 , section: section))
        if let outlinerCell = cellNGO as? OrderJourneyDetailTableViewCell {
        switch index {
        case 14:
            if outlinerCell.firstOutlIerCount.text != "0" {
       self.presenter?.ngoDetailOpenActions(type:"OpenActions",selected:Constants.OpenActionType.pendingAction,response:openActionResponse!)
            }
          
        case 15:
            
            if outlinerCell.secondOutlIerCount.text != "0" {
                   self.presenter?.ngoDetailOpenActions(type:"Open Actions",selected:Constants.OpenActionType.overdueAction,response:openActionResponse!)
            }
            
        default:
            print("nothing selected")
        }
     }
        
    }
    
    
   /// func ngoDetailOpenActions(type:String,selected:String,response:OpenActionModel)
    
    
    @objc func ngoDetailOpenChanges(sender:Any)
    {
        btn = (sender as? UIButton)!
        let section = btn.tag/100
        let index = btn.tag%100
        let cellNGO = self.NGOTableView.cellForRow(at: IndexPath(row:0 , section: section))
        if let outlinerCell = cellNGO as? OrderJourneyDetailTableViewCell {
            switch index {
            case 5:
                if outlinerCell.firstOutlIerCount.text != "0" {
                    self.presenter?.ngoDetailOpenChanges(type:"Open Changes",selected:"Emergency",response:openChangeResponse!)

                }
           case 6:
                if outlinerCell.secondOutlIerCount.text != "0" {
                    self.presenter?.ngoDetailOpenChanges(type:"Open Changes",selected:"Normal",response:openChangeResponse!)

                }
            case 7:
                if outlinerCell.thirdOutlIerCount.text != "0" {
                    self.presenter?.ngoDetailOpenChanges(type:"Open Changes",selected:"MACD",response:openChangeResponse!)

                }
            case 8:
                if outlinerCell.rejectedCount.text != "0" {
                    self.presenter?.ngoDetailOpenChanges(type:"Open Changes",selected:"Pending Closure",response:openChangeResponse!)
                }
                
            case 9:
                if outlinerCell.inProgressCount.text != "0" {
                     self.presenter?.ngoDetailOpenChanges(type:"Open Changes",selected:"Pending Implementation",response:openChangeResponse!)
                }
            default:
                print("nothing selected")
            }
        }

        
        
    }
    
    
    
    @objc func ngoDetailDeliquency(sender:Any)
    {
        let btn:UIButton = (sender as? UIButton)!
        switch btn.tag {
            //        case 0:
        //            self.presenter?.ngoDeliquency(type:"Delinquency",selected:"Clocked",ngoResponse:NGOresponse!)
        case 1:
            self.presenter?.ngoDeliquency(type:"Delinquency",selected:"Delinquent",ngoResponse:NGOresponse!)
        default:
            print("nothing selected")
        }
    }
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        if(sender.tag != senderSelectedIndex)
        {
            senderSelectedIndex = sender.tag
            
        }
        else
        {
            senderSelectedIndex = -1
            
        }
        self.NGOTableView.reloadData()
        self.view.setNeedsLayout()
    }
    
}

extension NGOViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:OrderJourneyDetailTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "OrderJourneyDetailTableViewCell", for: indexPath as IndexPath) as? OrderJourneyDetailTableViewCell)!
        cell.viewdetailBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        cell.viewdetailBtn.tag =  indexPath.section
        cell.rejectedBtn.tag =  indexPath.section
        cell.inProgressBtn.tag = indexPath.section
        cell.cnstViewDetailsHeight.constant = 30
        //cell.rejectedBtn.addTarget(self, action: #selector(rejectedBtnAction(_:)), for: .touchUpInside)
        // cell.inProgressBtn.addTarget(self, action: #selector(inProgressBtnAction(_:)), for: .touchUpInside)
        if(senderSelectedIndex ==  cell.viewdetailBtn.tag )
        {
            cell.viewdetailBtn.backgroundColor = UIColor.strongBlueColor
            cell.viewdetailBtn.setTitleColor(UIColor.white, for: .normal)
            cell.viewdetailBtn.setTitle("Hide Details", for: .normal)
            cell.secondView.isHidden = false
            
        }
        else
        {
            cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
            cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
            cell.viewdetailBtn.setTitle("View Details", for: .normal)
            cell.secondView.isHidden = true
            cell.detailView.isHidden = true
        }
        
        switch indexPath.section {
        case 0:
            cell.cnstViewDetailsHeight.constant = 0
            if(isFirstLoading)
            {
                cell.errorLabel.isHidden = true
                cell.initialStackView.isHidden = true
                cell.loader.isHidden = false
                cell.loader.startAnimating()
            }
            else
            {
                if(alertError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = alertError
                    cell.initialStackView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.initialStackView.isHidden = false
                }
                cell.loader.stopAnimating()
            }
            
            cell.viewdetailBtn.isHidden = true
            cell.headerLabel.text = "Open Alerts"
            cell.firstOutlIerName.text = "Fatal"
            cell.firstOutlIerCount.text = "\(ngoOpenAlertsResponse?.fatal ?? 0)"
            cell.secondOutlIerName.text = "Critical"
            cell.secondOutlIerCount.text = "\(ngoOpenAlertsResponse?.critical ?? 0)"
            cell.view3.isHidden = true
            cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
            cell.secondBtn.tag = indexPath.section *  100 + indexPath.item * 10 + 2
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
            
            
        case 1:
            
            if(isSecondLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.initialStackView.isHidden = true
                cell.loader.isHidden = false
                cell.loader.startAnimating()
                
            }
            else
            {
                if(incidentError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = incidentError
                    cell.initialStackView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.initialStackView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            cell.viewdetailBtn.isHidden = false
            cell.view3.isHidden = true
            cell.headerLabel.text = "Open Incidents"
            cell.firstOutlIerName.text = "Buisness Impacting"
            cell.firstOutlIerCount.text = "\(OpenIncidenetsResponse?.businessImpactingCount ?? 0)"
            cell.secondOutlIerName.text = "Non-Buisness Impacting"
            cell.secondOutlIerCount.text = "\(OpenIncidenetsResponse?.nonBusinessImpactingCount ?? 0)"
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenIncidents), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenIncidents), for: .touchUpInside)
            cell.detailFifthView.isHidden = true
            cell.rejectedName.text = "Severity -1"
            cell.inProgressName.text = "Severity -2"
            cell.tvPendingName.text = "Severity -3"
            cell.fourthOutierName.text = "Severity -4"
            cell.rejectedCount.text = "\(OpenIncidenetsResponse?.s1Count ?? 0)"
            cell.inProgressCount.text = "\(OpenIncidenetsResponse?.s2Count ?? 0)"
            cell.tvPendingCount.text = "\(OpenIncidenetsResponse?.s3Count ?? 0)"
            cell.fourthOutlierCount.text = "\(OpenIncidenetsResponse?.s4Count ?? 0)"
            cell.secondLastView.isHidden = false
            cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 3
            cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 4
            
            
        case 2:
            
            if(isThirdLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.initialStackView.isHidden = true
                cell.loader.isHidden = false
                cell.loader.startAnimating()
                
            }
            else
            {
                if(changeError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = changeError
                    cell.initialStackView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.initialStackView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            
            cell.headerLabel.text = "Open Changes"
            cell.viewdetailBtn.isHidden = false
            cell.view3.isHidden = false
            cell.firstOutlIerName.text = "Emergency"
            cell.firstOutlIerCount.text = "\(openChangeResponse?.emergency ?? 0)"
            cell.secondOutlIerName.text = "Normal"
            cell.secondOutlIerCount.text = "\(openChangeResponse?.normal ?? 0)"
            cell.thirdOutlIerName.text = "MACD"
            cell.thirdOutlIerCount.text = "\(openChangeResponse?.macD ?? 0)"
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenChanges), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenChanges), for: .touchUpInside)
            cell.thirdBtn.addTarget(self, action: #selector(ngoDetailOpenChanges), for: .touchUpInside)
            cell.detailFifthView.isHidden = true
            cell.rejectedName.text = "Pending Closure"
            cell.inProgressName.text = "Pending Implementation"
            cell.rejectedCount.text = "\(openChangeResponse?.pendingClosure ?? 0)"
            cell.inProgressCount.text = "\(openChangeResponse?.pendingImplementaion ?? 0)"
            cell.secondLastView.isHidden = true
            cell.secondThirdView.isHidden = true
            cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 5
            cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 6
            cell.thirdBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 7
            cell.rejectedBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 8
            cell.inProgressBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 9
            cell.rejectedBtn.addTarget(self, action: #selector(ngoDetailOpenChanges), for: .touchUpInside)
            cell.inProgressBtn.addTarget(self, action: #selector(ngoDetailOpenChanges), for: .touchUpInside)
   
            
        case 3:
            cell.cnstViewDetailsHeight.constant = 0
            if(isFourthLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.initialStackView.isHidden = true
                cell.loader.isHidden = false
                cell.loader.startAnimating()
                
            }
            else
            {
                if(changeError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = defectError
                    cell.initialStackView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.initialStackView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            cell.viewdetailBtn.isHidden = true
            cell.view3.isHidden = true
            cell.headerLabel.text = "Open Defects"
            
            cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 10
            cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 11
            cell.firstOutlIerName.text = "Hot Fix"
            cell.firstOutlIerCount.text = "\(opendefectsResponse?.hotFix ?? 0)"
            cell.secondOutlIerName.text = "Defects"
            cell.secondOutlIerCount.text = "\(opendefectsResponse?.defect ?? 0)"
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenDefects), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenDefects), for: .touchUpInside)
            
        case 4:
            cell.cnstViewDetailsHeight.constant = 0
            if(isFiveLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.initialStackView.isHidden = true
                cell.loader.isHidden = false
                cell.loader.startAnimating()
                
            }
            else
            {
                if(problemError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = problemError
                    cell.initialStackView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.initialStackView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            cell.viewdetailBtn.isHidden = true
            cell.headerLabel.text = "Problem Tickets"
            cell.view3.isHidden = true
            cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 12
            cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 13
            cell.firstOutlIerName.text = "Pending RCA"
            cell.firstOutlIerCount.text = "\(problemTicketResponse?.pendingRCA ?? 0)"
            cell.secondOutlIerName.text = "RCA Action Item"
            cell.secondOutlIerCount.text = "\(problemTicketResponse?.rcaactionItem ?? 0)"
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailProblemTickets), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailProblemTickets), for: .touchUpInside)
            
        case 5:
            cell.cnstViewDetailsHeight.constant = 0
            if(isSixthLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.initialStackView.isHidden = true
                cell.loader.isHidden = false
                cell.loader.startAnimating()
                
            }
            else
            {
                if(actionError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = actionError
                    cell.initialStackView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.initialStackView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            cell.viewdetailBtn.isHidden = true
            cell.view3.isHidden = true
            cell.headerLabel.text = "Open Action Item"
            cell.view3.isHidden = true
            cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 14
            cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 15
            cell.firstOutlIerName.text = "Pending Action"
            cell.firstOutlIerCount.text = "\(openActionResponse?.pendingAction ?? 0)"
            cell.secondOutlIerName.text = "Overdue Action"
            cell.secondOutlIerCount.text = "\(openActionResponse?.overdueAction ?? 0)"
            cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenActions), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(ngoDetailOpenActions), for: .touchUpInside)
            
            
            
        default:
            print("completed")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraint.constant = self.NGOTableView.contentSize.height + 40
        self.NGOTableView.setNeedsLayout()
        self.delegate?.viewDetailLoadNGO()
    }
}


