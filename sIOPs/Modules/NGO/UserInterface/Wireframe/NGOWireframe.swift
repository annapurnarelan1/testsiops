//
//  NGOWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGOWireFrame: NGOWireFrameProtocol {
   
    
    
    
    
    // MARK: NGOWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentNGOModule(fromView:AnyObject,baseVC:Any) -> BaseViewController {
        
        // Generating module components
        let view: NGOViewProtocol = NGOViewController.instantiate()
        let presenter: NGOPresenterProtocol & NGOInteractorOutputProtocol = NGOPresenter()
        let interactor: NGOInteractorInputProtocol = NGOInteractor()
        
        let wireFrame: NGOWireFrameProtocol = NGOWireFrame()
        
        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NGOViewController
        
        viewController.delegate = baseVC as? DashboardVC
        
        return viewController
    }
    
    func ngoDetailOpenAlerts(type:String,selected:String,response:NGOOpenALertsModel)
    {
        //NGODetailWireFrame.presentNGODetailModule(fromView: self, type: type, selected: selected, ngoResponse: ngoResponse)
        OpenAlertsWireFrame.presentOpenAlertsModule(fromView:self,type:type,selected:selected,response:response)
    }
    
    func ngoDeliquency(type:String,selected:String,ngoResponse:NGO)
    {
        DeliquencyWireFrame.presentDeliquencyModule(fromView:self,type:type,ngoResponse:ngoResponse,selected:selected)
    }
    
    
    func ngoDetailOpenIncidents(type:String,selected:String,response:OpenIncidentModel)
       {
           NGOOpenIncidentDetailWireframe.presentNGOOpenIncidentDetailModule(fromView: self, type: type, selected: selected, response: response)
       }
    
    func ngoDetailOpenChanges(type:String,selected:String,response:OpenChangeModel)
    {
        OpenChangesWireFrame.presentOpenChangesModule(fromView: self, type: type, selected: selected, response: response)

    }
    
    
    
    
    func ngoDetailOpendefects(type: String, selected: String, response: OpenDefectModel) {
            OpenDefectsWireFrame.presentOpenDefectsModule(fromView:self,type:type,selected:selected,response:response)
       }
    
    func ngoDetailOpenActions(type:String,selected:String,response:OpenActionModel)
       {
        OpenActionsDetailsWireFrame.presentOpenActionsDetailsModule(fromView:self,type:type,selected: selected,response: response)
       }
    
    
    func ngoDetailProblemTickets(type:String,selected:String,response:ProblemTickenModel)
    {
        ProblemTicketsDetailsWireFrame.presentProblemTicketsDetailsModule(fromView:self,type:type,selected: selected,response: response)
    }
}

