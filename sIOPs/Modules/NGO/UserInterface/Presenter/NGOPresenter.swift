//
//  NGOPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGOPresenter:BasePresenter, NGOPresenterProtocol, NGOInteractorOutputProtocol {
    
    
   
    
    

    // MARK: Variables
    weak var view: NGOViewProtocol?
    var interactor: NGOInteractorInputProtocol?
    var wireFrame: NGOWireFrameProtocol?
    let stringsTableName = "NGO"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
    func requestData()
    {
        self.interactor?.requestData()
    }
    
    func reloadData(NGOResponse:NGO)
    {
        self.view?.reloadData(NGOResponse:NGOResponse)
    }
    
    func ngoDetailOpenAlerts(type:String,selected:String,response:NGOOpenALertsModel)
    {
        self.wireFrame?.ngoDetailOpenAlerts(type: type, selected: selected, response: response)
    }
    
    func ngoDeliquency(type:String,selected:String,ngoResponse:NGO)
    {
        self.wireFrame?.ngoDeliquency(type: type, selected: selected, ngoResponse: ngoResponse)
    }
    
    func requestHistoryData()
    {
        self.interactor?.requestHistoryData()
    }
    
    func reloadHistoryData(NGOResponse:NGO)
    {
        self.view?.reloadHistoryData(NGOResponse:NGOResponse)
    }
    
    
    func reloadOpenALerts(response:NGOOpenALertsModel)
    {
        self.view?.reloadOpenALerts(response:response)
    }
    
    func requestNGOOpenAlerts()
    {
        self.interactor?.requestNGOOpenAlerts()
    }
    func requestNGOOpenIncidents()
    {
        self.interactor?.requestNGOOpenIncidents()
    }
    
    func reloadOpenIncidents(response:OpenIncidentModel)
    {
        self.view?.reloadOpenIncidents(response:response)
    }
    
    func ngoDetailOpenIncidents(type:String,selected:String,response:OpenIncidentModel)
    {
        self.wireFrame?.ngoDetailOpenIncidents(type:type,selected:selected,response:response)
    }
    
    
    func showAlertError(error:String)
    {
        self.view?.showAlertError(error:error)
    }
    func showIncidentError(error:String)
    {
        self.view?.showIncidentError(error:error)
    }
    func showChangesError(error:String)
    {
        self.view?.showChangesError(error:error)
    }
    func showDefectsError(error:String)
    {
        self.view?.showDefectsError(error:error)
    }
    func showProblemError(error:String)
    {
        self.view?.showProblemError(error:error)
    }
    func showActionError(error:String)
    {
        self.view?.showActionError(error:error)
    }
    
    func requestNGOOpenChange() {
        self.interactor?.requestNGOOpenChange() 
    }
    
    func reloadOpenChanges(response:OpenChangeModel)
    {
        self.view?.reloadOpenChanges(response:response)
    }
    
    func ngoDetailOpenChanges(type:String,selected:String,response:OpenChangeModel)
    {
        self.wireFrame?.ngoDetailOpenChanges(type:type,selected:selected,response:response)
    }
    
    func requestNGOOpenDefects()
    {
        self.interactor?.requestNGOOpenDefects()
    }
    
    func reloadOpenDefects(response:OpenDefectModel)
    {
        self.view?.reloadOpenDefects(response:response)
    }
   func ngoDetailOpenDefects(type:String,selected:String,response:OpenDefectModel)
    {
        self.wireFrame?.ngoDetailOpendefects(type:type,selected:selected,response:response)
    }
    
    func requestProblemTicket()
    {
        self.interactor?.requestProblemTicket()
    }
    
    func reloadProblemTickets(response:ProblemTickenModel)
    {
        self.view?.reloadProblemTickets(response:response)
    }
    
    func requestOpenAction()
    {
        self.interactor?.requestOpenAction()
    }
    
    func reloadOpenActions(response: OpenActionModel) {
        self.view?.reloadOpenActions(response: response)
    }
    
    func ngoDetailOpenActions(type:String,selected:String,response:OpenActionModel)
    {
        self.wireFrame?.ngoDetailOpenActions(type:type,selected:selected,response:response)
    }
    
    func ngoDetailProblemTickets(type:String,selected:String,response:ProblemTickenModel)
    {
        self.wireFrame?.ngoDetailProblemTickets(type:type,selected:selected,response:response)
    }
}
