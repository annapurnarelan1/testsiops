//
//  OpenAlertsDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenActionsDetailsViewProtocol: class {
    var presenter: OpenActionsDetailsPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenActionsDetailsWireFrameProtocol: class {
    static func presentOpenActionsDetailsModule(fromView:AnyObject,type:String,selected: String,response: OpenActionModel)
}

/// Method contract between VIEW -> PRESENTER
protocol OpenActionsDetailsPresenterProtocol: class {
    var view: OpenActionsDetailsViewProtocol? { get set }
    var interactor: OpenActionsDetailsInteractorInputProtocol? { get set }
    var wireFrame: OpenActionsDetailsWireFrameProtocol? { get set }
    func loadData(type:String , model: NGOOpenALertsModel)
    func requestHistoryData(type:String)
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenActionsDetailsInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:[Acknowledged])
}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenActionsDetailsInteractorInputProtocol: class
{
    var presenter: OpenActionsDetailsInteractorOutputProtocol? { get set }
    func loadData(type:String , model: NGOOpenALertsModel)
    func requestHistoryData(type:String)
}
