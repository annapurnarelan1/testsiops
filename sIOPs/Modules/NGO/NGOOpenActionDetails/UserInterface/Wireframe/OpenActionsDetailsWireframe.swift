//
//  OpenAlertsHistoryWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenActionsDetailsWireFrame: OpenActionsDetailsWireFrameProtocol {
 
        // MARK: OpenActionsDetailsWireFrameProtocol
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenActionsDetailsModule(fromView:AnyObject,type:String,selected: String,response: OpenActionModel) {

        let view: OpenActionsDetailsViewProtocol = OpenActionsDetailsVC.instantiate()
        let presenter: OpenActionsDetailsPresenterProtocol & OpenActionsDetailsInteractorOutputProtocol = OpenActionsDetailsPresenter()
        let interactor: OpenActionsDetailsInteractorInputProtocol = OpenActionsDetailsInteractor()
        let wireFrame: OpenActionsDetailsWireFrameProtocol = OpenActionsDetailsWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OpenActionsDetailsVC
        viewController.type = type
        viewController.selected = selected
        viewController.actionModelResponse = response
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
}
