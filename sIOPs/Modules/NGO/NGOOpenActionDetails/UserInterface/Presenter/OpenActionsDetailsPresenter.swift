//
//  OpenAlertsDetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OpenActionsDetailsPresenter:BasePresenter, OpenActionsDetailsPresenterProtocol, OpenActionsDetailsInteractorOutputProtocol {
    func requestHistoryData(type: String) {
        
    }
    
   
    // MARK: Variables
    weak var view: OpenActionsDetailsViewProtocol?
    var interactor: OpenActionsDetailsInteractorInputProtocol?
    var wireFrame: OpenActionsDetailsWireFrameProtocol?
    let stringsTableName = "OpenActionsDetails"
   
    /// Called to show error popup
    func show(error: String,description:String)
    {
      self.view?.show(image: "error_popup", error: error, description: description)
    }
       /// Called when there is no network
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    func loadData(type:String ,  model: NGOOpenALertsModel)
    {
        self.interactor?.loadData(type:type ,  model: model)
    }
    func reloadData(detail:[Acknowledged])
    {
       // self.view?.reloadData(detail:detail)
    }
//    func requestHistoryData(type:String)
//    {
//        self.interactor?.requestHistoryData(type:type)
//    }
}
