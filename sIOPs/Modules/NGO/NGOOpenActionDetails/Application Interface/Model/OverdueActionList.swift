//
//	OverdueActionList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OverdueActionList : NSObject, NSCoding{

	var action : String!
	var aging : Int!
	var assignee : String!
	var calSTATUS : String!
	var createdon : String!
	var meetingid : String!
	var targetdate : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		action = dictionary["action"] as? String
		aging = dictionary["aging"] as? Int
		assignee = dictionary["assignee"] as? String
		calSTATUS = dictionary["cal_STATUS"] as? String
		createdon = dictionary["createdon"] as? String
		meetingid = dictionary["meetingid"] as? String
		targetdate = dictionary["targetdate"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if action != nil{
			dictionary["action"] = action
		}
		if aging != nil{
			dictionary["aging"] = aging
		}
		if assignee != nil{
			dictionary["assignee"] = assignee
		}
		if calSTATUS != nil{
			dictionary["cal_STATUS"] = calSTATUS
		}
		if createdon != nil{
			dictionary["createdon"] = createdon
		}
		if meetingid != nil{
			dictionary["meetingid"] = meetingid
		}
		if targetdate != nil{
			dictionary["targetdate"] = targetdate
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         action = aDecoder.decodeObject(forKey: "action") as? String
         aging = aDecoder.decodeObject(forKey: "aging") as? Int
         assignee = aDecoder.decodeObject(forKey: "assignee") as? String
         calSTATUS = aDecoder.decodeObject(forKey: "cal_STATUS") as? String
         createdon = aDecoder.decodeObject(forKey: "createdon") as? String
         meetingid = aDecoder.decodeObject(forKey: "meetingid") as? String
         targetdate = aDecoder.decodeObject(forKey: "targetdate") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if action != nil{
			aCoder.encode(action, forKey: "action")
		}
		if aging != nil{
			aCoder.encode(aging, forKey: "aging")
		}
		if assignee != nil{
			aCoder.encode(assignee, forKey: "assignee")
		}
		if calSTATUS != nil{
			aCoder.encode(calSTATUS, forKey: "cal_STATUS")
		}
		if createdon != nil{
			aCoder.encode(createdon, forKey: "createdon")
		}
		if meetingid != nil{
			aCoder.encode(meetingid, forKey: "meetingid")
		}
		if targetdate != nil{
			aCoder.encode(targetdate, forKey: "targetdate")
		}

	}

}