//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenActionModel : NSObject, NSCoding{

	var overdueAction : Int!
	var overdueActionList : [OverdueActionList]!
	var pendingAction : Int!
	var pendingActionList : [OverdueActionList]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		overdueAction = dictionary["overdueAction"] as? Int
		overdueActionList = [OverdueActionList]()
		if let overdueActionListArray = dictionary["overdueActionList"] as? [[String:Any]]{
			for dic in overdueActionListArray{
				let value = OverdueActionList(fromDictionary: dic)
				overdueActionList.append(value)
			}
		}
		pendingAction = dictionary["pendingAction"] as? Int
		pendingActionList = [OverdueActionList]()
		if let pendingActionListArray = dictionary["pendingActionList"] as? [[String:Any]]{
			for dic in pendingActionListArray{
				let value = OverdueActionList(fromDictionary: dic)
				pendingActionList.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if overdueAction != nil{
			dictionary["overdueAction"] = overdueAction
		}
		if overdueActionList != nil{
			var dictionaryElements = [[String:Any]]()
			for overdueActionListElement in overdueActionList {
				dictionaryElements.append(overdueActionListElement.toDictionary())
			}
			dictionary["overdueActionList"] = dictionaryElements
		}
		if pendingAction != nil{
			dictionary["pendingAction"] = pendingAction
		}
		if pendingActionList != nil{
			var dictionaryElements = [[String:Any]]()
			for pendingActionListElement in pendingActionList {
				dictionaryElements.append(pendingActionListElement.toDictionary())
			}
			dictionary["pendingActionList"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         overdueAction = aDecoder.decodeObject(forKey: "overdueAction") as? Int
         overdueActionList = aDecoder.decodeObject(forKey :"overdueActionList") as? [OverdueActionList]
         pendingAction = aDecoder.decodeObject(forKey: "pendingAction") as? Int
         pendingActionList = aDecoder.decodeObject(forKey :"pendingActionList") as? [OverdueActionList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if overdueAction != nil{
			aCoder.encode(overdueAction, forKey: "overdueAction")
		}
		if overdueActionList != nil{
			aCoder.encode(overdueActionList, forKey: "overdueActionList")
		}
		if pendingAction != nil{
			aCoder.encode(pendingAction, forKey: "pendingAction")
		}
		if pendingActionList != nil{
			aCoder.encode(pendingActionList, forKey: "pendingActionList")
		}

	}

}
