//
//  OpenAlertsHistoryWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenDefectsHistoryWireFrame: OpenDefectsHistoryWireFrameProtocol {
   

    // MARK: OpenAlertsHistoryWireFrameProtocol
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenDefectsHistoryModule(fromView: AnyObject, type: String, count: String, response: [OpenDefectsDetailList]) {

        let view: OpenDefectsHistoryViewProtocol = OpenDefectsHistoryVC.instantiate()
        let presenter: OpenDefectsHistoryPresenterProtocol & OpenDefectsHistoryInteractorOutputProtocol = OpenDefectsHistoryPresenter()
        let interactor: OpenDefectsHistoryInteractorInputProtocol = OpenDefectsHistoryInteractor()
        let wireFrame: OpenDefectsHistoryWireFrameProtocol = OpenDefectsHistoryWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OpenDefectsHistoryVC
        viewController.type = type
        
        viewController.count = count
        
        viewController.selectedArray = response
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
