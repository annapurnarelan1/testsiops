//
//  OpenAlertsDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class OpenDefectsHistoryVC: BaseViewController,OpenDefectsHistoryViewProtocol {
 
    

    
    
    var presenter: OpenDefectsHistoryPresenterProtocol?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblViewDate: UILabel!
    @IBOutlet weak var lblViewTitle: UILabel!
    var arrHistory : [String] = []
    var type:String?
    var selected:String?
    var date:String?
    var isFromYes = false
    var respoonseType : Any?
    var openAlertsResponse:NGOOpenALertsModel?
    var senderSelectedDetailsIndex:Int?
    var senderSelectedTableIndex : Int?
    var openAlertsResponseYesterday: OpenAlertYesterdayModel?
    var selectedArray:[OpenDefectsDetailList]?
   
    var count : String?
  
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ?? UIFont.systemFont(ofSize: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> OpenDefectsHistoryViewProtocol{
        return UIStoryboard(name: "OpenDefectsHistory", bundle: nil).instantiateViewController(withIdentifier: "OpenDefectsHistoryVC") as! OpenDefectsHistoryVC
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ChangeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ChangeDetailTableViewCell")
     //   startAnimating()
        
//        if let responseRecieved = respoonseType {
//            self.presenter?.loadData(type: type ?? "" , model: responseRecieved)
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           HelperMethods().removeCustomBarButtons(self)
           HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
           self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
           self.navigationController?.interactivePopGestureRecognizer?.delegate = self
           self.navigationController?.setNavigationBarHidden(false, animated: false)
           self.navigationItem.setHidesBackButton(false, animated:true)
           self.lblTitle.text = type
           self.lblCount.text = count
           self.lblViewDate.text = date
        let titleLabel = String(UserDefaults.standard.string(forKey: "openAlertType") ?? "")
        self.lblViewTitle.text = "Service Glance - \(titleLabel)  Alerts"
    }

     func stopLoader() {
        stopAnimating()
    }
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    
    func reloadData(detail: [Acknowledged]) {
        stopAnimating()
//        self.selectedArray = detail
//        self.view.setNeedsLayout()
//        self.tableView.reloadData()
    }
    func reloadDataYesterday(detail: [OpenAlertYesterdayApplication]) {
         stopAnimating()
//        self.selectedYesArray = detail
//        self.view.setNeedsLayout()
//        tableView.reloadData()
     }
    
    func reloadHistoryData()  {
        stopAnimating()
       // arrHistory
        tableView.reloadData()
    }
    
    @objc func makeCall(_ sender:UIButton)
    {
//        if let phoneCallURL = URL(string: "telprompt://\(self.selectedArray?[sender.tag].ackByMobile ?? "")") {
//
//            let application:UIApplication = UIApplication.shared
//            if (application.canOpenURL(phoneCallURL)) {
//                if #available(iOS 10.0, *) {
//                    application.open(phoneCallURL, options: [:], completionHandler: nil)
//                } else {
//                    // Fallback on earlier versions
//                     application.openURL(phoneCallURL as URL)
//
//                }
//            }
//        }
    }
    
    
     @objc func expandDetailView(_ sender:UIButton)
        {
            if(sender.tag != senderSelectedDetailsIndex)
             {
                 senderSelectedDetailsIndex = sender.tag
                let point = tableView.convert(sender.center, from: sender.superview!)
                if let wantedIndexPath = tableView.indexPathForRow(at: point)  {
                    let cell = tableView.cellForRow(at: wantedIndexPath) as! ChangeDetailTableViewCell
                    //cell.textView.isHidden = false
                    tableView.reloadData()
                }
                 //startAnimating()
                 //self.presenter?.requestAlarmDownHistory(sapId: selectedArray?[sender.tag].sapId ?? "", id: alamreasonObject?.id ?? "")
             }
             else
             {
                 senderSelectedDetailsIndex = -1
                 self.tableView.reloadData()
             }
    }
}

extension OpenDefectsHistoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell:ChangeDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChangeDetailTableViewCell", for: indexPath as IndexPath) as! ChangeDetailTableViewCell
        
               cell.alertBtn.tag = indexPath.section
                  let response = selectedArray?[indexPath.section]
                  
                   cell.firstViewFirstDescriptionLabel.text = response?.defectid
                   cell.firstViewSecondDescriptionLabel.text = response?.status
                   cell.firstViewThirdTitleLabel.text = "Severity"
                   cell.firstViewThirdDescriptionLabel.text = response?.defectseverity
                   cell.ageingView.isHidden = false
        cell.SecondViewFirstTitleLabel.text = "Bucket"
                   cell.SecondViewFirstDescriptionLabel.text = response?.bucket ?? ""
                   cell.closedDate.isHidden = false
        cell.SecondViewThirdTitleLabel.text = "Due Date"
        cell.SecondViewThirdDescriptionLabel.text = response?.dueDATE ?? ""
                   cell.SecondViewSecondDescriptionLabel.text = response?.creationDATE ?? ""
                   cell.SecondViewFourthTitleLabel.text = "Application"
                   cell.impactDescriptionLabel.text = response?.requestCATEGORYNAME
                   cell.descriptionLabel.text = response?.summary ?? ""
        
       
        cell.alertBtn.tag = indexPath.section
        
         
         cell.alertBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
       if(senderSelectedDetailsIndex ==  cell.alertBtn.tag)
        {
            let buttonTitleStr = NSMutableAttributedString(string:"Hide Details", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = false
            //cell.dataArray = self.detailList
            //cell.dayTblView.reloadData()
        }
        else
        {
            let buttonTitleStr = NSMutableAttributedString(string:"Show Details", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = true
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if  let cell = tableView.cellForRow(at: indexPath)  as? ChangeDetailTableViewCell
        {
//            cell.heightConstraint.constant = cell.tblvHistory.contentSize.height
//            cell.tblvHistory.layoutIfNeeded()
        }

    }
}
