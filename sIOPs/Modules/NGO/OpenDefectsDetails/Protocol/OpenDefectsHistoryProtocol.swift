//
//  OpenAlertsDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenDefectsHistoryViewProtocol: class {
    var presenter: OpenDefectsHistoryPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:[Acknowledged])
    func reloadDataYesterday(detail:[OpenAlertYesterdayApplication])
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenDefectsHistoryWireFrameProtocol: class {
    static func presentOpenDefectsHistoryModule(fromView: AnyObject, type: String, count: String, response: [OpenDefectsDetailList])
}

/// Method contract between VIEW -> PRESENTER
protocol OpenDefectsHistoryPresenterProtocol: class {
    var view: OpenDefectsHistoryViewProtocol? { get set }
    var interactor: OpenDefectsHistoryInteractorInputProtocol? { get set }
    var wireFrame: OpenDefectsHistoryWireFrameProtocol? { get set }
    func loadData(type:String , model: Any)
    func requestHistoryData(type:String)
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenDefectsHistoryInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:[Acknowledged])
    func reloadDataYesterday(detail:[OpenAlertYesterdayApplication])
}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenDefectsHistoryInteractorInputProtocol: class
{
    var presenter: OpenDefectsHistoryInteractorOutputProtocol? { get set }
    func loadData(type:String , model: Any)
    func requestHistoryData(type:String)
}
