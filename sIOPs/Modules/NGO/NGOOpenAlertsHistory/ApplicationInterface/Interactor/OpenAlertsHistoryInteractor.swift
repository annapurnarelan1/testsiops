//
//  OpenAlertsDetailDetailInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

enum AlertTypes : String{
    case infrastructure = "Infrastructure"
    case application = "Application"
    case tools  = "Tools"
    case acknowledged = "Acknowledged"
    case unacknowledged = "Unacknowledged"
    case engineer  = "Enigineer"
    case system = "System"
    case met  = "MET"
    case missed = "Missed"
    case closed = "Closed"
    case open = "Open"
}

class OpenAlertsHistoryInteractor: OpenAlertsHistoryInteractorInputProtocol {

public enum OpenAlertsHistoryError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: OpenAlertsHistoryInteractorOutputProtocol?
    
    var arrHistoryDetails : [Acknowledged] = []
    var YesterDetails : [OpenAlertYesterdayApplication] = []
   func loadData(type:String ,  model: Any){
        
    switch AlertTypes(rawValue:type) {
    case .infrastructure:
        if let alertDetail = model as? NGOOpenALertsModel {
             if alertDetail.infrastructure.count > 0 {
                 self.arrHistoryDetails = alertDetail.infrastructure
                 self.presenter?.reloadData(detail: arrHistoryDetails)
               }
            else
            {
                self.presenter?.stopLoader()
            }
         }
    case .application:
        if let alertDetail = model as? NGOOpenALertsModel {
            if alertDetail.application.count > 0 {
                self.arrHistoryDetails = alertDetail.application
                self.presenter?.reloadData(detail: arrHistoryDetails)
              }
            else
            {
                self.presenter?.stopLoader()
            }
        }
    case  .tools:
        if let alertDetail = model as? NGOOpenALertsModel {
            if alertDetail.tools.count > 0 {
                self.arrHistoryDetails = alertDetail.tools
                self.presenter?.reloadData(detail: arrHistoryDetails)
              }
            else
            {
                self.presenter?.stopLoader()
            }
        }
    case .acknowledged :
        if let alertDetail = model as? NGOOpenALertsModel {
            if alertDetail.acknowledged.count > 0 {
                self.arrHistoryDetails = alertDetail.acknowledged
                self.presenter?.reloadData(detail: arrHistoryDetails)
              }
            else
            {
                self.presenter?.stopLoader()
            }
        }
    case .unacknowledged:
        if let alertDetail = model as? NGOOpenALertsModel {
                if alertDetail.unAcknowledged.count > 0 {
                    self.arrHistoryDetails = alertDetail.unAcknowledged
                    self.presenter?.reloadData(detail: arrHistoryDetails)
                  }
            else
            {
                self.presenter?.stopLoader()
            }
            }
    case .met:
        if let alertDetail = model as? NGOOpenALertsModel {
                if alertDetail.met.count > 0 {
                    self.arrHistoryDetails = alertDetail.met
                    self.presenter?.reloadData(detail: arrHistoryDetails)
                  }
            else
            {
                self.presenter?.stopLoader()
            }
            }
    case .missed:
        if let alertDetail = model as? NGOOpenALertsModel {
                if alertDetail.missed.count > 0 {
                    self.arrHistoryDetails = alertDetail.missed
                    self.presenter?.reloadData(detail: arrHistoryDetails)
                  }
            else
            {
                self.presenter?.stopLoader()
            }
            }
    default:
        print("in Default")
    }
    
 
    
    if let alertYesDetail = model as? OpenAlertYesterdayModel {
        
        switch AlertTypes(rawValue:type) {
        case .infrastructure:
            
            if alertYesDetail.infrastructure.count > 0 {
                     self.YesterDetails = alertYesDetail.infrastructure
                     self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                   }
            else
            {
                self.presenter?.stopLoader()
            }
          
        case .application:
                if alertYesDetail.application.count > 0 {
                    self.YesterDetails = alertYesDetail.application
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                  }
            else
            {
                self.presenter?.stopLoader()
            }
            
        case  .tools:
                if alertYesDetail.tools.count > 0 {
                    self.YesterDetails = alertYesDetail.tools
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                  }
            else
            {
                self.presenter?.stopLoader()
            }
            
        case .closed :
                if alertYesDetail.closed.count > 0 {
                    self.YesterDetails = alertYesDetail.closed
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                  }
            else
            {
                self.presenter?.stopLoader()
            }
        case .open:
                if alertYesDetail.open.count > 0 {
                    self.YesterDetails = alertYesDetail.open
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
            }
            else
            {
                self.presenter?.stopLoader()
            }
        case .engineer:
                if alertYesDetail.engineer.count > 0 {
                    self.YesterDetails = alertYesDetail.engineer
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                  
            }
            else
            {
                self.presenter?.stopLoader()
            }
        case .system:
                if alertYesDetail.system.count > 0 {
                self.YesterDetails = alertYesDetail.system
                self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
            }
            else
            {
                self.presenter?.stopLoader()
            }
            case .unacknowledged:
                if alertYesDetail.unAcknowledged.count > 0 {
                self.YesterDetails = alertYesDetail.unAcknowledged
                self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                }
            else
            {
                self.presenter?.stopLoader()
            }
            case .met:
                if alertYesDetail.met.count > 0 {
                    self.YesterDetails = alertYesDetail.met
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                }
            else
            {
                self.presenter?.stopLoader()
            }
            case .missed:
                    if alertYesDetail.missed.count > 0 {
                    self.YesterDetails = alertYesDetail.missed
                    self.presenter?.reloadDataYesterday(detail: self.YesterDetails)
                }
            else
            {
                self.presenter?.stopLoader()
            }
        default:
            print("in Default")
        }
        
    }
}
    
    func requestHistoryData(type:String){

        let applicationArray = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.applications
             //  usefullLinksCollectionView.reloadData()
        let dic = applicationArray?.filter { $0.applicationUCode == "F_NGO_NextGenOPS" }

        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]

        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]

        var application : LoginApplication?

        if(dic?.count ?? 0 > 0)
        {
            application = dic?[0]
        }
        let requestBody:[String : Any] = [

            "appRoleCode": application?.applicationCode ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            "outlierType":type
        ]

        let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]

        let busicode =  Constants.BusiCode.NGOAlertHistory


        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]

        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]

        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)

                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK

                switch status {
                case Constants.status.OK:
                        let openAlertsDetaildata = NGOClickHistory.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                       // self.presenter?.reloadData(detail:openAlertsDetaildata)
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }

           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)

                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)

                }
            }
        })

    }

//
//    func requestDataByCategory(type:String){
//
//        let applicationArray = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.applications
//             //  usefullLinksCollectionView.reloadData()
//        let dic = applicationArray?.filter { $0.applicationUCode == "F_NGO_NextGenOPS" }
//
//        let pubInfo: [String : Any] =
//            ["timestamp": String(Date().ticks),
//            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
//            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
//            "osType": "ios",
//            "lang": "en_US",
//            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
//
//        let requestHeader:[String :Any] = [
//            "serviceName": "userInfo"
//        ]
//
//        var application : LoginApplication?
//
//        if(dic?.count ?? 0 > 0)
//        {
//            application = dic?[0]
//        }
//        let requestBody:[String : Any] = [
//
//            "appRoleCode": application?.applicationCode ?? "",
//            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
//            "type": "userInfo",
//            "outlierType":type
//        ]
//
//        let busiParams: [String : Any] = [
//            "requestHeader": requestHeader,
//            "requestBody": requestBody,
//        ]
//
//        let busicode =  Constants.BusiCode.NGOOnclickType
//
//
//        let requestList: [String: Any] = [
//            "busiCode": busicode,
//            "isEncrypt": false,
//            "transactionId": "0001574162779054",
//            "busiParams":busiParams
//        ]
//
//        let jsonParameter = [
//            "pubInfo" : pubInfo,
//            "requestList" : [requestList]
//            ] as [String : Any]
//
//        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
//            switch result {
//            case .success(let returnJson) :
//                print(returnJson)
//
//
//
//                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
//
//                switch status {
//                case Constants.status.OK:
//                        let openAlertsDetaildata = NGOClickHistory.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//                        self.presenter?.reloadData(detail:openAlertsDetaildata)
//                  case Constants.status.NOK:
//                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
//                        self.presenter?.showFailError(error:message)
//                default:
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                }
//
//
//
//
//           case .failure(let failure) :
//                switch failure {
//                case .connectionError:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
//
//                case .authorizationError(let errorJson):
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .serverError:
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .newUpdate:
//                    self.presenter?.stopLoader()
//                default:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
//
//                }
//            }
//        })
//
//    }
//
    
    

    
}
