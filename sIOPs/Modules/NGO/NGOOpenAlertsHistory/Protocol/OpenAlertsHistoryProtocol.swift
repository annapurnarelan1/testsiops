//
//  OpenAlertsDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenAlertsHistoryViewProtocol: class {
    var presenter: OpenAlertsHistoryPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:[Acknowledged])
    func reloadDataYesterday(detail:[OpenAlertYesterdayApplication])
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenAlertsHistoryWireFrameProtocol: class {
    static func presentOpenAlertsHistoryModule(fromView:AnyObject,type:String,date: String, count: String, isFromYesterday: Bool, response: Any)
}

/// Method contract between VIEW -> PRESENTER
protocol OpenAlertsHistoryPresenterProtocol: class {
    var view: OpenAlertsHistoryViewProtocol? { get set }
    var interactor: OpenAlertsHistoryInteractorInputProtocol? { get set }
    var wireFrame: OpenAlertsHistoryWireFrameProtocol? { get set }
    func loadData(type:String , model: Any)
    func requestHistoryData(type:String)
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenAlertsHistoryInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:[Acknowledged])
    func reloadDataYesterday(detail:[OpenAlertYesterdayApplication])
}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenAlertsHistoryInteractorInputProtocol: class
{
    var presenter: OpenAlertsHistoryInteractorOutputProtocol? { get set }
    func loadData(type:String , model: Any)
    func requestHistoryData(type:String)
}
