//
//  OpenAlertsHistoryWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenAlertsHistoryWireFrame: OpenAlertsHistoryWireFrameProtocol {

    // MARK: OpenAlertsHistoryWireFrameProtocol
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenAlertsHistoryModule(fromView:AnyObject,type:String,date: String, count: String , isFromYesterday: Bool,response: Any) {

        let view: OpenAlertsHistoryViewProtocol = OpenAlertsHistoryVC.instantiate()
        let presenter: OpenAlertsHistoryPresenterProtocol & OpenAlertsHistoryInteractorOutputProtocol = OpenAlertsHistoryPresenter()
        let interactor: OpenAlertsHistoryInteractorInputProtocol = OpenAlertsHistoryInteractor()
        let wireFrame: OpenAlertsHistoryWireFrameProtocol = OpenAlertsHistoryWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OpenAlertsHistoryVC
        viewController.type = type
        viewController.isFromYes = isFromYesterday
        viewController.count = count
        viewController.respoonseType = response
        viewController.date = date
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
