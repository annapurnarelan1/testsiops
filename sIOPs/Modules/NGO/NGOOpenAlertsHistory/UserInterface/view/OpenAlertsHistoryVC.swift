//
//  OpenAlertsDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class OpenAlertsHistoryVC: BaseViewController,OpenAlertsHistoryViewProtocol {
 
    

    
    
    var presenter: OpenAlertsHistoryPresenterProtocol?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblViewDate: UILabel!
    @IBOutlet weak var lblViewTitle: UILabel!
    var arrHistory : [String] = []
    var type:String?
    var selected:String?
    var date:String?
    var isFromYes = false
    var respoonseType : Any?
    var openAlertsResponse:NGOOpenALertsModel?
    var senderSelectedDetailsIndex:Int?
    var senderSelectedTableIndex : Int?
    var openAlertsResponseYesterday: OpenAlertYesterdayModel?
    var selectedArray:[Acknowledged]?
    var selectedYesArray : [OpenAlertYesterdayApplication]?
    var count : String?
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ?? UIFont.systemFont(ofSize: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> OpenAlertsHistoryViewProtocol{
        return UIStoryboard(name: "OpenAlertsHistory", bundle: nil).instantiateViewController(withIdentifier: "OpenAlertsHistoryVC") as! OpenAlertsHistoryVC
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AlertHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "AlertHistoryTableViewCell")
        
        
        if let responseRecieved = respoonseType {
            startAnimating()
            self.presenter?.loadData(type: type ?? "" , model: responseRecieved)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           HelperMethods().removeCustomBarButtons(self)
           HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
           self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
           self.navigationController?.interactivePopGestureRecognizer?.delegate = self
           self.navigationController?.setNavigationBarHidden(false, animated: false)
           self.navigationItem.setHidesBackButton(false, animated:true)
           self.lblTitle.text = type
           self.lblCount.text = count
           self.lblViewDate.text = date
        let titleLabel = String(UserDefaults.standard.string(forKey: "openAlertType") ?? "")
        self.lblViewTitle.text = "Service Glance - \(titleLabel)  Alerts"
    }

     func stopLoader() {
        stopAnimating()
    }
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    
    func reloadData(detail: [Acknowledged]) {
        stopAnimating()
        self.selectedArray = detail
        self.view.setNeedsLayout()
        self.tableView.reloadData()
    }
    func reloadDataYesterday(detail: [OpenAlertYesterdayApplication]) {
         stopAnimating()
        self.selectedYesArray = detail
        self.view.setNeedsLayout()
        tableView.reloadData()
     }
    
    func reloadHistoryData()  {
        stopAnimating()
       // arrHistory
        tableView.reloadData()
    }
    
    @objc func makeCall(_ sender:UIButton)
    {
        if let phoneCallURL = URL(string: "telprompt://\(self.selectedArray?[sender.tag].ackByMobile ?? "")") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }
    
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        if(sender.tag != senderSelectedDetailsIndex)
         {
             senderSelectedDetailsIndex = sender.tag
            let point = tableView.convert(sender.center, from: sender.superview!)
            if let wantedIndexPath = tableView.indexPathForRow(at: point)  {
                let cell = tableView.cellForRow(at: wantedIndexPath) as! AlertHistoryTableViewCell
                //cell.textView.isHidden = false
                tableView.reloadData()
            }
             //startAnimating()
             //self.presenter?.requestAlarmDownHistory(sapId: selectedArray?[sender.tag].sapId ?? "", id: alamreasonObject?.id ?? "")
         }
         else
         {
             senderSelectedDetailsIndex = -1
             self.tableView.reloadData()
         }
        
        
    }
}

extension OpenAlertsHistoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell:AlertHistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlertHistoryTableViewCell", for: indexPath as IndexPath) as! AlertHistoryTableViewCell
        cell.alertBtn.tag = indexPath.section
        cell.heightConstraint.constant = 0
        cell.descriptionLabel?.text = isFromYes ? self.selectedYesArray?[indexPath.section].eventMessage  : self.selectedArray?[indexPath.section].eventMessage
        cell.idLbl?.text = (self.selectedArray?[indexPath.section].cname ?? "") + " " + "(\(self.selectedArray?[indexPath.section].ipAddress ?? ""))"

        cell.openAtLbl.text =  isFromYes ? self.selectedYesArray?[indexPath.section].createTime : self.selectedArray?[indexPath.section].createTime
        
        cell.nameLbl.text = isFromYes ? self.selectedYesArray?[indexPath.section].ackBY : self.selectedArray?[indexPath.section].ackBY
        
        
        cell.phone.tag = indexPath.section
        
        cell.timeLbl.text = isFromYes ? self.selectedYesArray?[indexPath.section].remaingTimeToAck : self.selectedArray?[indexPath.section].remaingTimeToAck
        
        cell.ageingHeadingLbl.text = isFromYes ? self.selectedYesArray?[indexPath.section].agingSinceInsert :  self.selectedArray?[indexPath.section].agingSinceInsert
        
        cell.acknolowledOnLbl.text = isFromYes ? self.selectedYesArray?[indexPath.section].ackON : self.selectedArray?[indexPath.section].ackON
        
        cell.applicationLabel.text = isFromYes ? self.selectedYesArray?[indexPath.section].appName : self.selectedArray?[indexPath.section].appName
        cell.alertBtn.tag = indexPath.section
        //cell.btnHistory.tag = 3
        
        if(senderSelectedDetailsIndex ==  cell.alertBtn.tag)
        {
            let buttonTitleStr = NSMutableAttributedString(string:"Hide History", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = false
            //cell.dataArray = self.detailList
            //cell.dayTblView.reloadData()
        }
        else
        {
            let buttonTitleStr = NSMutableAttributedString(string:"View History", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = true
        }
        
//        if(cell.alertBtn.isSelected)
//        {
//            let buttonTitleStr = NSMutableAttributedString(string:"Hide Details", attributes:attrs)
//            let attributedString = NSMutableAttributedString(string:"")
//            attributedString.append(buttonTitleStr)
//            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
//        }
//        else
//        {
//            let buttonTitleStr = NSMutableAttributedString(string:"Alert Details", attributes:attrs)
//            let attributedString = NSMutableAttributedString(string:"")
//            attributedString.append(buttonTitleStr)
//            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
//
//        }
//
//        if (cell.btnHistory.isSelected){
//            let buttonTitleStr = NSMutableAttributedString(string:"Hide History", attributes:attrs)
//            let attributedString = NSMutableAttributedString(string:"")
//            attributedString.append(buttonTitleStr)
//            cell.dataArray = self.arrHistory // after API call
//            cell.tblvHistory.estimatedRowHeight = 30
//            cell.heightConstraint.constant = 140
//            cell.btnHistory.setAttributedTitle(attributedString, for: .normal)
//        }
//        else{
//            let buttonTitleStr = NSMutableAttributedString(string:"show History", attributes:attrs)
//
//            let attributedString = NSMutableAttributedString(string:"")
//            attributedString.append(buttonTitleStr)
//            cell.btnHistory.setAttributedTitle(attributedString, for: .normal)
//        }
        if(isFromYes ? self.selectedYesArray?[indexPath.section].ackStatus == "1" : self.selectedArray?[indexPath.section].ackStatus == "1")
        {
            cell.acknoledgeLbl.backgroundColor = UIColor.gray
            cell.acknoledgeLbl.text = "Acknowledge"
            cell.acknoledgeByLbl.isHidden = true
            cell.timeLbl.isHidden = true
            cell.phone.isHidden = true
            cell.nameLbl.isHidden = true
        }
        else
        {
            cell.acknoledgeLbl.text = "Acknowledged"
            cell.acknoledgeLbl.backgroundColor = UIColor.init(hexString: "#2ECC71")
            cell.acknoledgeByLbl.isHidden = false
            cell.timeLbl.isHidden = false
            cell.phone.isHidden = false
            cell.nameLbl.isHidden = false
        }
        cell.phone.addTarget(self, action: #selector(makeCall(_:)), for: .touchUpInside)
        cell.alertBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        cell.btnHistory.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isFromYes ? self.selectedYesArray?.count ?? 0 : selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if  let cell = tableView.cellForRow(at: indexPath)  as? AlertHistoryTableViewCell
        {
            cell.heightConstraint.constant = cell.tblvHistory.contentSize.height
            cell.tblvHistory.layoutIfNeeded()
        }

    }
}
