//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class ProblemTicketsDetailsVC: BaseViewController,ProblemTicketsDetailsViewProtocol {
    
    
    
    
    
    var presenter: ProblemTicketsDetailsPresenterProtocol?
    @IBOutlet weak var tableView: UITableView!
    
    var type:String?
    var selected:String?
    var modelResponse:ProblemTickenModel?
    var loadChangeCell: Any?
    var count = 0
    var senderSelectedDetailsIndex:Int?
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ?? UIFont.systemFont(ofSize: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> ProblemTicketsDetailsViewProtocol{
        return UIStoryboard(name: "ProblemTicketsDetailsVC", bundle: nil).instantiateViewController(withIdentifier: "ProblemTickets") as! ProblemTicketsDetailsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ChangeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ChangeDetailTableViewCell")
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        
        
        switch selected {
        case Constants.ProblemTicketsType.pendingRCA:
            count = modelResponse?.pendingRCA ?? 0
            loadChangeCell = modelResponse?.pendingRCAList
        case Constants.ProblemTicketsType.RCAActionItem:
            count = modelResponse?.rcaactionItem ?? 0
            loadChangeCell = modelResponse?.rcaactionItemList
        default:
            print("Nothing selected")
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        if(sender.tag != senderSelectedDetailsIndex)
        {
            senderSelectedDetailsIndex = sender.tag
            let point = tableView.convert(sender.center, from: sender.superview!)
            if let wantedIndexPath = tableView.indexPathForRow(at: point)  {
                let cell = tableView.cellForRow(at: wantedIndexPath) as! ChangeDetailTableViewCell
                //cell.textView.isHidden = false
                tableView.reloadData()
            }
            //startAnimating()
            //self.presenter?.requestAlarmDownHistory(sapId: selectedArray?[sender.tag].sapId ?? "", id: alamreasonObject?.id ?? "")
        }
        else
        {
            senderSelectedDetailsIndex = -1
            self.tableView.reloadData()
        }
        
        
    }
}






extension ProblemTicketsDetailsVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ChangeDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChangeDetailTableViewCell", for: indexPath as IndexPath) as! ChangeDetailTableViewCell
        
        
        if(selected ==  Constants.ProblemTicketsType.pendingRCA)
        {
            if let response = (loadChangeCell as? [PendingRCAList])?[indexPath.row] {
                
                cell.firstViewFirstDescriptionLabel.text = response.id ?? ""
                cell.firstViewSecondDescriptionLabel.text = "Pending"
                cell.firstViewThirdTitleLabel.text = "Incident Id:"
                cell.firstViewThirdDescriptionLabel.text = response.incidentid
                cell.ageingView.isHidden = false
                cell.SecondViewFirstTitleLabel.text = "Assignment Group"
                cell.SecondViewFirstDescriptionLabel.text = response.assignment
                cell.closedDate.isHidden = true
                cell.SecondViewSecondDescriptionLabel.text = response.openTIME
                cell.SecondViewFourthTitleLabel.text = "Description"
                cell.impactDescriptionLabel.text = response.briefDESCRIPTION
                cell.descriptionLabel.text = response.briefDESCRIPTION
                
                if(response.briefDESCRIPTION == nil || response.briefDESCRIPTION == "")
                {
                    cell.alertBtn.isHidden = true
                }
            }
            
            
            
        }
        else
        {
            
            if let response = (loadChangeCell as? [RcaactionItemList])?[indexPath.row] {
                
              cell.firstViewFirstDescriptionLabel.text = response.id ?? ""
               cell.firstViewSecondDescriptionLabel.text = "NA"
               cell.firstViewThirdTitleLabel.text = "Due Date"
               cell.firstViewThirdDescriptionLabel.text = response.dueDATE
               cell.ageingView.isHidden = false
               cell.SecondViewFirstTitleLabel.text = "Assignment Group"
               cell.SecondViewFirstDescriptionLabel.text = response.assignment
               cell.closedDate.isHidden = true
               cell.SecondViewSecondDescriptionLabel.text = response.openTIME
               cell.SecondViewFourthTitleLabel.text = "Description"
               cell.impactDescriptionLabel.text = response.briefDESCRIPTION ?? ""
               cell.descriptionLabel.text = response.briefDESCRIPTION ?? ""
                
                if(response.briefDESCRIPTION == nil || response.briefDESCRIPTION == "")
                {
                    cell.alertBtn.isHidden = true
                }
            }
            
            
        }
        
        
        
        
        cell.alertBtn.tag = indexPath.row
       
        
        cell.alertBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        if(senderSelectedDetailsIndex ==  cell.alertBtn.tag)
        {
            let buttonTitleStr = NSMutableAttributedString(string:"Hide Details", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = false
            //cell.dataArray = self.detailList
            //cell.dayTblView.reloadData()
        }
        else
        {
            let buttonTitleStr = NSMutableAttributedString(string:"View Details", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = true
        }
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
       if let emergencyList = loadChangeCell as? [PendingRCAList]
         {
             return emergencyList.count
         }
         else if let macList = loadChangeCell as? [RcaactionItemList]
         {
             return macList.count
         }
        
         return 0
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell") as! OutlierTableViewCell
            cell.titleLbl.text = selected
            cell.titleLbl.textColor = UIColor.white
            cell.arrowImage.isHidden = true
            cell.countLbl.text = "\(count )"
            cell.countLbl.textColor = UIColor.white
            cell.expandAction.isHidden = true
            cell.backgroundColor = UIColor.init(red: 28, green: 122, blue: 189)
            
            return cell as UIView
        }
        else {
            return  UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            
            return   44
        }
        else{
            return     5
        }
    }
}
