//
//  OpenAlertsHistoryWireframe.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class ProblemTicketsDetailsWireFrame: ProblemTicketsDetailsWireFrameProtocol {
  
    

    // MARK: OpenAlertsHistoryWireFrameProtocol
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentProblemTicketsDetailsModule(fromView:AnyObject,type:String,selected: String,response: ProblemTickenModel) {

        let view: ProblemTicketsDetailsViewProtocol = ProblemTicketsDetailsVC.instantiate()
        let presenter: ProblemTicketsDetailsPresenterProtocol & ProblemTicketsDetailsInteractorOutputProtocol = ProblemTickectsDetailsPresenter()
        let interactor: ProblemTicketsDetailsInteractorInputProtocol = ProblemTickectsDetailsInteractor()
        let wireFrame: ProblemTicketsDetailsWireFrameProtocol = ProblemTicketsDetailsWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! ProblemTicketsDetailsVC
        viewController.type = type
        viewController.type = type
        viewController.selected = selected
        viewController.modelResponse = response
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
