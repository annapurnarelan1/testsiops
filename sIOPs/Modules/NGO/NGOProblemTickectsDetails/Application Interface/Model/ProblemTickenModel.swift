//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProblemTickenModel : NSObject, NSCoding{

	var pendingRCA : Int!
	var pendingRCAList : [PendingRCAList]!
	var rcaactionItem : Int!
	var rcaactionItemList : [RcaactionItemList]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		pendingRCA = dictionary["pendingRCA"] as? Int
		pendingRCAList = [PendingRCAList]()
		if let pendingRCAListArray = dictionary["pendingRCAList"] as? [[String:Any]]{
			for dic in pendingRCAListArray{
				let value = PendingRCAList(fromDictionary: dic)
				pendingRCAList.append(value)
			}
		}
		rcaactionItem = dictionary["rcaactionItem"] as? Int
		rcaactionItemList = [RcaactionItemList]()
		if let rcaactionItemListArray = dictionary["rcaactionItemList"] as? [[String:Any]]{
			for dic in rcaactionItemListArray{
				let value = RcaactionItemList(fromDictionary: dic)
				rcaactionItemList.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if pendingRCA != nil{
			dictionary["pendingRCA"] = pendingRCA
		}
		if pendingRCAList != nil{
			var dictionaryElements = [[String:Any]]()
			for pendingRCAListElement in pendingRCAList {
				dictionaryElements.append(pendingRCAListElement.toDictionary())
			}
			dictionary["pendingRCAList"] = dictionaryElements
		}
		if rcaactionItem != nil{
			dictionary["rcaactionItem"] = rcaactionItem
		}
		if rcaactionItemList != nil{
			var dictionaryElements = [[String:Any]]()
			for rcaactionItemListElement in rcaactionItemList {
				dictionaryElements.append(rcaactionItemListElement.toDictionary())
			}
			dictionary["rcaactionItemList"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         pendingRCA = aDecoder.decodeObject(forKey: "pendingRCA") as? Int
         pendingRCAList = aDecoder.decodeObject(forKey :"pendingRCAList") as? [PendingRCAList]
         rcaactionItem = aDecoder.decodeObject(forKey: "rcaactionItem") as? Int
         rcaactionItemList = aDecoder.decodeObject(forKey :"rcaactionItemList") as? [RcaactionItemList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if pendingRCA != nil{
			aCoder.encode(pendingRCA, forKey: "pendingRCA")
		}
		if pendingRCAList != nil{
			aCoder.encode(pendingRCAList, forKey: "pendingRCAList")
		}
		if rcaactionItem != nil{
			aCoder.encode(rcaactionItem, forKey: "rcaactionItem")
		}
		if rcaactionItemList != nil{
			aCoder.encode(rcaactionItemList, forKey: "rcaactionItemList")
		}

	}

}
