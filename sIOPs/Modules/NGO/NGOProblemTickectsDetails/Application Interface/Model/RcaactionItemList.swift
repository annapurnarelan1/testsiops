//
//	RcaactionItemList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RcaactionItemList : NSObject, NSCoding{

	var assignment : String!
	var briefDESCRIPTION : String!
	var dueDATE : String!
	var environment : AnyObject!
	var id : String!
	var incidentid : AnyObject!
	var openTIME : String!
	var parentPROBLEM : String!
	var rcaTOWER : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		assignment = dictionary["assignment"] as? String
		briefDESCRIPTION = dictionary["brief_DESCRIPTION"] as? String
		dueDATE = dictionary["due_DATE"] as? String
		environment = dictionary["environment"] as? AnyObject
		id = dictionary["id"] as? String
		incidentid = dictionary["incidentid"] as? AnyObject
		openTIME = dictionary["open_TIME"] as? String
		parentPROBLEM = dictionary["parent_PROBLEM"] as? String
		rcaTOWER = dictionary["rca_TOWER"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if assignment != nil{
			dictionary["assignment"] = assignment
		}
		if briefDESCRIPTION != nil{
			dictionary["brief_DESCRIPTION"] = briefDESCRIPTION
		}
		if dueDATE != nil{
			dictionary["due_DATE"] = dueDATE
		}
		if environment != nil{
			dictionary["environment"] = environment
		}
		if id != nil{
			dictionary["id"] = id
		}
		if incidentid != nil{
			dictionary["incidentid"] = incidentid
		}
		if openTIME != nil{
			dictionary["open_TIME"] = openTIME
		}
		if parentPROBLEM != nil{
			dictionary["parent_PROBLEM"] = parentPROBLEM
		}
		if rcaTOWER != nil{
			dictionary["rca_TOWER"] = rcaTOWER
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         assignment = aDecoder.decodeObject(forKey: "assignment") as? String
         briefDESCRIPTION = aDecoder.decodeObject(forKey: "brief_DESCRIPTION") as? String
         dueDATE = aDecoder.decodeObject(forKey: "due_DATE") as? String
         environment = aDecoder.decodeObject(forKey: "environment") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? String
         incidentid = aDecoder.decodeObject(forKey: "incidentid") as? AnyObject
         openTIME = aDecoder.decodeObject(forKey: "open_TIME") as? String
         parentPROBLEM = aDecoder.decodeObject(forKey: "parent_PROBLEM") as? String
         rcaTOWER = aDecoder.decodeObject(forKey: "rca_TOWER") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if assignment != nil{
			aCoder.encode(assignment, forKey: "assignment")
		}
		if briefDESCRIPTION != nil{
			aCoder.encode(briefDESCRIPTION, forKey: "brief_DESCRIPTION")
		}
		if dueDATE != nil{
			aCoder.encode(dueDATE, forKey: "due_DATE")
		}
		if environment != nil{
			aCoder.encode(environment, forKey: "environment")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if incidentid != nil{
			aCoder.encode(incidentid, forKey: "incidentid")
		}
		if openTIME != nil{
			aCoder.encode(openTIME, forKey: "open_TIME")
		}
		if parentPROBLEM != nil{
			aCoder.encode(parentPROBLEM, forKey: "parent_PROBLEM")
		}
		if rcaTOWER != nil{
			aCoder.encode(rcaTOWER, forKey: "rca_TOWER")
		}

	}

}
