//
//  OpenAlertsDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol ProblemTicketsDetailsViewProtocol: class {
    var presenter: ProblemTicketsDetailsPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
   // func reloadData(detail:[Acknowledged])
}

/// Method contract between PRESENTER -> WIREFRAME
protocol ProblemTicketsDetailsWireFrameProtocol: class {
    static func presentProblemTicketsDetailsModule(fromView:AnyObject,type:String,selected: String,response: ProblemTickenModel)
}

/// Method contract between VIEW -> PRESENTER
protocol ProblemTicketsDetailsPresenterProtocol: class {
    var view: ProblemTicketsDetailsViewProtocol? { get set }
    var interactor: ProblemTicketsDetailsInteractorInputProtocol? { get set }
    var wireFrame: ProblemTicketsDetailsWireFrameProtocol? { get set }
    func loadData(type:String , model: NGOOpenALertsModel)
    func requestHistoryData(type:String)
}

/// Method contract between INTERACTOR -> PRESENTER
protocol ProblemTicketsDetailsInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:[Acknowledged])
}

/// Method contract between PRESENTER -> INTERACTOR
protocol ProblemTicketsDetailsInteractorInputProtocol: class
{
    var presenter: ProblemTicketsDetailsInteractorOutputProtocol? { get set }
    func loadData(type:String , model: NGOOpenALertsModel)
    func requestHistoryData(type:String)
}
