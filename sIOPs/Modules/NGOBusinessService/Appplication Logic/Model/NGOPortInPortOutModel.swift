//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOPortInPortOutModel : NSObject, NSCoding{

	var portinoutListTrends : [NGOPortInPortOutTrend]!
	var todatPortOutTotalCompleted : Int!
	var todayPortInTotal : Int!
	var todayPortInTotalCompleted : Int!
	var todayPortOutTotal : Int!
	var yesPortInTotal : Int!
	var yesPortInTotalCompleted : Int!
	var yesPortOutTotal : Int!
	var yesPortOutTotalCompleted : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		portinoutListTrends = [NGOPortInPortOutTrend]()
		if let portinoutListTrendsArray = dictionary["portinoutListTrends"] as? [[String:Any]]{
			for dic in portinoutListTrendsArray{
				let value = NGOPortInPortOutTrend(fromDictionary: dic)
				portinoutListTrends.append(value)
			}
		}
		todatPortOutTotalCompleted = dictionary["todatPortOutTotalCompleted"] as? Int
		todayPortInTotal = dictionary["todayPortInTotal"] as? Int
		todayPortInTotalCompleted = dictionary["todayPortInTotalCompleted"] as? Int
		todayPortOutTotal = dictionary["todayPortOutTotal"] as? Int
		yesPortInTotal = dictionary["yesPortInTotal"] as? Int
		yesPortInTotalCompleted = dictionary["yesPortInTotalCompleted"] as? Int
		yesPortOutTotal = dictionary["yesPortOutTotal"] as? Int
		yesPortOutTotalCompleted = dictionary["yesPortOutTotalCompleted"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if portinoutListTrends != nil{
			var dictionaryElements = [[String:Any]]()
			for portinoutListTrendsElement in portinoutListTrends {
				dictionaryElements.append(portinoutListTrendsElement.toDictionary())
			}
			dictionary["portinoutListTrends"] = dictionaryElements
		}
		if todatPortOutTotalCompleted != nil{
			dictionary["todatPortOutTotalCompleted"] = todatPortOutTotalCompleted
		}
		if todayPortInTotal != nil{
			dictionary["todayPortInTotal"] = todayPortInTotal
		}
		if todayPortInTotalCompleted != nil{
			dictionary["todayPortInTotalCompleted"] = todayPortInTotalCompleted
		}
		if todayPortOutTotal != nil{
			dictionary["todayPortOutTotal"] = todayPortOutTotal
		}
		if yesPortInTotal != nil{
			dictionary["yesPortInTotal"] = yesPortInTotal
		}
		if yesPortInTotalCompleted != nil{
			dictionary["yesPortInTotalCompleted"] = yesPortInTotalCompleted
		}
		if yesPortOutTotal != nil{
			dictionary["yesPortOutTotal"] = yesPortOutTotal
		}
		if yesPortOutTotalCompleted != nil{
			dictionary["yesPortOutTotalCompleted"] = yesPortOutTotalCompleted
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         portinoutListTrends = aDecoder.decodeObject(forKey :"portinoutListTrends") as? [NGOPortInPortOutTrend]
         todatPortOutTotalCompleted = aDecoder.decodeObject(forKey: "todatPortOutTotalCompleted") as? Int
         todayPortInTotal = aDecoder.decodeObject(forKey: "todayPortInTotal") as? Int
         todayPortInTotalCompleted = aDecoder.decodeObject(forKey: "todayPortInTotalCompleted") as? Int
         todayPortOutTotal = aDecoder.decodeObject(forKey: "todayPortOutTotal") as? Int
         yesPortInTotal = aDecoder.decodeObject(forKey: "yesPortInTotal") as? Int
         yesPortInTotalCompleted = aDecoder.decodeObject(forKey: "yesPortInTotalCompleted") as? Int
         yesPortOutTotal = aDecoder.decodeObject(forKey: "yesPortOutTotal") as? Int
         yesPortOutTotalCompleted = aDecoder.decodeObject(forKey: "yesPortOutTotalCompleted") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if portinoutListTrends != nil{
			aCoder.encode(portinoutListTrends, forKey: "portinoutListTrends")
		}
		if todatPortOutTotalCompleted != nil{
			aCoder.encode(todatPortOutTotalCompleted, forKey: "todatPortOutTotalCompleted")
		}
		if todayPortInTotal != nil{
			aCoder.encode(todayPortInTotal, forKey: "todayPortInTotal")
		}
		if todayPortInTotalCompleted != nil{
			aCoder.encode(todayPortInTotalCompleted, forKey: "todayPortInTotalCompleted")
		}
		if todayPortOutTotal != nil{
			aCoder.encode(todayPortOutTotal, forKey: "todayPortOutTotal")
		}
		if yesPortInTotal != nil{
			aCoder.encode(yesPortInTotal, forKey: "yesPortInTotal")
		}
		if yesPortInTotalCompleted != nil{
			aCoder.encode(yesPortInTotalCompleted, forKey: "yesPortInTotalCompleted")
		}
		if yesPortOutTotal != nil{
			aCoder.encode(yesPortOutTotal, forKey: "yesPortOutTotal")
		}
		if yesPortOutTotalCompleted != nil{
			aCoder.encode(yesPortOutTotalCompleted, forKey: "yesPortOutTotalCompleted")
		}

	}

}
