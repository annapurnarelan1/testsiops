//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOOrderActivationModel : NSObject, NSCoding{

	var baseCount : Int!
	var healthyStatus : Int!
	var jioPhoneCount : Int!
	var orderStages : [NGOOrderStage]!
	var orderTrends : [NGOOrderTrend]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		baseCount = dictionary["baseCount"] as? Int
		healthyStatus = dictionary["healthyStatus"] as? Int
		jioPhoneCount = dictionary["jioPhoneCount"] as? Int
		orderStages = [NGOOrderStage]()
		if let orderStagesArray = dictionary["orderStages"] as? [[String:Any]]{
			for dic in orderStagesArray{
				let value = NGOOrderStage(fromDictionary: dic)
				orderStages.append(value)
			}
		}
		orderTrends = [NGOOrderTrend]()
		if let orderTrendsArray = dictionary["orderTrends"] as? [[String:Any]]{
			for dic in orderTrendsArray{
				let value = NGOOrderTrend(fromDictionary: dic)
				orderTrends.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if baseCount != nil{
			dictionary["baseCount"] = baseCount
		}
		if healthyStatus != nil{
			dictionary["healthyStatus"] = healthyStatus
		}
		if jioPhoneCount != nil{
			dictionary["jioPhoneCount"] = jioPhoneCount
		}
		if orderStages != nil{
			var dictionaryElements = [[String:Any]]()
			for orderStagesElement in orderStages {
				dictionaryElements.append(orderStagesElement.toDictionary())
			}
			dictionary["orderStages"] = dictionaryElements
		}
		if orderTrends != nil{
			var dictionaryElements = [[String:Any]]()
			for orderTrendsElement in orderTrends {
				dictionaryElements.append(orderTrendsElement.toDictionary())
			}
			dictionary["orderTrends"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         baseCount = aDecoder.decodeObject(forKey: "baseCount") as? Int
         healthyStatus = aDecoder.decodeObject(forKey: "healthyStatus") as? Int
         jioPhoneCount = aDecoder.decodeObject(forKey: "jioPhoneCount") as? Int
         orderStages = aDecoder.decodeObject(forKey :"orderStages") as? [NGOOrderStage]
         orderTrends = aDecoder.decodeObject(forKey :"orderTrends") as? [NGOOrderTrend]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if baseCount != nil{
			aCoder.encode(baseCount, forKey: "baseCount")
		}
		if healthyStatus != nil{
			aCoder.encode(healthyStatus, forKey: "healthyStatus")
		}
		if jioPhoneCount != nil{
			aCoder.encode(jioPhoneCount, forKey: "jioPhoneCount")
		}
		if orderStages != nil{
			aCoder.encode(orderStages, forKey: "orderStages")
		}
		if orderTrends != nil{
			aCoder.encode(orderTrends, forKey: "orderTrends")
		}

	}

}
