//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOMACDModel : NSObject, NSCoding{

	var appleIWatchCount : Int!
	var appleIWatchTotal : Int!
	var ddate : AnyObject!
	var irWatchCount : Int!
	var irWatchTotal : Int!
	var macdHeaderList : [NGOMACDTrendDetails]!
	var macdHeaderListfinal : AnyObject!
	var simChangeCount : Int!
	var simChangeTotal : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		appleIWatchCount = dictionary["appleIWatchCount"] as? Int
		appleIWatchTotal = dictionary["appleIWatchTotal"] as? Int
		ddate = dictionary["ddate"] as? AnyObject
		irWatchCount = dictionary["irWatchCount"] as? Int
		irWatchTotal = dictionary["irWatchTotal"] as? Int
		macdHeaderList = [NGOMACDTrendDetails]()
		if let macdHeaderListArray = dictionary["macdHeaderList"] as? [[String:Any]]{
			for dic in macdHeaderListArray{
				let value = NGOMACDTrendDetails(fromDictionary: dic)
				macdHeaderList.append(value)
			}
		}
		macdHeaderListfinal = dictionary["macdHeaderListfinal"] as? AnyObject
		simChangeCount = dictionary["simChangeCount"] as? Int
		simChangeTotal = dictionary["simChangeTotal"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if appleIWatchCount != nil{
			dictionary["appleIWatchCount"] = appleIWatchCount
		}
		if appleIWatchTotal != nil{
			dictionary["appleIWatchTotal"] = appleIWatchTotal
		}
		if ddate != nil{
			dictionary["ddate"] = ddate
		}
		if irWatchCount != nil{
			dictionary["irWatchCount"] = irWatchCount
		}
		if irWatchTotal != nil{
			dictionary["irWatchTotal"] = irWatchTotal
		}
		if macdHeaderList != nil{
			var dictionaryElements = [[String:Any]]()
			for macdHeaderListElement in macdHeaderList {
				dictionaryElements.append(macdHeaderListElement.toDictionary())
			}
			dictionary["macdHeaderList"] = dictionaryElements
		}
		if macdHeaderListfinal != nil{
			dictionary["macdHeaderListfinal"] = macdHeaderListfinal
		}
		if simChangeCount != nil{
			dictionary["simChangeCount"] = simChangeCount
		}
		if simChangeTotal != nil{
			dictionary["simChangeTotal"] = simChangeTotal
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         appleIWatchCount = aDecoder.decodeObject(forKey: "appleIWatchCount") as? Int
         appleIWatchTotal = aDecoder.decodeObject(forKey: "appleIWatchTotal") as? Int
         ddate = aDecoder.decodeObject(forKey: "ddate") as? AnyObject
         irWatchCount = aDecoder.decodeObject(forKey: "irWatchCount") as? Int
         irWatchTotal = aDecoder.decodeObject(forKey: "irWatchTotal") as? Int
         macdHeaderList = aDecoder.decodeObject(forKey :"macdHeaderList") as? [NGOMACDTrendDetails]
         macdHeaderListfinal = aDecoder.decodeObject(forKey: "macdHeaderListfinal") as? AnyObject
         simChangeCount = aDecoder.decodeObject(forKey: "simChangeCount") as? Int
         simChangeTotal = aDecoder.decodeObject(forKey: "simChangeTotal") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if appleIWatchCount != nil{
			aCoder.encode(appleIWatchCount, forKey: "appleIWatchCount")
		}
		if appleIWatchTotal != nil{
			aCoder.encode(appleIWatchTotal, forKey: "appleIWatchTotal")
		}
		if ddate != nil{
			aCoder.encode(ddate, forKey: "ddate")
		}
		if irWatchCount != nil{
			aCoder.encode(irWatchCount, forKey: "irWatchCount")
		}
		if irWatchTotal != nil{
			aCoder.encode(irWatchTotal, forKey: "irWatchTotal")
		}
		if macdHeaderList != nil{
			aCoder.encode(macdHeaderList, forKey: "macdHeaderList")
		}
		if macdHeaderListfinal != nil{
			aCoder.encode(macdHeaderListfinal, forKey: "macdHeaderListfinal")
		}
		if simChangeCount != nil{
			aCoder.encode(simChangeCount, forKey: "simChangeCount")
		}
		if simChangeTotal != nil{
			aCoder.encode(simChangeTotal, forKey: "simChangeTotal")
		}

	}

}
