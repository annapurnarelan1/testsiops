//
//	PortinoutListTrend.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOPortInPortOutTrend : NSObject, NSCoding{

	var ddate : String!
	var pinact : Int!
	var pintotal : Int!
	var poutact : Int!
	var pouttotal : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ddate = dictionary["ddate"] as? String
		pinact = dictionary["pinact"] as? Int
		pintotal = dictionary["pintotal"] as? Int
		poutact = dictionary["poutact"] as? Int
		pouttotal = dictionary["pouttotal"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ddate != nil{
			dictionary["ddate"] = ddate
		}
		if pinact != nil{
			dictionary["pinact"] = pinact
		}
		if pintotal != nil{
			dictionary["pintotal"] = pintotal
		}
		if poutact != nil{
			dictionary["poutact"] = poutact
		}
		if pouttotal != nil{
			dictionary["pouttotal"] = pouttotal
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ddate = aDecoder.decodeObject(forKey: "ddate") as? String
         pinact = aDecoder.decodeObject(forKey: "pinact") as? Int
         pintotal = aDecoder.decodeObject(forKey: "pintotal") as? Int
         poutact = aDecoder.decodeObject(forKey: "poutact") as? Int
         pouttotal = aDecoder.decodeObject(forKey: "pouttotal") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ddate != nil{
			aCoder.encode(ddate, forKey: "ddate")
		}
		if pinact != nil{
			aCoder.encode(pinact, forKey: "pinact")
		}
		if pintotal != nil{
			aCoder.encode(pintotal, forKey: "pintotal")
		}
		if poutact != nil{
			aCoder.encode(poutact, forKey: "poutact")
		}
		if pouttotal != nil{
			aCoder.encode(pouttotal, forKey: "pouttotal")
		}

	}

}
