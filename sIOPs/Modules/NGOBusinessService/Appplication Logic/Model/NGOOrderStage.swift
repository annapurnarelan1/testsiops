//
//    OrderStage.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOOrderStage : NSObject, NSCoding{

    var color : Int!
    var diff : Int!
    var perc : Int!
    var stage : String!
    var status : String!
    var todayCount : Int!
    var yesCount : Int!
    var yesTotal : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        color = dictionary["color"] as? Int
        diff = dictionary["diff"] as? Int
        perc = dictionary["perc"] as? Int
        stage = dictionary["stage"] as? String
        status = dictionary["status"] as? String
        todayCount = dictionary["todayCount"] as? Int
        yesCount = dictionary["yesCount"] as? Int
        yesTotal = dictionary["yesTotal"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if color != nil{
            dictionary["color"] = color
        }
        if diff != nil{
            dictionary["diff"] = diff
        }
        if perc != nil{
            dictionary["perc"] = perc
        }
        if stage != nil{
            dictionary["stage"] = stage
        }
        if status != nil{
            dictionary["status"] = status
        }
        if todayCount != nil{
            dictionary["todayCount"] = todayCount
        }
        if yesCount != nil{
            dictionary["yesCount"] = yesCount
        }
        if yesTotal != nil{
            dictionary["yesTotal"] = yesTotal
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         color = aDecoder.decodeObject(forKey: "color") as? Int
         diff = aDecoder.decodeObject(forKey: "diff") as? Int
         perc = aDecoder.decodeObject(forKey: "perc") as? Int
         stage = aDecoder.decodeObject(forKey: "stage") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         todayCount = aDecoder.decodeObject(forKey: "todayCount") as? Int
         yesCount = aDecoder.decodeObject(forKey: "yesCount") as? Int
         yesTotal = aDecoder.decodeObject(forKey: "yesTotal") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if color != nil{
            aCoder.encode(color, forKey: "color")
        }
        if diff != nil{
            aCoder.encode(diff, forKey: "diff")
        }
        if perc != nil{
            aCoder.encode(perc, forKey: "perc")
        }
        if stage != nil{
            aCoder.encode(stage, forKey: "stage")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if todayCount != nil{
            aCoder.encode(todayCount, forKey: "todayCount")
        }
        if yesCount != nil{
            aCoder.encode(yesCount, forKey: "yesCount")
        }
        if yesTotal != nil{
            aCoder.encode(yesTotal, forKey: "yesTotal")
        }

    }

}
