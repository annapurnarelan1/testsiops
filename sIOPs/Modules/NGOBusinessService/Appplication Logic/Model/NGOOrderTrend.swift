//
//	OrderTrend.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOOrderTrend : NSObject, NSCoding{

	var activated : Int!
	var completed : Int!
	var ddate : String!
	var dtime : AnyObject!
	var entered : Int!
	var percentage : Int!
	var rejected : Int!
	var total : Int!
	var tvpending : Int!
	var type : AnyObject!
	var wip : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		activated = dictionary["activated"] as? Int
		completed = dictionary["completed"] as? Int
		ddate = dictionary["ddate"] as? String
		dtime = dictionary["dtime"] as? AnyObject
		entered = dictionary["entered"] as? Int
		percentage = dictionary["percentage"] as? Int
		rejected = dictionary["rejected"] as? Int
		total = dictionary["total"] as? Int
		tvpending = dictionary["tvpending"] as? Int
		type = dictionary["type"] as? AnyObject
		wip = dictionary["wip"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if activated != nil{
			dictionary["activated"] = activated
		}
		if completed != nil{
			dictionary["completed"] = completed
		}
		if ddate != nil{
			dictionary["ddate"] = ddate
		}
		if dtime != nil{
			dictionary["dtime"] = dtime
		}
		if entered != nil{
			dictionary["entered"] = entered
		}
		if percentage != nil{
			dictionary["percentage"] = percentage
		}
		if rejected != nil{
			dictionary["rejected"] = rejected
		}
		if total != nil{
			dictionary["total"] = total
		}
		if tvpending != nil{
			dictionary["tvpending"] = tvpending
		}
		if type != nil{
			dictionary["type"] = type
		}
		if wip != nil{
			dictionary["wip"] = wip
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         activated = aDecoder.decodeObject(forKey: "activated") as? Int
         completed = aDecoder.decodeObject(forKey: "completed") as? Int
         ddate = aDecoder.decodeObject(forKey: "ddate") as? String
         dtime = aDecoder.decodeObject(forKey: "dtime") as? AnyObject
         entered = aDecoder.decodeObject(forKey: "entered") as? Int
         percentage = aDecoder.decodeObject(forKey: "percentage") as? Int
         rejected = aDecoder.decodeObject(forKey: "rejected") as? Int
         total = aDecoder.decodeObject(forKey: "total") as? Int
         tvpending = aDecoder.decodeObject(forKey: "tvpending") as? Int
         type = aDecoder.decodeObject(forKey: "type") as? AnyObject
         wip = aDecoder.decodeObject(forKey: "wip") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if activated != nil{
			aCoder.encode(activated, forKey: "activated")
		}
		if completed != nil{
			aCoder.encode(completed, forKey: "completed")
		}
		if ddate != nil{
			aCoder.encode(ddate, forKey: "ddate")
		}
		if dtime != nil{
			aCoder.encode(dtime, forKey: "dtime")
		}
		if entered != nil{
			aCoder.encode(entered, forKey: "entered")
		}
		if percentage != nil{
			aCoder.encode(percentage, forKey: "percentage")
		}
		if rejected != nil{
			aCoder.encode(rejected, forKey: "rejected")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}
		if tvpending != nil{
			aCoder.encode(tvpending, forKey: "tvpending")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if wip != nil{
			aCoder.encode(wip, forKey: "wip")
		}

	}

}
