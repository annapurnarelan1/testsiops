//
//	RechargeTrend.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGORechargeTrend : NSObject, NSCoding{

	var dtime : AnyObject!
	var initiated : Int!
	var outlier : Int!
	var paymentfailed : Int!
	var paymentsuccess : Int!
	var pending : Int!
	var rdate : String!
	var refund : Int!
	var succes : Int!
	var total : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		dtime = dictionary["dtime"] as? AnyObject
		initiated = dictionary["initiated"] as? Int
		outlier = dictionary["outlier"] as? Int
		paymentfailed = dictionary["paymentfailed"] as? Int
		paymentsuccess = dictionary["paymentsuccess"] as? Int
		pending = dictionary["pending"] as? Int
		rdate = dictionary["rdate"] as? String
		refund = dictionary["refund"] as? Int
		succes = dictionary["succes"] as? Int
		total = dictionary["total"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if dtime != nil{
			dictionary["dtime"] = dtime
		}
		if initiated != nil{
			dictionary["initiated"] = initiated
		}
		if outlier != nil{
			dictionary["outlier"] = outlier
		}
		if paymentfailed != nil{
			dictionary["paymentfailed"] = paymentfailed
		}
		if paymentsuccess != nil{
			dictionary["paymentsuccess"] = paymentsuccess
		}
		if pending != nil{
			dictionary["pending"] = pending
		}
		if rdate != nil{
			dictionary["rdate"] = rdate
		}
		if refund != nil{
			dictionary["refund"] = refund
		}
		if succes != nil{
			dictionary["succes"] = succes
		}
		if total != nil{
			dictionary["total"] = total
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dtime = aDecoder.decodeObject(forKey: "dtime") as? AnyObject
         initiated = aDecoder.decodeObject(forKey: "initiated") as? Int
         outlier = aDecoder.decodeObject(forKey: "outlier") as? Int
         paymentfailed = aDecoder.decodeObject(forKey: "paymentfailed") as? Int
         paymentsuccess = aDecoder.decodeObject(forKey: "paymentsuccess") as? Int
         pending = aDecoder.decodeObject(forKey: "pending") as? Int
         rdate = aDecoder.decodeObject(forKey: "rdate") as? String
         refund = aDecoder.decodeObject(forKey: "refund") as? Int
         succes = aDecoder.decodeObject(forKey: "succes") as? Int
         total = aDecoder.decodeObject(forKey: "total") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if dtime != nil{
			aCoder.encode(dtime, forKey: "dtime")
		}
		if initiated != nil{
			aCoder.encode(initiated, forKey: "initiated")
		}
		if outlier != nil{
			aCoder.encode(outlier, forKey: "outlier")
		}
		if paymentfailed != nil{
			aCoder.encode(paymentfailed, forKey: "paymentfailed")
		}
		if paymentsuccess != nil{
			aCoder.encode(paymentsuccess, forKey: "paymentsuccess")
		}
		if pending != nil{
			aCoder.encode(pending, forKey: "pending")
		}
		if rdate != nil{
			aCoder.encode(rdate, forKey: "rdate")
		}
		if refund != nil{
			aCoder.encode(refund, forKey: "refund")
		}
		if succes != nil{
			aCoder.encode(succes, forKey: "succes")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}

	}

}
