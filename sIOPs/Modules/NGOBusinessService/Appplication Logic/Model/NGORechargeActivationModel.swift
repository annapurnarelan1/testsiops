//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGORechargeActivationModel : NSObject, NSCoding{

	var healthyStatus : Int!
	var rechargeStages : [NGORechargeStage]!
	var rechargeTrends : [NGORechargeTrend]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		healthyStatus = dictionary["healthyStatus"] as? Int
		rechargeStages = [NGORechargeStage]()
		if let rechargeStagesArray = dictionary["rechargeStages"] as? [[String:Any]]{
			for dic in rechargeStagesArray{
				let value = NGORechargeStage(fromDictionary: dic)
				rechargeStages.append(value)
			}
		}
		rechargeTrends = [NGORechargeTrend]()
		if let rechargeTrendsArray = dictionary["rechargeTrends"] as? [[String:Any]]{
			for dic in rechargeTrendsArray{
				let value = NGORechargeTrend(fromDictionary: dic)
				rechargeTrends.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if healthyStatus != nil{
			dictionary["healthyStatus"] = healthyStatus
		}
		if rechargeStages != nil{
			var dictionaryElements = [[String:Any]]()
			for rechargeStagesElement in rechargeStages {
				dictionaryElements.append(rechargeStagesElement.toDictionary())
			}
			dictionary["rechargeStages"] = dictionaryElements
		}
		if rechargeTrends != nil{
			var dictionaryElements = [[String:Any]]()
			for rechargeTrendsElement in rechargeTrends {
				dictionaryElements.append(rechargeTrendsElement.toDictionary())
			}
			dictionary["rechargeTrends"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         healthyStatus = aDecoder.decodeObject(forKey: "healthyStatus") as? Int
         rechargeStages = aDecoder.decodeObject(forKey :"rechargeStages") as? [NGORechargeStage]
         rechargeTrends = aDecoder.decodeObject(forKey :"rechargeTrends") as? [NGORechargeTrend]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if healthyStatus != nil{
			aCoder.encode(healthyStatus, forKey: "healthyStatus")
		}
		if rechargeStages != nil{
			aCoder.encode(rechargeStages, forKey: "rechargeStages")
		}
		if rechargeTrends != nil{
			aCoder.encode(rechargeTrends, forKey: "rechargeTrends")
		}

	}

}
