//
//  NGOBusinessServiceInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class NGOBusinessServiceInteractor: NGOBusinessServiceInteractorInputProtocol {
  
    
    
    typealias tupleVar = (NGOOrderActivationModel, NGORechargeActivationModel, NGOPortInPortOutModel ,NGOMACDModel)
    
    public enum NGOBusinessError {
        case internetError(String)
        case serverMessage(String)
    }
    
    // MARK: Variables
    weak var presenter: NGOBusinessServiceInteractorOutputProtocol?
    let userListDispatchGroup = DispatchGroup()
    var isErrorOrderJourney = false
    var isErrorRechargeJourney = false
    var isErrorPort = false
    var isErrorMAC     = false
    var tupleType : tupleVar = tupleVar(NGOOrderActivationModel (fromDictionary: [:]),NGORechargeActivationModel (fromDictionary: [:]),NGOPortInPortOutModel (fromDictionary: [:]),NGOMACDModel(fromDictionary: [:]))
    
    func requestCollectionOfAPIS() {
        
        userListDispatchGroup.enter()
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        let busicode =  Constants.BusiCode.NGOOrderActivation
        
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams": busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                     self.isErrorOrderJourney = false
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                        let NGOOrderActivationResponse = NGOOrderActivationModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                        //self.presenter?.reloadOrderActivationData(NGOOrderActivationResponse: NGOOrderActivationResponse)
                        
                        self.tupleType.0 = NGOOrderActivationResponse
                        
                    case Constants.status.NOK:
                        self.isErrorOrderJourney = true
//                        let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
//                        self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
                    
                case .failure(let failure) :
                    self.isErrorOrderJourney = true
//                switch failure {
//                case .connectionError:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
//
//                case .authorizationError(let errorJson):
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .serverError:
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .newUpdate:
//                    self.presenter?.stopLoader()
//                default:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
//
//                }
                }
                self.userListDispatchGroup.leave()   //Leaves the First API Call
            })
        
   
       userListDispatchGroup.enter()    // enters the Secind Call
    
        let pubInfoSecond: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeaderSecond:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        
        let requestBodySecond:[String : Any] = [
            
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            
        ]
        
        let busiParamsSecond: [String : Any] = [
            "requestHeader": requestHeaderSecond,
            "requestBody": requestBodySecond,
        ]
        
        let busicodeSecond =  Constants.BusiCode.NGORechargeActivation
        
        
        let requestListSecond: [String: Any] = [
            "busiCode": busicodeSecond,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParamsSecond
        ]
        
        let jsonParameterSecond = [
            "pubInfo" : pubInfoSecond,
            "requestList" : [requestListSecond]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameterSecond, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                self.isErrorRechargeJourney = false
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let NGORechargeActivationData = NGORechargeActivationModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                   // self.presenter?.reloadNGORechargeActivationData(NGORechargeActivationResponse: NGORechargeActivationData)
                    
                    self.tupleType.1 = NGORechargeActivationData
                //                                        self.presenter?.reloadHistoryData(detail: siteDownDetaildata)
                case Constants.status.NOK:
                    self.isErrorRechargeJourney = true
//                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
//                    self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
            case .failure(let failure) :
                self.isErrorRechargeJourney = true
//                switch failure {
//                case .connectionError:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
//
//                case .authorizationError(let errorJson):
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .serverError:
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .newUpdate:
//                    self.presenter?.stopLoader()
//                default:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
//
//                }
            }
            self.userListDispatchGroup.leave()  //Second API Calls ends
        })
        
        
        userListDispatchGroup.enter()   // Third API Call starts
        
        let pubInfoThird: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeaderThird:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        
        let requestBodyThird:[String : Any] = [
            
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            
        ]
        
        let busiParamsThird: [String : Any] = [
            "requestHeader": requestHeaderThird,
            "requestBody": requestBodyThird,
        ]
        
        let busicodeThird =  Constants.BusiCode.NGOPortInPortOutCountTrends
        
        
        let requestListThird: [String: Any] = [
            "busiCode": busicodeThird,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParamsThird
        ]
        
        let jsonParameterThird = [
            "pubInfo" : pubInfoThird,
            "requestList" : [requestListThird]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameterThird, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                self.isErrorPort = false
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let NGOPortInPortOutData = NGOPortInPortOutModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                   // self.presenter?.reloadMobilePortInPortOutData(NGOMobilePortInPortOutResponse: NGOPortInPortOutData)
                    
                    self.tupleType.2 = NGOPortInPortOutData
                    
                case Constants.status.NOK:
                    self.isErrorPort = true
//                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
//                    self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
            case .failure(let failure) :
                self.isErrorPort = true
//                switch failure {
//                case .connectionError:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
//
//                case .authorizationError(let errorJson):
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .serverError:
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .newUpdate:
//                    self.presenter?.stopLoader()
//                default:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
//
//                }
            }
            self.userListDispatchGroup.leave()   //Third API Completes
        })
        
    
        userListDispatchGroup.enter()  // Fourth API Starts
        
        let pubInfoFourth: [String : Any] =
                ["timestamp": String(Date().ticks),
                 "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                 "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                 "osType": "ios",
                 "lang": "en_US",
                 "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeaderFourth:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
            
            let requestBodyFourth:[String : Any] = [
                
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                
            ]
            
            let busiParamsFourth: [String : Any] = [
                "requestHeader": requestHeaderFourth,
                "requestBody": requestBodyFourth,
            ]
            
        let busicodeFourth =  Constants.BusiCode.NGOMACDDetails
            
            
            let requestListFourth: [String: Any] = [
                "busiCode": busicodeFourth,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParamsFourth
            ]
            
            let jsonParameterFourth = [
                "pubInfo" : pubInfoFourth,
                "requestList" : [requestListFourth]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameterFourth, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    self.isErrorMAC = false
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                        let NGOMACDData = NGOMACDModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                       // self.presenter?.reloadMACDDetailsData(NGOMACDResponse: NGOMACDData)
                        
                        self.tupleType.3 = NGOMACDData
                        
                    case Constants.status.NOK:
                        self.isErrorMAC = true
//                        let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
//                        self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
         
                case .failure(let failure) :
                    self.isErrorMAC       = true
//                switch failure {
//                case .connectionError:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
//
//                case .authorizationError(let errorJson):
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .serverError:
//                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
//                case .newUpdate:
//                    self.presenter?.stopLoader()
//                default:
//                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
//
//                }
                }
                
                self.userListDispatchGroup.leave()//Fourth API Call Finishes
                
            })
            
        userListDispatchGroup.notify(queue: .main) {
               print("all APIs have completed successfully 👌")
            if(self.isErrorOrderJourney && self.isErrorRechargeJourney && self.isErrorPort  && self.isErrorMAC)
            {
                self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
            }
            else
            {
                self.presenter?.reloadALLApiSData(self.tupleType)
            }
            
           }
        }
}
    

