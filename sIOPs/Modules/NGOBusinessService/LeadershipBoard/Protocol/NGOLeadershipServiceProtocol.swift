//
//  NGOBusinessServiceProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol NGOLeadershipServiceProtocol: class {
    var presenter: NGOLeadershipServicePresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadData (data: [List])
    
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGOLeadershipServiceWireFrameProtocol: class {
    static  func presentNGOLeadershipServiceModule(fromView:AnyObject,loginRes:LoginModel)
    func presentBuisnessBoard(loginRes:LoginModel , key : String)
    func presentTechnicalBoard(loginRes:LoginModel)
    func presentOrderJourneyDetail()
    func presentRechargeJourneyDetail()
}

/// Method contract between VIEW -> PRESENTER
protocol NGOLeadershipServicePresenterProtocol: class {
    var view: NGOLeadershipServiceProtocol? { get set }
    var interactor: NGOLeadershipServiceInteractorInputProtocol? { get set }
    var wireFrame: NGOLeadershipServiceWireFrameProtocol? { get set }
    func request(loginApp:LoginModel)
    func presentBuisnessBoard(loginRes:LoginModel , key : String)
    func presentTechnicalBoard(loginRes:LoginModel)
    func presentOrderJourneyDetail()
    func presentRechargeJourneyDetail()
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGOLeadershipServiceInteractorOutputProtocol: class {
    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadData (data: [List])
}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGOLeadershipServiceInteractorInputProtocol: class
{
    var presenter: NGOLeadershipServiceInteractorOutputProtocol? { get set }
    func request(loginApp:LoginModel)
}
