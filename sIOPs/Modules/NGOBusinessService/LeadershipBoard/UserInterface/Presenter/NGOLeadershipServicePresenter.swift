//
//  NGOBusinessServicePresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGOLeadershipServicePresenter:BasePresenter, NGOLeadershipServicePresenterProtocol, NGOLeadershipServiceInteractorOutputProtocol {
 
    // MARK: Variables
    weak var view: NGOLeadershipServiceProtocol?
    var interactor: NGOLeadershipServiceInteractorInputProtocol?
    var wireFrame: NGOLeadershipServiceWireFrameProtocol?
    let stringsTableName = "NGOLeadershipService"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    func presentBuisnessBoard(loginRes: LoginModel, key: String) {
           self.wireFrame?.presentBuisnessBoard(loginRes: loginRes, key: key)
    }
//    func presentBusinnessBoard(loginRes:LoginModel , key: String)
//       {
//           self.wireFrame?.presentBuisnessBoard(loginRes: loginRes, key: key)
//       }
    
    func presentTechnicalBoard(loginRes:LoginModel)
    {
        self.wireFrame?.presentTechnicalBoard(loginRes:loginRes)
    }
    func reloadData(data: [List]) {
         self.view?.reloadData(data: data)
    }
    

    func request(loginApp: LoginModel) {
        self.interactor?.request(loginApp: loginApp)
    }
    
    func presentOrderJourneyDetail()
    {
        self.wireFrame?.presentOrderJourneyDetail()
    }
    
    func presentRechargeJourneyDetail()
    {
        self.wireFrame?.presentRechargeJourneyDetail()

    }

}
