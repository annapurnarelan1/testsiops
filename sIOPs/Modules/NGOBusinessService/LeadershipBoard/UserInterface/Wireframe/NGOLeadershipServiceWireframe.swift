//
//  NGOBusinessServiceWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGOLeadershipServiceWireFrame: NGOLeadershipServiceWireFrameProtocol {
   
    
    // MARK: NGOLeadershipServiceWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentNGOLeadershipServiceModule(fromView:AnyObject,loginRes:LoginModel) {

        // Generating module components
        let view: NGOLeadershipServiceProtocol = NGOLeadershipVC.instantiate()
        let presenter: NGOLeadershipServicePresenterProtocol & NGOLeadershipServiceInteractorOutputProtocol = NGOLeadershipServicePresenter()
        let interactor: NGOLeadershipServiceInteractorInputProtocol = NGOLeadershipServiceInteractor()
       
        let wireFrame: NGOLeadershipServiceWireFrameProtocol = NGOLeadershipServiceWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        let viewController = view as! NGOLeadershipVC
        viewController.loginRes = loginRes
        NavigationHelper.setRootViewController(withViewController: viewController)
    }
    
    func presentBuisnessBoard(loginRes:LoginModel , key : String)
    {
        NGOBusinessServiceWireFrame.presentNGOBusinessServiceModule(fromView: self, loginRes: loginRes , key:key )
           //NGOLeadershipServiceWireFrame.presentNGOLeadershipServiceModule(fromView: self, loginRes: loginRes)
    }
    
    func presentTechnicalBoard(loginRes:LoginModel)
    {
        DashboardWireFrame.presentDashboardModule(fromView: self, loginRes: loginRes)
    }
    
    func presentOrderJourneyDetail()
    {
         OrderDetailWireFrame.presentOrderJourneyDetailModule(fromView: self)
    }
    func presentRechargeJourneyDetail()
      {
           NGORechargeJourneyDetailWireFrame.presentNGORechargeJourneyDetailModule(fromView: self )
      }
      

}
