//
//  NGOBusinessServiceVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit


class NGOLeadershipVC: BaseViewController,NGOLeadershipServiceProtocol,UITableViewDelegate,UITableViewDataSource  {
    
    
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var CollectionVwldrShip: UICollectionView!
    @IBOutlet weak var currentDate: UILabel!
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var itemsPerRow: CGFloat = 1
    @IBOutlet weak var tblVwLeadership: UITableView!
    var presenter: NGOLeadershipServicePresenterProtocol?
    var itemsInRow: CGFloat = 1
    var arrList : [List]?
    var loginRes : LoginModel?
    var applicationArray = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateFormat = "yyyy-MM-dd"
       // let dateString = dateFormatterGet.string(from: Date())
        //self.currentDate.text = HelperMethods().formatDateOrderJourney(date:dateFormatterGet.string(from: Date()))
       // print(self.currentDate.text)
         tblVwLeadership.register(UINib(nibName: "LeaderShipBoardTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaderShipBoardTableViewCell")
        CollectionVwldrShip?.register(UINib(nibName: "UserfullLinksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"UserfullLinksCollectionViewCell")
        CollectionVwldrShip.register(UINib(nibName:"HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        print("presenter value = \(self.loginRes!)")
        self.presenter?.request(loginApp:self.loginRes!)
         usefullLinkArrayForNGOBuisnessBoard()
    }

    static func instantiate() -> NGOLeadershipServiceProtocol{
        return UIStoryboard(name: "NGOLeadershipVC", bundle: nil).instantiateViewController(withIdentifier: "NGOLeadershipVC") as! NGOLeadershipVC
    }
    
    override func viewDidLayoutSubviews() {
         self.tblHeightConstraint.constant = self.tblVwLeadership.contentSize.height + 40
               self.view.setNeedsLayout()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:true)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated:true)
    }    
    
     func usefullLinkArrayForNGOBuisnessBoard()
     {
         applicationArray.removeAll()
         let arr = loginRes?.respData[0].respMsg.responsePayload.applications
         let ngoArray = arr?.filter { ($0).applicability == "NGO" } ?? []
         let bothAppArray = arr?.filter { ($0).applicability == "BOTH" } ?? []
         
         applicationArray.append(contentsOf: ngoArray)
         applicationArray.append(contentsOf: bothAppArray)
         applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != "F_NGO_NextGenOPS_Busi" }
         applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != "F_RAN" }
         
     }
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        DashboardVC.addedToggleView?.removeFromSuperview()
        super.showToastMsg(error: error)
    }
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    func reloadData(data: [List]) {
        arrList = data
        print("count-arrlist \(arrList?.count)")
        self.tblVwLeadership.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return arrList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:LeaderShipBoardTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LeaderShipBoardTableViewCell", for: indexPath as IndexPath) as! LeaderShipBoardTableViewCell
        print(cell)
        if let listObj = arrList?[indexPath.row] {
            cell.lblboardItem.text = listObj.title
            cell.vwCircle.backgroundColor = UIColor.init(hexString: listObj.color ?? "00000")
        }
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
           1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: false)
        let list = arrList?[indexPath.row]
        self.presenter?.presentBuisnessBoard(loginRes: self.loginRes!, key: list?.key ?? "")
       }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopAnimating()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tblHeightConstraint.constant = self.tblVwLeadership.contentSize.height
        self.view.setNeedsLayout()
    }
}

extension NGOLeadershipVC :UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserfullLinksCollectionViewCell", for: indexPath) as! UserfullLinksCollectionViewCell
            cell.titleLabel.text = (self.applicationArray[indexPath.item] as? LoginApplication)?.applicationName ?? ""
            let color2 = (self.applicationArray[indexPath.item] as? LoginApplication)?.colourCodeForApp ?? ""
            cell.backgroundVw.backgroundColor = UIColor.init(hexString : color2)
            let iconImage = (self.applicationArray[indexPath.item] as? LoginApplication)?.applicationIcon ?? ""
            cell.iconImageVw.image = UIImage(named:iconImage)
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  1
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
             return 1
    }
         
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
                let normalText1 = "Useful Links"
        
//                let attributedString1 = NSMutableAttributedString(string:normalText1)
//
//                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 15.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
//                let boldString1 = NSMutableAttributedString(string: "", attributes:attrs1 as [NSAttributedString.Key : Any])
//                attributedString1.append(boldString1)
                headerView?.headerTitleLbl.font = UIFont.init(name:"JioType-Medium", size: 15.0)
                headerView?.headerTitleLbl.text = normalText1
                headerView?.headerTitleLbl.textColor = UIColor.black
                headerView?.alarmLbl.isHidden = true
                return headerView ?? UICollectionReusableView()
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
            if((self.applicationArray[indexPath.item] as? LoginApplication)?.applicationName.contains("Technical Board") ?? false)
            {
                self.presenter?.presentTechnicalBoard(loginRes:loginRes!)
            }
    }
}

extension NGOLeadershipVC : UICollectionViewDelegateFlowLayout {

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    if(itemsPerRow == 1)
    {
        return CGSize(width: 100, height: 140)
    }
    else
    {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsInRow = 2
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        return CGSize(width: widthPerItem, height: 130)
    }
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return sectionInsets
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return sectionInsets.left
}


func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
{
    return CGSize(width: collectionView.frame.width, height:20)
}

}
