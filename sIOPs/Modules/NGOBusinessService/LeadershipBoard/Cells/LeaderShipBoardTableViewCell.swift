//
//  LeaderShipBoardTableViewCell.swift
//  sIOPs
//
//  Created by Annapurna on 14/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import UIKit

class LeaderShipBoardTableViewCell: UITableViewCell {

    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var vwCircle: UIView!
    @IBOutlet weak var lblboardItem: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        //yourTableViewCell.contentView.layer.cornerRadius = 5
        self.vwBackground.layer.borderWidth = 0.5
        self.vwBackground.layer.borderColor = UIColor.white.cgColor
        self.vwBackground.layer.masksToBounds = true
        self.vwBackground.layer.cornerRadius = 5
        
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
