// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let leadershipBoardModule = try LeadershipBoardModule(json)

import Foundation

// MARK: - LeadershipBoardModule
@objcMembers class LeadershipBoardModule: NSObject, Codable {
    let respInfo: leadershipRespInfo?
    let respData: [leadershipRespDatum]?

    init(respInfo: leadershipRespInfo?, respData: [leadershipRespDatum]?) {
        self.respInfo = respInfo
        self.respData = respData
    }
}

// MARK: LeadershipBoardModule convenience initializers and mutators

extension LeadershipBoardModule {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(LeadershipBoardModule.self, from: data)
        self.init(respInfo: me.respInfo, respData: me.respData)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        respInfo: leadershipRespInfo?? = nil,
        respData: [leadershipRespDatum]?? = nil
    ) -> LeadershipBoardModule {
        return LeadershipBoardModule(
            respInfo: respInfo ?? self.respInfo,
            respData: respData ?? self.respData
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespDatum
@objcMembers class leadershipRespDatum: NSObject, Codable {
    let busiCode, transactionID, code, message: String?
    let isEncrypt: Bool?
    let respMsg: leadershipRespMsg?

    enum CodingKeys: String, CodingKey {
        case busiCode
        case transactionID = "transactionId"
        case code, message, isEncrypt, respMsg
    }

    init(busiCode: String?, transactionID: String?, code: String?, message: String?, isEncrypt: Bool?, respMsg: leadershipRespMsg?) {
        self.busiCode = busiCode
        self.transactionID = transactionID
        self.code = code
        self.message = message
        self.isEncrypt = isEncrypt
        self.respMsg = respMsg
    }
}

// MARK: RespDatum convenience initializers and mutators

extension leadershipRespDatum {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(leadershipRespDatum.self, from: data)
        self.init(busiCode: me.busiCode, transactionID: me.transactionID, code: me.code, message: me.message, isEncrypt: me.isEncrypt, respMsg: me.respMsg)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        busiCode: String?? = nil,
        transactionID: String?? = nil,
        code: String?? = nil,
        message: String?? = nil,
        isEncrypt: Bool?? = nil,
        respMsg: leadershipRespMsg?? = nil
    ) -> leadershipRespDatum {
        return leadershipRespDatum(
            busiCode: busiCode ?? self.busiCode,
            transactionID: transactionID ?? self.transactionID,
            code: code ?? self.code,
            message: message ?? self.message,
            isEncrypt: isEncrypt ?? self.isEncrypt,
            respMsg: respMsg ?? self.respMsg
        )
    }

    func jsonData() throws -> Data {
        return try newLJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespMsg
@objcMembers class leadershipRespMsg: NSObject, Codable {
    let responseHeader: leadershipResponseHeader?
    let responsePayload: leadershipResponsePayload?

    init(responseHeader: leadershipResponseHeader?, responsePayload: leadershipResponsePayload?) {
        self.responseHeader = responseHeader
        self.responsePayload = responsePayload
    }
}

// MARK: RespMsg convenience initializers and mutators

extension leadershipRespMsg {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(leadershipRespMsg.self, from: data)
        self.init(responseHeader: me.responseHeader, responsePayload: me.responsePayload)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        responseHeader: leadershipResponseHeader?? = nil,
        responsePayload: leadershipResponsePayload?? = nil
    ) -> leadershipRespMsg {
        return leadershipRespMsg(
            responseHeader: responseHeader ?? self.responseHeader,
            responsePayload: responsePayload ?? self.responsePayload
        )
    }

    func jsonData() throws -> Data {
        return try newLJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponseHeader
@objcMembers class leadershipResponseHeader: NSObject, Codable {
    let timestamp, message: String?
    let status: Int?
    let title, path: String?

    init(timestamp: String?, message: String?, status: Int?, title: String?, path: String?) {
        self.timestamp = timestamp
        self.message = message
        self.status = status
        self.title = title
        self.path = path
    }
}

// MARK: ResponseHeader convenience initializers and mutators

extension leadershipResponseHeader {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(leadershipResponseHeader.self, from: data)
        self.init(timestamp: me.timestamp, message: me.message, status: me.status, title: me.title, path: me.path)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        timestamp: String?? = nil,
        message: String?? = nil,
        status: Int?? = nil,
        title: String?? = nil,
        path: String?? = nil
    ) -> leadershipResponseHeader {
        return leadershipResponseHeader(
            timestamp: timestamp ?? self.timestamp,
            message: message ?? self.message,
            status: status ?? self.status,
            title: title ?? self.title,
            path: path ?? self.path
        )
    }

    func jsonData() throws -> Data {
        return try newLJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponsePayload
@objcMembers class leadershipResponsePayload: NSObject, Codable {
    let userName: String?
    let list: [List]?

    init(userName: String?, list: [List]?) {
        self.userName = userName
        self.list = list
    }
}

// MARK: ResponsePayload convenience initializers and mutators

extension leadershipResponsePayload {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(leadershipResponsePayload.self, from: data)
        self.init(userName: me.userName, list: me.list)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        userName: String?? = nil,
        list: [List]?? = nil
    ) -> leadershipResponsePayload {
        return leadershipResponsePayload(
            userName: userName ?? self.userName,
            list: list ?? self.list
        )
    }

    func jsonData() throws -> Data {
        return try newLJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - List
@objcMembers class List: NSObject, Codable {
    let title, color, key: String?

    init(title: String?, color: String?, key: String?) {
        self.title = title
        self.color = color
        self.key = key
    }
}

// MARK: List convenience initializers and mutators

extension List {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(List.self, from: data)
        self.init(title: me.title, color: me.color, key: me.key)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        title: String?? = nil,
        color: String?? = nil,
        key: String?? = nil
    ) -> List {
        return List(
            title: title ?? self.title,
            color: color ?? self.color,
            key: key ?? self.key
        )
    }

    func jsonData() throws -> Data {
        return try newLJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespInfo
@objcMembers class leadershipRespInfo: NSObject, Codable {
    let code, message: String?

    init(code: String?, message: String?) {
        self.code = code
        self.message = message
    }
}

// MARK: RespInfo convenience initializers and mutators

extension leadershipRespInfo {
    convenience init(data: Data) throws {
        let me = try newLJSONDecoder().decode(RespInfo.self, from: data)
        self.init(code: me.code, message: me.message)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        message: String?? = nil
    ) -> RespInfo {
        return RespInfo(
            code: code ?? self.code,
            message: message ?? self.message
        )
    }

    func jsonData() throws -> Data {
        return try newLJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newLJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newLJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
