//
//  NGOBusinessServiceInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class NGOLeadershipServiceInteractor: NGOLeadershipServiceInteractorInputProtocol {
  
      
    public enum NGOLeadershipError {
        case internetError(String)
        case serverMessage(String)
    }
    // MARK: Variables
    weak var presenter: NGOLeadershipServiceInteractorOutputProtocol?
    
    
    func request(loginApp: LoginModel) {
        
        let pubInfo: [String : Any] =
              ["timestamp": "20191119165948",
                                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                                "osType": "ios",
                                "lang": "en_US",
                                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
                
                let requestHeader:[String :Any] = [
                    "serviceName": "userInfo"
                ]
                
                let requestBody:[String : Any]
                     requestBody = [
                        "appRoleCode": LoginModel.sharedInstance.respData[0].code ?? "",
                        "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                        "type": "userInfo"]
      
                
               let busiParams: [String : Any] = [
                    
                    "requestHeader": requestHeader,
                    "requestBody": requestBody,
                ]
                               
                let requestList: [String: Any] = [
                    "busiCode": "NGOBusinessSummary",
                    "isEncrypt": false,
                    "transactionId": "0001576654405065",
                    "busiParams":busiParams
                ]
                
                let jsonParameter = [
                    "pubInfo" : pubInfo,
                    "requestList" : [requestList]
                    ] as [String : Any]
                
                APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                    switch result {
                    case .success(let returnJson) :
                        print(returnJson)
                        
                        let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                        
                        switch status {
                        case Constants.status.OK:
                     
                                     let jsonDict =  returnJson as? [String:Any]
                                                     var respMsg : leadershipResponsePayload?
                                                     var model : LeadershipBoardModule?
                                                     do {                                                                                                                    
                                                        let json = try JSONSerialization.data(withJSONObject: jsonDict as Any)
                                                        model = try LeadershipBoardModule(data: json)
                                                        let arrRespData  = model?.respData
                                                        if let arrayResponse = arrRespData  {
                                                            for respsumObj in arrayResponse  {
                                                                respMsg = ((respsumObj.respMsg?.responsePayload!)!)
                                                            }
                                                        }
                                                                        print(model!)
                                                        self.presenter?.reloadData(data: respMsg?.list ?? [])
                                                                    } catch {
                                                                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                                                                        print(error)
                                                                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                                                                    }
                                 
                          case Constants.status.NOK:
                                 let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                self.presenter?.showFailError(error:message)
                        default:
                            self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                        }
                       
                   case .failure(let failure) :
                        switch failure {
                        case .connectionError:
                            self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                            
                        case .authorizationError(let errorJson):
                            self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .serverError:
                            self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                        case .newUpdate:
                            self.presenter?.stopLoader()
                        default:
                            self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                            
                        }
                    }
                })

            }


}

