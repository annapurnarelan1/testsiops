//
//  NGOBusinessServiceProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

typealias tupleVar = (NGOOrderActivationModel,NGORechargeActivationModel, NGOPortInPortOutModel ,NGOMACDModel)

/// Method contract between PRESENTER -> VIEW
protocol NGOBusinessServiceProtocol: class {
    var presenter: NGOBusinessServicePresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadALLApiSData (_ tuple : tupleVar)
    
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGOBusinessServiceWireFrameProtocol: class {
    static  func presentNGOBusinessServiceModule(fromView:AnyObject,loginRes:LoginModel , key: String)
    func presentTechnicalBoard(loginRes:LoginModel)
    func presentOrderJourneyDetail()
    func presentRechargeJourneyDetail()
}

/// Method contract between VIEW -> PRESENTER
protocol NGOBusinessServicePresenterProtocol: class {
    var view: NGOBusinessServiceProtocol? { get set }
    var interactor: NGOBusinessServiceInteractorInputProtocol? { get set }
    var wireFrame: NGOBusinessServiceWireFrameProtocol? { get set }
    func requestCollectionOfAPIS()
    func presentTechnicalBoard(loginRes:LoginModel)
    func presentOrderJourneyDetail()
    func presentRechargeJourneyDetail()

    
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGOBusinessServiceInteractorOutputProtocol: class {
    
    func show(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadALLApiSData (_ tuple : tupleVar)
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGOBusinessServiceInteractorInputProtocol: class
{
    var presenter: NGOBusinessServiceInteractorOutputProtocol? { get set }
    func requestCollectionOfAPIS()
    
    
    
}
