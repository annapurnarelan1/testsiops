//
//  NGOBusinessServiceVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

enum sectionHeaderype : Int {
    case journey
    case rechargeJourney
    case bdscollection
    case ngoMacD
    case usefulllinks
}

struct Section {
    var name : String
    var items : [Item]
    var details : Any?
    var isExpnaded : Bool
    var headerType : sectionHeaderype
    
    init(name :  String , isExpnaded: Bool = false , items: [Item] , details : Any, headerType : sectionHeaderype) {
        self.name =  name
        self.isExpnaded = isExpnaded
        self.items = items
        self.details = details
        self.headerType = headerType
    }
}

struct Item {
    var name : String
    var detail : Any?
    var cellType: String
    var items : [Item]
    
    init(name :  String , detail: Any , items: [Item] , cellType : String) {
        self.name =  name
        self.detail = detail
        self.cellType = cellType
        self.items = items
    }
}


class NGOBusinessServiceVC: BaseViewController,NGOBusinessServiceProtocol  {
    
    var presenter: NGOBusinessServicePresenterProtocol?
    var apiTimer: Timer?
    public var detailSections: [Section] = []
    @IBOutlet weak var NGOBusinessCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 2.0, left: 10.0, bottom: 10.0, right: 10.0)
    var NGORechargeActivationModelResponse :NGORechargeActivationModel?
    var orderActivationModelResponse: NGOOrderActivationModel?
    var mobilePortInPortOutModelResponse : NGOPortInPortOutModel?
    var MacdModelResponse: NGOMACDModel?
    var isVisible = false
    var applicationDic : LoginApplication?
    var siteDownbtn:Int?
    var rechargeStateDetailList:[NGORechargeStage]?
    var rechargeTableView: UITableView!
    var activationTableview: UITableView!
    var loginRes:LoginModel?
    var applicationArray = [Any]()
    var senderSelectedIndex:Int?
    private let spacing:CGFloat = 10.0
    var refresher:UIRefreshControl!
    var keyBusinness : String = ""
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    var largePhotoIndexPath: IndexPath? {
      didSet {
        // 2
        var indexPaths: [IndexPath] = []
        if let largePhotoIndexPath = largePhotoIndexPath {
          indexPaths.append(largePhotoIndexPath)
        }

        if let oldValue = oldValue {
          indexPaths.append(oldValue)
        }
        // 3
        self.NGOBusinessCollectionView.performBatchUpdates({
        }) { _ in
          // 4
          if let largePhotoIndexPath = self.largePhotoIndexPath {
            let detail = self.detailSections[largePhotoIndexPath.section]
            if detail.headerType == .journey {
//                 OrderDetailWireFrame.presentOrderDetailModule(fromView: self)
                NGORechargeJourneyDetailWireFrame.presentNGORechargeJourneyDetailModule(fromView: self)
            }
          }
        }
      }
    }

    
    func createDetailSection()  {
        self.detailSections = []
        
        print("key \(keyBusinness)")
        if keyBusinness == "NGOOrderActivation"  {
            
            if (orderActivationModelResponse?.orderTrends!.count)! > 0{
                     var journeySectionItems:[Item] = []
                     journeySectionItems.append(Item(name: "", detail: orderActivationModelResponse?.orderTrends as Any ,items: [], cellType: "Text"))
//                     self.detailSections.append(Section(name: "journeySection", items: journeySectionItems, details: orderActivationModelResponse?.orderStages as Any  , headerType: .journey))
                
                 self.detailSections.append(Section(name: "journeySection", isExpnaded: true, items: journeySectionItems, details: orderActivationModelResponse?.orderStages as Any, headerType: .journey))  // SECTION FIRST
                 }
            }
        
        
        else if keyBusinness == "NGORechargeActivation" {
            if (NGORechargeActivationModelResponse?.rechargeTrends.count)! > 0 {
                       var rechargeJourneySectionItems : [Item] = []
                       rechargeJourneySectionItems.append(Item(name: "", detail: NGORechargeActivationModelResponse?.rechargeTrends as Any ,items: [], cellType: "Text"))
//                       self.detailSections.append(Section(name: "rechargeJourneySection", items: rechargeJourneySectionItems, details: NGORechargeActivationModelResponse?.rechargeStages as Any   , headerType: .rechargeJourney))
                
                self.detailSections.append(Section(name: "rechargeJourneySection", isExpnaded: true, items: rechargeJourneySectionItems, details: NGORechargeActivationModelResponse?.rechargeStages as Any, headerType: .rechargeJourney)) //SECTION SECOND
                   }
        }
        else if keyBusinness == "NGOMACDDetails" {
            
            var macDItems : [Item] = []
                      macDItems.append(Item(name: "", detail: MacdModelResponse?.macdHeaderList as Any , items: [], cellType: "Text"))
//                      self.detailSections.append(Section(name: "macdSection", items: macDItems, details: MacdModelResponse as Any, headerType: .ngoMacD))
            
                self.detailSections.append(Section(name: "macdSection", isExpnaded: true, items: macDItems, details: MacdModelResponse as Any, headerType: .ngoMacD)) //SECTION THIRD
        }
        
        else if keyBusinness == "NGOPortInPortOutCountTrends" {
          if (mobilePortInPortOutModelResponse?.portinoutListTrends.count)! > 0{
                                        var BDSSectionItems:[Item] = []
                                        BDSSectionItems.append(Item(name: "", detail: mobilePortInPortOutModelResponse?.portinoutListTrends as Any ,items: [], cellType: "Text"))
            
//                                        self.detailSections.append(Section(name: "bdsSection", items: BDSSectionItems, details: mobilePortInPortOutModelResponse as Any   , headerType: .bdscollection))
            
            self.detailSections.append(Section(name: "bdsSection", isExpnaded: true, items: BDSSectionItems, details: mobilePortInPortOutModelResponse as Any , headerType: .bdscollection)) // SECTION Fourth
                                    }
        }
        
        /*
         
         if (orderActivationModelResponse?.orderTrends!.count)! > 0{
                             var journeySectionItems:[Item] = []
                             journeySectionItems.append(Item(name: "", detail: orderActivationModelResponse?.orderTrends as Any ,items: [], cellType: "Text"))
                             self.detailSections.append(Section(name: "journeySection", items: journeySectionItems, details: orderActivationModelResponse?.orderStages as Any   , headerType: .journey))  // SECTION FIRST
                         }
        if (orderActivationModelResponse?.orderTrends!.count)! > 0{
            var journeySectionItems:[Item] = []
            journeySectionItems.append(Item(name: "", detail: orderActivationModelResponse?.orderTrends as Any ,items: [], cellType: "Text"))
            self.detailSections.append(Section(name: "journeySection", items: journeySectionItems, details: orderActivationModelResponse?.orderStages as Any   , headerType: .journey))  // SECTION FIRST
        }
        
        if (NGORechargeActivationModelResponse?.rechargeTrends.count)! > 0 {
            var rechargeJourneySectionItems : [Item] = []
            rechargeJourneySectionItems.append(Item(name: "", detail: NGORechargeActivationModelResponse?.rechargeTrends as Any ,items: [], cellType: "Text"))
            self.detailSections.append(Section(name: "rechargeJourneySection", items: rechargeJourneySectionItems, details: NGORechargeActivationModelResponse?.rechargeStages as Any   , headerType: .rechargeJourney))  //SECTION SECOND
        }
        
        if (mobilePortInPortOutModelResponse?.portinoutListTrends.count)! > 0{
            var BDSSectionItems:[Item] = []
            BDSSectionItems.append(Item(name: "", detail: mobilePortInPortOutModelResponse?.portinoutListTrends as Any ,items: [], cellType: "Text"))
            self.detailSections.append(Section(name: "bdsSection", items: BDSSectionItems, details: mobilePortInPortOutModelResponse as Any   , headerType: .bdscollection))  // SECTION THIRD
        }
        
        
        if (MacdModelResponse?.macdHeaderList.count)! > 0 {
            var macDItems : [Item] = []
            macDItems.append(Item(name: "", detail: MacdModelResponse?.macdHeaderList as Any , items: [], cellType: "Text"))
            self.detailSections.append(Section(name: "macdSection", items: macDItems, details: MacdModelResponse as Any, headerType: .ngoMacD))  // SECTION FOURTH
        }
        
        if self.applicationArray.count > 0 {
            var usefulLinks : [Item] = []
            usefulLinks.append(Item(name: "", detail: self.applicationArray , items: [], cellType: "Text"))
            self.detailSections.append(Section(name: "usefulLinksSection", isExpnaded: true, items: usefulLinks, details: (Any).self , headerType: .usefulllinks))  // SECTION FIFTH : NO ITEMS
        }
  */
        NGOBusinessCollectionView.reloadData()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refresher = UIRefreshControl()
        self.NGOBusinessCollectionView.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.init(red: 44, green: 88, blue: 156)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.NGOBusinessCollectionView.addSubview(refresher)
        
        
        self.createTimer()
        NGOBusinessCollectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier:"defaultCollectionCell")
        
        NGOBusinessCollectionView?.register(UINib(nibName: "UserfullLinksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"UserfullLinksCollectionViewCell")
        
        NGOBusinessCollectionView?.register(UINib(nibName: "NGOMACDTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"NGOMACDTrendCollectionViewCell")
        NGOBusinessCollectionView?.register(UINib(nibName: "NGORechargeJourneyTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"NGORechargeJourneyTrendCollectionViewCell")
        NGOBusinessCollectionView?.register(UINib(nibName: "NGOOrderActivationTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"NGOOrderActivationTrendCollectionViewCell")
        NGOBusinessCollectionView?.register(UINib(nibName: "NGOPortInPortOutTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"NGOPortInPortOutTrendCollectionViewCell")
        
        NGOBusinessCollectionView?.register(UINib(nibName:  "ThreeButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ThreeButtonCollectionView")
        
        
        NGOBusinessCollectionView.register(UINib(nibName:  "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        NGOBusinessCollectionView.register(UINib(nibName:  "NGOMACDCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "NGOMACDCollectionViewCell")
        
        NGOBusinessCollectionView.register(UINib(nibName:  "BSDCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "BSDCollectionViewCell")
        
        NGOBusinessCollectionView.register(UINib(nibName:  "JourneyCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "JourneyCollectionViewCell")
        NGOBusinessCollectionView.register(UINib(nibName:  "NGORechargeJourneyCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "NGORechargeJourneyCollectionViewCell")
        
        NGOBusinessCollectionView.register(UINib(nibName: "NGOTrendsReusableFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "NGOTrendsReusableFooterView")
        
        NGOBusinessCollectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "DefultFooterIdentifier")
        
        NGOBusinessCollectionView?.register(UINib(nibName: "SingleButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"SingleButtonCollectionViewCell")
        
       
        usefullLinkArrayForNGOBuisnessBoard()
    }
    
    func createTimer() {
        if apiTimer == nil {
            apiTimer = Timer.scheduledTimer(timeInterval: 240 ,
                                            target: self,
                                            selector: #selector(updateAPIs),
                                            userInfo: nil,
                                            repeats: true)
        }
    }
    @objc func updateAPIs()
    {
        startAnimating()
        self.presenter?.requestCollectionOfAPIS()
    }
    
    
    @objc func loadData() {
        stopRefresher()
        self.presenter?.requestCollectionOfAPIS()
        //Call this to stop refresher
    }
    
    func stopRefresher() {
        refresher.endRefreshing()
    }
    static func instantiate() -> NGOBusinessServiceProtocol{
        return UIStoryboard(name: "NGOBusinessService", bundle: nil).instantiateViewController(withIdentifier: "NGOBusinessServiceVC") as! NGOBusinessServiceVC
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
         super.viewWillAppear(animated)
         HelperMethods().removeCustomBarButtons(self)
         HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
         self.navigationController?.interactivePopGestureRecognizer?.delegate = self
         self.navigationController?.setNavigationBarHidden(false, animated: false)
         self.navigationItem.setHidesBackButton(false, animated:true)
        startAnimating()
               self.presenter?.requestCollectionOfAPIS()
    }
    
    
    
    func reloadALLApiSData(_ tuple: tupleVar) {
        
        stopAnimating()
        NGORechargeActivationModelResponse = tuple.1
        rechargeStateDetailList = NGORechargeActivationModelResponse?.rechargeStages
        orderActivationModelResponse = tuple.0
        mobilePortInPortOutModelResponse = tuple.2
        MacdModelResponse = tuple.3
        createDetailSection()
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        DashboardVC.addedToggleView?.removeFromSuperview()
        super.showToastMsg(error: error)
    }
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    
    @objc func infraDetailSiteDown(sender:Any)
    {
        
        let btn = (sender as? UIButton)!
        siteDownbtn = btn.tag
        if(btn.tag <= 3)
        {
            startAnimating()
        }
    }
    
    func usefullLinkArrayForNGOBuisnessBoard()
    {
        applicationArray.removeAll()
        let arr = loginRes?.respData[0].respMsg.responsePayload.applications
        let ngoArray = arr?.filter { ($0).applicability == "NGO" } ?? []
        let bothAppArray = arr?.filter { ($0).applicability == "BOTH" } ?? []
        
        applicationArray.append(contentsOf: ngoArray)
        applicationArray.append(contentsOf: bothAppArray)
        applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != "F_NGO_NextGenOPS_Busi" }
        applicationArray = applicationArray.filter { ($0 as? LoginApplication)?.applicationUCode != "F_RAN" }
        
    }
    
    @objc func infracellImpactedAction(sender:Any)
    {
        let btn = (sender as? UIButton)!
        if(btn.tag > 3)
        {
            self.showFailError(error: "Coming Soon")
        }
    }
    
    @objc func makeAlarmCall()
    {
        startAnimating()
    }
    
    @objc func viewDetailAction(_ sender:UIButton)
    {
        
        self.toggleSection(section: sender.tag)
    }
    
}

extension NGOBusinessServiceVC :UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "defaultCollectionCell", for: indexPath)
        
        let item: Item =  self.detailSections[indexPath.section].items[indexPath.row] ?? Item(name: "", detail: "",  items: [] ,cellType: "Text")
        
        if self.detailSections[indexPath.section].headerType == .journey {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NGOOrderActivationTrendCollectionViewCell", for: indexPath) as! NGOOrderActivationTrendCollectionViewCell
            
            if let detailArray = item.detail as? [NGOOrderTrend] , detailArray.count > 0
            {
                cell.orderActivationTrendsArray = detailArray
                cell.journeyTableView.scrollToRow(at: IndexPath(row: detailArray.count-1, section: 0), at: .bottom, animated: true)
            }
            
            return cell
        }
            
        else if self.detailSections[indexPath.section].headerType == .rechargeJourney {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NGORechargeJourneyTrendCollectionViewCell", for: indexPath) as! NGORechargeJourneyTrendCollectionViewCell
            
            if let detailArray = item.detail as? [NGORechargeTrend] , detailArray.count > 0 {
                cell.recharegeActivationTrendsArray = detailArray
                cell.journeyTableView.scrollToRow(at: IndexPath(row: detailArray.count-1, section: 0), at: .bottom, animated: true)
                
            }
            return cell
        }
            
        else if self.detailSections[indexPath.section].headerType == .bdscollection {
            
            if let detailArray = item.detail as? [NGOPortInPortOutTrend] , detailArray.count > 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NGOPortInPortOutTrendCollectionViewCell", for: indexPath) as! NGOPortInPortOutTrendCollectionViewCell
                if(( mobilePortInPortOutModelResponse?.portinoutListTrends?.count) != nil) {
                    cell.portInPortOutTrendArray = mobilePortInPortOutModelResponse?.portinoutListTrends
                }
                return cell
            }
        }
            
        else if self.detailSections[indexPath.section].headerType == .ngoMacD{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NGOMACDTrendCollectionViewCell", for: indexPath) as! NGOMACDTrendCollectionViewCell
            if let detailArray = item.detail as? [NGOMACDTrendDetails] , detailArray.count > 0 {
                cell.macdTrendsArray = detailArray
                
            }
            
            return cell
        }
            
        else if  self.detailSections[indexPath.section].headerType == .usefulllinks {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserfullLinksCollectionViewCell", for: indexPath) as! UserfullLinksCollectionViewCell
            if let detailArray = item.detail as? [Any] , detailArray.count > 0 {
                cell.titleLabel.text = (self.applicationArray[indexPath.item] as? LoginApplication)?.applicationName ?? ""
                let color2 = (self.applicationArray[indexPath.item] as? LoginApplication)?.colourCodeForApp ?? ""
                cell.backgroundVw.backgroundColor = UIColor.init(hexString : color2)
                let iconImage = (self.applicationArray[indexPath.item] as? LoginApplication)?.applicationIcon ?? ""
                cell.iconImageVw.image = UIImage(named:iconImage)
                
            }
            return cell
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  self.detailSections[section].isExpnaded ? self.detailSections[section].items.count : 0
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.detailSections.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionType : Section =  self.detailSections[indexPath.section]
        
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            if sectionType.headerType == .journey {
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "JourneyCollectionViewCell", for: indexPath) as? JourneyCollectionViewCell
                
                headerView?.btnJourneySatus.backgroundColor = UIColor.green
                headerView?.btnJourneySatus.layer.cornerRadius = (headerView?.btnJourneySatus.frame.size.height)!/2
                
                if (( orderActivationModelResponse?.orderStages?.count) != nil) {
                    headerView?.orderActivationArray = orderActivationModelResponse?.orderStages
                    headerView?.btnJourney.tag = sectionType.headerType.rawValue
                    headerView?.btnJourney.addTarget(self, action: #selector(handleTap(_:)), for: .touchUpInside)
                    headerView?.journeyTableView.reloadData()
                }
                return headerView ?? UICollectionReusableView()
            }
                
            else if sectionType.headerType == .rechargeJourney {
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "NGORechargeJourneyCollectionViewCell", for: indexPath) as? NGORechargeJourneyCollectionViewCell
                
                headerView?.btnJourneySatus.backgroundColor = UIColor.green
                headerView?.btnJourneySatus.layer.cornerRadius = (headerView?.btnJourneySatus.frame.size.height)!/2
                headerView?.btnJourneySatus.isHidden = false
                headerView?.btnRechargeJourney.tag = sectionType.headerType.rawValue
                headerView?.btnRechargeJourney.addTarget(self, action: #selector(handleTap(_:)), for: .touchUpInside)
                if(( rechargeStateDetailList?.count) != nil) {
                    headerView?.rechargeActivationArray = rechargeStateDetailList
                    headerView?.rechargeJourneyTableView.reloadData()
                }
                //                tapGesture.delegate = self
                //                headerView?.isUserInteractionEnabled = true
                //                headerView?.addGestureRecognizer(tapGesture)
                return headerView ?? UICollectionReusableView()
            }
                
            else if sectionType.headerType == .bdscollection {
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "BSDCollectionViewCell", for: indexPath) as? BSDCollectionViewCell
                
                headerView?.btnStatus.backgroundColor = UIColor.green
                headerView?.btnStatus.layer.cornerRadius = (headerView?.btnStatus.frame.size.height)!/2
                headerView?.btnStatus.isHidden = true
                
                headerView?.firstLabel.text = "Port In"
                headerView?.descriptionFirstLbl.text = "Yesterday: \(mobilePortInPortOutModelResponse?.yesPortInTotal ?? 0)"
                headerView?.titleLbl.text = "\(mobilePortInPortOutModelResponse?.todayPortInTotal ?? 0)"
                
                
                headerView?.poFirstLabel.text = "Port out"
                headerView?.poDescriotionLabel.text = " Yesterday : \(mobilePortInPortOutModelResponse?.yesPortOutTotalCompleted ?? 0)"
                headerView?.poTitleLabel.text = "  \(mobilePortInPortOutModelResponse?.todatPortOutTotalCompleted ?? 0)"
                
                return headerView ?? UICollectionReusableView()
            }
                
                
            else if sectionType.headerType == .ngoMacD {
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "NGOMACDCollectionViewCell", for: indexPath) as? NGOMACDCollectionViewCell
                
                headerView?.firstLabel.text = "Sim change"
                headerView?.descriptionFirstLbl.text = "\(MacdModelResponse?.simChangeCount ?? 0)"
                headerView?.secondlbl.text = "IR"
                headerView?.descriptionSecondLbl.text = "\(MacdModelResponse?.irWatchCount ?? 0)"
                headerView?.thirdLbl.text = "Apple iWatch"
                
                headerView?.descriptionThirdLbl.text = "\(MacdModelResponse?.appleIWatchCount ?? 0)"
                return headerView ?? UICollectionReusableView()
            }
                
            else if sectionType.headerType == .usefulllinks
            {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
                let normalText1 = "Useful Links"
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 16.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
                let boldString1 = NSMutableAttributedString(string: "", attributes:attrs1 as [NSAttributedString.Key : Any])
                attributedString1.append(boldString1)
                headerView?.headerTitleLbl.attributedText = attributedString1
                headerView?.headerTitleLbl.textColor = UIColor.black
                headerView?.alarmLbl.isHidden = true
                return headerView ?? UICollectionReusableView()
            }
            
            return UICollectionReusableView()
            
        case UICollectionView.elementKindSectionFooter:
            
            if sectionType.headerType != .usefulllinks {
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "NGOTrendsReusableFooterView", for: indexPath) as? NGOTrendsReusableFooterView
                footerView?.trendsButton.addTarget(self, action: #selector(viewDetailAction(_:)), for: .touchUpInside)
                let myString = NSMutableAttributedString(string: "Trend", attributes:
                    [.underlineStyle: NSUnderlineStyle.single.rawValue])
                footerView?.trendsButton.titleLabel?.textColor = UIColor.black
                footerView?.trendsButton.setAttributedTitle(myString, for: .normal)
                footerView?.trendsButton.tag = indexPath.section
                return footerView ?? UICollectionReusableView()
            }
            else {
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DefultFooterIdentifier", for: indexPath)
                return footerView ?? UICollectionReusableView()
            }
        default:
            
            print("in default")
            //            /assert(false, "Unexpected element kind")
        }
        
        return UICollectionReusableView()
    }
    
    @objc func handleTap(_ sender : UIButton) {
        print(sender.tag)
        switch sender.tag {
        case 0:
           self.presenter?.presentOrderJourneyDetail()

        case 1:
            self.presenter?.presentRechargeJourneyDetail()

        default:
            print("In Default")
        }
        // do something here
    }
    
    func toggleSection(section: Int) {
        let collapsed = (self.detailSections[section].isExpnaded)
        
        if !collapsed {
            var detailSectionTemp = self.detailSections
            for index in 0..<detailSectionTemp.count-1 {
                let collapsed = self.detailSections[index].isExpnaded
                if collapsed {
                    self.detailSections[index].isExpnaded = !collapsed
                    self.NGOBusinessCollectionView.reloadSections(NSIndexSet(index: index) as IndexSet)
                }
            }
            detailSectionTemp.removeAll()
            let ExpandSection = !(self.detailSections[section].isExpnaded)
            self.detailSections[section].isExpnaded = ExpandSection
            self.NGOBusinessCollectionView.reloadData()
        }
        else if collapsed{
            let ExpandSection = !(self.detailSections[section].isExpnaded)
            self.detailSections[section].isExpnaded = ExpandSection
            self.NGOBusinessCollectionView.reloadData()
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if  self.detailSections[indexPath.section].headerType == .usefulllinks
        {
            if((self.applicationArray[indexPath.item] as? LoginApplication)?.applicationName.contains("Technical Board") ?? false)
            {
                self.presenter?.presentTechnicalBoard(loginRes:loginRes!)
            }
        }
    }
    
}

extension NGOBusinessServiceVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 2
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        return CGSize(width: widthPerItem, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        let sectionHeader = self.detailSections[section].headerType
        switch sectionHeader {
        case .journey :
            return CGSize(width: collectionView.frame.width, height:350)
        case .rechargeJourney :
            return CGSize(width: collectionView.frame.width, height:380)
        case .bdscollection , .ngoMacD:
            return CGSize(width: collectionView.frame.width, height:140)
        case .usefulllinks:
            return CGSize(width: collectionView.frame.width, height:80)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height:20)
    }
    
//    func collectionView(_ collectionView: UICollectionView,
//                        shouldSelectItemAt indexPath: IndexPath) -> Bool
//    {
//        if largePhotoIndexPath == indexPath {
//            largePhotoIndexPath = nil
//        } else {
//            largePhotoIndexPath = indexPath
//        }
//
//        return false
//    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopAnimating()
        apiTimer?.invalidate()
    }
}
