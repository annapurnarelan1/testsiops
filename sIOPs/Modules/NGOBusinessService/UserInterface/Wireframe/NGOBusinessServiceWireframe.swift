//
//  NGOBusinessServiceWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGOBusinessServiceWireFrame: NGOBusinessServiceWireFrameProtocol {
   
    
  
    
    
    // MARK: NGOBusinessServiceWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentNGOBusinessServiceModule(fromView:AnyObject,loginRes:LoginModel , key: String) {

        // Generating module components
        let view: NGOBusinessServiceProtocol = NGOBusinessServiceVC.instantiate()
        let presenter: NGOBusinessServicePresenterProtocol & NGOBusinessServiceInteractorOutputProtocol = NGOBusinessServicePresenter()
        let interactor: NGOBusinessServiceInteractorInputProtocol = NGOBusinessServiceInteractor()
       
        let wireFrame: NGOBusinessServiceWireFrameProtocol = NGOBusinessServiceWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NGOBusinessServiceVC
        viewController.loginRes = loginRes
        viewController.keyBusinness = key
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
    func presentTechnicalBoard(loginRes:LoginModel)
    {
        DashboardWireFrame.presentDashboardModule(fromView: self, loginRes: loginRes)
    }
    
    func presentOrderJourneyDetail()
    {
         OrderDetailWireFrame.presentOrderJourneyDetailModule(fromView: self)
    }
    func presentRechargeJourneyDetail()
      {
           NGORechargeJourneyDetailWireFrame.presentNGORechargeJourneyDetailModule(fromView: self )
      }
      

}
