//
//  NGOBusinessServicePresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGOBusinessServicePresenter:BasePresenter, NGOBusinessServicePresenterProtocol, NGOBusinessServiceInteractorOutputProtocol {
    
    typealias tupleVar = (NGOOrderActivationModel,NGORechargeActivationModel, NGOPortInPortOutModel ,NGOMACDModel)
    
    // MARK: Variables
    weak var view: NGOBusinessServiceProtocol?
    var interactor: NGOBusinessServiceInteractorInputProtocol?
    var wireFrame: NGOBusinessServiceWireFrameProtocol?
    let stringsTableName = "NGOBusinessService"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func presentTechnicalBoard(loginRes:LoginModel)
    {
        self.wireFrame?.presentTechnicalBoard(loginRes:loginRes)
    }
    
    func reloadALLApiSData(_ tuple: tupleVar) {
        self.view?.reloadALLApiSData(tuple)
    }
    
    func requestCollectionOfAPIS() {
        self.interactor?.requestCollectionOfAPIS()
    }
    
    func presentOrderJourneyDetail()
    {
        self.wireFrame?.presentOrderJourneyDetail()
    }
    
    func presentRechargeJourneyDetail()
    {
        self.wireFrame?.presentRechargeJourneyDetail()

    }

}
