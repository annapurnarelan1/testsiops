//
//	RechargeJourney.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RechargeJourneyDetailList : NSObject, NSCoding{

	var channel : String!
	var initiated : Int!
	var outlier : Int!
	var paymentAborted : Int!
	var paymentsuccess : Int!
	var pending : Int!
	var rechargePending : AnyObject!
	var rechargeSuccess : Int!
	var rechargeTotal : Int!
	var refund : Int!
	var total : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		channel = dictionary["channel"] as? String
		initiated = dictionary["initiated"] as? Int
		outlier = dictionary["outlier"] as? Int
		paymentAborted = dictionary["paymentAborted"] as? Int
		paymentsuccess = dictionary["paymentsuccess"] as? Int
		pending = dictionary["pending"] as? Int
		rechargePending = dictionary["rechargePending"] as? AnyObject
		rechargeSuccess = dictionary["rechargeSuccess"] as? Int
		rechargeTotal = dictionary["rechargeTotal"] as? Int
		refund = dictionary["refund"] as? Int
		total = dictionary["total"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if channel != nil{
			dictionary["channel"] = channel
		}
		if initiated != nil{
			dictionary["initiated"] = initiated
		}
		if outlier != nil{
			dictionary["outlier"] = outlier
		}
		if paymentAborted != nil{
			dictionary["paymentAborted"] = paymentAborted
		}
		if paymentsuccess != nil{
			dictionary["paymentsuccess"] = paymentsuccess
		}
		if pending != nil{
			dictionary["pending"] = pending
		}
		if rechargePending != nil{
			dictionary["rechargePending"] = rechargePending
		}
		if rechargeSuccess != nil{
			dictionary["rechargeSuccess"] = rechargeSuccess
		}
		if rechargeTotal != nil{
			dictionary["rechargeTotal"] = rechargeTotal
		}
		if refund != nil{
			dictionary["refund"] = refund
		}
		if total != nil{
			dictionary["total"] = total
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         channel = aDecoder.decodeObject(forKey: "channel") as? String
         initiated = aDecoder.decodeObject(forKey: "initiated") as? Int
         outlier = aDecoder.decodeObject(forKey: "outlier") as? Int
         paymentAborted = aDecoder.decodeObject(forKey: "paymentAborted") as? Int
         paymentsuccess = aDecoder.decodeObject(forKey: "paymentsuccess") as? Int
         pending = aDecoder.decodeObject(forKey: "pending") as? Int
         rechargePending = aDecoder.decodeObject(forKey: "rechargePending") as? AnyObject
         rechargeSuccess = aDecoder.decodeObject(forKey: "rechargeSuccess") as? Int
         rechargeTotal = aDecoder.decodeObject(forKey: "rechargeTotal") as? Int
         refund = aDecoder.decodeObject(forKey: "refund") as? Int
         total = aDecoder.decodeObject(forKey: "total") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if channel != nil{
			aCoder.encode(channel, forKey: "channel")
		}
		if initiated != nil{
			aCoder.encode(initiated, forKey: "initiated")
		}
		if outlier != nil{
			aCoder.encode(outlier, forKey: "outlier")
		}
		if paymentAborted != nil{
			aCoder.encode(paymentAborted, forKey: "paymentAborted")
		}
		if paymentsuccess != nil{
			aCoder.encode(paymentsuccess, forKey: "paymentsuccess")
		}
		if pending != nil{
			aCoder.encode(pending, forKey: "pending")
		}
		if rechargePending != nil{
			aCoder.encode(rechargePending, forKey: "rechargePending")
		}
		if rechargeSuccess != nil{
			aCoder.encode(rechargeSuccess, forKey: "rechargeSuccess")
		}
		if rechargeTotal != nil{
			aCoder.encode(rechargeTotal, forKey: "rechargeTotal")
		}
		if refund != nil{
			aCoder.encode(refund, forKey: "refund")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}

	}

}
