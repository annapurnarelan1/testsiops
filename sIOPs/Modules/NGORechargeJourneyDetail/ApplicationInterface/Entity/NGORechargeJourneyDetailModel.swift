//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGORechargeJourneyDetailModel : NSObject, NSCoding{

	var healthyStatus : Int!
	var rechargeJourney : [RechargeJourneyDetailList]!
	var rechargePending : [RechargePendingDetailList]!
	var rechargeStages : AnyObject!
	var rechargeTrends : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		healthyStatus = dictionary["healthyStatus"] as? Int
		rechargeJourney = [RechargeJourneyDetailList]()
		if let rechargeJourneyArray = dictionary["rechargeJourney"] as? [[String:Any]]{
			for dic in rechargeJourneyArray{
				let value = RechargeJourneyDetailList(fromDictionary: dic)
				rechargeJourney.append(value)
			}
		}
		rechargePending = [RechargePendingDetailList]()
		if let rechargePendingArray = dictionary["rechargePending"] as? [[String:Any]]{
			for dic in rechargePendingArray{
				let value = RechargePendingDetailList(fromDictionary: dic)
				rechargePending.append(value)
			}
		}
		rechargeStages = dictionary["rechargeStages"] as? AnyObject
		rechargeTrends = dictionary["rechargeTrends"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if healthyStatus != nil{
			dictionary["healthyStatus"] = healthyStatus
		}
		if rechargeJourney != nil{
			var dictionaryElements = [[String:Any]]()
			for rechargeJourneyElement in rechargeJourney {
				dictionaryElements.append(rechargeJourneyElement.toDictionary())
			}
			dictionary["rechargeJourney"] = dictionaryElements
		}
		if rechargePending != nil{
			var dictionaryElements = [[String:Any]]()
			for rechargePendingElement in rechargePending {
				dictionaryElements.append(rechargePendingElement.toDictionary())
			}
			dictionary["rechargePending"] = dictionaryElements
		}
		if rechargeStages != nil{
			dictionary["rechargeStages"] = rechargeStages
		}
		if rechargeTrends != nil{
			dictionary["rechargeTrends"] = rechargeTrends
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         healthyStatus = aDecoder.decodeObject(forKey: "healthyStatus") as? Int
         rechargeJourney = aDecoder.decodeObject(forKey :"rechargeJourney") as? [RechargeJourneyDetailList]
         rechargePending = aDecoder.decodeObject(forKey :"rechargePending") as? [RechargePendingDetailList]
         rechargeStages = aDecoder.decodeObject(forKey: "rechargeStages") as? AnyObject
         rechargeTrends = aDecoder.decodeObject(forKey: "rechargeTrends") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if healthyStatus != nil{
			aCoder.encode(healthyStatus, forKey: "healthyStatus")
		}
		if rechargeJourney != nil{
			aCoder.encode(rechargeJourney, forKey: "rechargeJourney")
		}
		if rechargePending != nil{
			aCoder.encode(rechargePending, forKey: "rechargePending")
		}
		if rechargeStages != nil{
			aCoder.encode(rechargeStages, forKey: "rechargeStages")
		}
		if rechargeTrends != nil{
			aCoder.encode(rechargeTrends, forKey: "rechargeTrends")
		}

	}

}
