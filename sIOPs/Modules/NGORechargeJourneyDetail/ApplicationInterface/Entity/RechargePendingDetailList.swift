//
//	RechargePending.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RechargePendingDetailList : NSObject, NSCoding{

	var enterprise : Int!
	var errormessage : String!
	var jiocom : Int!
	var jiomoney : Int!
	var jiophone : Int!
	var jioposlite : Int!
	var loadmoney : Int!
	var myjio : Int!
	var rposgt : Int!
	var rposrr : Int!
	var tpa : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		enterprise = dictionary["enterprise"] as? Int
		errormessage = dictionary["errormessage"] as? String
		jiocom = dictionary["jiocom"] as? Int
		jiomoney = dictionary["jiomoney"] as? Int
		jiophone = dictionary["jiophone"] as? Int
		jioposlite = dictionary["jioposlite"] as? Int
		loadmoney = dictionary["loadmoney"] as? Int
		myjio = dictionary["myjio"] as? Int
		rposgt = dictionary["rposgt"] as? Int
		rposrr = dictionary["rposrr"] as? Int
		tpa = dictionary["tpa"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if enterprise != nil{
			dictionary["enterprise"] = enterprise
		}
		if errormessage != nil{
			dictionary["errormessage"] = errormessage
		}
		if jiocom != nil{
			dictionary["jiocom"] = jiocom
		}
		if jiomoney != nil{
			dictionary["jiomoney"] = jiomoney
		}
		if jiophone != nil{
			dictionary["jiophone"] = jiophone
		}
		if jioposlite != nil{
			dictionary["jioposlite"] = jioposlite
		}
		if loadmoney != nil{
			dictionary["loadmoney"] = loadmoney
		}
		if myjio != nil{
			dictionary["myjio"] = myjio
		}
		if rposgt != nil{
			dictionary["rposgt"] = rposgt
		}
		if rposrr != nil{
			dictionary["rposrr"] = rposrr
		}
		if tpa != nil{
			dictionary["tpa"] = tpa
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         enterprise = aDecoder.decodeObject(forKey: "enterprise") as? Int
         errormessage = aDecoder.decodeObject(forKey: "errormessage") as? String
         jiocom = aDecoder.decodeObject(forKey: "jiocom") as? Int
         jiomoney = aDecoder.decodeObject(forKey: "jiomoney") as? Int
         jiophone = aDecoder.decodeObject(forKey: "jiophone") as? Int
         jioposlite = aDecoder.decodeObject(forKey: "jioposlite") as? Int
         loadmoney = aDecoder.decodeObject(forKey: "loadmoney") as? Int
         myjio = aDecoder.decodeObject(forKey: "myjio") as? Int
         rposgt = aDecoder.decodeObject(forKey: "rposgt") as? Int
         rposrr = aDecoder.decodeObject(forKey: "rposrr") as? Int
         tpa = aDecoder.decodeObject(forKey: "tpa") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if enterprise != nil{
			aCoder.encode(enterprise, forKey: "enterprise")
		}
		if errormessage != nil{
			aCoder.encode(errormessage, forKey: "errormessage")
		}
		if jiocom != nil{
			aCoder.encode(jiocom, forKey: "jiocom")
		}
		if jiomoney != nil{
			aCoder.encode(jiomoney, forKey: "jiomoney")
		}
		if jiophone != nil{
			aCoder.encode(jiophone, forKey: "jiophone")
		}
		if jioposlite != nil{
			aCoder.encode(jioposlite, forKey: "jioposlite")
		}
		if loadmoney != nil{
			aCoder.encode(loadmoney, forKey: "loadmoney")
		}
		if myjio != nil{
			aCoder.encode(myjio, forKey: "myjio")
		}
		if rposgt != nil{
			aCoder.encode(rposgt, forKey: "rposgt")
		}
		if rposrr != nil{
			aCoder.encode(rposrr, forKey: "rposrr")
		}
		if tpa != nil{
			aCoder.encode(tpa, forKey: "tpa")
		}

	}

}
