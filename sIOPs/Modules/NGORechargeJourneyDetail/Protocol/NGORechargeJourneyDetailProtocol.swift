//
//  NGORechargeJourneyDetailProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//




import Foundation


/// Method contract between PRESENTER -> VIEW
protocol NGORechargeJourneyDetailViewProtocol: class {
    var presenter: NGORechargeJourneyDetailPresenterProtocol? { get set }
    
    
    
    func show(image:String,error: String,description:String)
       func showError(error: String,description:String)
        func stopLoader()
       
       func showFailError(error:String)
       func reloadData(rechargeDetailResponse: NGORechargeJourneyDetailModel)
    
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGORechargeJourneyDetailWireFrameProtocol: class {
    static   func presentNGORechargeJourneyDetailModule(fromView:AnyObject)
    
    
}

/// Method contract between VIEW -> PRESENTER
protocol NGORechargeJourneyDetailPresenterProtocol: class {
    var view: NGORechargeJourneyDetailViewProtocol? { get set }
    var interactor: NGORechargeJourneyDetailInteractorInputProtocol? { get set }
    var wireFrame: NGORechargeJourneyDetailWireFrameProtocol? { get set }
    
    func requestRechargeJourneyDetailData()
    
    
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGORechargeJourneyDetailInteractorOutputProtocol: class {
    
    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func showAvailabilityError(error:String)
    func  reloadData(rechargeDetailResponse: NGORechargeJourneyDetailModel)
    
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGORechargeJourneyDetailInteractorInputProtocol: class
{
    var presenter: NGORechargeJourneyDetailInteractorOutputProtocol? { get set }
    func requestRechargeJourneyDetailData()
    
    
}
