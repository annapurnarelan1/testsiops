//
// NGORechargeJourneyDetailViewController.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGORechargeJourneyDetailViewController: BaseViewController,NGORechargeJourneyDetailViewProtocol{
    
    
    
    
    var presenter: NGORechargeJourneyDetailPresenterProtocol?
    
    @IBOutlet weak var tblRechargeDetails: UITableView!
    
    var type:String?
    var selected:String?
    var NGOResponse:NGO?
    var selectedArray:[NGOApplication]?
    var senderSelectedIndex:Int?
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    
    var rechargeResponse:NGORechargeJourneyDetailModel?
    var availableError = ""
    var performanceError = ""
    
    
    
    
    
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> NGORechargeJourneyDetailViewProtocol{
        return UIStoryboard(name: "NGORechargeJourneyDetail", bundle: nil).instantiateViewController(withIdentifier: "NGORechargeJourneyDetail") as! NGORechargeJourneyDetailViewController
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblRechargeDetails.register(UINib(nibName: "OrderJourneyDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderJourneyDetailTableViewCell")
        
//        self.presenter?.requestRechargeJourneyDetailData()
        
    }
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        if(sender.tag != senderSelectedIndex)
        {
            senderSelectedIndex = sender.tag
            
        }
        else
        {
            senderSelectedIndex = -1
            
        }
        self.tblRechargeDetails.reloadData()
        self.view.setNeedsLayout()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
        startAnimating()
        self.presenter?.requestRechargeJourneyDetailData()
        
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    func reloadData(rechargeDetailResponse: NGORechargeJourneyDetailModel) {
        stopAnimating()
        rechargeResponse = rechargeDetailResponse
        self.tblRechargeDetails.reloadData()
        
    }
    
}

extension NGORechargeJourneyDetailViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:OrderJourneyDetailTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "OrderJourneyDetailTableViewCell", for: indexPath as IndexPath) as? OrderJourneyDetailTableViewCell)!
        cell.viewdetailBtn.tag = indexPath.section
        cell.viewdetailBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)

        if(senderSelectedIndex ==  cell.viewdetailBtn.tag )
        {
            cell.viewdetailBtn.backgroundColor = UIColor.strongBlueColor
            cell.viewdetailBtn.setTitleColor(UIColor.white, for: .normal)
            cell.viewdetailBtn.setTitle("Hide Details", for: .normal)
            cell.secondView.isHidden = false
        }
        else
        {
            cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
            cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
            cell.viewdetailBtn.setTitle("View Details", for: .normal)
            cell.secondView.isHidden = true
            cell.detailView.isHidden = true
        }
        
        cell.headerLabel.text = rechargeResponse?.rechargeJourney[indexPath.section].channel ?? ""
        cell.firstOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(rechargeResponse?.rechargeJourney[indexPath.section].total ?? 0)")
        cell.firstOutlIerName.text = "Initiated"
        cell.secondOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(rechargeResponse?.rechargeJourney[indexPath.section].paymentAborted ?? 0)")
        cell.secondOutlIerName.text = "Payment Aborted"
        cell.thirdOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(rechargeResponse?.rechargeJourney[indexPath.section].paymentsuccess ?? 0)")
        cell.thirdOutlIerName.text = "Payment Success"
        cell.rejectedCount.text = HelperMethods().nuumberFormatting(value:"\(rechargeResponse?.rechargeJourney[indexPath.section].rechargeSuccess ?? 0)")
        cell.rejectedName.text = "Recharge Success"
        cell.inProgressCount.text = HelperMethods().nuumberFormatting(value:"\(rechargeResponse?.rechargeJourney[indexPath.section].pending ?? 0)")
        cell.inProgressName.text = "Pending"
        
        cell.tvPendingCount.text = HelperMethods().nuumberFormatting(value:"\(rechargeResponse?.rechargeJourney[indexPath.section].refund ?? 0)")
        cell.tvPendingName.text = "Refund"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //            return self.selectedArray?.count ?? 0
        
        return self.rechargeResponse?.rechargeJourney?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    
   
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
        {
           self.view.setNeedsLayout()


        }
    
    
    
    
}
