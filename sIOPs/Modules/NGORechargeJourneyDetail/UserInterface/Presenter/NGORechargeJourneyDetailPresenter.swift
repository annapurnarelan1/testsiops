//
//  NGORechargeJourneyDetailPresenter.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGORechargeJourneyDetailPresenter:BasePresenter, NGORechargeJourneyDetailPresenterProtocol, NGORechargeJourneyDetailInteractorOutputProtocol {
   
   
    
  
    
    
   
    // MARK: Variables
    weak var view: NGORechargeJourneyDetailViewProtocol?
    var interactor: NGORechargeJourneyDetailInteractorInputProtocol?
    var wireFrame: NGORechargeJourneyDetailWireFrameProtocol?
    let stringsTableName = "NGORechargeJourneyDetail"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
        
    
     func requestRechargeJourneyDetailData() {
                              self.interactor?.requestRechargeJourneyDetailData()

        }
        
    func reloadData(rechargeDetailResponse: NGORechargeJourneyDetailModel) {
                  self.view?.reloadData(rechargeDetailResponse: rechargeDetailResponse)

      }
      
    

    func showAvailabilityError(error:String)
    {
    }
   
    
   
}
