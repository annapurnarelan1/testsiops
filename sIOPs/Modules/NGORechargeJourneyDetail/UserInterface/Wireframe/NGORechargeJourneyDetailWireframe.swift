//
//  NGORechargeJourneyDetailWireframe.swift
//  sIOPs
//
//  Created by ASM ESPL on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGORechargeJourneyDetailWireFrame: NGORechargeJourneyDetailWireFrameProtocol {
    
    
    func presentNGORechargeJourneyDetailModule(fromView:AnyObject)  {
        NGORechargeJourneyDetailWireFrame.presentNGORechargeJourneyDetailModule(fromView: self)
    }
    
    
    // MARK: NGORechargeJourneyDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
  class func presentNGORechargeJourneyDetailModule(fromView:AnyObject)  {

        // Generating module components
        let view: NGORechargeJourneyDetailViewProtocol = NGORechargeJourneyDetailViewController.instantiate()
        let presenter: NGORechargeJourneyDetailPresenterProtocol & NGORechargeJourneyDetailInteractorOutputProtocol = NGORechargeJourneyDetailPresenter()
        let interactor: NGORechargeJourneyDetailInteractorInputProtocol = NGORechargeJourneyDetailInteractor()
       
        let wireFrame: NGORechargeJourneyDetailWireFrameProtocol = NGORechargeJourneyDetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NGORechargeJourneyDetailViewController
      //  viewController.application = application
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    

   
}

