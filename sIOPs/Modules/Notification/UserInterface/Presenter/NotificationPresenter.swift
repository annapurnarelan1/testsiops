//
//  NotificationPresenter.swift
//  sIOPs
//
//  Created by ASM ESPL on 26/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NotificationPresenter:BasePresenter, NotificationPresenterProtocol, NotificationInteractorOutputProtocol {
    
    
   
    // MARK: Variables
    weak var view: NotificationViewProtocol?
    var interactor: NotificationInteractorInputProtocol?
    var wireFrame: NotificationWireFrameProtocol?
    let stringsTableName = "Notification"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
     func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func requestData()
    {
        self.interactor?.requestData()
    }
    
     func reloadData(notificationResponse:NotificationModel)
    {
        self.view?.reloadData(notificationResponse:notificationResponse)
    }
    

    
   
}

