//
//  NotificationTableViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 26/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rfoLabel: UILabel!
    @IBOutlet weak var siteLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
