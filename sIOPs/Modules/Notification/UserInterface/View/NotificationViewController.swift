//
//  NotificationViewController.swift
//  sIOPs
//
//  Created by ASM ESPL on 26/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController,NotificationViewProtocol {
    var presenter: NotificationPresenterProtocol?
    
    @IBOutlet weak var notificationTableView: UITableView!
    var notificationRes:NotificationModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startAnimating()
        self.presenter?.requestData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> NotificationViewProtocol{
        return UIStoryboard(name: "Notification", bundle: nil).instantiateViewController(withIdentifier: "Notification") as! NotificationViewController
    }
    
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    func showFailError(error:String)
    {
        stopAnimating()
        
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        
    }
    
    func reloadData(notificationResponse:NotificationModel)
    {
        stopAnimating()
        notificationRes = notificationResponse
        notificationTableView.reloadData()
        
        
        
    }
    
}
extension NotificationViewController:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        notificationRes?.list.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "notification", for: indexPath as IndexPath) as! NotificationTableViewCell
        
        cell.siteLabel.text = notificationRes?.list[indexPath.section].title ?? ""
        cell.rfoLabel.text = notificationRes?.list[indexPath.section].message ?? ""
        cell.timeLabel.text = HelperMethods().formatDateNotif(date: notificationRes?.list[indexPath.section].insertTime ?? "")
        
        
        
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        10
    }
    
    
}
