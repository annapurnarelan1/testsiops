//
//  NotificationWireframe.swift
//  sIOPs
//
//  Created by ASM ESPL on 26/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NotificationWireFrame: NotificationWireFrameProtocol {
    
    
    
    // MARK: NotificationWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
  class func presentNotificationModule(fromView:AnyObject)  {

        // Generating module components
        let view: NotificationViewProtocol = NotificationViewController.instantiate()
        let presenter: NotificationPresenterProtocol & NotificationInteractorOutputProtocol = NotificationPresenter()
        let interactor: NotificationInteractorInputProtocol = NotificationInteractor()
       
        let wireFrame: NotificationWireFrameProtocol = NotificationWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NotificationViewController
       
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
}
