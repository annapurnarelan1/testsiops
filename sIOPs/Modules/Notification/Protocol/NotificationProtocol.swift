//
//  NotificationProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 26/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol NotificationViewProtocol: class {
    var presenter: NotificationPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(notificationResponse:NotificationModel)

  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NotificationWireFrameProtocol: class {
 static   func presentNotificationModule(fromView:AnyObject)
    

}

/// Method contract between VIEW -> PRESENTER
protocol NotificationPresenterProtocol: class {
    var view: NotificationViewProtocol? { get set }
    var interactor: NotificationInteractorInputProtocol? { get set }
    var wireFrame: NotificationWireFrameProtocol? { get set }
    
   
    
    
    
    func requestData()

  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NotificationInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(notificationResponse:NotificationModel)


}

/// Method contract between PRESENTER -> INTERACTOR
protocol NotificationInteractorInputProtocol: class
{
    var presenter: NotificationInteractorOutputProtocol? { get set }
     func requestData()

    
}

