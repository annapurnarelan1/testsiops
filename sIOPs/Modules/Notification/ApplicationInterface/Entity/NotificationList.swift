//
//	List.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NotificationList : NSObject, NSCoding{

	var ackBy : AnyObject!
	var ackCode : AnyObject!
	var ackDescription : AnyObject!
	var ackName : AnyObject!
	var ackRequired : String!
	var ackStatus : Int!
	var aging : AnyObject!
	var alarmId : Int!
	var alarmType : AnyObject!
	var alarmValue : AnyObject!
	var apkInstallationStatus : Int!
	var channel : AnyObject!
	var channelType : AnyObject!
	var count : Int!
	var createdBy : AnyObject!
	var currentNotificationCount : Int!
	var domainGroupId : AnyObject!
	var domainId : AnyObject!
	var endDate : AnyObject!
	var endPageNo : AnyObject!
	var fcmAuthKey : AnyObject!
	var fcmUrl : AnyObject!
	var fcmUserDeviceKey : AnyObject!
	var featureCategory : AnyObject!
	var featureId : AnyObject!
	var featureName : AnyObject!
	var functionId : Int!
	var functionName : AnyObject!
	var hourOfDay : AnyObject!
	var id : Int!
	var insertTime : String!
	var kpiDate : AnyObject!
	var message : String!
	var notifyReadStatus : Int!
	var reasonCode : AnyObject!
	var reasonCodeName : AnyObject!
	var reasonSubCode : AnyObject!
	var reasonSubCodeName : AnyObject!
	var responseMessage : AnyObject!
	var responseStatus : AnyObject!
	var rfo : AnyObject!
	var rrc : AnyObject!
	var sapId : AnyObject!
	var sendReminderCode : AnyObject!
	var source : AnyObject!
	var startDate : AnyObject!
	var startPageNo : AnyObject!
	var status : Int!
	var title : String!
	var toggleView : AnyObject!
	var type : String!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ackBy = dictionary["ackBy"] as? AnyObject
		ackCode = dictionary["ackCode"] as? AnyObject
		ackDescription = dictionary["ackDescription"] as? AnyObject
		ackName = dictionary["ackName"] as? AnyObject
		ackRequired = dictionary["ackRequired"] as? String
		ackStatus = dictionary["ackStatus"] as? Int
		aging = dictionary["aging"] as? AnyObject
		alarmId = dictionary["alarmId"] as? Int
		alarmType = dictionary["alarmType"] as? AnyObject
		alarmValue = dictionary["alarmValue"] as? AnyObject
		apkInstallationStatus = dictionary["apkInstallationStatus"] as? Int
		channel = dictionary["channel"] as? AnyObject
		channelType = dictionary["channelType"] as? AnyObject
		count = dictionary["count"] as? Int
		createdBy = dictionary["createdBy"] as? AnyObject
		currentNotificationCount = dictionary["currentNotificationCount"] as? Int
		domainGroupId = dictionary["domainGroupId"] as? AnyObject
		domainId = dictionary["domainId"] as? AnyObject
		endDate = dictionary["endDate"] as? AnyObject
		endPageNo = dictionary["endPageNo"] as? AnyObject
		fcmAuthKey = dictionary["fcmAuthKey"] as? AnyObject
		fcmUrl = dictionary["fcmUrl"] as? AnyObject
		fcmUserDeviceKey = dictionary["fcmUserDeviceKey"] as? AnyObject
		featureCategory = dictionary["featureCategory"] as? AnyObject
		featureId = dictionary["featureId"] as? AnyObject
		featureName = dictionary["featureName"] as? AnyObject
		functionId = dictionary["functionId"] as? Int
		functionName = dictionary["functionName"] as? AnyObject
		hourOfDay = dictionary["hourOfDay"] as? AnyObject
		id = dictionary["id"] as? Int
		insertTime = dictionary["insertTime"] as? String
		kpiDate = dictionary["kpiDate"] as? AnyObject
		message = dictionary["message"] as? String
		notifyReadStatus = dictionary["notifyReadStatus"] as? Int
		reasonCode = dictionary["reasonCode"] as? AnyObject
		reasonCodeName = dictionary["reasonCodeName"] as? AnyObject
		reasonSubCode = dictionary["reasonSubCode"] as? AnyObject
		reasonSubCodeName = dictionary["reasonSubCodeName"] as? AnyObject
		responseMessage = dictionary["responseMessage"] as? AnyObject
		responseStatus = dictionary["responseStatus"] as? AnyObject
		rfo = dictionary["rfo"] as? AnyObject
		rrc = dictionary["rrc"] as? AnyObject
		sapId = dictionary["sapId"] as? AnyObject
		sendReminderCode = dictionary["sendReminderCode"] as? AnyObject
		source = dictionary["source"] as? AnyObject
		startDate = dictionary["startDate"] as? AnyObject
		startPageNo = dictionary["startPageNo"] as? AnyObject
		status = dictionary["status"] as? Int
		title = dictionary["title"] as? String
		toggleView = dictionary["toggleView"] as? AnyObject
		type = dictionary["type"] as? String
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ackBy != nil{
			dictionary["ackBy"] = ackBy
		}
		if ackCode != nil{
			dictionary["ackCode"] = ackCode
		}
		if ackDescription != nil{
			dictionary["ackDescription"] = ackDescription
		}
		if ackName != nil{
			dictionary["ackName"] = ackName
		}
		if ackRequired != nil{
			dictionary["ackRequired"] = ackRequired
		}
		if ackStatus != nil{
			dictionary["ackStatus"] = ackStatus
		}
		if aging != nil{
			dictionary["aging"] = aging
		}
		if alarmId != nil{
			dictionary["alarmId"] = alarmId
		}
		if alarmType != nil{
			dictionary["alarmType"] = alarmType
		}
		if alarmValue != nil{
			dictionary["alarmValue"] = alarmValue
		}
		if apkInstallationStatus != nil{
			dictionary["apkInstallationStatus"] = apkInstallationStatus
		}
		if channel != nil{
			dictionary["channel"] = channel
		}
		if channelType != nil{
			dictionary["channelType"] = channelType
		}
		if count != nil{
			dictionary["count"] = count
		}
		if createdBy != nil{
			dictionary["createdBy"] = createdBy
		}
		if currentNotificationCount != nil{
			dictionary["currentNotificationCount"] = currentNotificationCount
		}
		if domainGroupId != nil{
			dictionary["domainGroupId"] = domainGroupId
		}
		if domainId != nil{
			dictionary["domainId"] = domainId
		}
		if endDate != nil{
			dictionary["endDate"] = endDate
		}
		if endPageNo != nil{
			dictionary["endPageNo"] = endPageNo
		}
		if fcmAuthKey != nil{
			dictionary["fcmAuthKey"] = fcmAuthKey
		}
		if fcmUrl != nil{
			dictionary["fcmUrl"] = fcmUrl
		}
		if fcmUserDeviceKey != nil{
			dictionary["fcmUserDeviceKey"] = fcmUserDeviceKey
		}
		if featureCategory != nil{
			dictionary["featureCategory"] = featureCategory
		}
		if featureId != nil{
			dictionary["featureId"] = featureId
		}
		if featureName != nil{
			dictionary["featureName"] = featureName
		}
		if functionId != nil{
			dictionary["functionId"] = functionId
		}
		if functionName != nil{
			dictionary["functionName"] = functionName
		}
		if hourOfDay != nil{
			dictionary["hourOfDay"] = hourOfDay
		}
		if id != nil{
			dictionary["id"] = id
		}
		if insertTime != nil{
			dictionary["insertTime"] = insertTime
		}
		if kpiDate != nil{
			dictionary["kpiDate"] = kpiDate
		}
		if message != nil{
			dictionary["message"] = message
		}
		if notifyReadStatus != nil{
			dictionary["notifyReadStatus"] = notifyReadStatus
		}
		if reasonCode != nil{
			dictionary["reasonCode"] = reasonCode
		}
		if reasonCodeName != nil{
			dictionary["reasonCodeName"] = reasonCodeName
		}
		if reasonSubCode != nil{
			dictionary["reasonSubCode"] = reasonSubCode
		}
		if reasonSubCodeName != nil{
			dictionary["reasonSubCodeName"] = reasonSubCodeName
		}
		if responseMessage != nil{
			dictionary["responseMessage"] = responseMessage
		}
		if responseStatus != nil{
			dictionary["responseStatus"] = responseStatus
		}
		if rfo != nil{
			dictionary["rfo"] = rfo
		}
		if rrc != nil{
			dictionary["rrc"] = rrc
		}
		if sapId != nil{
			dictionary["sapId"] = sapId
		}
		if sendReminderCode != nil{
			dictionary["sendReminderCode"] = sendReminderCode
		}
		if source != nil{
			dictionary["source"] = source
		}
		if startDate != nil{
			dictionary["startDate"] = startDate
		}
		if startPageNo != nil{
			dictionary["startPageNo"] = startPageNo
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if toggleView != nil{
			dictionary["toggleView"] = toggleView
		}
		if type != nil{
			dictionary["type"] = type
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ackBy = aDecoder.decodeObject(forKey: "ackBy") as? AnyObject
         ackCode = aDecoder.decodeObject(forKey: "ackCode") as? AnyObject
         ackDescription = aDecoder.decodeObject(forKey: "ackDescription") as? AnyObject
         ackName = aDecoder.decodeObject(forKey: "ackName") as? AnyObject
         ackRequired = aDecoder.decodeObject(forKey: "ackRequired") as? String
         ackStatus = aDecoder.decodeObject(forKey: "ackStatus") as? Int
         aging = aDecoder.decodeObject(forKey: "aging") as? AnyObject
         alarmId = aDecoder.decodeObject(forKey: "alarmId") as? Int
         alarmType = aDecoder.decodeObject(forKey: "alarmType") as? AnyObject
         alarmValue = aDecoder.decodeObject(forKey: "alarmValue") as? AnyObject
         apkInstallationStatus = aDecoder.decodeObject(forKey: "apkInstallationStatus") as? Int
         channel = aDecoder.decodeObject(forKey: "channel") as? AnyObject
         channelType = aDecoder.decodeObject(forKey: "channelType") as? AnyObject
         count = aDecoder.decodeObject(forKey: "count") as? Int
         createdBy = aDecoder.decodeObject(forKey: "createdBy") as? AnyObject
         currentNotificationCount = aDecoder.decodeObject(forKey: "currentNotificationCount") as? Int
         domainGroupId = aDecoder.decodeObject(forKey: "domainGroupId") as? AnyObject
         domainId = aDecoder.decodeObject(forKey: "domainId") as? AnyObject
         endDate = aDecoder.decodeObject(forKey: "endDate") as? AnyObject
         endPageNo = aDecoder.decodeObject(forKey: "endPageNo") as? AnyObject
         fcmAuthKey = aDecoder.decodeObject(forKey: "fcmAuthKey") as? AnyObject
         fcmUrl = aDecoder.decodeObject(forKey: "fcmUrl") as? AnyObject
         fcmUserDeviceKey = aDecoder.decodeObject(forKey: "fcmUserDeviceKey") as? AnyObject
         featureCategory = aDecoder.decodeObject(forKey: "featureCategory") as? AnyObject
         featureId = aDecoder.decodeObject(forKey: "featureId") as? AnyObject
         featureName = aDecoder.decodeObject(forKey: "featureName") as? AnyObject
         functionId = aDecoder.decodeObject(forKey: "functionId") as? Int
         functionName = aDecoder.decodeObject(forKey: "functionName") as? AnyObject
         hourOfDay = aDecoder.decodeObject(forKey: "hourOfDay") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? Int
         insertTime = aDecoder.decodeObject(forKey: "insertTime") as? String
         kpiDate = aDecoder.decodeObject(forKey: "kpiDate") as? AnyObject
         message = aDecoder.decodeObject(forKey: "message") as? String
         notifyReadStatus = aDecoder.decodeObject(forKey: "notifyReadStatus") as? Int
         reasonCode = aDecoder.decodeObject(forKey: "reasonCode") as? AnyObject
         reasonCodeName = aDecoder.decodeObject(forKey: "reasonCodeName") as? AnyObject
         reasonSubCode = aDecoder.decodeObject(forKey: "reasonSubCode") as? AnyObject
         reasonSubCodeName = aDecoder.decodeObject(forKey: "reasonSubCodeName") as? AnyObject
         responseMessage = aDecoder.decodeObject(forKey: "responseMessage") as? AnyObject
         responseStatus = aDecoder.decodeObject(forKey: "responseStatus") as? AnyObject
         rfo = aDecoder.decodeObject(forKey: "rfo") as? AnyObject
         rrc = aDecoder.decodeObject(forKey: "rrc") as? AnyObject
         sapId = aDecoder.decodeObject(forKey: "sapId") as? AnyObject
         sendReminderCode = aDecoder.decodeObject(forKey: "sendReminderCode") as? AnyObject
         source = aDecoder.decodeObject(forKey: "source") as? AnyObject
         startDate = aDecoder.decodeObject(forKey: "startDate") as? AnyObject
         startPageNo = aDecoder.decodeObject(forKey: "startPageNo") as? AnyObject
         status = aDecoder.decodeObject(forKey: "status") as? Int
         title = aDecoder.decodeObject(forKey: "title") as? String
         toggleView = aDecoder.decodeObject(forKey: "toggleView") as? AnyObject
         type = aDecoder.decodeObject(forKey: "type") as? String
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ackBy != nil{
			aCoder.encode(ackBy, forKey: "ackBy")
		}
		if ackCode != nil{
			aCoder.encode(ackCode, forKey: "ackCode")
		}
		if ackDescription != nil{
			aCoder.encode(ackDescription, forKey: "ackDescription")
		}
		if ackName != nil{
			aCoder.encode(ackName, forKey: "ackName")
		}
		if ackRequired != nil{
			aCoder.encode(ackRequired, forKey: "ackRequired")
		}
		if ackStatus != nil{
			aCoder.encode(ackStatus, forKey: "ackStatus")
		}
		if aging != nil{
			aCoder.encode(aging, forKey: "aging")
		}
		if alarmId != nil{
			aCoder.encode(alarmId, forKey: "alarmId")
		}
		if alarmType != nil{
			aCoder.encode(alarmType, forKey: "alarmType")
		}
		if alarmValue != nil{
			aCoder.encode(alarmValue, forKey: "alarmValue")
		}
		if apkInstallationStatus != nil{
			aCoder.encode(apkInstallationStatus, forKey: "apkInstallationStatus")
		}
		if channel != nil{
			aCoder.encode(channel, forKey: "channel")
		}
		if channelType != nil{
			aCoder.encode(channelType, forKey: "channelType")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "createdBy")
		}
		if currentNotificationCount != nil{
			aCoder.encode(currentNotificationCount, forKey: "currentNotificationCount")
		}
		if domainGroupId != nil{
			aCoder.encode(domainGroupId, forKey: "domainGroupId")
		}
		if domainId != nil{
			aCoder.encode(domainId, forKey: "domainId")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "endDate")
		}
		if endPageNo != nil{
			aCoder.encode(endPageNo, forKey: "endPageNo")
		}
		if fcmAuthKey != nil{
			aCoder.encode(fcmAuthKey, forKey: "fcmAuthKey")
		}
		if fcmUrl != nil{
			aCoder.encode(fcmUrl, forKey: "fcmUrl")
		}
		if fcmUserDeviceKey != nil{
			aCoder.encode(fcmUserDeviceKey, forKey: "fcmUserDeviceKey")
		}
		if featureCategory != nil{
			aCoder.encode(featureCategory, forKey: "featureCategory")
		}
		if featureId != nil{
			aCoder.encode(featureId, forKey: "featureId")
		}
		if featureName != nil{
			aCoder.encode(featureName, forKey: "featureName")
		}
		if functionId != nil{
			aCoder.encode(functionId, forKey: "functionId")
		}
		if functionName != nil{
			aCoder.encode(functionName, forKey: "functionName")
		}
		if hourOfDay != nil{
			aCoder.encode(hourOfDay, forKey: "hourOfDay")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if insertTime != nil{
			aCoder.encode(insertTime, forKey: "insertTime")
		}
		if kpiDate != nil{
			aCoder.encode(kpiDate, forKey: "kpiDate")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if notifyReadStatus != nil{
			aCoder.encode(notifyReadStatus, forKey: "notifyReadStatus")
		}
		if reasonCode != nil{
			aCoder.encode(reasonCode, forKey: "reasonCode")
		}
		if reasonCodeName != nil{
			aCoder.encode(reasonCodeName, forKey: "reasonCodeName")
		}
		if reasonSubCode != nil{
			aCoder.encode(reasonSubCode, forKey: "reasonSubCode")
		}
		if reasonSubCodeName != nil{
			aCoder.encode(reasonSubCodeName, forKey: "reasonSubCodeName")
		}
		if responseMessage != nil{
			aCoder.encode(responseMessage, forKey: "responseMessage")
		}
		if responseStatus != nil{
			aCoder.encode(responseStatus, forKey: "responseStatus")
		}
		if rfo != nil{
			aCoder.encode(rfo, forKey: "rfo")
		}
		if rrc != nil{
			aCoder.encode(rrc, forKey: "rrc")
		}
		if sapId != nil{
			aCoder.encode(sapId, forKey: "sapId")
		}
		if sendReminderCode != nil{
			aCoder.encode(sendReminderCode, forKey: "sendReminderCode")
		}
		if source != nil{
			aCoder.encode(source, forKey: "source")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "startDate")
		}
		if startPageNo != nil{
			aCoder.encode(startPageNo, forKey: "startPageNo")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if toggleView != nil{
			aCoder.encode(toggleView, forKey: "toggleView")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
