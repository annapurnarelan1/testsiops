//
//  OpenAlertsDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class OpenAlertsDetailVC: BaseViewController,OpenAlertsDetailViewProtocol {
    
    var presenter: OpenAlertsDetailPresenterProtocol?
    
   
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!

    @IBOutlet weak var tableView: UITableView!
    
    var type:String?
    var selected:String?
    var NGOResponse:NGO?
    var selectedArray:[MainOutlier]?
    var count : String?
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> OpenAlertsDetailViewProtocol{
        return UIStoryboard(name: "OpenAlertsDetail", bundle: nil).instantiateViewController(withIdentifier: "OpenAlertsDetailVC") as! OpenAlertsDetailVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "AlertDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "AlertDetailTableViewCell")
        if(type?.contains("History") ?? false)
        {
           titleLbl.text = type?.replacingOccurrences(of: "History", with: "") ?? ""
            countLbl.text = count
        }
        else{
            titleLbl.text = type
            countLbl.text = count
        }
        
        
        
        
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No Outliers Available")
       
        startAnimating()
        
        if(type?.contains("History") ?? false)
        {
            self.presenter?.requestHistoryData(type:type?.replacingOccurrences(of: "History", with: "") ?? "")
        }
        else{
            self.presenter?.requestData(type: type ?? "")
        }

        // Do any additional setup after loading the view.
    }
    
  
    

   override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated);
        
       HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       self.navigationController?.setNavigationBarHidden(false, animated: false)
       self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
     
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
     func reloadData(detail:NGOClickHistory)
     {
        stopAnimating()
        self.selectedArray = detail.mainOutlier
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No Outliers Available")
        self.tableView.reloadData()
    }
    
    @objc func makeCall(_ sender:UIButton)
    {
        
        if let phoneCallURL = URL(string: "telprompt://\(self.selectedArray?[sender.tag].ackByMobile ?? "")") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
       
    }
    
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        
        if(sender.isSelected)
        {
            if let tableView = self.tableView {
                let point = tableView.convert(sender.center, from: sender.superview!)

                if let wantedIndexPath = tableView.indexPathForRow(at: point) {
                    let cell = tableView.cellForRow(at: wantedIndexPath) as! AlertDetailTableViewCell
                    cell.textView.isHidden = true
                    let buttonTitleStr = NSMutableAttributedString(string:"Alert Details", attributes:attrs)
                    let attributedString = NSMutableAttributedString(string:"")
                    attributedString.append(buttonTitleStr)
                    cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
                     
                    cell.alertBtn.isSelected = false

                }
            }
        }
        else
        {
            
            if let tableView = self.tableView {
                let point = tableView.convert(sender.center, from: sender.superview!)

                if let wantedIndexPath = tableView.indexPathForRow(at: point) {
                    let cell = tableView.cellForRow(at: wantedIndexPath) as! AlertDetailTableViewCell
                    cell.textView.isHidden = false
                    let buttonTitleStr = NSMutableAttributedString(string:"Hide Details", attributes:attrs)
                    let attributedString = NSMutableAttributedString(string:"")
                    attributedString.append(buttonTitleStr)
                    cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
                    cell.alertBtn.isSelected = true
                    cell.alertBtn.setTitleColor(UIColor.init(hexString: "#0078C1"), for: .selected)
                    

                }
            }
        }
        
        
        self.tableView.reloadData()
    }
    

}

extension OpenAlertsDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell:AlertDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlertDetailTableViewCell", for: indexPath as IndexPath) as! AlertDetailTableViewCell
        cell.alertBtn.tag = indexPath.section
        cell.descriptionLabel?.text = self.selectedArray?[indexPath.section].eventMessage
        cell.idLbl?.text = (self.selectedArray?[indexPath.section].cname ?? "") + " " +  (self.selectedArray?[indexPath.section].ipAddress ?? "")
        cell.openAtLbl.text = self.selectedArray?[indexPath.section].createTime
        cell.openAtLbl.text = self.selectedArray?[indexPath.section].createTime
        cell.nameLbl.text = self.selectedArray?[indexPath.section].ackBY
       // cell.statusLabel.text = self.selectedArray?[indexPath.section].ackStatus
        cell.phone.tag = indexPath.section
        
        cell.timeLbl.text =  self.selectedArray?[indexPath.section].remaingTimeToAck
        cell.ageingHeadingLbl.text = self.selectedArray?[indexPath.section].agingSinceInsert
        cell.acknolowledOnLbl.text = self.selectedArray?[indexPath.section].ackON
        cell.applicationLabel.text = self.selectedArray?[indexPath.section].appName
        if(cell.alertBtn.isSelected)
        {
            
                    let buttonTitleStr = NSMutableAttributedString(string:"Hide Details", attributes:attrs)
                    let attributedString = NSMutableAttributedString(string:"")
                    attributedString.append(buttonTitleStr)
                    cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
        }
        else
        {
            let buttonTitleStr = NSMutableAttributedString(string:"Alert Details", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
           
        }
        if(self.selectedArray?[indexPath.section].ackStatus == "1")
        {
            cell.acknoledgeLbl.backgroundColor = UIColor.gray
            cell.acknoledgeLbl.text = "Acknowledge"
            cell.acknoledgeByLbl.isHidden = true
            cell.timeLbl.isHidden = true
            cell.phone.isHidden = true
            cell.nameLbl.isHidden = true
        }
        else
        {
            cell.acknoledgeLbl.text = "Acknowledged"
            cell.acknoledgeLbl.backgroundColor = UIColor.init(hexString: "#2ECC71")
            cell.acknoledgeByLbl.isHidden = false
            cell.timeLbl.isHidden = false
            cell.phone.isHidden = false
            cell.nameLbl.isHidden = false
        }
        cell.phone.addTarget(self, action: #selector(makeCall(_:)), for: .touchUpInside)
        cell.alertBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return selectedArray?.count ?? 0
        return self.selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
