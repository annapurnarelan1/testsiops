//
//  OpenAlertsDetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenAlertsDetailWireFrame: OpenAlertsDetailWireFrameProtocol {
    
    
    
    // MARK: OpenAlertsDetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenAlertsDetailModule(fromView:AnyObject,type:String,ngoResponse:NGO,count:String) {

        // Generating module components
        let view: OpenAlertsDetailViewProtocol = OpenAlertsDetailVC.instantiate()
        let presenter: OpenAlertsDetailPresenterProtocol & OpenAlertsDetailInteractorOutputProtocol = OpenAlertsDetailPresenter()
        let interactor: OpenAlertsDetailInteractorInputProtocol = OpenAlertsDetailInteractor()
       
        let wireFrame: OpenAlertsDetailWireFrameProtocol = OpenAlertsDetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OpenAlertsDetailVC
        viewController.type = type
        
        viewController.NGOResponse =  ngoResponse
        viewController.count = count
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
