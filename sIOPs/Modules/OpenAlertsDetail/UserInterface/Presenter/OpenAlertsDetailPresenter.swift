//
//  OpenAlertsDetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OpenAlertsDetailPresenter:BasePresenter, OpenAlertsDetailPresenterProtocol, OpenAlertsDetailInteractorOutputProtocol {
   
    
    

    // MARK: Variables
    weak var view: OpenAlertsDetailViewProtocol?
    var interactor: OpenAlertsDetailInteractorInputProtocol?
    var wireFrame: OpenAlertsDetailWireFrameProtocol?
    let stringsTableName = "OpenAlertsDetail"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
       func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
     func requestData(type:String)
    {
        self.interactor?.requestData(type:type)
    }
    func reloadData(detail:NGOClickHistory)
    {
        self.view?.reloadData(detail:detail)
    }
    func requestHistoryData(type:String)
    {
        self.interactor?.requestHistoryData(type:type)
    }
}
