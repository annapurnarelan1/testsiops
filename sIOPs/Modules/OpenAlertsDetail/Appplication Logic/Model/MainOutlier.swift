//
//	MainOutlier.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MainOutlier : NSObject, NSCoding{

	var ackByMobile : String!
	var ackBY : String!
	var ackON : String!
	var ackStatus : String!
	var agingSinceInsert : String!
	var appName : String!
	var appOwner : String!
	var classType : AnyObject!
	var cname : String!
	var count : Int!
	var createTime : String!
	var delayInAcknowledge : String!
	var delayInCreation : AnyObject!
	var eventKPI : AnyObject!
	var eventMessage : String!
	var eventStatus : AnyObject!
	var eventTime : String!
	var hostName : String!
	var ipAddress : String!
	var openDuration : String!
	var outlierName : AnyObject!
	var platformOwner : String!
	var remaingTimeToAck : String!
	var state : AnyObject!
	var subType : String!
	var type : String!
	var unAckCount : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ackByMobile = dictionary["ackByMobile"] as? String
		ackBY = dictionary["ack_BY"] as? String
		ackON = dictionary["ack_ON"] as? String
		ackStatus = dictionary["ack_status"] as? String
		agingSinceInsert = dictionary["aging_since_insert"] as? String
		appName = dictionary["appName"] as? String
		appOwner = dictionary["appOwner"] as? String
		classType = dictionary["classType"] as? AnyObject
		cname = dictionary["cname"] as? String
		count = dictionary["count"] as? Int
		createTime = dictionary["createTime"] as? String
		delayInAcknowledge = dictionary["delayInAcknowledge"] as? String
		delayInCreation = dictionary["delayInCreation"] as? AnyObject
		eventKPI = dictionary["eventKPI"] as? AnyObject
		eventMessage = dictionary["eventMessage"] as? String
		eventStatus = dictionary["eventStatus"] as? AnyObject
		eventTime = dictionary["eventTime"] as? String
		hostName = dictionary["hostName"] as? String
		ipAddress = dictionary["ipAddress"] as? String
		openDuration = dictionary["openDuration"] as? String
		outlierName = dictionary["outlierName"] as? AnyObject
		platformOwner = dictionary["platformOwner"] as? String
		remaingTimeToAck = dictionary["remaing_time_to_ack"] as? String
		state = dictionary["state"] as? AnyObject
		subType = dictionary["subType"] as? String
		type = dictionary["type"] as? String
		unAckCount = dictionary["unAckCount"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ackByMobile != nil{
			dictionary["ackByMobile"] = ackByMobile
		}
		if ackBY != nil{
			dictionary["ack_BY"] = ackBY
		}
		if ackON != nil{
			dictionary["ack_ON"] = ackON
		}
		if ackStatus != nil{
			dictionary["ack_status"] = ackStatus
		}
		if agingSinceInsert != nil{
			dictionary["aging_since_insert"] = agingSinceInsert
		}
		if appName != nil{
			dictionary["appName"] = appName
		}
		if appOwner != nil{
			dictionary["appOwner"] = appOwner
		}
		if classType != nil{
			dictionary["classType"] = classType
		}
		if cname != nil{
			dictionary["cname"] = cname
		}
		if count != nil{
			dictionary["count"] = count
		}
		if createTime != nil{
			dictionary["createTime"] = createTime
		}
		if delayInAcknowledge != nil{
			dictionary["delayInAcknowledge"] = delayInAcknowledge
		}
		if delayInCreation != nil{
			dictionary["delayInCreation"] = delayInCreation
		}
		if eventKPI != nil{
			dictionary["eventKPI"] = eventKPI
		}
		if eventMessage != nil{
			dictionary["eventMessage"] = eventMessage
		}
		if eventStatus != nil{
			dictionary["eventStatus"] = eventStatus
		}
		if eventTime != nil{
			dictionary["eventTime"] = eventTime
		}
		if hostName != nil{
			dictionary["hostName"] = hostName
		}
		if ipAddress != nil{
			dictionary["ipAddress"] = ipAddress
		}
		if openDuration != nil{
			dictionary["openDuration"] = openDuration
		}
		if outlierName != nil{
			dictionary["outlierName"] = outlierName
		}
		if platformOwner != nil{
			dictionary["platformOwner"] = platformOwner
		}
		if remaingTimeToAck != nil{
			dictionary["remaing_time_to_ack"] = remaingTimeToAck
		}
		if state != nil{
			dictionary["state"] = state
		}
		if subType != nil{
			dictionary["subType"] = subType
		}
		if type != nil{
			dictionary["type"] = type
		}
		if unAckCount != nil{
			dictionary["unAckCount"] = unAckCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ackByMobile = aDecoder.decodeObject(forKey: "ackByMobile") as? String
         ackBY = aDecoder.decodeObject(forKey: "ack_BY") as? String
         ackON = aDecoder.decodeObject(forKey: "ack_ON") as? String
         ackStatus = aDecoder.decodeObject(forKey: "ack_status") as? String
         agingSinceInsert = aDecoder.decodeObject(forKey: "aging_since_insert") as? String
         appName = aDecoder.decodeObject(forKey: "appName") as? String
         appOwner = aDecoder.decodeObject(forKey: "appOwner") as? String
         classType = aDecoder.decodeObject(forKey: "classType") as? AnyObject
         cname = aDecoder.decodeObject(forKey: "cname") as? String
         count = aDecoder.decodeObject(forKey: "count") as? Int
         createTime = aDecoder.decodeObject(forKey: "createTime") as? String
         delayInAcknowledge = aDecoder.decodeObject(forKey: "delayInAcknowledge") as? String
         delayInCreation = aDecoder.decodeObject(forKey: "delayInCreation") as? AnyObject
         eventKPI = aDecoder.decodeObject(forKey: "eventKPI") as? AnyObject
         eventMessage = aDecoder.decodeObject(forKey: "eventMessage") as? String
         eventStatus = aDecoder.decodeObject(forKey: "eventStatus") as? AnyObject
         eventTime = aDecoder.decodeObject(forKey: "eventTime") as? String
         hostName = aDecoder.decodeObject(forKey: "hostName") as? String
         ipAddress = aDecoder.decodeObject(forKey: "ipAddress") as? String
         openDuration = aDecoder.decodeObject(forKey: "openDuration") as? String
         outlierName = aDecoder.decodeObject(forKey: "outlierName") as? AnyObject
         platformOwner = aDecoder.decodeObject(forKey: "platformOwner") as? String
         remaingTimeToAck = aDecoder.decodeObject(forKey: "remaing_time_to_ack") as? String
         state = aDecoder.decodeObject(forKey: "state") as? AnyObject
         subType = aDecoder.decodeObject(forKey: "subType") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         unAckCount = aDecoder.decodeObject(forKey: "unAckCount") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ackByMobile != nil{
			aCoder.encode(ackByMobile, forKey: "ackByMobile")
		}
		if ackBY != nil{
			aCoder.encode(ackBY, forKey: "ack_BY")
		}
		if ackON != nil{
			aCoder.encode(ackON, forKey: "ack_ON")
		}
		if ackStatus != nil{
			aCoder.encode(ackStatus, forKey: "ack_status")
		}
		if agingSinceInsert != nil{
			aCoder.encode(agingSinceInsert, forKey: "aging_since_insert")
		}
		if appName != nil{
			aCoder.encode(appName, forKey: "appName")
		}
		if appOwner != nil{
			aCoder.encode(appOwner, forKey: "appOwner")
		}
		if classType != nil{
			aCoder.encode(classType, forKey: "classType")
		}
		if cname != nil{
			aCoder.encode(cname, forKey: "cname")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if createTime != nil{
			aCoder.encode(createTime, forKey: "createTime")
		}
		if delayInAcknowledge != nil{
			aCoder.encode(delayInAcknowledge, forKey: "delayInAcknowledge")
		}
		if delayInCreation != nil{
			aCoder.encode(delayInCreation, forKey: "delayInCreation")
		}
		if eventKPI != nil{
			aCoder.encode(eventKPI, forKey: "eventKPI")
		}
		if eventMessage != nil{
			aCoder.encode(eventMessage, forKey: "eventMessage")
		}
		if eventStatus != nil{
			aCoder.encode(eventStatus, forKey: "eventStatus")
		}
		if eventTime != nil{
			aCoder.encode(eventTime, forKey: "eventTime")
		}
		if hostName != nil{
			aCoder.encode(hostName, forKey: "hostName")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ipAddress")
		}
		if openDuration != nil{
			aCoder.encode(openDuration, forKey: "openDuration")
		}
		if outlierName != nil{
			aCoder.encode(outlierName, forKey: "outlierName")
		}
		if platformOwner != nil{
			aCoder.encode(platformOwner, forKey: "platformOwner")
		}
		if remaingTimeToAck != nil{
			aCoder.encode(remaingTimeToAck, forKey: "remaing_time_to_ack")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if subType != nil{
			aCoder.encode(subType, forKey: "subType")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if unAckCount != nil{
			aCoder.encode(unAckCount, forKey: "unAckCount")
		}

	}

}