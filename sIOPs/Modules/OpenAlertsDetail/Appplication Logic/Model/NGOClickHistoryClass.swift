//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NGOClickHistory : NSObject, NSCoding{

	var application : [AnyObject]!
	var delinquency : AnyObject!
	var delinquencyCount : Int!
	var infra : [AnyObject]!
	var mainOutlier : [MainOutlier]!
	var openAlerts : AnyObject!
	var openAlertsCount : Int!
	var tools : [AnyObject]!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		application = dictionary["application"] as? [AnyObject]
		delinquency = dictionary["delinquency"] as? AnyObject
		delinquencyCount = dictionary["delinquencyCount"] as? Int
		infra = dictionary["infra"] as? [AnyObject]
		mainOutlier = [MainOutlier]()
		if let mainOutlierArray = dictionary["mainOutlier"] as? [[String:Any]]{
			for dic in mainOutlierArray{
				let value = MainOutlier(fromDictionary: dic)
				mainOutlier.append(value)
			}
		}
		openAlerts = dictionary["openAlerts"] as? AnyObject
		openAlertsCount = dictionary["openAlertsCount"] as? Int
		tools = dictionary["tools"] as? [AnyObject]
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if application != nil{
			dictionary["application"] = application
		}
		if delinquency != nil{
			dictionary["delinquency"] = delinquency
		}
		if delinquencyCount != nil{
			dictionary["delinquencyCount"] = delinquencyCount
		}
		if infra != nil{
			dictionary["infra"] = infra
		}
		if mainOutlier != nil{
			var dictionaryElements = [[String:Any]]()
			for mainOutlierElement in mainOutlier {
				dictionaryElements.append(mainOutlierElement.toDictionary())
			}
			dictionary["mainOutlier"] = dictionaryElements
		}
		if openAlerts != nil{
			dictionary["openAlerts"] = openAlerts
		}
		if openAlertsCount != nil{
			dictionary["openAlertsCount"] = openAlertsCount
		}
		if tools != nil{
			dictionary["tools"] = tools
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         application = aDecoder.decodeObject(forKey: "application") as? [AnyObject]
         delinquency = aDecoder.decodeObject(forKey: "delinquency") as? AnyObject
         delinquencyCount = aDecoder.decodeObject(forKey: "delinquencyCount") as? Int
         infra = aDecoder.decodeObject(forKey: "infra") as? [AnyObject]
         mainOutlier = aDecoder.decodeObject(forKey :"mainOutlier") as? [MainOutlier]
         openAlerts = aDecoder.decodeObject(forKey: "openAlerts") as? AnyObject
         openAlertsCount = aDecoder.decodeObject(forKey: "openAlertsCount") as? Int
         tools = aDecoder.decodeObject(forKey: "tools") as? [AnyObject]
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if application != nil{
			aCoder.encode(application, forKey: "application")
		}
		if delinquency != nil{
			aCoder.encode(delinquency, forKey: "delinquency")
		}
		if delinquencyCount != nil{
			aCoder.encode(delinquencyCount, forKey: "delinquencyCount")
		}
		if infra != nil{
			aCoder.encode(infra, forKey: "infra")
		}
		if mainOutlier != nil{
			aCoder.encode(mainOutlier, forKey: "mainOutlier")
		}
		if openAlerts != nil{
			aCoder.encode(openAlerts, forKey: "openAlerts")
		}
		if openAlertsCount != nil{
			aCoder.encode(openAlertsCount, forKey: "openAlertsCount")
		}
		if tools != nil{
			aCoder.encode(tools, forKey: "tools")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
