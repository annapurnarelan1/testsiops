//
//  OpenAlertsDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenAlertsDetailViewProtocol: class {
    var presenter: OpenAlertsDetailPresenterProtocol? { get set }
    //func OpenAlertsDetailDone(OpenAlertsDetailRes :OpenAlertsDetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:NGOClickHistory)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenAlertsDetailWireFrameProtocol: class {
    static func presentOpenAlertsDetailModule(fromView:AnyObject,type:String,ngoResponse:NGO,count:String)

}

/// Method contract between VIEW -> PRESENTER
protocol OpenAlertsDetailPresenterProtocol: class {
    var view: OpenAlertsDetailViewProtocol? { get set }
    var interactor: OpenAlertsDetailInteractorInputProtocol? { get set }
    var wireFrame: OpenAlertsDetailWireFrameProtocol? { get set }
    
     func requestData(type:String)
    func requestHistoryData(type:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenAlertsDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:NGOClickHistory)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenAlertsDetailInteractorInputProtocol: class
{
    var presenter: OpenAlertsDetailInteractorOutputProtocol? { get set }
    func requestData(type:String)
    func requestHistoryData(type:String)
    
}
