//
//  NGOOpenIncidentDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGOOpenIncidentDetailWireframe: NGOOpenIncidentDetailWireFrameProtocol {
    func presentIncidentHistoryModule(fromView: AnyObject, type: String, selected: String, date:String) {
        NGOIncidentHistoryWireframe.presentNGOIncidentHistoryModule(fromView: self, type: type, selected: selected, date:date)
    }
    
    static func presentAlarmDetailModule(fromView: AnyObject, type: String, selected: String, response: Any, applicationDic: LoginApplication) {
        
    }
    
    
    
    
    
    // MARK: NGOOpenIncidentDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
//    class func presentAlarmDetailModule(fromView:AnyObject,type:String,selected:String,response:Any,applicationDic:LoginApplication) {
//
//        // Generating module components
//        let view: NGOOpenIncidentDetailViewProtocol = NGOOpenIncidentDetailVC.instantiate()
//        let presenter: NGOOpenIncidentDetailPresenterProtocol & NGOOpenIncidentDetailInteractorOutputProtocol = NGOOpenIncidentDetailPresenter()
//        let interactor: NGOOpenIncidentDetailInteractorInputProtocol = NGOOpenIncidentDetailInteractor()
//
//        let wireFrame: NGOOpenIncidentDetailWireframeProtocol = NGOOpenIncidentDetailWireframe()
//
//        // Connecting
//        view.presenter = presenter
//        presenter.view = view
//        presenter.wireFrame = wireFrame
//        presenter.interactor = interactor
//        interactor.presenter = presenter
//
//        let viewController = view as! AlarmViewController
//        viewController.type = type
//        viewController.selected = selected
//        viewController.infraResponse =  response
//        viewController.applicationCode = applicationDic.applicationCode ?? ""
//
//
//        NavigationHelper.pushViewController(viewController: viewController)
//    }
//
    
    class func presentNGOOpenIncidentDetailModule(fromView:AnyObject,type:String,selected:String,response:OpenIncidentModel) {

        // Generating module components
        let view: NGOOpenIncidentDetailViewProtocol = NGOOpenIncidentDetailVC.instantiate()
        let presenter: NGOOpenIncidentDetailPresenterProtocol & NGOOpenIncidentDetailInteractorOutputProtocol = NGOOpenIncidentDetailPresenter()
        let interactor: NGOOpenIncidentDetailInteractorInputProtocol = NGOOpenIncidentDetailInteractor()
       
        let wireFrame: NGOOpenIncidentDetailWireFrameProtocol = NGOOpenIncidentDetailWireframe()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NGOOpenIncidentDetailVC
        viewController.type = type
        viewController.selected = selected
        viewController.openIncidentResponse =  response
//        viewController.applicationCode = applicationDic.applicationCode ?? ""
//        viewController.selectedSite = selectedSite
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
    func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
   {
    OpenAlertsDetailWireFrame.presentOpenAlertsDetailModule(fromView:self,type:type,ngoResponse:ngoResponse,count:count )
    }
    
    
    func presentFilterScreen(type:String,selected:String,previousView:Any)
    {
        FilterWireFrame.presentFilterModule(fromView: self, type: type, selected: selected,previousView:previousView)
    }

    
    func presentAlarmDetailModule(alarmOutlierList:AlarmReasonList,applicationCode:String,response:Any,filterSelected:[String:Any])
    {
        AlarmDetailWireFrame.presentAlarmDetailModule(fromView: self, alarmOutlierList: alarmOutlierList, applicationCode: applicationCode, response: response,filterSelected:filterSelected)
    }
    
    
    func presentSiteDownDetailModule(type:String,selected:String,response:DownList,selectedSite:InfraCellImpacted,filterSelected:[String:Any])
    {
        SiteDownDetailWireFrame.presentSiteDownDetailModule(fromView: self, type: type, selected: selected, response: response, selectedSite: selectedSite,filterSelected:filterSelected)
    }
}
