//
//  NGOOpenIncidentDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGOOpenIncidentDetailPresenter:BasePresenter, NGOOpenIncidentDetailPresenterProtocol, NGOOpenIncidentDetailInteractorOutputProtocol {
   
  
    func presentIncidentHistoryModule(fromView: AnyObject, type: String, selected: String, date:String) {
        self.wireFrame?.presentIncidentHistoryModule(fromView: self, type: type, selected: selected, date:date)
    }
    
   
    
    

    // MARK: Variables
    weak var view: NGOOpenIncidentDetailViewProtocol?
    var interactor: NGOOpenIncidentDetailInteractorInputProtocol?
    var wireFrame: NGOOpenIncidentDetailWireFrameProtocol?
    let stringsTableName = "NGOOpenIncidentDetail"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
       func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    func requestIncidentHistory(date:String) {
        self.interactor?.requestIncidentHistory(date: date)
    }
//    func reloadHistoryData(response:IncidentHistoryModel)
//    {
//        self.view?.reloadHistoryData(response:response)
//    }
    
   
   
   

}
