//
//  InfraDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class NGOOpenIncidentDetailVC: BaseViewController,NGOOpenIncidentDetailViewProtocol,FSCalendarDataSource, FSCalendarDelegate {
    var presenter: NGOOpenIncidentDetailPresenterProtocol?
    
    
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var filteBtn: BadgeButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cnstHeightCalender: NSLayoutConstraint!
    
    var type:String?
    var selected:String?
    var selectedSeverity:Int?
    var openIncidentResponse:OpenIncidentModel?
    var selectedArray:[Any]?
    var applicationCode:String?
    var selectedSite:InfraCellImpacted?
    var selectedCategory :String = "All"
    var response:Any?
    
    var selectedFilter = [String:Any]()
    
    let date = Date()
       var selectedDate:String?
       let formatter = DateFormatter()
        @IBOutlet weak var calendarView: FSCalendar!
       
       
       fileprivate lazy var dateFormatter: DateFormatter = {
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy-MM-dd"
           return formatter
       }()
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> NGOOpenIncidentDetailViewProtocol{
        return UIStoryboard(name: "NGOOpenIncidentDetail", bundle: nil).instantiateViewController(withIdentifier: "NGOOpenIncidentDetail") as! NGOOpenIncidentDetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 180

        //self.calendarView.select(Date())
        self.calendarView.scope = .week
        //self.calendarView.preferredRowHeight
        self.calendarView.setScope(.week, animated: false)
        

        // For UITest
        self.calendarView.accessibilityIdentifier = "calendar"
        
        tableView.register(UINib(nibName: "SeverityTableViewCell", bundle: nil), forCellReuseIdentifier: "SeverityTableViewCell")
        tableView.register(UINib(nibName: "IncidentSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "IncidentSummaryTableViewCell")
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        let button = UIButton(frame: CGRect(x: 5, y: 5, width: 5, height: 5))
        button.tag = 1
        OpenIncidentDetail(sender:button)
        // Do any additional setup after loading the view.
    }
    
    
    
    func reloadHistoryData(response:AlarmModel)
    {
        stopAnimating()
        
        
        self.selectedArray = response.list
        tableView.reloadData()
        
        
    }
    
    func reloadsiteDownData(response:SiteDownModel)
    {
        stopAnimating()
        self.selectedArray = response.list
        tableView.reloadData()
    }
    
    @objc func OpenIncidentDetail(sender:Any)
    {
        
        let btn:UIButton = (sender as? UIButton)!
        selectedSeverity = btn.tag
        if (selected == Constants.OpenIncidentType.buisnessImpacting) {
            switch btn.tag {
            case 1:
                selectedArray = openIncidentResponse?.busiS1
                tableView.reloadData()
            case 2:
                
                selectedArray = openIncidentResponse?.busiS2
                tableView.reloadData()
                
                
            default:
                print("nothing selected")
            }
        }
        else {
            
            
            switch btn.tag {
            case 1:
                selectedArray = openIncidentResponse?.nonBusiS1
                tableView.reloadData()
            case 2:
                
                selectedArray = openIncidentResponse?.nonBusiS2
                tableView.reloadData()
            case 3:
                
                selectedArray = openIncidentResponse?.nonBusiS3
                tableView.reloadData()
            case 4:
                
                selectedArray = openIncidentResponse?.nonBusiS4
                
                tableView.reloadData()
                
            default:
                print("nothing selected")
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.calendarView.select(Date())
        self.calendarView.appearance.todaySelectionColor = UIColor(red: 31.0/255.0, green: 119.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    @IBAction func filterBtnAcn(_ sender: Any) {
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.cnstHeightCalender.constant = bounds.height
        self.view.layoutIfNeeded()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.calendarView.appearance.todaySelectionColor = UIColor(red: 31.0/255.0, green: 119.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        selectedDate = "\(self.dateFormatter.string(from: date))"
        print("did select date \(self.dateFormatter.string(from: date))")
      let  formttedDate = self.dateFormatter.string(from: date)
        
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
            
        }
        self.reloadIncidentHistory(date:formttedDate)
        //self.presenter?.requestData(date: selectedDate ?? "")
    }
    
    func reloadIncidentHistory(date:String)
    {
        startAnimating()
        self.presenter?.presentIncidentHistoryModule(fromView: self, type: type!, selected: selected!, date:date)
        self.calendarView.appearance.selectionColor = UIColor.clear
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        let fromDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        return self.dateFormatter.date(from: self.dateFormatter.string(from: fromDate!))!
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return self.dateFormatter.date(from: self.dateFormatter.string(from:Date()))!
    }
    
}

extension NGOOpenIncidentDetailVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell:IncidentSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "IncidentSummaryTableViewCell", for: indexPath as IndexPath) as! IncidentSummaryTableViewCell
            cell.firstViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
            cell.SecondViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
            if (selected == Constants.OpenIncidentType.buisnessImpacting) {
                cell.headerTitleLabel.text = "Open Business Imapcting Incidents(\(openIncidentResponse?.businessImpactingCount ?? 0))"
                cell.ThirdView.isHidden = true
                cell.FourthView.isHidden = true
                
                cell.firstViewTitleLabel.text = openIncidentResponse?.busiOI[0].featureName
                cell.firstViewDescriptionLabel.text = "\(openIncidentResponse?.busiOI[0].outlierCount ?? 0)"
                cell.SecondViewTitleLabel.text = openIncidentResponse?.busiOI[1].featureName
                cell.SecondViewDescriptionLabel.text = "\(openIncidentResponse?.busiOI[1].outlierCount ?? 0)"

            }
            else {
                cell.headerTitleLabel.text = "Open Non-Business Imapcting Incidents(\(openIncidentResponse?.nonBusinessImpactingCount ?? 0))"
                
                cell.firstViewTitleLabel.text = openIncidentResponse?.nonBusiOI[0].featureName
                cell.firstViewDescriptionLabel.text = "\(openIncidentResponse?.nonBusiOI[0].outlierCount ?? 0)"
                
                cell.SecondViewTitleLabel.text = openIncidentResponse?.nonBusiOI[1].featureName
                cell.SecondViewDescriptionLabel.text = "\(openIncidentResponse?.nonBusiOI[1].outlierCount ?? 0)"
                
                cell.ThirdViewTitleLabel.text = openIncidentResponse?.nonBusiOI[2].featureName
                cell.ThirdViewDescriptionLabel.text = "\(openIncidentResponse?.nonBusiOI[2].outlierCount ?? 0)"
                
                cell.FourthViewTitleLabel.text = openIncidentResponse?.nonBusiOI[3].featureName
                cell.FourthViewDescriptionLabel.text = "\(openIncidentResponse?.nonBusiOI[3].outlierCount ?? 0)"
                
                cell.ThirdView.isHidden = false
                cell.FourthView.isHidden = false
                cell.ThirdViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
                cell.FourthViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
                
                
            }
            
            return cell
        }
            
        else {
            let cell:SeverityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SeverityTableViewCell", for: indexPath as IndexPath) as! SeverityTableViewCell
            
            
            
            if let response = selectedArray?[indexPath.row] as? NonBusiS3 {
                
                cell.firstViewFirstDescriptionLabel.text = response.incidentno
                cell.firstViewSecondDescriptionLabel.text = response.problemSTATUS
                cell.firstViewThirdDescriptionLabel.text = response.severity
                cell.impactDescriptionLabel.text = response.businessIMPACT
                cell.resolverDescriptionLabel.text = response.assignmentGROUP
                cell.SecondViewFirstDescriptionLabel.text =  "\(response.rilTTR  ?? 0)"
                cell.SecondViewSecondDescriptionLabel.text = response.openTIME
                cell.SecondViewThirdDescriptionLabel.text = response.resolvedTIME
                cell.SecondViewFourthDescriptionLabel.text = response.impactedAPPLICATIONS
            }
            if let response = selectedArray?[indexPath.row] as? NonBusiS2 {
                cell.firstViewFirstDescriptionLabel.text = response.incidentno
                cell.firstViewSecondDescriptionLabel.text = response.problemSTATUS
                cell.firstViewThirdDescriptionLabel.text = response.severity
                cell.impactDescriptionLabel.text = response.businessIMPACT
                cell.resolverDescriptionLabel.text = response.assignmentGROUP
                cell.SecondViewFirstDescriptionLabel.text =  "\(response.rilTTR  ?? 0)"
                cell.SecondViewSecondDescriptionLabel.text = response.openTIME
                cell.SecondViewThirdDescriptionLabel.text = response.resolvedTIME
                cell.SecondViewFourthDescriptionLabel.text = response.impactedAPPLICATIONS
                
            }
            
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else
        {
            return selectedArray?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    internal func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell") as! OutlierTableViewCell
            cell.titleLbl.text = "Open Severity \(selectedSeverity ?? 0) Incident"
            cell.titleLbl.textColor = UIColor.white
            cell.arrowImage.isHidden = true
            cell.countLbl.text = "\(selectedArray?.count ?? 0)"
            cell.countLbl.textColor = UIColor.white
            cell.expandAction.isHidden = true
            cell.backgroundColor = UIColor.init(red: 28, green: 122, blue: 189)
            
            return cell as UIView
        }
        else {
            return  UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            
            return   44
        }
        else{
            return     5
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        15
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView(frame: .zero)
    }
}



