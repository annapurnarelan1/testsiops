//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class IncidentHistoryModel : NSObject, NSCoding{

	var busiART : [IncidentHistoryBusiART]!
	var busiARTCount : Float!
	var busiMRT : AnyObject!
	var busiMRTCount : Int!
	var busiOI : [IncidentHistoryBusiART]!
	var busiS1 : [IncidentHistoryNonBusiS2]!
	var busiS2 : [IncidentHistoryNonBusiS2]!
	var businessImpactingCount : Int!
	var nonBusiART : [IncidentHistoryNonBusiART]!
	var nonBusiARTCount : Float!
	var nonBusiMRT : AnyObject!
	var nonBusiMRTCount : Int!
	var nonBusiOI : [IncidentHistoryBusiART]!
	var nonBusiS1 : [IncidentHistoryNonBusiS2]!
	var nonBusiS2 : [IncidentHistoryNonBusiS2]!
	var nonBusiS3 : [IncidentHistoryNonBusiS3]!
	var nonBusiS4 : [IncidentHistoryNonBusiS3]!
	var nonBusinessImpactingCount : Int!
	var s1Count : Int!
	var s2Count : Int!
	var s3Count : Int!
	var s4Count : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		busiART = [IncidentHistoryBusiART]()
		if let busiARTArray = dictionary["busiART"] as? [[String:Any]]{
			for dic in busiARTArray{
				let value = IncidentHistoryBusiART(fromDictionary: dic)
				busiART.append(value)
			}
		}
		busiARTCount = dictionary["busiARTCount"] as? Float
		busiMRT = dictionary["busiMRT"] as? AnyObject
		busiMRTCount = dictionary["busiMRTCount"] as? Int
		busiOI = [IncidentHistoryBusiART]()
		if let busiOIArray = dictionary["busiOI"] as? [[String:Any]]{
			for dic in busiOIArray{
				let value = IncidentHistoryBusiART(fromDictionary: dic)
				busiOI.append(value)
			}
		}
		
        
        busiS1 = dictionary["busiS1"] as? [IncidentHistoryNonBusiS2]
        if let nonBusiS1Array = dictionary["busiS1"] as? [[String:Any]]{
            for dic in nonBusiS1Array{
                let value = IncidentHistoryNonBusiS2(fromDictionary: dic)
                busiS1.append(value)
            }
        
        }
        busiS2 = [IncidentHistoryNonBusiS2]()
        if let nonBusiS2Array = dictionary["busiS2"] as? [[String:Any]]{
            for dic in nonBusiS2Array{
                let value = IncidentHistoryNonBusiS2(fromDictionary: dic)
                busiS2.append(value)
            }
        }
        
		businessImpactingCount = dictionary["businessImpactingCount"] as? Int
		nonBusiART = [IncidentHistoryNonBusiART]()
		if let nonBusiARTArray = dictionary["nonBusiART"] as? [[String:Any]]{
			for dic in nonBusiARTArray{
				let value = IncidentHistoryNonBusiART(fromDictionary: dic)
				nonBusiART.append(value)
			}
		}
		nonBusiARTCount = dictionary["nonBusiARTCount"] as? Float
		nonBusiMRT = dictionary["nonBusiMRT"] as? AnyObject
		nonBusiMRTCount = dictionary["nonBusiMRTCount"] as? Int
		nonBusiOI = [IncidentHistoryBusiART]()
		if let nonBusiOIArray = dictionary["nonBusiOI"] as? [[String:Any]]{
			for dic in nonBusiOIArray{
				let value = IncidentHistoryBusiART(fromDictionary: dic)
				nonBusiOI.append(value)
			}
		}
		nonBusiS1 = dictionary["nonBusiS1"] as? [IncidentHistoryNonBusiS2]
        if let nonBusiS1Array = dictionary["nonBusiS1"] as? [[String:Any]]{
            for dic in nonBusiS1Array{
                let value = IncidentHistoryNonBusiS2(fromDictionary: dic)
                nonBusiS1.append(value)
            }
        
        }
		nonBusiS2 = [IncidentHistoryNonBusiS2]()
		if let nonBusiS2Array = dictionary["nonBusiS2"] as? [[String:Any]]{
			for dic in nonBusiS2Array{
				let value = IncidentHistoryNonBusiS2(fromDictionary: dic)
				nonBusiS2.append(value)
			}
		}
		nonBusiS3 = [IncidentHistoryNonBusiS3]()
		if let nonBusiS3Array = dictionary["nonBusiS3"] as? [[String:Any]]{
			for dic in nonBusiS3Array{
				let value = IncidentHistoryNonBusiS3(fromDictionary: dic)
				nonBusiS3.append(value)
			}
		}
		nonBusiS4 = [IncidentHistoryNonBusiS3]()
		if let nonBusiS4Array = dictionary["nonBusiS4"] as? [[String:Any]]{
			for dic in nonBusiS4Array{
				let value = IncidentHistoryNonBusiS3(fromDictionary: dic)
				nonBusiS4.append(value)
			}
		}
		nonBusinessImpactingCount = dictionary["nonBusinessImpactingCount"] as? Int
		s1Count = dictionary["s1Count"] as? Int
		s2Count = dictionary["s2Count"] as? Int
		s3Count = dictionary["s3Count"] as? Int
		s4Count = dictionary["s4Count"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if busiART != nil{
			var dictionaryElements = [[String:Any]]()
			for busiARTElement in busiART {
				dictionaryElements.append(busiARTElement.toDictionary())
			}
			dictionary["busiART"] = dictionaryElements
		}
		if busiARTCount != nil{
			dictionary["busiARTCount"] = busiARTCount
		}
		if busiMRT != nil{
			dictionary["busiMRT"] = busiMRT
		}
		if busiMRTCount != nil{
			dictionary["busiMRTCount"] = busiMRTCount
		}
		if busiOI != nil{
			var dictionaryElements = [[String:Any]]()
			for busiOIElement in busiOI {
				dictionaryElements.append(busiOIElement.toDictionary())
			}
			dictionary["busiOI"] = dictionaryElements
		}
        
        if busiS1 != nil{
            var dictionaryElements = [[String:Any]]()
            for busiS1Element in busiS1 {
                dictionaryElements.append(busiS1Element.toDictionary())
            }
            dictionary["busiS1"] = dictionaryElements
        }
        if busiS2 != nil{
            var dictionaryElements = [[String:Any]]()
            for busiS2Element in busiS2 {
                dictionaryElements.append(busiS2Element.toDictionary())
            }
            dictionary["busiS2"] = dictionaryElements
        }
		if businessImpactingCount != nil{
			dictionary["businessImpactingCount"] = businessImpactingCount
		}
		if nonBusiART != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiARTElement in nonBusiART {
				dictionaryElements.append(nonBusiARTElement.toDictionary())
			}
			dictionary["nonBusiART"] = dictionaryElements
		}
		if nonBusiARTCount != nil{
			dictionary["nonBusiARTCount"] = nonBusiARTCount
		}
		if nonBusiMRT != nil{
			dictionary["nonBusiMRT"] = nonBusiMRT
		}
		if nonBusiMRTCount != nil{
			dictionary["nonBusiMRTCount"] = nonBusiMRTCount
		}
		if nonBusiOI != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiOIElement in nonBusiOI {
				dictionaryElements.append(nonBusiOIElement.toDictionary())
			}
			dictionary["nonBusiOI"] = dictionaryElements
		}
		if nonBusiS1 != nil{
            var dictionaryElements = [[String:Any]]()
            for nonBusiS1Element in nonBusiS1 {
                dictionaryElements.append(nonBusiS1Element.toDictionary())
            }
			dictionary["nonBusiS1"] = dictionaryElements
		}
		if nonBusiS2 != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiS2Element in nonBusiS2 {
				dictionaryElements.append(nonBusiS2Element.toDictionary())
			}
			dictionary["nonBusiS2"] = dictionaryElements
		}
		if nonBusiS3 != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiS3Element in nonBusiS3 {
				dictionaryElements.append(nonBusiS3Element.toDictionary())
			}
			dictionary["nonBusiS3"] = dictionaryElements
		}
		if nonBusiS4 != nil{
			var dictionaryElements = [[String:Any]]()
			for nonBusiS4Element in nonBusiS4 {
				dictionaryElements.append(nonBusiS4Element.toDictionary())
			}
			dictionary["nonBusiS4"] = dictionaryElements
		}
		if nonBusinessImpactingCount != nil{
			dictionary["nonBusinessImpactingCount"] = nonBusinessImpactingCount
		}
		if s1Count != nil{
			dictionary["s1Count"] = s1Count
		}
		if s2Count != nil{
			dictionary["s2Count"] = s2Count
		}
		if s3Count != nil{
			dictionary["s3Count"] = s3Count
		}
		if s4Count != nil{
			dictionary["s4Count"] = s4Count
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         busiART = aDecoder.decodeObject(forKey :"busiART") as? [IncidentHistoryBusiART]
         busiARTCount = aDecoder.decodeObject(forKey: "busiARTCount") as? Float
         busiMRT = aDecoder.decodeObject(forKey: "busiMRT") as? AnyObject
         busiMRTCount = aDecoder.decodeObject(forKey: "busiMRTCount") as? Int
         busiOI = aDecoder.decodeObject(forKey :"busiOI") as? [IncidentHistoryBusiART]
         busiS1 = aDecoder.decodeObject(forKey: "busiS1") as? [IncidentHistoryNonBusiS2]
         busiS2 = aDecoder.decodeObject(forKey: "busiS2") as? [IncidentHistoryNonBusiS2]
         businessImpactingCount = aDecoder.decodeObject(forKey: "businessImpactingCount") as? Int
         nonBusiART = aDecoder.decodeObject(forKey :"nonBusiART") as? [IncidentHistoryNonBusiART]
         nonBusiARTCount = aDecoder.decodeObject(forKey: "nonBusiARTCount") as? Float
         nonBusiMRT = aDecoder.decodeObject(forKey: "nonBusiMRT") as? AnyObject
         nonBusiMRTCount = aDecoder.decodeObject(forKey: "nonBusiMRTCount") as? Int
         nonBusiOI = aDecoder.decodeObject(forKey :"nonBusiOI") as? [IncidentHistoryBusiART]
         nonBusiS1 = aDecoder.decodeObject(forKey: "nonBusiS1") as? [IncidentHistoryNonBusiS2]
         nonBusiS2 = aDecoder.decodeObject(forKey :"nonBusiS2") as? [IncidentHistoryNonBusiS2]
         nonBusiS3 = aDecoder.decodeObject(forKey :"nonBusiS3") as? [IncidentHistoryNonBusiS3]
         nonBusiS4 = aDecoder.decodeObject(forKey :"nonBusiS4") as? [IncidentHistoryNonBusiS3]
         nonBusinessImpactingCount = aDecoder.decodeObject(forKey: "nonBusinessImpactingCount") as? Int
         s1Count = aDecoder.decodeObject(forKey: "s1Count") as? Int
         s2Count = aDecoder.decodeObject(forKey: "s2Count") as? Int
         s3Count = aDecoder.decodeObject(forKey: "s3Count") as? Int
         s4Count = aDecoder.decodeObject(forKey: "s4Count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if busiART != nil{
			aCoder.encode(busiART, forKey: "busiART")
		}
		if busiARTCount != nil{
			aCoder.encode(busiARTCount, forKey: "busiARTCount")
		}
		if busiMRT != nil{
			aCoder.encode(busiMRT, forKey: "busiMRT")
		}
		if busiMRTCount != nil{
			aCoder.encode(busiMRTCount, forKey: "busiMRTCount")
		}
		if busiOI != nil{
			aCoder.encode(busiOI, forKey: "busiOI")
		}
		if busiS1 != nil{
			aCoder.encode(busiS1, forKey: "busiS1")
		}
		if busiS2 != nil{
			aCoder.encode(busiS2, forKey: "busiS2")
		}
		if businessImpactingCount != nil{
			aCoder.encode(businessImpactingCount, forKey: "businessImpactingCount")
		}
		if nonBusiART != nil{
			aCoder.encode(nonBusiART, forKey: "nonBusiART")
		}
		if nonBusiARTCount != nil{
			aCoder.encode(nonBusiARTCount, forKey: "nonBusiARTCount")
		}
		if nonBusiMRT != nil{
			aCoder.encode(nonBusiMRT, forKey: "nonBusiMRT")
		}
		if nonBusiMRTCount != nil{
			aCoder.encode(nonBusiMRTCount, forKey: "nonBusiMRTCount")
		}
		if nonBusiOI != nil{
			aCoder.encode(nonBusiOI, forKey: "nonBusiOI")
		}
		if nonBusiS1 != nil{
			aCoder.encode(nonBusiS1, forKey: "nonBusiS1")
		}
		if nonBusiS2 != nil{
			aCoder.encode(nonBusiS2, forKey: "nonBusiS2")
		}
		if nonBusiS3 != nil{
			aCoder.encode(nonBusiS3, forKey: "nonBusiS3")
		}
		if nonBusiS4 != nil{
			aCoder.encode(nonBusiS4, forKey: "nonBusiS4")
		}
		if nonBusinessImpactingCount != nil{
			aCoder.encode(nonBusinessImpactingCount, forKey: "nonBusinessImpactingCount")
		}
		if s1Count != nil{
			aCoder.encode(s1Count, forKey: "s1Count")
		}
		if s2Count != nil{
			aCoder.encode(s2Count, forKey: "s2Count")
		}
		if s3Count != nil{
			aCoder.encode(s3Count, forKey: "s3Count")
		}
		if s4Count != nil{
			aCoder.encode(s4Count, forKey: "s4Count")
		}

	}

}
