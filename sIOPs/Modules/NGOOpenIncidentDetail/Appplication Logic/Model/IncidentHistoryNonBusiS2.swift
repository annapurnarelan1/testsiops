//
//	NonBusiS2.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class IncidentHistoryNonBusiS2 : NSObject, NSCoding{

	var assignmentGROUP : String!
	var businessIMPACT : String!
	var impactedAPPLICATIONS : String!
	var incidentno : String!
	var openTIME : String!
	var problemSTATUS : String!
	var resolvedTIME : String!
	var rilTTR : Float!
	var severity : String!
	var type : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		assignmentGROUP = dictionary["assignment_GROUP"] as? String
		businessIMPACT = dictionary["business_IMPACT"] as? String
		impactedAPPLICATIONS = dictionary["impacted_APPLICATIONS"] as? String
		incidentno = dictionary["incidentno"] as? String
		openTIME = dictionary["open_TIME"] as? String
		problemSTATUS = dictionary["problem_STATUS"] as? String
		resolvedTIME = dictionary["resolved_TIME"] as? String
		rilTTR = dictionary["ril_TTR"] as? Float
		severity = dictionary["severity"] as? String
		type = dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if assignmentGROUP != nil{
			dictionary["assignment_GROUP"] = assignmentGROUP
		}
		if businessIMPACT != nil{
			dictionary["business_IMPACT"] = businessIMPACT
		}
		if impactedAPPLICATIONS != nil{
			dictionary["impacted_APPLICATIONS"] = impactedAPPLICATIONS
		}
		if incidentno != nil{
			dictionary["incidentno"] = incidentno
		}
		if openTIME != nil{
			dictionary["open_TIME"] = openTIME
		}
		if problemSTATUS != nil{
			dictionary["problem_STATUS"] = problemSTATUS
		}
		if resolvedTIME != nil{
			dictionary["resolved_TIME"] = resolvedTIME
		}
		if rilTTR != nil{
			dictionary["ril_TTR"] = rilTTR
		}
		if severity != nil{
			dictionary["severity"] = severity
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         assignmentGROUP = aDecoder.decodeObject(forKey: "assignment_GROUP") as? String
         businessIMPACT = aDecoder.decodeObject(forKey: "business_IMPACT") as? String
         impactedAPPLICATIONS = aDecoder.decodeObject(forKey: "impacted_APPLICATIONS") as? String
         incidentno = aDecoder.decodeObject(forKey: "incidentno") as? String
         openTIME = aDecoder.decodeObject(forKey: "open_TIME") as? String
         problemSTATUS = aDecoder.decodeObject(forKey: "problem_STATUS") as? String
         resolvedTIME = aDecoder.decodeObject(forKey: "resolved_TIME") as? String
         rilTTR = aDecoder.decodeObject(forKey: "ril_TTR") as? Float
         severity = aDecoder.decodeObject(forKey: "severity") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if assignmentGROUP != nil{
			aCoder.encode(assignmentGROUP, forKey: "assignment_GROUP")
		}
		if businessIMPACT != nil{
			aCoder.encode(businessIMPACT, forKey: "business_IMPACT")
		}
		if impactedAPPLICATIONS != nil{
			aCoder.encode(impactedAPPLICATIONS, forKey: "impacted_APPLICATIONS")
		}
		if incidentno != nil{
			aCoder.encode(incidentno, forKey: "incidentno")
		}
		if openTIME != nil{
			aCoder.encode(openTIME, forKey: "open_TIME")
		}
		if problemSTATUS != nil{
			aCoder.encode(problemSTATUS, forKey: "problem_STATUS")
		}
		if resolvedTIME != nil{
			aCoder.encode(resolvedTIME, forKey: "resolved_TIME")
		}
		if rilTTR != nil{
			aCoder.encode(rilTTR, forKey: "ril_TTR")
		}
		if severity != nil{
			aCoder.encode(severity, forKey: "severity")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}
