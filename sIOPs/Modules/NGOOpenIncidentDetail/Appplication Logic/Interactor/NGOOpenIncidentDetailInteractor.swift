//
//  InfraDetailDetailInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class NGOOpenIncidentDetailInteractor: NGOOpenIncidentDetailInteractorInputProtocol {
    func requestIncidentHistory(date: String) {
        
    }
    
    

public enum NGOOpenIncidentDetailError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: NGOOpenIncidentDetailInteractorOutputProtocol?
    
    
    func requestAlarmData(category:String){
            
            
            let pubInfo: [String : Any] =
                ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "appRoleCode":  "723",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                "category":category
                
                
            ]
            
            let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            var busicode = ""
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
                busicode = Constants.BusiCode.UtilityAlarmCategory
          //  }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    
                   
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                             let data = IncidentHistoryModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//                                self.presenter?.reloadHistoryData(response: data)
                      case Constants.status.NOK:
                             let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                            self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
    //                let loginRes: LoginModel = LoginModel.sharedInstance
    //
    //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
    //                LoginModel.sharedInstance.saveUser()
                    
                   // self.presenter?.loginDone(loginRes:loginRes)
                    
                   
                case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
                }
            })
            
        }
    
    
    
    
    
    
    func requestAlarmDataFilter(category:String,filter:[String:Any]){
            
            
            let pubInfo: [String : Any] =
                ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
        let filter :[String : Any] = [
            
            "startAge": (filter["Ageing"] as? Filter)?.startRang ?? "All",
            "endAge": (filter["Ageing"] as? Filter)?.endRang ?? "All",
            "region": (filter["geography"] as? NSDictionary ?? [:])["Region"] ?? "All",
            "circle": (filter["geography"] as? NSDictionary ?? [:])["R4G State"] ?? "All",
            "mp": (filter["geography"] as? NSDictionary ?? [:])["MP"] ?? "All",
            "jc": (filter["geography"] as? NSDictionary ?? [:])["JC"] ?? "All",
        ]
        
        
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "appRoleCode":  "723",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                "category":category,
                "filters": filter
                    
                
                
            ]
            
            let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            var busicode = ""
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
        busicode = Constants.BusiCode.UtilityAlarmCategory
          //  }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    
                   
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                             let data = AlarmModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//                                self.presenter?.reloadHistoryData(response: data)
                      case Constants.status.NOK:
                             let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                            self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
    //                let loginRes: LoginModel = LoginModel.sharedInstance
    //
    //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
    //                LoginModel.sharedInstance.saveUser()
                    
                   // self.presenter?.loginDone(loginRes:loginRes)
                    
                   
                case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
                }
            })
            
        }
    
    
    
    
    
    
    
    func requestSiteDownData(category:String,id:String){
            
            
            let pubInfo: [String : Any] =
                ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               // "appRoleCode":  "723",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                "category":category,
                "outlierId":id
                
                
                
            ]
            
            let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            var busicode = ""
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
                busicode = Constants.BusiCode.SiteDownCategory
          //  }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    
                   
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                             let data = SiteDownModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//                                self.presenter?.reloadsiteDownData(response: data)
                      case Constants.status.NOK:
                             let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                            self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
    //                let loginRes: LoginModel = LoginModel.sharedInstance
    //
    //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
    //                LoginModel.sharedInstance.saveUser()
                    
                   // self.presenter?.loginDone(loginRes:loginRes)
                    
                   
                case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
                }
            })
            
        }
    
    func requestSiteDownDataFilter(category:String,id:String,filter:[String:Any]){
            
            
            let pubInfo: [String : Any] =
                ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
        let filter :[String : Any] = [
            
            "startAge": (filter["Ageing"] as? Filter)?.startRang ?? "All",
            "endAge": (filter["Ageing"] as? Filter)?.endRang ?? "All",
            "startIC":(filter["Impacted customers"] as? Filter)?.startRang ?? "All",
            "endIC":(filter["Impacted customers"] as? Filter)?.endRang ?? "All",
            "region": (filter["geography"] as? NSDictionary ?? [:])["Region"] ?? "All",
            "circle": (filter["geography"] as? NSDictionary ?? [:])["R4G State"] ?? "All",
            "mp": (filter["geography"] as? NSDictionary ?? [:])["MP"] ?? "All",
            "jc": (filter["geography"] as? NSDictionary ?? [:])["JC"] ?? "All",
        ]
            
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               // "appRoleCode":  "723",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                "category":category,
                "outlierId":id,
                "filters": filter
                
                
                
            ]
            
            let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            var busicode = ""
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
        busicode = Constants.BusiCode.SiteDownCategory
          //  }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                switch result {
                case .success(let returnJson) :
                    print(returnJson)
                    
                   
                    let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                    
                    switch status {
                    case Constants.status.OK:
                             let data = SiteDownModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//                                self.presenter?.reloadsiteDownData(response: data)
                      case Constants.status.NOK:
                             let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                            self.presenter?.showFailError(error:message)
                    default:
                        self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                    }
                    
    //                let loginRes: LoginModel = LoginModel.sharedInstance
    //
    //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
    //                LoginModel.sharedInstance.saveUser()
                    
                   // self.presenter?.loginDone(loginRes:loginRes)
                    
                   
                case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
                }
            })
            
        }
}
