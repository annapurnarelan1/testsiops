//
//  NGOIncidentHistoryProtocol.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol NGOIncidentHistoryViewProtocol: class {
    var presenter: NGOIncidentHistoryPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadHistoryData(response:IncidentHistoryModel)

}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGOIncidentHistoryWireFrameProtocol: class {
    static func presentNGOIncidentHistoryModule(fromView:AnyObject,type:String,selected:String,date:String)
 //  func presentOpenAlertsDetailModule(type:String,infraResponse:InfraModel)
}

/// Method contract between VIEW -> PRESENTER
protocol NGOIncidentHistoryPresenterProtocol: class {
    var view: NGOIncidentHistoryViewProtocol? { get set }
    var interactor: NGOIncidentHistoryInteractorInputProtocol? { get set }
    var wireFrame: NGOIncidentHistoryWireFrameProtocol? { get set }
  //  func presentInfraDetailModule(type:String,infraResponse:InfraModel)
    
  func requestIncidentHistory(date:String)
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGOIncidentHistoryInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
        func showFailError(error:String)
    func reloadHistoryData(response:IncidentHistoryModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGOIncidentHistoryInteractorInputProtocol: class
{
    var presenter: NGOIncidentHistoryInteractorOutputProtocol? { get set }
   func requestIncidentHistory(date:String)
    
}
