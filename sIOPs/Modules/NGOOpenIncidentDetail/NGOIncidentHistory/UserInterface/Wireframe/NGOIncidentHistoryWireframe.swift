//
//  NGOIncidentHistoryWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGOIncidentHistoryWireframe: NGOIncidentHistoryWireFrameProtocol {
    
    
    // MARK: NGOIncidentHistoryWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
//    class func presentAlarmDetailModule(fromView:AnyObject,type:String,selected:String,response:Any,applicationDic:LoginApplication) {
//
//        // Generating module components
//        let view: NGOOpenIncidentDetailViewProtocol = NGOOpenIncidentDetailVC.instantiate()
//        let presenter: NGOOpenIncidentDetailPresenterProtocol & NGOOpenIncidentDetailInteractorOutputProtocol = NGOOpenIncidentDetailPresenter()
//        let interactor: NGOOpenIncidentDetailInteractorInputProtocol = NGOOpenIncidentDetailInteractor()
//
//        let wireFrame: NGOOpenIncidentDetailWireframeProtocol = NGOOpenIncidentDetailWireframe()
//
//        // Connecting
//        view.presenter = presenter
//        presenter.view = view
//        presenter.wireFrame = wireFrame
//        presenter.interactor = interactor
//        interactor.presenter = presenter
//
//        let viewController = view as! AlarmViewController
//        viewController.type = type
//        viewController.selected = selected
//        viewController.infraResponse =  response
//        viewController.applicationCode = applicationDic.applicationCode ?? ""
//
//
//        NavigationHelper.pushViewController(viewController: viewController)
//    }
//
    
    class func presentNGOIncidentHistoryModule(fromView:AnyObject,type:String,selected:String, date:String) {

        // Generating module components
        let view: NGOIncidentHistoryViewProtocol = NGOIncidentHistoryVC.instantiate()
        let presenter: NGOIncidentHistoryPresenterProtocol & NGOIncidentHistoryInteractorOutputProtocol = NGOIncidentHistoryPresenter()
        let interactor: NGOIncidentHistoryInteractorInputProtocol = NGOIncidentHistoryInteractor()
       
        let wireFrame: NGOIncidentHistoryWireFrameProtocol = NGOIncidentHistoryWireframe()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NGOIncidentHistoryVC
        viewController.type = type
        viewController.selected = selected
        viewController.selectedDate = date
//        viewController.applicationCode = applicationDic.applicationCode ?? ""
//        viewController.selectedSite = selectedSite
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   
    
   
}
