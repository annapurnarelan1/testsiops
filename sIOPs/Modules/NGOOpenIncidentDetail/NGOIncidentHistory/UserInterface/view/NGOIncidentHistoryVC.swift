//
//  InfraDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class NGOIncidentHistoryVC: BaseViewController,NGOIncidentHistoryViewProtocol {
    var presenter: NGOIncidentHistoryPresenterProtocol?
    
    
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var filteBtn: BadgeButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    var type:String?
    var selected:String?
    var selectedSeverity:Int?
    var historyResponse:IncidentHistoryModel?
    var selectedArray:[Any]?
    var applicationCode:String?
    var selectedSite:InfraCellImpacted?
    var selectedCategory :String = "All"
    var response:Any?
    var selectedDate:String?
    var selectedFilter = [String:Any]()
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> NGOIncidentHistoryViewProtocol{
        return UIStoryboard(name: "NGOIncidentHistory", bundle: nil).instantiateViewController(withIdentifier: "NGOIncidentHistory") as! NGOIncidentHistoryVC
    }
    
    @IBOutlet weak var lblSelectedDate: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 180
        tableView.register(UINib(nibName: "SeverityTableViewCell", bundle: nil), forCellReuseIdentifier: "SeverityTableViewCell")
        tableView.register(UINib(nibName: "IncidentSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "IncidentSummaryTableViewCell")
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        self.presenter?.requestIncidentHistory(date:selectedDate!)
        
        lblSelectedDate.text = HelperMethods().formatDateOrderJourney(date: selectedDate ?? "")
        // Do any additional setup after loading the view.
    }
    
    
    func reloadHistoryData(response:IncidentHistoryModel)
    {
        stopAnimating()
        historyResponse = response
        tableView.reloadData()
        let button = UIButton(frame: CGRect(x: 5, y: 5, width: 5, height: 5))
        button.tag = 1
        OpenIncidentDetail(sender:button)
    }
    
   
    @objc func OpenIncidentDetail(sender:Any)
    {
        
        let btn:UIButton = (sender as? UIButton)!
        selectedSeverity = btn.tag
        if (selected == Constants.OpenIncidentType.buisnessImpacting) {
            switch btn.tag {
            case 1:
                selectedArray = historyResponse?.busiS1
                tableView.reloadData()
            case 2:
                
                selectedArray = historyResponse?.busiS2
                tableView.reloadData()
                
                
            default:
                
                print("nothing selected")
            }
        }
        else {
            
            
            switch btn.tag {
            case 1:
                selectedArray = historyResponse?.nonBusiS1
                tableView.reloadData()
            case 2:
                
                selectedArray = historyResponse?.nonBusiS2
                tableView.reloadData()
            case 3:
                
                selectedArray = historyResponse?.nonBusiS3
                tableView.reloadData()
            case 4:
                
                selectedArray = historyResponse?.nonBusiS4
                tableView.reloadData()
                
            default:
                print("nothing selected")
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
        startAnimating()

    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    @IBAction func filterBtnAcn(_ sender: Any) {
        
    }
    
}

extension NGOIncidentHistoryVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell:IncidentSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "IncidentSummaryTableViewCell", for: indexPath as IndexPath) as! IncidentSummaryTableViewCell
            cell.firstViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
            cell.SecondViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
            
            if (selected == Constants.OpenIncidentType.buisnessImpacting) {
                cell.headerTitleLabel.text = "Open Business Imapcting Incidents(\(historyResponse?.businessImpactingCount ?? 0))"
                cell.ThirdView.isHidden = true
                cell.FourthView.isHidden = true
                
                cell.firstViewTitleLabel.text = historyResponse?.busiOI[0].featureName
                cell.firstViewDescriptionLabel.text = "\(historyResponse?.busiOI[0].outlierCount ?? 0)"
                cell.SecondViewTitleLabel.text = historyResponse?.busiOI[1].featureName
                cell.SecondViewDescriptionLabel.text = "\(historyResponse?.busiOI[1].outlierCount ?? 0)"
                
            }
            else {
                cell.headerTitleLabel.text = "Open Non-Business Imapcting Incidents(\(historyResponse?.nonBusinessImpactingCount ?? 0))"
                
                cell.firstViewTitleLabel.text = historyResponse?.nonBusiOI[0].featureName
                cell.firstViewDescriptionLabel.text = "\(historyResponse?.nonBusiOI[0].outlierCount ?? 0)"
                
                cell.SecondViewTitleLabel.text = historyResponse?.nonBusiOI[1].featureName
                cell.SecondViewDescriptionLabel.text = "\(historyResponse?.nonBusiOI[1].outlierCount ?? 0)"
                
                cell.ThirdViewTitleLabel.text = historyResponse?.nonBusiOI[2].featureName
                cell.ThirdViewDescriptionLabel.text = "\(historyResponse?.nonBusiOI[2].outlierCount ?? 0)"
                
                cell.FourthViewTitleLabel.text = historyResponse?.nonBusiOI[3].featureName
                cell.FourthViewDescriptionLabel.text = "\(historyResponse?.nonBusiOI[3].outlierCount ?? 0)"
                
                cell.ThirdView.isHidden = false
                cell.FourthView.isHidden = false
                cell.ThirdViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
                cell.FourthViewButton.addTarget(self, action: #selector(OpenIncidentDetail(sender:)), for: .touchUpInside)
                
                
            }
            
            return cell
        case 1:
            let cell:IncidentSummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "IncidentSummaryTableViewCell", for: indexPath as IndexPath) as! IncidentSummaryTableViewCell
            cell.headerTitleLabel.text = "Average resolution Time"
            
            if (selected == Constants.OpenIncidentType.buisnessImpacting) {
                cell.ThirdView.isHidden = true
                cell.FourthView.isHidden = true
                
                cell.firstViewTitleLabel.text = historyResponse?.busiART[0].featureName
                cell.firstViewDescriptionLabel.text = historyResponse?.busiART[0].value ?? ""
                cell.SecondViewTitleLabel.text = historyResponse?.busiART[1].featureName
                cell.SecondViewDescriptionLabel.text = historyResponse?.busiART[1].value ?? ""
                
            }
            else {
                cell.ThirdView.isHidden = false
                cell.FourthView.isHidden = false
                cell.firstViewTitleLabel.text = historyResponse?.nonBusiART[0].featureName
                cell.firstViewDescriptionLabel.text = historyResponse?.nonBusiART[0].value ?? ""
                
                cell.SecondViewTitleLabel.text = historyResponse?.nonBusiART[1].featureName
                cell.SecondViewDescriptionLabel.text = historyResponse?.nonBusiART[1].value ?? ""
                
                cell.ThirdViewTitleLabel.text = historyResponse?.nonBusiART[2].featureName
                cell.ThirdViewDescriptionLabel.text = historyResponse?.nonBusiART[2].value ?? ""
                
                cell.FourthViewTitleLabel.text = historyResponse?.nonBusiART[3].featureName
                cell.FourthViewDescriptionLabel.text = historyResponse?.nonBusiART[3].value ?? ""
                
            }
            
            return cell
        case 2:
            let cell:SeverityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SeverityTableViewCell", for: indexPath as IndexPath) as! SeverityTableViewCell
            
            if let response = selectedArray?[indexPath.row] as? IncidentHistoryNonBusiS2 {
                
                cell.firstViewFirstDescriptionLabel.text = response.incidentno
                cell.firstViewSecondDescriptionLabel.text = response.problemSTATUS
                cell.firstViewThirdDescriptionLabel.text = response.severity
                cell.impactDescriptionLabel.text = response.businessIMPACT
                cell.resolverDescriptionLabel.text = response.assignmentGROUP
                cell.SecondViewFirstDescriptionLabel.text =  "\(response.rilTTR  ?? 0)"
                cell.SecondViewSecondDescriptionLabel.text = response.openTIME
                cell.SecondViewThirdDescriptionLabel.text = response.resolvedTIME
                cell.SecondViewFourthDescriptionLabel.text = response.impactedAPPLICATIONS
            }
            if let response = selectedArray?[indexPath.row] as? IncidentHistoryNonBusiS3 {
                cell.firstViewFirstDescriptionLabel.text = response.incidentno
                cell.firstViewSecondDescriptionLabel.text = response.problemSTATUS
                cell.firstViewThirdDescriptionLabel.text = response.severity
                cell.impactDescriptionLabel.text = response.businessIMPACT
                cell.resolverDescriptionLabel.text = response.assignmentGROUP
                cell.SecondViewFirstDescriptionLabel.text =  "\(response.rilTTR  ?? 0)"
                cell.SecondViewSecondDescriptionLabel.text = response.openTIME
                cell.SecondViewThirdDescriptionLabel.text = response.resolvedTIME
                cell.SecondViewFourthDescriptionLabel.text = response.impactedAPPLICATIONS

            }
            
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (historyResponse != nil) {
            return 3

        }
        else{
            return 0

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (historyResponse != nil) {
             if section == 2 {
                       return selectedArray?.count ?? 0
                       
                   }
                   else
                   {
                       return 1
                   }
        }
        else {
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    internal func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell") as! OutlierTableViewCell
            cell.titleLbl.text = "Open Severity \(selectedSeverity ?? 0) Incident"
            cell.titleLbl.textColor = UIColor.white
            cell.arrowImage.isHidden = true
            cell.countLbl.text = "\(selectedArray?.count ?? 0)"
            cell.countLbl.textColor = UIColor.white
            cell.expandAction.isHidden = true
            cell.backgroundColor = UIColor.init(red: 28, green: 122, blue: 189)
            
            return cell as UIView
        }
        else {
            return  UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            
            return   44
        }
        else{
            return     5
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        15
    }
}



