//
//  NGOIncidentHistoryPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGOIncidentHistoryPresenter:BasePresenter, NGOIncidentHistoryPresenterProtocol, NGOIncidentHistoryInteractorOutputProtocol {
    func requestIncidentHistory(date: String) {
        self.interactor?.requestIncidentHistory(date: date)
    }
    
   
    
    

    // MARK: Variables
    weak var view: NGOIncidentHistoryViewProtocol?
    var interactor: NGOIncidentHistoryInteractorInputProtocol?
    var wireFrame: NGOIncidentHistoryWireFrameProtocol?
    let stringsTableName = "NGOIncidentHistory"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
       func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
//    func requestData()
//    {
//        self.interactor?.requestData()
//    }
    
//func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO)
//{
//    self.wireFrame?.presentOpenAlertsDetailModule(type:type,ngoResponse:ngoResponse)
//
//    }
    
    
    
    func reloadHistoryData(response:IncidentHistoryModel)
    {
        self.view?.reloadHistoryData(response:response)
    }
   
    
   
    
}
