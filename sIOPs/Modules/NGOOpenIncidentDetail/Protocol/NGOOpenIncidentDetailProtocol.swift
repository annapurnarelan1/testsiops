//
//  NGOOpenIncidentDetailProtocol.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol NGOOpenIncidentDetailViewProtocol: class {
    var presenter: NGOOpenIncidentDetailPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
//    func reloadHistoryData(response:IncidentHistoryModel)

  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGOOpenIncidentDetailWireFrameProtocol: class {
    static func presentAlarmDetailModule(fromView:AnyObject,type:String,selected:String,response:Any,applicationDic:LoginApplication)
     static func presentNGOOpenIncidentDetailModule(fromView:AnyObject,type:String,selected:String,response:OpenIncidentModel)
    func presentIncidentHistoryModule(fromView:AnyObject,type:String,selected:String, date:String)

 //  func presentOpenAlertsDetailModule(type:String,infraResponse:InfraModel)
}

/// Method contract between VIEW -> PRESENTER
protocol NGOOpenIncidentDetailPresenterProtocol: class {
    var view: NGOOpenIncidentDetailViewProtocol? { get set }
    var interactor: NGOOpenIncidentDetailInteractorInputProtocol? { get set }
    var wireFrame: NGOOpenIncidentDetailWireFrameProtocol? { get set }
     func presentIncidentHistoryModule(fromView:AnyObject,type:String,selected:String, date:String)
    func requestIncidentHistory(date:String)

  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGOOpenIncidentDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
//    func reloadHistoryData(response:IncidentHistoryModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGOOpenIncidentDetailInteractorInputProtocol: class
{
    var presenter: NGOOpenIncidentDetailInteractorOutputProtocol? { get set }
    func requestIncidentHistory(date:String)
    
}
