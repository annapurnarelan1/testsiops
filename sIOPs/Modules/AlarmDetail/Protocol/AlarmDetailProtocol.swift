//
//  AlarmDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol AlarmDetailViewProtocol: class {
    var presenter: AlarmDetailPresenterProtocol? { get set }
    //func AlarmDetailDone(AlarmDetailRes :AlarmDetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:AlarmDetailModel)
    func reloadHistoryData(detail:SiteDownHistoryModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol AlarmDetailWireFrameProtocol: class {
    static func presentAlarmDetailModule(fromView:AnyObject,alarmOutlierList:AlarmReasonList,applicationCode:String,response:Any,filterSelected:[String:Any])

}

/// Method contract between VIEW -> PRESENTER
protocol AlarmDetailPresenterProtocol: class {
    var view: AlarmDetailViewProtocol? { get set }
    var interactor: AlarmDetailInteractorInputProtocol? { get set }
    var wireFrame: AlarmDetailWireFrameProtocol? { get set }
    
    func requestData(alarmOutlierList:AlarmReasonList,applicationCode:String,filterSelected:[String:Any])
    func requestAlarmDownHistory(sapId:String,id:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol AlarmDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(detail:AlarmDetailModel)
    func reloadHistoryData(detail:SiteDownHistoryModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol AlarmDetailInteractorInputProtocol: class
{
    var presenter: AlarmDetailInteractorOutputProtocol? { get set }
    func requestData(alarmOutlierList:AlarmReasonList,applicationCode:String,filterSelected:[String:Any])
    func requestAlarmDownHistory(sapId:String,id:String)
    
}
