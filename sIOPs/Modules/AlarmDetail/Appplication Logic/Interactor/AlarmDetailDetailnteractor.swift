//
//  AlarmDetailDetailInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class AlarmDetailInteractor: AlarmDetailInteractorInputProtocol {

public enum AlarmDetailError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: AlarmDetailInteractorOutputProtocol?
    
    
    func requestData(alarmOutlierList:AlarmReasonList,applicationCode:String,filterSelected:[String:Any]){
        
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        
        let filter :[String : Any] = [
                   
                   "startAge": (filterSelected["Ageing"] as? Filter)?.startRang ?? "All",
                   "endAge": (filterSelected["Ageing"] as? Filter)?.endRang ?? "All",
                   "startIC":(filterSelected["Impacted customers"] as? Filter)?.startRang ?? "All",
                   "endIC":(filterSelected["Impacted customers"] as? Filter)?.endRang ?? "All",
                   "region": (filterSelected["geography"] as? NSDictionary ?? [:])["Region"] ?? "All",
                   "circle": (filterSelected["geography"] as? NSDictionary ?? [:])["R4G State"] ?? "All",
                   "mp": (filterSelected["geography"] as? NSDictionary ?? [:])["MP"] ?? "All",
                   "jc": (filterSelected["geography"] as? NSDictionary ?? [:])["JC"] ?? "All",
               ]
        
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            
            "appRoleCode": "723",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            "outlierId":alarmOutlierList.id ?? "",
            "filters": filter
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        let busicode =  Constants.BusiCode.UtilityAlarmDetail
        
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                       let AlarmDetaildata = AlarmDetailModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                     self.presenter?.reloadData(detail:AlarmDetaildata)
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
               
                
                


                
               
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestAlarmDownHistory(sapId:String,id:String){
           
           
           
           let pubInfo: [String : Any] =
               ["timestamp": String(Date().ticks),
               "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
               "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               "osType": "ios",
               "lang": "en_US",
               "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
           
           let requestHeader:[String :Any] = [
               "serviceName": "userInfo"
           ]
           
           
           let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               
               "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
               "type": "userInfo",
               "outlierId":id,
               "sapId":sapId
           ]
           
          let busiParams: [String : Any] = [
               "requestHeader": requestHeader,
               "requestBody": requestBody,
           ]
          
        let busicode =  Constants.BusiCode.UtilityAlarmHistory
           
           
           let requestList: [String: Any] = [
               "busiCode": busicode,
               "isEncrypt": false,
               "transactionId": "0001574162779054",
               "busiParams":busiParams
           ]
           
           let jsonParameter = [
               "pubInfo" : pubInfo,
               "requestList" : [requestList]
               ] as [String : Any]
           
           APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
               switch result {
               case .success(let returnJson) :
                   print(returnJson)
                   
                   let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                   
                   switch status {
                   case Constants.status.OK:
                          let siteDownDetaildata = SiteDownHistoryModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                        self.presenter?.reloadHistoryData(detail: siteDownDetaildata)
                     case Constants.status.NOK:
                            let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                           self.presenter?.showFailError(error:message)
                   default:
                       self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                   }
                   
                  
                   
                   


                   
                  
              case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
               }
           })
           
       }
    
}
