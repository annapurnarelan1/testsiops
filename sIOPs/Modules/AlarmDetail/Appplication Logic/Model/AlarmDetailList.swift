//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AlarmDetailList : NSObject, NSCoding{

	var ageing : Double!
	var category : AnyObject!
	var circleName : AnyObject!
	var color : Int!
	var count : AnyObject!
	var iconURI : AnyObject!
	var id : AnyObject!
	var jcName : AnyObject!
	var jobOwner : String!
	var keyCount : AnyObject!
	var keyName : AnyObject!
	var message : AnyObject!
	var mobileNumber : AnyObject!
	var mpName : AnyObject!
	var name : AnyObject!
	var pageNo : Int!
	var priority : Int!
	var query : AnyObject!
	var queryIndex : Int!
	var sapId : String!
	var workOrder : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ageing = dictionary["ageing"] as? Double
		category = dictionary["category"] as? AnyObject
		circleName = dictionary["circleName"] as? AnyObject
		color = dictionary["color"] as? Int
		count = dictionary["count"] as? AnyObject
		iconURI = dictionary["iconURI"] as? AnyObject
		id = dictionary["id"] as? AnyObject
		jcName = dictionary["jcName"] as? AnyObject
		jobOwner = dictionary["jobOwner"] as? String
		keyCount = dictionary["keyCount"] as? AnyObject
		keyName = dictionary["keyName"] as? AnyObject
		message = dictionary["message"] as? AnyObject
		mobileNumber = dictionary["mobileNumber"] as? AnyObject
		mpName = dictionary["mpName"] as? AnyObject
		name = dictionary["name"] as? AnyObject
		pageNo = dictionary["pageNo"] as? Int
		priority = dictionary["priority"] as? Int
		query = dictionary["query"] as? AnyObject
		queryIndex = dictionary["queryIndex"] as? Int
		sapId = dictionary["sapId"] as? String
		workOrder = dictionary["workOrder"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ageing != nil{
			dictionary["ageing"] = ageing
		}
		if category != nil{
			dictionary["category"] = category
		}
		if circleName != nil{
			dictionary["circleName"] = circleName
		}
		if color != nil{
			dictionary["color"] = color
		}
		if count != nil{
			dictionary["count"] = count
		}
		if iconURI != nil{
			dictionary["iconURI"] = iconURI
		}
		if id != nil{
			dictionary["id"] = id
		}
		if jcName != nil{
			dictionary["jcName"] = jcName
		}
		if jobOwner != nil{
			dictionary["jobOwner"] = jobOwner
		}
		if keyCount != nil{
			dictionary["keyCount"] = keyCount
		}
		if keyName != nil{
			dictionary["keyName"] = keyName
		}
		if message != nil{
			dictionary["message"] = message
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if mpName != nil{
			dictionary["mpName"] = mpName
		}
		if name != nil{
			dictionary["name"] = name
		}
		if pageNo != nil{
			dictionary["pageNo"] = pageNo
		}
		if priority != nil{
			dictionary["priority"] = priority
		}
		if query != nil{
			dictionary["query"] = query
		}
		if queryIndex != nil{
			dictionary["queryIndex"] = queryIndex
		}
		if sapId != nil{
			dictionary["sapId"] = sapId
		}
		if workOrder != nil{
			dictionary["workOrder"] = workOrder
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ageing = aDecoder.decodeObject(forKey: "ageing") as? Double
         category = aDecoder.decodeObject(forKey: "category") as? AnyObject
         circleName = aDecoder.decodeObject(forKey: "circleName") as? AnyObject
         color = aDecoder.decodeObject(forKey: "color") as? Int
         count = aDecoder.decodeObject(forKey: "count") as? AnyObject
         iconURI = aDecoder.decodeObject(forKey: "iconURI") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? AnyObject
         jcName = aDecoder.decodeObject(forKey: "jcName") as? AnyObject
         jobOwner = aDecoder.decodeObject(forKey: "jobOwner") as? String
         keyCount = aDecoder.decodeObject(forKey: "keyCount") as? AnyObject
         keyName = aDecoder.decodeObject(forKey: "keyName") as? AnyObject
         message = aDecoder.decodeObject(forKey: "message") as? AnyObject
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? AnyObject
         mpName = aDecoder.decodeObject(forKey: "mpName") as? AnyObject
         name = aDecoder.decodeObject(forKey: "name") as? AnyObject
         pageNo = aDecoder.decodeObject(forKey: "pageNo") as? Int
         priority = aDecoder.decodeObject(forKey: "priority") as? Int
         query = aDecoder.decodeObject(forKey: "query") as? AnyObject
         queryIndex = aDecoder.decodeObject(forKey: "queryIndex") as? Int
         sapId = aDecoder.decodeObject(forKey: "sapId") as? String
         workOrder = aDecoder.decodeObject(forKey: "workOrder") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ageing != nil{
			aCoder.encode(ageing, forKey: "ageing")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if circleName != nil{
			aCoder.encode(circleName, forKey: "circleName")
		}
		if color != nil{
			aCoder.encode(color, forKey: "color")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if iconURI != nil{
			aCoder.encode(iconURI, forKey: "iconURI")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if jcName != nil{
			aCoder.encode(jcName, forKey: "jcName")
		}
		if jobOwner != nil{
			aCoder.encode(jobOwner, forKey: "jobOwner")
		}
		if keyCount != nil{
			aCoder.encode(keyCount, forKey: "keyCount")
		}
		if keyName != nil{
			aCoder.encode(keyName, forKey: "keyName")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if mpName != nil{
			aCoder.encode(mpName, forKey: "mpName")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if pageNo != nil{
			aCoder.encode(pageNo, forKey: "pageNo")
		}
		if priority != nil{
			aCoder.encode(priority, forKey: "priority")
		}
		if query != nil{
			aCoder.encode(query, forKey: "query")
		}
		if queryIndex != nil{
			aCoder.encode(queryIndex, forKey: "queryIndex")
		}
		if sapId != nil{
			aCoder.encode(sapId, forKey: "sapId")
		}
		if workOrder != nil{
			aCoder.encode(workOrder, forKey: "workOrder")
		}

	}

}
