//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AlarmDetailModel : NSObject, NSCoding{

	var list : [AlarmDetailList]!
	var listOfMap : AnyObject!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		list = [AlarmDetailList]()
		if let listArray = dictionary["list"] as? [[String:Any]]{
			for dic in listArray{
				let value = AlarmDetailList(fromDictionary: dic)
				list.append(value)
			}
		}
		listOfMap = dictionary["listOfMap"] as? AnyObject
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if listOfMap != nil{
			dictionary["listOfMap"] = listOfMap
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey :"list") as? [AlarmDetailList]
         listOfMap = aDecoder.decodeObject(forKey: "listOfMap") as? AnyObject
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if listOfMap != nil{
			aCoder.encode(listOfMap, forKey: "listOfMap")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
