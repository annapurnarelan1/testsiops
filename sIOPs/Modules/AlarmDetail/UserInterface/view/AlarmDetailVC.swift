//
//  AlarmDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class AlarmDetailVC: BaseViewController,AlarmDetailViewProtocol,UIPopoverPresentationControllerDelegate {
    
    var presenter: AlarmDetailPresenterProtocol?
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var type:String?
    var selected:String?
    var response:Any?
    var applicationCode:String?
    var alamreasonObject : AlarmReasonList?
    var selectedArray:[AlarmDetailList]?
    var count : String?
    var senderSelectedIndex:Int?
    var detailList:[SiteDownHistoryList]?
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    var selectedFilter:[String:Any]?
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> AlarmDetailViewProtocol{
        return UIStoryboard(name: "AlarmDetail", bundle: nil).instantiateViewController(withIdentifier: "AlarmDetailVC") as! AlarmDetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "AlarmTableViewCell", bundle: nil), forCellReuseIdentifier: "AlarmTableViewCell")
        
        titleLbl.text = alamreasonObject?.name
        countLbl.text = alamreasonObject?.count
        
        
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No  detail Available")
        
        startAnimating()
        self.presenter?.requestData(alarmOutlierList:alamreasonObject!,applicationCode:applicationCode ?? "0",filterSelected:selectedFilter ?? [:])
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(detail:AlarmDetailModel)
    {
        stopAnimating()
        self.selectedArray = detail.list
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No  detail Available")
        self.tableView.reloadData()
    }
    
    func reloadHistoryData(detail:SiteDownHistoryModel)
    {
        stopAnimating()
        
        self.detailList = detail.list
        self.tableView.reloadData()
        self.view.setNeedsLayout()
    }
    
    @objc func makeCall(_ sender:UIButton)
    {
        self.showFailError(error: "Coming soon")
        
    }
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        
        
        if(sender.tag != senderSelectedIndex)
        {
            senderSelectedIndex = sender.tag
            startAnimating()
            self.presenter?.requestAlarmDownHistory(sapId: selectedArray?[sender.tag].sapId ?? "", id: alamreasonObject?.id ?? "")
        }
        else
        {
            senderSelectedIndex = -1
            self.tableView.reloadData()
        }
        
        
        
        
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    
    @objc func showAgeiingInfo(_ sender: UIButton)
    {
        let popOverVC = UIStoryboard(name: "AgeingPopUp", bundle: nil).instantiateViewController(withIdentifier: "AgeingPopup") as! AgeingPopupViewController
        
        popOverVC.modalPresentationStyle = .popover
        let popover =  popOverVC.popoverPresentationController
        popover?.delegate = self
        //sourceRect and sourceView is required in case of iPad
        popover?.sourceRect = sender.bounds //give anchor frame
        popover?.sourceView = sender //give anchor view
        popOverVC.preferredContentSize = CGSize(width: 150 , height: 100)
        
        self.present(popOverVC, animated: true)
    }
    
    
}

extension AlarmDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AlarmTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlarmTableViewCell", for: indexPath as IndexPath) as! AlarmTableViewCell
        cell.idLbl.text = "Site ID: \(selectedArray?[indexPath.section].sapId ?? "")"
        cell.ageingHeadingLbl.text = "\(selectedArray?[indexPath.section].ageing ?? 0.00)"
        
        if( (selectedArray?[indexPath.section].ageing ?? 0.00) > 0.00 &&  (selectedArray?[indexPath.section].ageing ?? 0.00) < 2.00)
        {
            //cell.ageingHeadingLbl.textColor = UIColor.init(hexString: "#D8B600")
            cell.ageingHeadingLbl.textColor = UIColor.init(hexString: "#D8B600")
        }
        else if( (selectedArray?[indexPath.section].ageing ?? 0.00) < 5.00 &&   (selectedArray?[indexPath.section].ageing ?? 0.00) > 2.00)
        {
            cell.ageingHeadingLbl.textColor = UIColor.init(hexString: "#FF9E00")
        }
        else
        {
            cell.ageingHeadingLbl.textColor = UIColor.init(hexString: "#FF3B30")
        }
        
        
        cell.nameLbl.text = selectedArray?[indexPath.section].jobOwner ?? ""
        cell.acknolowledOnLbl.text = selectedArray?[indexPath.section].workOrder ?? ""
        cell.ranLbl.text = "Energy"
        cell.phone.addTarget(self, action: #selector(makeCall(_:)), for: .touchUpInside)
        cell.alertBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        cell.alertBtn.tag =  indexPath.section
        
        
        cell.ageingInfoBtn.addTarget(self, action: #selector(showAgeiingInfo(_:)), for: .touchUpInside)
        
        if(senderSelectedIndex ==  cell.alertBtn.tag )
        {
            
            let buttonTitleStr = NSMutableAttributedString(string:"Hide History", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = false
            cell.dataArray = self.detailList
            cell.dayTblView.reloadData()
        }
        else
        {
            let buttonTitleStr = NSMutableAttributedString(string:"View History", attributes:attrs)
            let attributedString = NSMutableAttributedString(string:"")
            attributedString.append(buttonTitleStr)
            cell.alertBtn.setAttributedTitle(attributedString, for: .normal)
            cell.textView.isHidden = true
            
        }
        
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return selectedArray?.count ?? 0
        return self.selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        UITableView.automaticDimension
    //    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if  let cell = tableView.cellForRow(at: indexPath)  as? AlarmTableViewCell
        {
            cell.heightConstraint.constant = cell.dayTblView.contentSize.height
            cell.dayTblView.layoutIfNeeded()
        }
        
        //        viewheightConstraint.constant = self.dayTblView.contentSize.height + 20
        //        self.dayTblView.layoutIfNeeded()
    }
    
    
    
}
