//
//  AlarmDetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class AlarmDetailWireFrame: AlarmDetailWireFrameProtocol {
    
    
    
    // MARK: AlarmDetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentAlarmDetailModule(fromView:AnyObject,alarmOutlierList:AlarmReasonList,applicationCode:String,response:Any,filterSelected:[String:Any]) {

        // Generating module components
        let view: AlarmDetailViewProtocol = AlarmDetailVC.instantiate()
        let presenter: AlarmDetailPresenterProtocol & AlarmDetailInteractorOutputProtocol = AlarmDetailPresenter()
        let interactor: AlarmDetailInteractorInputProtocol = AlarmDetailInteractor()
       
        let wireFrame: AlarmDetailWireFrameProtocol = AlarmDetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
       filterSelected
        let viewController = view as! AlarmDetailVC
        viewController.alamreasonObject = alarmOutlierList
        viewController.applicationCode = applicationCode
        viewController.response =  response
        viewController.selectedFilter = filterSelected ?? [:]
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
