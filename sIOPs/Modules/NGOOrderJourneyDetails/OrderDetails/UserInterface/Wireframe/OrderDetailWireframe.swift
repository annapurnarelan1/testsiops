//
//  NGODetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OrderDetailWireFrame: OrderDetailWireFrameProtocol {
    
    // MARK: OrderJourneyDetaillWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOrderJourneyDetailModule(fromView:AnyObject) {

        // Generating module components
        let view: OrderDetailProtocol = OrderDetailVC.instantiate()
        let presenter: OrderDetailPresenterProtocol & OrderDetailInteractorOutputProtocol = OrderDetailPresenter()
        let interactor: OrderDetailInteractorInputProtocol = OrderDetailServiceInteractor()
       
        let wireFrame: OrderDetailWireFrameProtocol = OrderDetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OrderDetailVC
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
    
    func presentOrderRegionModule(allChannelsDetail:AllChannel,date:String) {
        
       // OrderRegionWireFrame.presentOrderRegionModule(fromView: self, type: "", selected: "")
        OrderRegionWireFrame.presentOrderRegionModule(fromView:self,allChannelsDetail:allChannelsDetail,date:date)

    }
}
