//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class OrderDetailVC: BaseViewController,OrderDetailProtocol,FSCalendarDataSource, FSCalendarDelegate {
    
    
    var presenter: OrderDetailPresenterProtocol?
    
    @IBOutlet weak var cnstHeightCalender: NSLayoutConstraint!
    @IBOutlet weak var tblOrderDetails: UITableView!

    @IBOutlet weak var regionBtn: UIButton!
    @IBOutlet weak var calendarView: FSCalendar!

   
    
    var type:String?
    var selected:String?
    var NGOResponse:NGO?
    var selectedArray:[NGOApplication]?
    var senderSelectedIndex:Int?
    var typeSelected :String?
    var senderSelectedTypeIndex:Int?
    var senderSelectedRejected:Int?
    var orderDetail :OrderDetailModel?
    var selectedDate:String?
    let date = Date()
    let formatter = DateFormatter()
    var inprocessDetail :O2AinProcessModel?
    var rejectedDetail :O2ARejectedModel?
    var attrs = [
     
       NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView, action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    static func instantiate() -> OrderDetailProtocol{
        return UIStoryboard(name: "OrderDetailVC", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblOrderDetails.register(UINib(nibName: "OrderJourneyDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderJourneyDetailTableViewCell")
        formatter.dateFormat = "yyyy-MM-dd"
        if UIDevice.current.model.hasPrefix("iPad") {
            self.cnstHeightCalender.constant = 400
        }

        self.calendarView.select(Date())

        self.view.addGestureRecognizer(self.scopeGesture)
        self.tblOrderDetails.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendarView.scope = .week

        // For UITest
        self.calendarView.accessibilityIdentifier = "calendar"
        
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
        selectedDate = formatter.string(from: date)
        startAnimating()
        self.presenter?.requestData(date: formatter.string(from: date))
    }
    
    @IBAction func moveToRegion(_ sender: Any) {
        
        self.presenter?.presentOrderRegionModule(allChannelsDetail:orderDetail!.allChannels, date:selectedDate ?? "")
    }
    
    
    @objc func expandDetailView(_ sender:UIButton)
       {
        
        senderSelectedTypeIndex = -1
        senderSelectedRejected = -1
        if(sender.tag != senderSelectedIndex)
           {
               senderSelectedIndex = sender.tag
            
           }
           else
           {
            let cell = tblOrderDetails.cellForRow(at: IndexPath(row: 0, section: sender.tag)) as? OrderJourneyDetailTableViewCell
          
               senderSelectedIndex = -1
               
           }
        self.tblOrderDetails.reloadData()
        self.view.setNeedsLayout()
       }
    
    @objc func rejectedBtnAction(_ sender:UIButton) {
        senderSelectedTypeIndex = -1
        typeSelected = "rejected"
        if(sender.tag != senderSelectedRejected)
           {
             senderSelectedRejected = sender.tag
            var channel : String = ""
                   if(senderSelectedRejected == 0)
                   {
                       channel = "ALL"
                   }
                   else if(senderSelectedRejected == 1)
                   {
                       channel = "GT"
                   }
                   else
                   {
                       channel = "RR"
                   }
            startAnimating()
            self.presenter?.requestInRejectedData(date: selectedDate ?? "", channel: channel)
              
            
           }
           else
           {
               senderSelectedRejected = -1
            self.tblOrderDetails.reloadData()
                   self.view.setNeedsLayout()
               
           }
       
    }
    @objc func inProgressBtnAction(_ sender:UIButton) {
       senderSelectedRejected = -1
        typeSelected = "inProgress"
        if(sender.tag != senderSelectedTypeIndex)
           {
               senderSelectedTypeIndex = sender.tag
            var channel : String = ""
                   if(senderSelectedTypeIndex == 0)
                   {
                       channel = "ALL"
                   }
                   else if(senderSelectedTypeIndex == 1)
                   {
                       channel = "GT"
                   }
                   else
                   {
                       channel = "RR"
                   }
            startAnimating()
            self.presenter?.requestInProcessData(date: selectedDate ?? "", channel: channel)
            
           }
           else
           {
               senderSelectedTypeIndex = -1
               self.tblOrderDetails.reloadData()
               self.view.setNeedsLayout()
           }
        
        
        
       
    }
    
    func stopLoader() {
        stopAnimating()
    }
    func reloadData(orderDetailResponse: OrderDetailModel) {
        stopAnimating()
        regionBtn.isEnabled = true
        orderDetail = orderDetailResponse
        self.tblOrderDetails.reloadData()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    
    func gotInProcess(detail:O2AinProcessModel)
    {
        stopAnimating()
        inprocessDetail = detail
        self.tblOrderDetails.reloadData()
        self.view.setNeedsLayout()
        
    }
    
    func gotRejected(detail:O2ARejectedModel)
    {
        stopAnimating()
        rejectedDetail = detail
        self.tblOrderDetails.reloadData()
        self.view.setNeedsLayout()
    }
    
    
    // MARK:- UIGestureRecognizerDelegate

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tblOrderDetails.contentOffset.y <= -self.tblOrderDetails.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendarView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }

    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.cnstHeightCalender.constant = bounds.height
        self.view.layoutIfNeeded()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedDate = "\(self.dateFormatter.string(from: date))"
        print("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
            
        }
        startAnimating()
        self.presenter?.requestData(date: selectedDate ?? "")
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        let fromDate = Calendar.current.date(byAdding: .day, value: -30, to: Date())
        return self.dateFormatter.date(from: self.dateFormatter.string(from: fromDate!))!
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return self.dateFormatter.date(from: self.dateFormatter.string(from:Date()))!
    }
}



extension OrderDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:OrderJourneyDetailTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "OrderJourneyDetailTableViewCell", for: indexPath as IndexPath) as? OrderJourneyDetailTableViewCell)!
        cell.viewdetailBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        cell.viewdetailBtn.tag = indexPath.section
        cell.rejectedBtn.tag = indexPath.section
        cell.inProgressBtn.tag = indexPath.section
        cell.rejectedBtn.addTarget(self, action: #selector(rejectedBtnAction(_:)), for: .touchUpInside)
        cell.inProgressBtn.addTarget(self, action: #selector(inProgressBtnAction(_:)), for: .touchUpInside)
        cell.detailView.isHidden = true
        if(senderSelectedIndex ==  cell.viewdetailBtn.tag )
        {
            
            cell.viewdetailBtn.backgroundColor = UIColor.strongBlueColor
            cell.viewdetailBtn.setTitleColor(UIColor.white, for: .normal)
            cell.viewdetailBtn.setTitle("Hide Details", for: .normal)
            cell.secondView.isHidden = false
        }
        else
        {
           
            cell.secondView.isHidden = true
             
             
            cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
            cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
            cell.viewdetailBtn.setTitle("View Details", for: .normal)
            
            
            self.view.setNeedsLayout()
        }
        if(typeSelected == "rejected")
        {
            
            if(senderSelectedRejected ==  cell.rejectedBtn.tag)
            {
                 cell.detailFifthView.isHidden = false
                cell.detailtitleLabel.text = "Rejected"
                cell.detailFirstOutlierName.text = "De Dupe"
                cell.detailSecondOutlierName.text = "De Dupe Sys"
                cell.detailThirdOutlierName.text = "AO"
                cell.detailForthOutlierName.text = "LR Check"
                cell.detailFifthOutlierName.text = "Cancelled"
                cell.detailFirstOutLierCount.text = "\(rejectedDetail?.deDupe ?? 0)"
                cell.detailSecondOutLierCount.text = "\(rejectedDetail?.deDupeSys ?? 0)"
                cell.detailThirdOutLierCount.text = "\(rejectedDetail?.ao ?? 0)"
                cell.detailForthOutLierCount.text = "\(rejectedDetail?.lrCheck ?? 0)"
                cell.detailFifthOutLierCount.text = "\(rejectedDetail?.cancelled ?? 0)"
                
                
                cell.detailView.isHidden = false
            }
            else
            {
               // cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
                //cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
               // cell.viewdetailBtn.setTitle("View Details", for: .normal)
               
                cell.detailView.isHidden = true
               // cell.detailViewHeight.constant = 0
            }
            
            
        }
        else
        {
            if(senderSelectedTypeIndex ==  cell.inProgressBtn.tag )
            {
                cell.detailFifthView.isHidden = true
                cell.detailtitleLabel.text = "In Process"
                cell.detailFirstOutlierName.text = "CAF Scanning"
                cell.detailSecondOutlierName.text = "In Transit"
                cell.detailThirdOutlierName.text = "In Approval"
                cell.detailForthOutlierName.text = "LR Check"
                cell.detailFirstOutLierCount.text = "\(inprocessDetail?.cafScanning ?? 0)"
                cell.detailSecondOutLierCount.text = "\(inprocessDetail?.inTransit ?? 0)"
                cell.detailThirdOutLierCount.text = "\(inprocessDetail?.inApproval ?? 0)"
                cell.detailForthOutLierCount.text = "\(inprocessDetail?.lrCheck ?? 0)"
                
                
                cell.detailView.isHidden = false
            }
            else
            {
               // cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
                //cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
               // cell.viewdetailBtn.setTitle("View Details", for: .normal)
               
                cell.detailView.isHidden = true
               // cell.detailViewHeight.constant = 0
            }
            
        }
        
        switch indexPath.section {
        case 0:
            cell.headerLabel.text = orderDetail?.allChannels.zone ?? ""
            cell.firstOutlIerName.text = "Entered"
            cell.firstOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.allChannels.entered ?? 0)")
            cell.secondOutlIerName.text = "Activated"
            cell.secondOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.allChannels.activated ?? 0)")
            cell.thirdOutlIerName.text = "Network Latched"
            cell.thirdOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.allChannels.networkLatched ?? 0)")
            cell.rejectedName.attributedText = NSMutableAttributedString(string: "Rejected", attributes:attrs)
           
            cell.rejectedCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.allChannels.rejected ?? 0)")
            cell.inProgressName.attributedText = NSMutableAttributedString(string: "In Process", attributes:attrs)
            cell.inProgressCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.allChannels.inProcess ?? 0)")
            cell.tvPendingName.text = "TV Pending"
            cell.tvPendingCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.allChannels.tvPending ?? 0)")
        case 1:
            cell.headerLabel.text = orderDetail?.generalTrade.zone ?? ""
            cell.firstOutlIerName.text = "Entered"
            cell.firstOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.generalTrade.entered ?? 0)")
            cell.secondOutlIerName.text = "Activated"
            cell.secondOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.generalTrade.activated ?? 0)")
            cell.thirdOutlIerName.text = "Network Latched"
            cell.thirdOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.generalTrade.networkLatched ?? 0)")
            cell.rejectedName.attributedText = NSMutableAttributedString(string: "Rejected", attributes:attrs)
            cell.rejectedCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.generalTrade.rejected ?? 0)")
             cell.inProgressName.attributedText = NSMutableAttributedString(string: "In Process", attributes:attrs)
            cell.inProgressCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.generalTrade.inProcess ?? 0)")
            cell.tvPendingName.text = "TV Pending"
            cell.tvPendingCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.generalTrade.tvPending ?? 0)")
        case 2:
            cell.headerLabel.text = orderDetail?.relianceRetail.zone ?? ""
            cell.firstOutlIerName.text = "Entered"
            cell.firstOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.relianceRetail.entered ?? 0)")
            cell.secondOutlIerName.text = "Activated"
            cell.secondOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.relianceRetail.activated ?? 0)")
            cell.thirdOutlIerName.text = "Network Latched"
            cell.thirdOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.relianceRetail.networkLatched ?? 0)")
            cell.rejectedName.attributedText = NSMutableAttributedString(string: "Rejected", attributes:attrs)
            cell.rejectedCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.relianceRetail.rejected ?? 0)")
             cell.inProgressName.attributedText = NSMutableAttributedString(string: "In Process", attributes:attrs)
            cell.inProgressCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.relianceRetail.inProcess ?? 0)")
            cell.tvPendingName.text = "TV Pending"
            cell.tvPendingCount.text = HelperMethods().nuumberFormatting(value:"\(orderDetail?.relianceRetail.tvPending ?? 0)")
            
        default:
            print("completed")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(orderDetail != nil)
        {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        UITableView.automaticDimension
//    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
       self.view.setNeedsLayout()


    }
}
