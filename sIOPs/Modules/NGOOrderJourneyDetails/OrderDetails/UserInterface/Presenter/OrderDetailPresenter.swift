//
//  OrderDetailPresenter.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OrderDetailPresenter:BasePresenter, OrderDetailPresenterProtocol, OrderDetailInteractorOutputProtocol {
    
    
    
    
    
    // MARK: Variables
    weak var view: OrderDetailProtocol?
    var interactor: OrderDetailInteractorInputProtocol?
    var wireFrame: OrderDetailWireFrameProtocol?
    let stringsTableName = "OrderDetail"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    func reloadData(orderDetailResponse: OrderDetailModel) {
        self.view?.reloadData(orderDetailResponse: orderDetailResponse)
    }
    
    func requestData(date:String)
    {
        self.interactor?.requestData(date:date)
    }
    
    func presentOrderRegionModule(allChannelsDetail:AllChannel,date:String)
    {
        self.wireFrame?.presentOrderRegionModule(allChannelsDetail:allChannelsDetail,date:date)

    }
    
    func requestInProcessData(date:String,channel:String)
    {
        self.interactor?.requestInProcessData(date:date,channel:channel)
    }
    
    func gotInProcess(detail:O2AinProcessModel)
    {
        self.view?.gotInProcess(detail: detail)
    }
    func requestInRejectedData(date:String,channel:String)
    {
        self.interactor?.requestInRejectedData(date:date,channel:channel)
    }
    
    func gotRejected(detail:O2ARejectedModel)
    {
        self.view?.gotRejected(detail:detail)
    }
    
    
}
