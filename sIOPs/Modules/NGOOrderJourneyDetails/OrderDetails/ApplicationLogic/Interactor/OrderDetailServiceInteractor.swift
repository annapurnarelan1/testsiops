//
//  NGOBusinessServiceInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class OrderDetailServiceInteractor: OrderDetailInteractorInputProtocol {
    
    public enum OrderDetailError {
        case internetError(String)
        case serverMessage(String)
    }
    
    // MARK: Variables
    weak var presenter: OrderDetailInteractorOutputProtocol?
    
    
    
    func requestData(date:String){
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            "date":date
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        let busicode = Constants.BusiCode.O2ASummary
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let detail = OrderDetailModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                    self.presenter?.reloadData(orderDetailResponse: detail)
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    func requestInProcessData(date:String,channel:String)
    {
        let pubInfo: [String : Any] =
             ["timestamp": String(Date().ticks),
              "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
              "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
              "osType": "ios",
              "lang": "en_US",
              "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
         
         let requestHeader:[String :Any] = [
             "serviceName": "userInfo"
         ]
         
         let requestBody:[String : Any] = [
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "channel":channel,
             "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
             "type": "userInfo",
             "date":date
             
         ]
         
        let busiParams: [String : Any] = [
             "requestHeader": requestHeader,
             "requestBody": requestBody,
         ]
         
         let busicode = Constants.BusiCode.O2AInProcess
         
         let requestList: [String: Any] = [
             "busiCode": busicode,
             "isEncrypt": false,
             "transactionId": "0001574162779054",
             "busiParams":busiParams
         ]
         let jsonParameter = [
             "pubInfo" : pubInfo,
             "requestList" : [requestList]
             ] as [String : Any]
         
         APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
             switch result {
             case .success(let returnJson) :
                 print(returnJson)
                 
                 let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                 
                 switch status {
                 case Constants.status.OK:
                     let detail = O2AinProcessModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                     self.presenter?.gotInProcess(detail:detail)
                 case Constants.status.NOK:
                     let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                     self.presenter?.showFailError(error:message)
                 default:
                     self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                 }
                 
             case .failure(let failure) :
                 switch failure {
                 case .connectionError:
                     self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                     
                 case .authorizationError(let errorJson):
                     self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                 case .serverError:
                     self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                 case .newUpdate:
                     self.presenter?.stopLoader()
                 default:
                     self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                     
                 }
             }
         })
    }
    
    
    func requestInRejectedData(date:String,channel:String)
    {
        let pubInfo: [String : Any] =
             ["timestamp": String(Date().ticks),
              "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
              "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
              "osType": "ios",
              "lang": "en_US",
              "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
         
         let requestHeader:[String :Any] = [
             "serviceName": "userInfo"
         ]
         
         let requestBody:[String : Any] = [
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "channel":channel,
             "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
             "type": "userInfo",
             "date":date
             
         ]
         
        let busiParams: [String : Any] = [
             "requestHeader": requestHeader,
             "requestBody": requestBody,
         ]
         
         let busicode = Constants.BusiCode.O2ARejected
         
         let requestList: [String: Any] = [
             "busiCode": busicode,
             "isEncrypt": false,
             "transactionId": "0001574162779054",
             "busiParams":busiParams
         ]
         let jsonParameter = [
             "pubInfo" : pubInfo,
             "requestList" : [requestList]
             ] as [String : Any]
         
         APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
             switch result {
             case .success(let returnJson) :
                 print(returnJson)
                 
                 let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                 
                 switch status {
                 case Constants.status.OK:
                     let detail = O2ARejectedModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                     self.presenter?.gotRejected(detail:detail)
                 case Constants.status.NOK:
                     let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                     self.presenter?.showFailError(error:message)
                 default:
                     self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                 }
                 
             case .failure(let failure) :
                 switch failure {
                 case .connectionError:
                     self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                     
                 case .authorizationError(let errorJson):
                     self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                 case .serverError:
                     self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                 case .newUpdate:
                     self.presenter?.stopLoader()
                 default:
                     self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                     
                 }
             }
         })
    }
}


