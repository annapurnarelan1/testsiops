//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class O2ARejectedModel : NSObject, NSCoding{

	var ao : Int!
	var cancelled : Int!
	var deDupe : Int!
	var deDupeSys : Int!
	var lrCheck : Int!
	var region : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ao = dictionary["ao"] as? Int
		cancelled = dictionary["cancelled"] as? Int
		deDupe = dictionary["deDupe"] as? Int
		deDupeSys = dictionary["deDupeSys"] as? Int
		lrCheck = dictionary["lrCheck"] as? Int
		region = dictionary["region"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ao != nil{
			dictionary["ao"] = ao
		}
		if cancelled != nil{
			dictionary["cancelled"] = cancelled
		}
		if deDupe != nil{
			dictionary["deDupe"] = deDupe
		}
		if deDupeSys != nil{
			dictionary["deDupeSys"] = deDupeSys
		}
		if lrCheck != nil{
			dictionary["lrCheck"] = lrCheck
		}
		if region != nil{
			dictionary["region"] = region
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ao = aDecoder.decodeObject(forKey: "ao") as? Int
         cancelled = aDecoder.decodeObject(forKey: "cancelled") as? Int
         deDupe = aDecoder.decodeObject(forKey: "deDupe") as? Int
         deDupeSys = aDecoder.decodeObject(forKey: "deDupeSys") as? Int
         lrCheck = aDecoder.decodeObject(forKey: "lrCheck") as? Int
         region = aDecoder.decodeObject(forKey: "region") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ao != nil{
			aCoder.encode(ao, forKey: "ao")
		}
		if cancelled != nil{
			aCoder.encode(cancelled, forKey: "cancelled")
		}
		if deDupe != nil{
			aCoder.encode(deDupe, forKey: "deDupe")
		}
		if deDupeSys != nil{
			aCoder.encode(deDupeSys, forKey: "deDupeSys")
		}
		if lrCheck != nil{
			aCoder.encode(lrCheck, forKey: "lrCheck")
		}
		if region != nil{
			aCoder.encode(region, forKey: "region")
		}

	}

}
