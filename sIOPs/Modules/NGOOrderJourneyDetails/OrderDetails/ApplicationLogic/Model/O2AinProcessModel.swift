//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class O2AinProcessModel : NSObject, NSCoding{

	var cafScanning : Int!
	var inApproval : Int!
	var inTransit : Int!
	var lrCheck : Int!
	var region : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		cafScanning = dictionary["cafScanning"] as? Int
		inApproval = dictionary["inApproval"] as? Int
		inTransit = dictionary["inTransit"] as? Int
		lrCheck = dictionary["lrCheck"] as? Int
		region = dictionary["region"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cafScanning != nil{
			dictionary["cafScanning"] = cafScanning
		}
		if inApproval != nil{
			dictionary["inApproval"] = inApproval
		}
		if inTransit != nil{
			dictionary["inTransit"] = inTransit
		}
		if lrCheck != nil{
			dictionary["lrCheck"] = lrCheck
		}
		if region != nil{
			dictionary["region"] = region
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cafScanning = aDecoder.decodeObject(forKey: "cafScanning") as? Int
         inApproval = aDecoder.decodeObject(forKey: "inApproval") as? Int
         inTransit = aDecoder.decodeObject(forKey: "inTransit") as? Int
         lrCheck = aDecoder.decodeObject(forKey: "lrCheck") as? Int
         region = aDecoder.decodeObject(forKey: "region") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cafScanning != nil{
			aCoder.encode(cafScanning, forKey: "cafScanning")
		}
		if inApproval != nil{
			aCoder.encode(inApproval, forKey: "inApproval")
		}
		if inTransit != nil{
			aCoder.encode(inTransit, forKey: "inTransit")
		}
		if lrCheck != nil{
			aCoder.encode(lrCheck, forKey: "lrCheck")
		}
		if region != nil{
			aCoder.encode(region, forKey: "region")
		}

	}

}
