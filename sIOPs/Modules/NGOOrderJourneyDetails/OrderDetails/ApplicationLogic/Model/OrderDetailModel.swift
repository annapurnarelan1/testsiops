//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderDetailModel : NSObject, NSCoding{

    var allChannels : AllChannel!
    var generalTrade : AllChannel!
    var relianceRetail : AllChannel!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let allChannelsData = dictionary["allChannels"] as? [String:Any]{
            allChannels = AllChannel(fromDictionary: allChannelsData)
        }
        if let generalTradeData = dictionary["generalTrade"] as? [String:Any]{
            generalTrade = AllChannel(fromDictionary: generalTradeData)
        }
        if let relianceRetailData = dictionary["relianceRetail"] as? [String:Any]{
            relianceRetail = AllChannel(fromDictionary: relianceRetailData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if allChannels != nil{
            dictionary["allChannels"] = allChannels.toDictionary()
        }
        if generalTrade != nil{
            dictionary["generalTrade"] = generalTrade.toDictionary()
        }
        if relianceRetail != nil{
            dictionary["relianceRetail"] = relianceRetail.toDictionary()
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         allChannels = aDecoder.decodeObject(forKey: "allChannels") as? AllChannel
         generalTrade = aDecoder.decodeObject(forKey: "generalTrade") as? AllChannel
         relianceRetail = aDecoder.decodeObject(forKey: "relianceRetail") as? AllChannel

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if allChannels != nil{
            aCoder.encode(allChannels, forKey: "allChannels")
        }
        if generalTrade != nil{
            aCoder.encode(generalTrade, forKey: "generalTrade")
        }
        if relianceRetail != nil{
            aCoder.encode(relianceRetail, forKey: "relianceRetail")
        }

    }

}
