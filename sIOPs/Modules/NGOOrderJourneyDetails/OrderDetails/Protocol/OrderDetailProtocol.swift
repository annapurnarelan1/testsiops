//
//  RANProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OrderDetailProtocol: class {
    var presenter: OrderDetailPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
     func reloadData(orderDetailResponse:OrderDetailModel)
    func showFailError(error:String)
    func gotInProcess(detail:O2AinProcessModel)
    func gotRejected(detail:O2ARejectedModel)
   
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OrderDetailWireFrameProtocol: class {
    static func presentOrderJourneyDetailModule(fromView:AnyObject)
    func presentOrderRegionModule(allChannelsDetail:AllChannel,date:String)

}

/// Method contract between VIEW -> PRESENTER
protocol OrderDetailPresenterProtocol: class {
    var view: OrderDetailProtocol? { get set }
    var interactor: OrderDetailInteractorInputProtocol? { get set }
    var wireFrame: OrderDetailWireFrameProtocol? { get set }
    func showFailError(error:String)
   
    func presentOrderRegionModule(allChannelsDetail:AllChannel,date:String)

    func requestData(date:String)
    func requestInProcessData(date:String,channel:String)
    func requestInRejectedData(date:String,channel:String)
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OrderDetailInteractorOutputProtocol: class {
    
    func show(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadData(orderDetailResponse:OrderDetailModel)
    func gotInProcess(detail:O2AinProcessModel)
    func gotRejected(detail:O2ARejectedModel)
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol OrderDetailInteractorInputProtocol: class
{
    var presenter: OrderDetailInteractorOutputProtocol? { get set }
    func requestData(date:String)
    func requestInProcessData(date:String,channel:String)
     func requestInRejectedData(date:String,channel:String)
}
