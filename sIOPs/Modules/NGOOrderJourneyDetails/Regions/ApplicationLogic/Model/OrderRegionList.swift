//
//	List.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderRegionList : NSObject, NSCoding{

	var activated : Int!
	var entered : Int!
	var inProcess : Int!
	var networkLatched : Int!
	var rejected : Int!
	var tvPending : Int!
	var zone : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		activated = dictionary["activated"] as? Int
		entered = dictionary["entered"] as? Int
		inProcess = dictionary["in_process"] as? Int
		networkLatched = dictionary["network_latched"] as? Int
		rejected = dictionary["rejected"] as? Int
		tvPending = dictionary["tv_pending"] as? Int
		zone = dictionary["zone"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if activated != nil{
			dictionary["activated"] = activated
		}
		if entered != nil{
			dictionary["entered"] = entered
		}
		if inProcess != nil{
			dictionary["in_process"] = inProcess
		}
		if networkLatched != nil{
			dictionary["network_latched"] = networkLatched
		}
		if rejected != nil{
			dictionary["rejected"] = rejected
		}
		if tvPending != nil{
			dictionary["tv_pending"] = tvPending
		}
		if zone != nil{
			dictionary["zone"] = zone
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         activated = aDecoder.decodeObject(forKey: "activated") as? Int
         entered = aDecoder.decodeObject(forKey: "entered") as? Int
         inProcess = aDecoder.decodeObject(forKey: "in_process") as? Int
         networkLatched = aDecoder.decodeObject(forKey: "network_latched") as? Int
         rejected = aDecoder.decodeObject(forKey: "rejected") as? Int
         tvPending = aDecoder.decodeObject(forKey: "tv_pending") as? Int
         zone = aDecoder.decodeObject(forKey: "zone") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if activated != nil{
			aCoder.encode(activated, forKey: "activated")
		}
		if entered != nil{
			aCoder.encode(entered, forKey: "entered")
		}
		if inProcess != nil{
			aCoder.encode(inProcess, forKey: "in_process")
		}
		if networkLatched != nil{
			aCoder.encode(networkLatched, forKey: "network_latched")
		}
		if rejected != nil{
			aCoder.encode(rejected, forKey: "rejected")
		}
		if tvPending != nil{
			aCoder.encode(tvPending, forKey: "tv_pending")
		}
		if zone != nil{
			aCoder.encode(zone, forKey: "zone")
		}

	}

}
