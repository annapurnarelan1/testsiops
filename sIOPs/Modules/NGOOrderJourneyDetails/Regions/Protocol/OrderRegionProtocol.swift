//
//  RANProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//




import Foundation


/// Method contract between PRESENTER -> VIEW
protocol OrderRegionViewProtocol: class {
    var presenter: OrderRegionPresenterProtocol? { get set }
       func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(response:OrderRegionModel)
    func gotInProcess(detail:O2AinProcessModel)
    func gotRejected(detail:O2ARejectedModel)
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OrderRegionWireFrameProtocol: class {
    static func presentOrderRegionModule(fromView:AnyObject,allChannelsDetail:AllChannel,date:String)
     func presentOrderCicleDetailModule(regionList:OrderRegionList,selectionType:String,date:String)
    
}

/// Method contract between VIEW -> PRESENTER
protocol OrderRegionPresenterProtocol: class {
    var view: OrderRegionViewProtocol? { get set }
    var interactor: OrderRegionInteractorInputProtocol? { get set }
    var wireFrame: OrderRegionWireFrameProtocol? { get set }

    
    func requestData(date:String,category:String)
    func presentOrderCicleDetailModule(regionList:OrderRegionList,selectionType:String,date:String)
    func requestInProcessData(date:String,channel:String,zone:String)
    func requestInRejectedData(date:String,channel:String,zone:String)
    
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OrderRegionInteractorOutputProtocol: class {
    
     func show(error: String,description:String)
      func stopLoader()
     
     func showFailError(error:String)
     func reloadData(response:OrderRegionModel)
    func gotInProcess(detail:O2AinProcessModel)
    func gotRejected(detail:O2ARejectedModel)
    
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol OrderRegionInteractorInputProtocol: class
{
    var presenter:  OrderRegionInteractorOutputProtocol? { get set }
    
    func requestData(date:String,category:String)
    func requestInProcessData(date:String,channel:String,zone:String)
    func requestInRejectedData(date:String,channel:String,zone:String)
    
}
