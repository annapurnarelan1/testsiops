//
//  NGODetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OrderRegionPresenter:BasePresenter, OrderRegionPresenterProtocol, OrderRegionInteractorOutputProtocol {
    
    
    
    // MARK: Variables
    weak var view: OrderRegionViewProtocol?
    var interactor: OrderRegionInteractorInputProtocol?
    var wireFrame: OrderRegionWireFrameProtocol?
    let stringsTableName = "NGOOrderRegion"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    func requestData(date: String, category: String) {
        self.interactor?.requestData(date: date, category: category)
    }
    
    func reloadData(response: OrderRegionModel) {
        self.view?.reloadData(response: response)
    }
    
    
     func presentOrderCicleDetailModule(regionList:OrderRegionList,selectionType:String,date:String)
     {
        self.wireFrame?.presentOrderCicleDetailModule(regionList:regionList,selectionType:selectionType,date:date)
    }
    
    func requestInProcessData(date:String,channel:String,zone:String)
    {
        self.interactor?.requestInProcessData(date:date,channel:channel,zone:zone)
    }
    func requestInRejectedData(date:String,channel:String,zone:String)
    {
        self.interactor?.requestInRejectedData(date:date,channel:channel,zone:zone)
    }
    
    func gotInProcess(detail:O2AinProcessModel)
    {
        self.view?.gotInProcess(detail:detail)
    }
    func gotRejected(detail:O2ARejectedModel)
    {
        self.view?.gotRejected(detail:detail)
    }
}
