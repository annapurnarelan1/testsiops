//
//  NGODetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OrderRegionWireFrame: OrderRegionWireFrameProtocol {
    
    
    
    // MARK: OrderRegionDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOrderRegionModule(fromView:AnyObject,allChannelsDetail:AllChannel,date:String) {


        // Generating module components
        let view: OrderRegionViewProtocol = OrderRegionVC.instantiate()
        let presenter: OrderRegionPresenterProtocol & OrderRegionInteractorOutputProtocol = OrderRegionPresenter()
        let interactor: OrderRegionInteractorInputProtocol = OrderRegionServiceInteractor()
       
        let wireFrame: OrderRegionWireFrameProtocol = OrderRegionWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OrderRegionVC
        viewController.allchannelResponse = allChannelsDetail
        viewController.date = date
           
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
   {
         OpenAlertsDetailWireFrame.presentOpenAlertsDetailModule(fromView:self,type:type,ngoResponse:ngoResponse,count:count)
   }
    
    func presentOrderCicleDetailModule(regionList:OrderRegionList,selectionType:String,date:String)
     {
        CircleWireFrame.presentOrderCicleDetailModule(fromView: self, regionList: regionList, selectionType: selectionType, date: date)
    }

}
