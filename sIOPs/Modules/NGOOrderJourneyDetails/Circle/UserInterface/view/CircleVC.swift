//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class CircleVC: BaseViewController,CircleProtocol,FSCalendarDataSource, FSCalendarDelegate {
    
    
    var presenter: CirclePresenterProtocol?
    
    @IBOutlet weak var tblCircles: UITableView!
    @IBOutlet weak var calendarHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var dateLabel: UILabel!
    var type:String?
    var selected:String?
    var NGOResponse:NGO?
    var selectedArray:[NGOApplication]?
    var senderSelectedIndex:Int?
    var typeSelected :String?
    var senderSelectedTypeIndex:Int?
    var senderSelectedRejected:Int?
    var circleResponse :OrderRegionModel?
    var selectionType:String?
    var date:String?
    var regionList :OrderRegionList?
    var inprocessDetail :O2AinProcessModel?
    var rejectedDetail :O2ARejectedModel?
    
    @IBOutlet weak var rrBtn: UIButton!
    @IBOutlet weak var GTBtn: UIButton!
    @IBOutlet weak var regionBtn: UIButton!
    var attrs = [
         
          NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView, action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    static func instantiate() -> CircleProtocol{
        return UIStoryboard(name: "CircleVC", bundle: nil).instantiateViewController(withIdentifier: "CircleVC") as! CircleVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblCircles.register(UINib(nibName: "OrderJourneyDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderJourneyDetailTableViewCell")
        self.dateLabel.text = HelperMethods().formatDateOrderJourney(date:date ?? "")
        if(selectionType == "ALL")
        {
            regionBtn.backgroundColor = UIColor.strongBlueColor
            regionBtn.setTitleColor(UIColor.white, for: .normal)
        }
        else if(selectionType == "GT")
        {
            GTBtn.backgroundColor = UIColor.strongBlueColor
            GTBtn.setTitleColor(UIColor.white, for: .normal)
        }
        else if(selectionType == "RR")
        {
            rrBtn.backgroundColor = UIColor.strongBlueColor
            rrBtn.setTitleColor(UIColor.white, for: .normal)
        }
        
    }
    
    
    @IBAction func regionAction(_ sender: Any) {
        
        regionBtn.backgroundColor = UIColor.strongBlueColor
        regionBtn.setTitleColor(UIColor.white, for: .normal)
        GTBtn.backgroundColor = UIColor.white
        GTBtn.setTitleColor(UIColor.black, for: .normal)
        rrBtn.backgroundColor = UIColor.white
        rrBtn.setTitleColor(UIColor.black, for: .normal)
        startAnimating()
        selectionType = "ALL"
        self.presenter?.requestData(date: date ?? "", category: "ALL", region: regionList?.zone.uppercased() ?? "")
    }
    @IBAction func gtAction(_ sender: Any) {
        
        GTBtn.backgroundColor = UIColor.strongBlueColor
        GTBtn.setTitleColor(UIColor.white, for: .normal)
        regionBtn.backgroundColor = UIColor.white
        regionBtn.setTitleColor(UIColor.black, for: .normal)
        rrBtn.backgroundColor = UIColor.white
        rrBtn.setTitleColor(UIColor.black, for: .normal)
        startAnimating()
        selectionType = "GT"
        self.presenter?.requestData(date: date ?? "", category: "GT", region: regionList?.zone.uppercased() ?? "")
    }
    
    @IBAction func rrAction(_ sender: Any) {
        rrBtn.backgroundColor = UIColor.strongBlueColor
        rrBtn.setTitleColor(UIColor.white, for: .normal)
        GTBtn.backgroundColor = UIColor.white
        GTBtn.setTitleColor(UIColor.black, for: .normal)
        regionBtn.backgroundColor = UIColor.white
        regionBtn.setTitleColor(UIColor.black, for: .normal)
        startAnimating()
        selectionType = "RR"
        self.presenter?.requestData(date: date ?? "", category: "RR", region: regionList?.zone.uppercased() ?? "")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
        
        startAnimating()
        self.presenter?.requestData(date: date ?? "", category: selectionType ?? "", region: regionList?.zone.uppercased() ?? "")
    }
    
    @IBAction func moveToRegion(_ sender: Any) {
        
        self.presenter?.presentRegionScreen()
        
    }
    
    
    @objc func expandDetailView(_ sender:UIButton)
    {
        senderSelectedTypeIndex = -1
        senderSelectedRejected = -1
        if(sender.tag != senderSelectedIndex)
        {
            senderSelectedIndex = sender.tag
            
        }
        else
        {
            senderSelectedIndex = -1
            
        }
        self.tblCircles.reloadData()
        self.view.setNeedsLayout()
    }
    
    @objc func rejectedBtnAction(_ sender:UIButton) {
        senderSelectedTypeIndex = -1
        typeSelected = "rejected"
        if(sender.tag != senderSelectedRejected)
           {
             senderSelectedRejected = sender.tag
            var circle : String = ""
                   if(senderSelectedRejected == 0)
                   {
                       circle = "ALL"
                   }
                   else
                   {
                    circle = circleResponse?.list[senderSelectedRejected ?? 0].zone ?? ""
            }
            startAnimating()
            self.presenter?.requestInRejectedData(date: date ?? "", channel: selectionType ?? "",zone:regionList?.zone ?? "" , circle: circle)
              
            
           }
           else
           {
               senderSelectedRejected = -1
            self.tblCircles.reloadData()
                   self.view.setNeedsLayout()
               
           }
       
    }
    @objc func inProgressBtnAction(_ sender:UIButton) {
       senderSelectedRejected = -1
        typeSelected = "inProgress"
        if(sender.tag != senderSelectedTypeIndex)
           {
               senderSelectedTypeIndex = sender.tag
            var circle : String = ""
                   if(senderSelectedTypeIndex == 0)
                   {
                       circle = "ALL"
                   }
                   else
                   {
                   circle = circleResponse?.list[senderSelectedTypeIndex ?? 0].zone ?? ""
                   }
            startAnimating()
            self.presenter?.requestInProcessData(date: date ?? "", channel: selectionType ?? "",zone:regionList?.zone ?? "" , circle: circle)
            
           }
           else
           {
               senderSelectedTypeIndex = -1
               self.tblCircles.reloadData()
               self.view.setNeedsLayout()
           }
        
        
        
       
    }
    
    
    func gotInProcess(detail:O2AinProcessModel)
    {
        stopAnimating()
        inprocessDetail = detail
        self.tblCircles.reloadData()
        self.view.setNeedsLayout()
        
    }
    
    func gotRejected(detail:O2ARejectedModel)
    {
        stopAnimating()
        rejectedDetail = detail
        self.tblCircles.reloadData()
        self.view.setNeedsLayout()
    }
    
    func stopLoader() {
        stopAnimating()
    }
    func reloadData(CircleResponse: OrderRegionModel) {
        stopAnimating()
        circleResponse = CircleResponse
        self.tblCircles.reloadData()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
    }
    
    
    
}



extension CircleVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OrderJourneyDetailTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "OrderJourneyDetailTableViewCell", for: indexPath as IndexPath) as? OrderJourneyDetailTableViewCell)!
        cell.viewdetailBtn.addTarget(self, action: #selector(expandDetailView(_:)), for: .touchUpInside)
        cell.viewdetailBtn.tag = indexPath.section
        cell.rejectedBtn.tag = indexPath.section
        cell.inProgressBtn.tag = indexPath.section
        cell.rejectedBtn.addTarget(self, action: #selector(rejectedBtnAction(_:)), for: .touchUpInside)
         cell.inProgressBtn.addTarget(self, action: #selector(inProgressBtnAction(_:)), for: .touchUpInside)
        cell.detailView.isHidden = true
        if(senderSelectedIndex ==  cell.viewdetailBtn.tag )
        {
            cell.viewdetailBtn.backgroundColor = UIColor.strongBlueColor
            cell.viewdetailBtn.setTitleColor(UIColor.white, for: .normal)
            cell.viewdetailBtn.setTitle("Hide Details", for: .normal)
            cell.secondView.isHidden = false
        }
        else
        {
            cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
            cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
            cell.viewdetailBtn.setTitle("View Details", for: .normal)
            cell.secondView.isHidden = true
            
        }
        if(typeSelected == "rejected")
        {
            
            if(senderSelectedRejected ==  cell.rejectedBtn.tag)
            {
                 cell.detailFifthView.isHidden = false
                cell.detailtitleLabel.text = "Rejected"
                cell.detailFirstOutlierName.text = "De Dupe"
                cell.detailSecondOutlierName.text = "De Dupe Sys"
                cell.detailThirdOutlierName.text = "AO"
                cell.detailForthOutlierName.text = "LR Check"
                cell.detailFifthOutlierName.text = "Cancelled"
                cell.detailFirstOutLierCount.text = "\(rejectedDetail?.deDupe ?? 0)"
                cell.detailSecondOutLierCount.text = "\(rejectedDetail?.deDupeSys ?? 0)"
                cell.detailThirdOutLierCount.text = "\(rejectedDetail?.ao ?? 0)"
                cell.detailForthOutLierCount.text = "\(rejectedDetail?.lrCheck ?? 0)"
                cell.detailFifthOutLierCount.text = "\(rejectedDetail?.cancelled ?? 0)"
                
                
                cell.detailView.isHidden = false
            }
            else
            {
               // cell.viewdetailBtn.backgroundColor = UIColor.verylightGreyColor
                //cell.viewdetailBtn.setTitleColor(UIColor.init(hexString: "#7B7B7B"), for: .normal)
               // cell.viewdetailBtn.setTitle("View Details", for: .normal)
               
                cell.detailView.isHidden = true
               // cell.detailViewHeight.constant = 0
            }
            
            
        }
        else
        {
            if(senderSelectedTypeIndex ==  cell.inProgressBtn.tag )
            {
                cell.detailFifthView.isHidden = true
                cell.detailtitleLabel.text = "In Process"
                cell.detailFirstOutlierName.text = "CAF Scanning"
                cell.detailSecondOutlierName.text = "In Transit"
                cell.detailThirdOutlierName.text = "In Approval"
                cell.detailForthOutlierName.text = "LR Check"
                cell.detailFirstOutLierCount.text = "\(inprocessDetail?.cafScanning ?? 0)"
                cell.detailSecondOutLierCount.text = "\(inprocessDetail?.inTransit ?? 0)"
                cell.detailThirdOutLierCount.text = "\(inprocessDetail?.inApproval ?? 0)"
                cell.detailForthOutLierCount.text = "\(inprocessDetail?.lrCheck ?? 0)"
                
                
                cell.detailView.isHidden = false
            }
            else
            {
              
                cell.detailView.isHidden = true
               // cell.detailViewHeight.constant = 0
            }
            
        }
        
        if indexPath.section == 0
        {
            var typeStr = ""
            if((selectionType ?? "") == "ALL")
            {
                typeStr = "(All Channels)"
            }
            else if((selectionType ?? "") == "GT")
            {
                typeStr = "(General Trade)"
            }
            else if((selectionType ?? "") == "RR")
            {
                typeStr = "(Reliance Retail)"
            }
            cell.headerLabel.text = (circleResponse?.list[indexPath.section].zone ?? "")  +  " (\(circleResponse?.category ?? ""))"
            cell.firstOutlIerName.text = "Entered"
            cell.firstOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].entered ?? 0)")
            cell.secondOutlIerName.text = "Activated"
            cell.secondOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].activated ?? 0)")
            cell.thirdOutlIerName.text = "Network Latched"
            cell.thirdOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].networkLatched ?? 0)")
            cell.rejectedName.attributedText = NSMutableAttributedString(string: "Rejected", attributes:attrs)
                       
            cell.inProgressName.attributedText = NSMutableAttributedString(string: "In Process", attributes:attrs)
           // cell.rejectedName.text = "Rejected"
            cell.rejectedCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].rejected ?? 0)")
           // cell.inProgressName.text = "In Process"
            cell.inProgressCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].inProcess ?? 0)")
            cell.tvPendingName.text = "TV Pending"
            cell.tvPendingCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].tvPending ?? 0)")
        }
        else
        {
            cell.headerLabel.text = circleResponse?.list[indexPath.section].zone ?? ""
            cell.firstOutlIerName.text = "Entered"
            cell.firstOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].entered ?? 0)")
            cell.secondOutlIerName.text = "Activated"
            cell.secondOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].activated ?? 0)")
            cell.thirdOutlIerName.text = "Network Latched"
            cell.thirdOutlIerCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].networkLatched ?? 0)")
            cell.rejectedName.attributedText = NSMutableAttributedString(string: "Rejected", attributes:attrs)
                       
            cell.inProgressName.attributedText = NSMutableAttributedString(string: "In Process", attributes:attrs)
           // cell.rejectedName.text = "Rejected"
            cell.rejectedCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].rejected ?? 0)")
           // cell.inProgressName.text = "In Process"
            cell.inProgressCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].inProcess ?? 0)")
            cell.tvPendingName.text = "TV Pending"
            cell.tvPendingCount.text = HelperMethods().nuumberFormatting(value:"\(circleResponse?.list[indexPath.section].tvPending ?? 0)")
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(circleResponse != nil)
        {
            return (circleResponse?.list.count ?? 0)
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
