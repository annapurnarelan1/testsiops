//
//  CirclePresenter.swift
//  sIOPs
//
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class CirclePresenter:BasePresenter, CirclePresenterProtocol, CircleInteractorOutputProtocol {
    
    
    
    
    
    // MARK: Variables
    weak var view: CircleProtocol?
    var interactor: CircleInteractorInputProtocol?
    var wireFrame: CircleWireFrameProtocol?
    let stringsTableName = "Circle"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    func reloadData(CircleResponse: OrderRegionModel) {
        self.view?.reloadData(CircleResponse: CircleResponse)
    }
    
    func requestData(date:String,category:String,region:String)
    {
        self.interactor?.requestData(date:date,category:category,region:region)
    }
    
    func presentRegionScreen()
    {
        self.wireFrame?.presentRegionScreen()
    }
    
    
    func requestInProcessData(date:String,channel:String,zone:String,circle:String)
    {
        self.interactor?.requestInProcessData(date:date,channel:channel,zone:zone,circle:circle)
    }
    func requestInRejectedData(date:String,channel:String,zone:String,circle:String)
    {
        self.interactor?.requestInRejectedData(date:date,channel:channel,zone:zone,circle:circle)
    }
    
    func gotInProcess(detail:O2AinProcessModel)
    {
        self.view?.gotInProcess(detail:detail)
    }
    func gotRejected(detail:O2ARejectedModel)
    {
        self.view?.gotRejected(detail:detail)
    }
    
    
}
