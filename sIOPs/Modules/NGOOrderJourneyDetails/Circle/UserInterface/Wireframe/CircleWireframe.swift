//
//  NGODetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class CircleWireFrame: CircleWireFrameProtocol {
    
    // MARK: OrderJourneyDetaillWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOrderCicleDetailModule(fromView:AnyObject,regionList:OrderRegionList,selectionType:String,date:String) {

        // Generating module components
        let view: CircleProtocol = CircleVC.instantiate()
        let presenter: CirclePresenterProtocol & CircleInteractorOutputProtocol = CirclePresenter()
        let interactor: CircleInteractorInputProtocol = CircleInteractor()
       
        let wireFrame: CircleWireFrameProtocol = CircleWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! CircleVC
        viewController.regionList = regionList
        viewController.selectionType  = selectionType
        viewController.date = date
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
    
    func presentRegionScreen() {
        
    }
}
