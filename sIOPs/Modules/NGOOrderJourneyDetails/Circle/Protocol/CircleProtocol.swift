//
//  RANProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol CircleProtocol: class {
    var presenter: CirclePresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    func stopLoader()
     func reloadData(CircleResponse:OrderRegionModel)
    func showFailError(error:String)
    func gotInProcess(detail:O2AinProcessModel)
       func gotRejected(detail:O2ARejectedModel)
   
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol CircleWireFrameProtocol: class {
    static func presentOrderCicleDetailModule(fromView:AnyObject,regionList:OrderRegionList,selectionType:String,date:String)
    func presentRegionScreen()
}

/// Method contract between VIEW -> PRESENTER
protocol CirclePresenterProtocol: class {
    var view: CircleProtocol? { get set }
    var interactor: CircleInteractorInputProtocol? { get set }
    var wireFrame: CircleWireFrameProtocol? { get set }
    func showFailError(error:String)
   
    func presentRegionScreen()
    func requestData(date:String,category:String,region:String)
    func requestInProcessData(date:String,channel:String,zone:String,circle:String)
    func requestInRejectedData(date:String,channel:String,zone:String,circle:String)
    
}

/// Method contract between INTERACTOR -> PRESENTER
protocol CircleInteractorOutputProtocol: class {
    
    func show(error: String,description:String)
    func stopLoader()
    
    func showFailError(error:String)
    func reloadData(CircleResponse:OrderRegionModel)
    func gotInProcess(detail:O2AinProcessModel)
       func gotRejected(detail:O2ARejectedModel)
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol CircleInteractorInputProtocol: class
{
    var presenter: CircleInteractorOutputProtocol? { get set }
    func requestData(date:String,category:String,region:String)
    func requestInProcessData(date:String,channel:String,zone:String,circle:String)
    func requestInRejectedData(date:String,channel:String,zone:String,circle:String)
}
