//
//  OpenDefectsProtocol.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenDefectsViewProtocol: class {
    var presenter: OpenDefectsPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    func reloadData(defectonClick:OpenDefectOnClickModel)
    func reloadData(defectonDetailClick:[OpenDefectsDetailList])
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenDefectsWireFrameProtocol: class {
static func presentOpenDefectsModule(fromView:AnyObject,type:String,selected:String,response:OpenDefectModel)
    func presentOpenDefectsHistoryModule( type: String, count: String, response: [OpenDefectsDetailList])
    
  
}

/// Method contract between VIEW -> PRESENTER
protocol OpenDefectsPresenterProtocol: class {
    var view: OpenDefectsViewProtocol? { get set }
    var interactor: OpenDefectsInteractorInputProtocol? { get set }
    var wireFrame: OpenDefectsWireFrameProtocol? { get set }
    func requestData(defecteType:String, category:String)
    func requestHistoryData(defecteType:String, category:String, duration:String)
    func presentOpenDefectsHistoryModule( type: String, count: String, response: [OpenDefectsDetailList])
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenDefectsInteractorOutputProtocol: class {

    func show(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadData(defectonClick:OpenDefectOnClickModel)
    func reloadData(defectonDetailClick:[OpenDefectsDetailList])

}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenDefectsInteractorInputProtocol: class
{
    var presenter: OpenDefectsInteractorOutputProtocol? { get set }
    func requestData(defecteType:String, category:String)
    func requestHistoryData(defecteType:String, category:String, duration:String)
    
}
