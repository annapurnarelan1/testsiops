//
//  OpenDefectsPresenter.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OpenDefectsPresenter:BasePresenter, OpenDefectsPresenterProtocol, OpenDefectsInteractorOutputProtocol {
//    func requestHistoryData(defecteType: String, category: String, duration: String) {
//        self.interactor?.requestHistoryData(defecteType: defecteType, category: category, duration: duration)
//    }
//    
    
    
    
    
    // MARK: Variables
    weak var view: OpenDefectsViewProtocol?
    var interactor: OpenDefectsInteractorInputProtocol?
    var wireFrame: OpenDefectsWireFrameProtocol?
    let stringsTableName = "NGODetail"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
    {
        self.view?.stopLoader()
    }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func requestData(defecteType:String, category:String)
    {
        self.interactor?.requestData(defecteType:defecteType, category:category)
    }
    
    func requestHistoryData(defecteType:String, category:String, duration:String)
    {
        self.interactor?.requestHistoryData(defecteType:defecteType, category:category,  duration:duration)
    }
    
    func reloadData(defectonClick:OpenDefectOnClickModel) {
        self.view?.reloadData(defectonClick: defectonClick)
    }
    
    func reloadData(defectonDetailClick:[OpenDefectsDetailList]) {
        self.view?.reloadData(defectonDetailClick: defectonDetailClick)
    }
    
     func presentOpenDefectsHistoryModule( type: String, count: String, response: [OpenDefectsDetailList])
     {
        self.wireFrame?.presentOpenDefectsHistoryModule( type: type, count: count, response: response)
    }
}
