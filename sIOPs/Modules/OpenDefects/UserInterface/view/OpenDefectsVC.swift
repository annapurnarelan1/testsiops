//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class OpenDefectsVC: BaseViewController,OpenDefectsViewProtocol {
    
    
    var presenter: OpenDefectsPresenterProtocol?
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var firstLbl: UILabel!
    @IBOutlet weak var firstDescLbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondLbl: UILabel!
    @IBOutlet weak var secondDescLbl: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var thirdDescLbl: UILabel!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var frthLbl: UILabel!
    @IBOutlet weak var frthDescLbl: UILabel!
    @IBOutlet weak var frthBtn: UIButton!
    @IBOutlet weak var fifthLbl: UILabel!
    @IBOutlet weak var fifthDescLbl: UILabel!
    @IBOutlet weak var fifthBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var type:String?
    var selected:String?
    var defectResponse:OpenDefectModel?
    var defectClickResponse:OpenDefectOnClickModel?
    var feature: String?
    var titlecount: Int = 0
    var arrDefectsDetails : [OpenDefectsDetailList] = []
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> OpenDefectsViewProtocol{
        return UIStoryboard(name: "OpenDefects", bundle: nil).instantiateViewController(withIdentifier: "OpenDefects") as! OpenDefectsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addDefectsData()
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    func addDefectsData() {
        if selected == "Hot Fix" as String {
            let normalText = "Hot Fix "
            let boldText  = "(" + "\(defectResponse?.hotFix ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            attributedString.append(boldString)
            titleLbl.attributedText = attributedString
            firstDescLbl.text = defectResponse?.hotFixList[0].featureName ?? ""
            firstLbl.text = "\(defectResponse?.hotFixList[0].outlierCount ?? 0)"
            //firstBtn.addTarget(self, action: #selector(firstBtnOnClick), for:.touchUpInside)
            secondDescLbl.text = defectResponse?.hotFixList[1].featureName ?? ""
            secondLbl.text = "\(defectResponse?.hotFixList[1].outlierCount ?? 0)"
            thirdDescLbl.text = defectResponse?.hotFixList[2].featureName ?? ""
            thirdLbl.text = "\(defectResponse?.hotFixList[2].outlierCount ?? 0)"
            frthDescLbl.text = defectResponse?.hotFixList[3].featureName ?? ""
            frthLbl.text = "\(defectResponse?.hotFixList[3].outlierCount ?? 0)"
            fifthDescLbl.text = defectResponse?.hotFixList[4].featureName ?? ""
            fifthLbl.text = "\(defectResponse?.hotFixList[4].outlierCount ?? 0)"
            self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.hotFixList[0].featureName ?? "")
                          feature = defectResponse?.hotFixList[0].featureName ?? ""
                          titlecount = defectResponse?.hotFixList[0].outlierCount ?? 0
        }
        else if selected == "Defects"{
            let normalText = "Defects "
            let boldText  = "(" + "\(defectResponse?.defect ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            attributedString.append(boldString)
            titleLbl.attributedText = attributedString
            firstDescLbl.text = defectResponse?.defectList[0].featureName ?? ""
            firstLbl.text = "\(defectResponse?.defectList[0].outlierCount ?? 0)"
            secondDescLbl.text = defectResponse?.defectList[1].featureName ?? ""
            secondLbl.text = "\(defectResponse?.defectList[1].outlierCount ?? 0)"
            thirdDescLbl.text = defectResponse?.defectList[2].featureName ?? ""
            thirdLbl.text = "\(defectResponse?.defectList[2].outlierCount ?? 0)"
            frthDescLbl.text = defectResponse?.defectList[3].featureName ?? ""
            frthLbl.text = "\(defectResponse?.defectList[3].outlierCount ?? 0)"
            fifthDescLbl.text = defectResponse?.defectList[4].featureName ?? ""
            fifthLbl.text = "\(defectResponse?.defectList[4].outlierCount ?? 0)"
            self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.defectList[0].featureName ?? "")
                          feature = defectResponse?.defectList[0].featureName ?? ""
                          titlecount = defectResponse?.defectList[0].outlierCount ?? 0
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    @IBAction func BtnOnClick(sender:UIButton)
    {
        
        startAnimating()
        if selected == "Hot Fix" {
            switch sender.tag {
            case 0:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.hotFixList[0].featureName ?? "")
               feature = defectResponse?.hotFixList[0].featureName ?? ""
               titlecount = defectResponse?.hotFixList[0].outlierCount ?? 0
                //features.
            case 1:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.hotFixList[1].featureName ?? "")
                feature = defectResponse?.hotFixList[1].featureName ?? ""
                titlecount = defectResponse?.hotFixList[1].outlierCount ?? 0
            case 2:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.hotFixList[2].featureName ?? "")
                feature = defectResponse?.hotFixList[2].featureName ?? ""
                titlecount = defectResponse?.hotFixList[2].outlierCount ?? 0
            case 3:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.hotFixList[3].featureName ?? "")
                feature = defectResponse?.hotFixList[3].featureName ?? ""
                titlecount = defectResponse?.hotFixList[3].outlierCount ?? 0
            case 4:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.hotFixList[4].featureName ?? "")
                feature = defectResponse?.hotFixList[4].featureName ?? ""
                titlecount = defectResponse?.hotFixList[4].outlierCount ?? 0
            default:
                print("nothing selected")
            }
        }
        else if selected == "Defects" {
            switch sender.tag {
            case 0:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.defectList[0].featureName ?? "")
                feature = defectResponse?.defectList[0].featureName ?? ""
                titlecount = defectResponse?.defectList[0].outlierCount ?? 0
            case 1:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.defectList[1].featureName ?? "")
                feature = defectResponse?.defectList[1].featureName ?? ""
                titlecount = defectResponse?.defectList[1].outlierCount ?? 0
            case 2:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.defectList[2].featureName ?? "")
                feature = defectResponse?.defectList[2].featureName ?? ""
                titlecount = defectResponse?.defectList[2].outlierCount ?? 0
            case 3:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.defectList[3].featureName ?? "")
                feature = defectResponse?.defectList[3].featureName ?? ""
                titlecount = defectResponse?.defectList[3].outlierCount ?? 0
            case 4:
                self.presenter?.requestData(defecteType:selected ?? "", category:defectResponse?.defectList[4].featureName ?? "")
                feature = defectResponse?.defectList[4].featureName ?? ""
                titlecount = defectResponse?.defectList[4].outlierCount ?? 0
                
            default:
                print("nothing selected")
            }
        }
        
    }
    
    func reloadData(defectonClick:OpenDefectOnClickModel) {
        stopAnimating()
        defectClickResponse = defectonClick
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    func reloadData(defectonDetailClick: [OpenDefectsDetailList]) {
        self.presenter?.presentOpenDefectsHistoryModule( type: feature ?? "", count: "\(titlecount)", response: defectonDetailClick)
    }
    
}

 extension OpenDefectsVC : UITableViewDelegate,UITableViewDataSource
 {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OutlierTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell", for: indexPath as IndexPath) as! OutlierTableViewCell
        cell.titleLbl.text = defectClickResponse?.categoryList?[indexPath.row].featureName ?? ""
        cell.countLbl.text = "\(defectClickResponse?.categoryList?[indexPath.row].outlierCount ?? 0)"
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return defectClickResponse?.categoryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell") as! OutlierTableViewCell
            cell.titleLbl.text = feature//"Under Development" ///*\(selectedSeverity ?? 0)*/"
            cell.titleLbl.textColor = UIColor.white
            cell.arrowImage.isHidden = true
            cell.countLbl.text = "\(titlecount ?? 0)"
            cell.countLbl.textColor = UIColor.white
            cell.expandAction.isHidden = true
            cell.backgroundColor = UIColor.init(red: 28, green: 122, blue: 189)
            
            return cell as UIView
        }
        else {
            return  UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.requestHistoryData(defecteType: selected ?? "", category: feature ?? "", duration: defectClickResponse?.categoryList?[indexPath.row].featureName ?? "")
        
    }
}
