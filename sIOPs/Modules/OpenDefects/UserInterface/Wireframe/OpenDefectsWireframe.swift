//
//  OpenDefectsWireframe.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenDefectsWireFrame: OpenDefectsWireFrameProtocol {
    
    
    
    // MARK: NGODetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenDefectsModule(fromView:AnyObject,type:String,selected:String,response:OpenDefectModel) {

        // Generating module components
        let view: OpenDefectsViewProtocol = OpenDefectsVC.instantiate()
        let presenter: OpenDefectsPresenterProtocol & OpenDefectsInteractorOutputProtocol = OpenDefectsPresenter()
        let interactor: OpenDefectsInteractorInputProtocol = OpenDefectsInteractor()
       
        let wireFrame: OpenDefectsWireFrameProtocol = OpenDefectsWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! OpenDefectsVC
        viewController.type = type
        viewController.selected = selected
        viewController.defectResponse = response
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    func presentOpenDefectsHistoryModule( type: String, count: String, response: [OpenDefectsDetailList])
    {
        OpenDefectsHistoryWireFrame.presentOpenDefectsHistoryModule(fromView: self, type: type, count: count, response: response)
    }
   

}
