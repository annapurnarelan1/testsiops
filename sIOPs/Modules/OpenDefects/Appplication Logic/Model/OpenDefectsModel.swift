//
//  OpenDefectsModel.swift
//  sIOPs
//
//  Created by Annapurna on 10/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let openDefectsModel = try OpenDefectsModel(json)

import Foundation

// MARK: - OpenDefectsModel
@objcMembers class OpenDefectsModel: NSObject, Codable {
    let respInfo: defectsInfo?
    let respData: [defectsDatum]?

    init(respInfo: defectsInfo?, respData: [defectsDatum]?) {
        self.respInfo = respInfo
        self.respData = respData
    }
}

// MARK: OpenDefectsModel convenience initializers and mutators

extension OpenDefectsModel {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(OpenDefectsModel.self, from: data)
        self.init(respInfo: me.respInfo, respData: me.respData)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        respInfo: defectsInfo?? = nil,
        respData: [defectsDatum]?? = nil
    ) -> OpenDefectsModel {
        return OpenDefectsModel(
            respInfo: respInfo ?? self.respInfo,
            respData: respData ?? self.respData
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespDatum
@objcMembers class defectsDatum: NSObject, Codable {
    let busiCode, transactionID, code, message: String?
    let isEncrypt: Bool?
    let respMsg: defectsMsg?

    enum CodingKeys: String, CodingKey {
        case busiCode
        case transactionID
        case code, message, isEncrypt, respMsg
    }

    init(busiCode: String?, transactionID: String?, code: String?, message: String?, isEncrypt: Bool?, respMsg: defectsMsg?) {
        self.busiCode = busiCode
        self.transactionID = transactionID
        self.code = code
        self.message = message
        self.isEncrypt = isEncrypt
        self.respMsg = respMsg
    }
}

// MARK: defectsDatum convenience initializers and mutators

extension defectsDatum {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(defectsDatum.self, from: data)
        self.init(busiCode: me.busiCode, transactionID: me.transactionID, code: me.code, message: me.message, isEncrypt: me.isEncrypt, respMsg: me.respMsg)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        busiCode: String?? = nil,
        transactionID: String?? = nil,
        code: String?? = nil,
        message: String?? = nil,
        isEncrypt: Bool?? = nil,
        respMsg: defectsMsg?? = nil
    ) -> defectsDatum {
        return defectsDatum(
            busiCode: busiCode ?? self.busiCode,
            transactionID: transactionID ?? self.transactionID,
            code: code ?? self.code,
            message: message ?? self.message,
            isEncrypt: isEncrypt ?? self.isEncrypt,
            respMsg: respMsg ?? self.respMsg
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespMsg
@objcMembers class defectsMsg: NSObject, Codable {
    let responseHeader: ResponseDefectsHeader?
    let responsePayload: [ResponseDefectsPayload]?

    init(responseHeader: ResponseDefectsHeader?, responsePayload: [ResponseDefectsPayload]?) {
        self.responseHeader = responseHeader
        self.responsePayload = responsePayload
    }
}

// MARK: RespMsg convenience initializers and mutators

extension defectsMsg {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(defectsMsg.self, from: data)
        self.init(responseHeader: me.responseHeader, responsePayload: me.responsePayload)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        responseHeader: ResponseDefectsHeader?? = nil,
        responsePayload: [ResponseDefectsPayload]?? = nil
    ) -> defectsMsg {
        return defectsMsg(
            responseHeader: responseHeader ?? self.responseHeader,
            responsePayload: responsePayload ?? self.responsePayload
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponseHeader
@objcMembers class ResponseDefectsHeader: NSObject, Codable {
    let timestamp, message: String?
    let status: Int?
    let title, path: String?

    init(timestamp: String?, message: String?, status: Int?, title: String?, path: String?) {
        self.timestamp = timestamp
        self.message = message
        self.status = status
        self.title = title
        self.path = path
    }
}

// MARK: ResponseHeader convenience initializers and mutators

extension ResponseDefectsHeader {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ResponseDefectsHeader.self, from: data)
        self.init(timestamp: me.timestamp, message: me.message, status: me.status, title: me.title, path: me.path)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        timestamp: String?? = nil,
        message: String?? = nil,
        status: Int?? = nil,
        title: String?? = nil,
        path: String?? = nil
    ) -> ResponseDefectsHeader {
        return ResponseDefectsHeader(
            timestamp: timestamp ?? self.timestamp,
            message: message ?? self.message,
            status: status ?? self.status,
            title: title ?? self.title,
            path: path ?? self.path
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponsePayload
@objcMembers class ResponseDefectsPayload: NSObject, Codable {
    let defectid, creationDATE, dueDATE, requestSTATE: String?
    let status, summary, createdby, requestCATEGORYNAME: String?
    let defectseverity, attribute2, bucket: String?
    let requiredHOTFIX: String?
    let agingINDAY: Int?
    let requestSTATET: String?

    enum CodingKeys: String, CodingKey {
        case defectid
        case creationDATE
        case dueDATE
        case requestSTATE
        case status, summary, createdby
        case requestCATEGORYNAME
        case defectseverity
        case attribute2
        case bucket
        case requiredHOTFIX
        case agingINDAY
        case requestSTATET
    }

    init(defectid: String?, creationDATE: String?, dueDATE: String?, requestSTATE: String?, status: String?, summary: String?, createdby: String?, requestCATEGORYNAME: String?, defectseverity: String?, attribute2: String?, bucket: String?, requiredHOTFIX: String?, agingINDAY: Int?, requestSTATET: String?) {
        self.defectid = defectid
        self.creationDATE = creationDATE
        self.dueDATE = dueDATE
        self.requestSTATE = requestSTATE
        self.status = status
        self.summary = summary
        self.createdby = createdby
        self.requestCATEGORYNAME = requestCATEGORYNAME
        self.defectseverity = defectseverity
        self.attribute2 = attribute2
        self.bucket = bucket
        self.requiredHOTFIX = requiredHOTFIX
        self.agingINDAY = agingINDAY
        self.requestSTATET = requestSTATET
    }
}

// MARK: ResponsePayload convenience initializers and mutators

extension ResponseDefectsPayload {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(ResponseDefectsPayload.self, from: data)
        self.init(defectid: me.defectid, creationDATE: me.creationDATE, dueDATE: me.dueDATE, requestSTATE: me.requestSTATE, status: me.status, summary: me.summary, createdby: me.createdby, requestCATEGORYNAME: me.requestCATEGORYNAME, defectseverity: me.defectseverity, attribute2: me.attribute2, bucket: me.bucket, requiredHOTFIX: me.requiredHOTFIX, agingINDAY: me.agingINDAY, requestSTATET: me.requestSTATET)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        defectid: String?? = nil,
        creationDATE: String?? = nil,
        dueDATE: String?? = nil,
        requestSTATE: String?? = nil,
        status: String?? = nil,
        summary: String?? = nil,
        createdby: String?? = nil,
        requestCATEGORYNAME: String?? = nil,
        defectseverity: String?? = nil,
        attribute2: String?? = nil,
        bucket: String?? = nil,
        requiredHOTFIX: String?? = nil,
        agingINDAY: Int?? = nil,
        requestSTATET: String?? = nil
    ) -> ResponseDefectsPayload {
        return ResponseDefectsPayload(
            defectid: defectid ?? self.defectid,
            creationDATE: creationDATE ?? self.creationDATE,
            dueDATE: dueDATE ?? self.dueDATE,
            requestSTATE: requestSTATE ?? self.requestSTATE,
            status: status ?? self.status,
            summary: summary ?? self.summary,
            createdby: createdby ?? self.createdby,
            requestCATEGORYNAME: requestCATEGORYNAME ?? self.requestCATEGORYNAME,
            defectseverity: defectseverity ?? self.defectseverity,
            attribute2: attribute2 ?? self.attribute2,
            bucket: bucket ?? self.bucket,
            requiredHOTFIX: requiredHOTFIX ?? self.requiredHOTFIX,
            agingINDAY: agingINDAY ?? self.agingINDAY,
            requestSTATET: requestSTATET ?? self.requestSTATET
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespInfo
@objcMembers class defectsInfo: NSObject, Codable {
    let code, message: String?

    init(code: String?, message: String?) {
        self.code = code
        self.message = message
    }
}

// MARK: RespInfo convenience initializers and mutators

extension defectsInfo {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(RespInfo.self, from: data)
        self.init(code: me.code, message: me.message)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        message: String?? = nil
    ) -> defectsInfo {
        return defectsInfo(
            code: code ?? self.code,
            message: message ?? self.message
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newDJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newDJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

