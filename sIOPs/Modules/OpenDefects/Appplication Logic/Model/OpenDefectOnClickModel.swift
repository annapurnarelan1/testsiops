//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenDefectOnClickModel : NSObject, NSCoding{

	var defect : Int!
	var categoryList : [DefectList]!
	


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		//defect = dictionary["defect"] as? Int
		categoryList = [DefectList]()
		if let defectListArray = dictionary["categoryList"] as? [[String:Any]]{
			for dic in defectListArray{
				let value = DefectList(fromDictionary: dic)
				categoryList.append(value)
			}
            defect = defectListArray.count
		}
		}
	//}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if defect != nil{
			dictionary["defect"] = defect
		}
		if categoryList != nil{
			var dictionaryElements = [[String:Any]]()
			for categoryListElement in categoryList {
				dictionaryElements.append(categoryListElement.toDictionary())
			}
			dictionary["categoryList"] = dictionaryElements
		}
//		if hotFix != nil{
//			dictionary["hotFix"] = hotFix
//		}
//		if hotFixList != nil{
//			var dictionaryElements = [[String:Any]]()
//			for hotFixListElement in hotFixList {
//				dictionaryElements.append(hotFixListElement.toDictionary())
//			}
//			dictionary["hotFixList"] = dictionaryElements
//		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         defect = aDecoder.decodeObject(forKey: "defect") as? Int
         categoryList = aDecoder.decodeObject(forKey :"categoryList") as? [DefectList]
         

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if defect != nil{
			aCoder.encode(defect, forKey: "defect")
		}
		if categoryList != nil{
			aCoder.encode(categoryList, forKey: "categoryList")
		}
		

	}

}
