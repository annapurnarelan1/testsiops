//
//	List.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenDefectsDetailList : NSObject, NSCoding{

	var agingINDAY : Int!
	var attribute2 : String!
	var bucket : String!
	var createdby : String!
	var creationDATE : String!
	var defectid : String!
	var defectseverity : String!
	var dueDATE : String!
	var requestCATEGORYNAME : String!
	var requestSTATE : String!
	var requestSTATET : String!
	var requiredHOTFIX : String!
	var status : String!
	var summary : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		agingINDAY = dictionary["aging_IN_DAY"] as? Int
		attribute2 = dictionary["attribute_2"] as? String
		bucket = dictionary["bucket"] as? String
		createdby = dictionary["createdby"] as? String
		creationDATE = dictionary["creation_DATE"] as? String
		defectid = dictionary["defectid"] as? String
		defectseverity = dictionary["defectseverity"] as? String
		dueDATE = dictionary["due_DATE"] as? String
		requestCATEGORYNAME = dictionary["request_CATEGORY_NAME"] as? String
		requestSTATE = dictionary["request_STATE"] as? String
		requestSTATET = dictionary["request_STATE_T"] as? String
		requiredHOTFIX = dictionary["required_HOT_FIX"] as? String
		status = dictionary["status"] as? String
		summary = dictionary["summary"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if agingINDAY != nil{
			dictionary["aging_IN_DAY"] = agingINDAY
		}
		if attribute2 != nil{
			dictionary["attribute_2"] = attribute2
		}
		if bucket != nil{
			dictionary["bucket"] = bucket
		}
		if createdby != nil{
			dictionary["createdby"] = createdby
		}
		if creationDATE != nil{
			dictionary["creation_DATE"] = creationDATE
		}
		if defectid != nil{
			dictionary["defectid"] = defectid
		}
		if defectseverity != nil{
			dictionary["defectseverity"] = defectseverity
		}
		if dueDATE != nil{
			dictionary["due_DATE"] = dueDATE
		}
		if requestCATEGORYNAME != nil{
			dictionary["request_CATEGORY_NAME"] = requestCATEGORYNAME
		}
		if requestSTATE != nil{
			dictionary["request_STATE"] = requestSTATE
		}
		if requestSTATET != nil{
			dictionary["request_STATE_T"] = requestSTATET
		}
		if requiredHOTFIX != nil{
			dictionary["required_HOT_FIX"] = requiredHOTFIX
		}
		if status != nil{
			dictionary["status"] = status
		}
		if summary != nil{
			dictionary["summary"] = summary
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agingINDAY = aDecoder.decodeObject(forKey: "aging_IN_DAY") as? Int
         attribute2 = aDecoder.decodeObject(forKey: "attribute_2") as? String
         bucket = aDecoder.decodeObject(forKey: "bucket") as? String
         createdby = aDecoder.decodeObject(forKey: "createdby") as? String
         creationDATE = aDecoder.decodeObject(forKey: "creation_DATE") as? String
         defectid = aDecoder.decodeObject(forKey: "defectid") as? String
         defectseverity = aDecoder.decodeObject(forKey: "defectseverity") as? String
         dueDATE = aDecoder.decodeObject(forKey: "due_DATE") as? String
         requestCATEGORYNAME = aDecoder.decodeObject(forKey: "request_CATEGORY_NAME") as? String
         requestSTATE = aDecoder.decodeObject(forKey: "request_STATE") as? String
         requestSTATET = aDecoder.decodeObject(forKey: "request_STATE_T") as? String
         requiredHOTFIX = aDecoder.decodeObject(forKey: "required_HOT_FIX") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         summary = aDecoder.decodeObject(forKey: "summary") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if agingINDAY != nil{
			aCoder.encode(agingINDAY, forKey: "aging_IN_DAY")
		}
		if attribute2 != nil{
			aCoder.encode(attribute2, forKey: "attribute_2")
		}
		if bucket != nil{
			aCoder.encode(bucket, forKey: "bucket")
		}
		if createdby != nil{
			aCoder.encode(createdby, forKey: "createdby")
		}
		if creationDATE != nil{
			aCoder.encode(creationDATE, forKey: "creation_DATE")
		}
		if defectid != nil{
			aCoder.encode(defectid, forKey: "defectid")
		}
		if defectseverity != nil{
			aCoder.encode(defectseverity, forKey: "defectseverity")
		}
		if dueDATE != nil{
			aCoder.encode(dueDATE, forKey: "due_DATE")
		}
		if requestCATEGORYNAME != nil{
			aCoder.encode(requestCATEGORYNAME, forKey: "request_CATEGORY_NAME")
		}
		if requestSTATE != nil{
			aCoder.encode(requestSTATE, forKey: "request_STATE")
		}
		if requestSTATET != nil{
			aCoder.encode(requestSTATET, forKey: "request_STATE_T")
		}
		if requiredHOTFIX != nil{
			aCoder.encode(requiredHOTFIX, forKey: "required_HOT_FIX")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if summary != nil{
			aCoder.encode(summary, forKey: "summary")
		}

	}

}
