//
//  DelinquentDetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class DelinquentDetailWireFrame: DelinquentDetailWireFrameProtocol {
    
    
    
    // MARK: DelinquentDetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentDelinquentDetailModule(fromView:AnyObject,domainName:String,platformDomain:String,ngoResponse:NGO,count:String) {

        // Generating module components
        let view: DelinquentDetailViewProtocol = DelinquentDetailVC.instantiate()
        let presenter: DelinquentDetailPresenterProtocol & DelinquentDetailInteractorOutputProtocol = DelinquentDetailPresenter()
        let interactor: DelinquentDetailInteractorInputProtocol = DelinquentDetailInteractor()
       
        let wireFrame: DelinquentDetailWireFrameProtocol = DelinquentDetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! DelinquentDetailVC
        viewController.domainName = domainName
        viewController.platformName = platformDomain
        viewController.NGOResponse =  ngoResponse
        viewController.count = count
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   

}
