//
//  DelinquentDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class DelinquentDetailVC: BaseViewController,DelinquentDetailViewProtocol {
    
    var presenter: DelinquentDetailPresenterProtocol?
    
   
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!

    @IBOutlet weak var tableView: UITableView!
    
    var type:String?
    var selected:String?
    var NGOResponse:NGO?
    var domainName:String?
    var platformName : String?
    var selectedArray:[EmployeeList]?
    var count : String?
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> DelinquentDetailViewProtocol{
        return UIStoryboard(name: "DelinquentDetail", bundle: nil).instantiateViewController(withIdentifier: "DelinquentDetailVC") as! DelinquentDetailVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "EmployeeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "EmployeeDetail")
        
        titleLbl.text = platformName
        countLbl.text = count
        
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No employee detail Available")
       
        startAnimating()
        self.presenter?.requestData(domainName: self.domainName ?? "", platformName: self.platformName ?? "")

        // Do any additional setup after loading the view.
    }
    
  
    

   override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated);
        
       HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       self.navigationController?.setNavigationBarHidden(false, animated: false)
       self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
     
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
     func reloadData(detail:EmployeeModel)
     {
        stopAnimating()
        self.selectedArray = detail.employeeList
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No employee detail Available")
        self.tableView.reloadData()
    }
    
    @objc func makeCall(_ sender:UIButton)
    {
        startAnimating()
        self.presenter?.sendReminderRequest(employlist:(selectedArray?[sender.tag])!)
       
    }
    
    func reloadSendReminder(detail:SendReminderModel)
    {
        stopAnimating()
        super.senReminderPopUp(image: "",error:  "Reminder Sent", description: "\(detail.message ?? "")",apkInstallationStatus:detail.apkInstallationStatus)
        
    }
    
    

}

extension DelinquentDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell:EmployeeDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EmployeeDetail", for: indexPath as IndexPath) as! EmployeeDetailTableViewCell
        
        cell.bellBtn.tag = indexPath.section
        cell.bellBtn.addTarget(self, action: #selector(makeCall(_:)), for: .touchUpInside)
        cell.nameLabel.text = selectedArray?[indexPath.section].domainId
        
        cell.hour1Lbl.text = (selectedArray?[indexPath.section].dates[0].hrs ?? "") + "hrs"
        cell.hour2Lbl.text =  (selectedArray?[indexPath.section].dates[1].hrs ?? "") + "hrs"
        cell.hour3Lbl.text =  (selectedArray?[indexPath.section].dates[2].hrs ?? "") + "hrs"
        cell.hour4Lbl.text =  (selectedArray?[indexPath.section].dates[3].hrs ?? "") + "hrs"
        cell.hour5Lbl.text =  (selectedArray?[indexPath.section].dates[4].hrs ?? "") + "hrs"
        cell.hour6Lbl.text =  (selectedArray?[indexPath.section].dates[5].hrs ?? "") + "hrs"
        cell.hour7Lbl.text =  (selectedArray?[indexPath.section].dates[6].hrs ?? "") + "hrs"
        
        cell.date1Lbl.text = (selectedArray?[indexPath.section].dates[0].dates ?? "")
        cell.date2Lbl.text =  (selectedArray?[indexPath.section].dates[1].dates ?? "")
        cell.date3Lbl.text =  (selectedArray?[indexPath.section].dates[2].dates ?? "")
        cell.date4Lbl.text =  (selectedArray?[indexPath.section].dates[3].dates ?? "")
        cell.date5Lbl.text =  (selectedArray?[indexPath.section].dates[4].dates ?? "")
        cell.date6Lbl.text =  (selectedArray?[indexPath.section].dates[5].dates ?? "")
        cell.date7Lbl.text =  (selectedArray?[indexPath.section].dates[6].dates ?? "") 
        
        cell.status1.backgroundColor = (selectedArray?[indexPath.section].dates[0].status == "1") ? UIColor.red : UIColor.darkGray
        cell.status2.backgroundColor = (selectedArray?[indexPath.section].dates[1].status == "1") ? UIColor.red : UIColor.darkGray
        cell.status3.backgroundColor = (selectedArray?[indexPath.section].dates[2].status == "1") ? UIColor.red : UIColor.darkGray
        cell.status4.backgroundColor = (selectedArray?[indexPath.section].dates[3].status == "1") ? UIColor.red : UIColor.darkGray
        cell.status5.backgroundColor = (selectedArray?[indexPath.section].dates[4].status == "1") ? UIColor.red : UIColor.darkGray
        cell.status6.backgroundColor = (selectedArray?[indexPath.section].dates[5].status == "1") ? UIColor.red : UIColor.darkGray
       
       cell.status7.backgroundColor = (selectedArray?[indexPath.section].dates[6].status == "1") ? UIColor.red : UIColor.darkGray
        
       
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return selectedArray?.count ?? 0
        return self.selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
