//
//  DelinquentDetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class DelinquentDetailPresenter:BasePresenter, DelinquentDetailPresenterProtocol, DelinquentDetailInteractorOutputProtocol {
   
    
    

    // MARK: Variables
    weak var view: DelinquentDetailViewProtocol?
    var interactor: DelinquentDetailInteractorInputProtocol?
    var wireFrame: DelinquentDetailWireFrameProtocol?
    let stringsTableName = "DelinquentDetail"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
       func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
     func requestData(domainName:String,platformName:String)
    {
        self.interactor?.requestData(domainName:domainName,platformName:platformName)
    }
    func reloadData(detail:EmployeeModel)
    {
        self.view?.reloadData(detail:detail)
    }
    
    
    func sendReminderRequest(employlist:EmployeeList)
    {
        self.interactor?.sendReminderRequest(employlist:employlist)
    }
    
    func reloadSendReminder(detail:SendReminderModel)
    {
        self.view?.reloadSendReminder(detail:detail)
    }
    
}
