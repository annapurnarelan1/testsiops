//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EmployeeModel : NSObject, NSCoding{

	var domainList : AnyObject!
	var employeeList : [EmployeeList]!
	var mainList : AnyObject!
	var platformList : AnyObject!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		domainList = dictionary["domainList"] as? AnyObject
		employeeList = [EmployeeList]()
		if let employeeListArray = dictionary["employeeList"] as? [[String:Any]]{
			for dic in employeeListArray{
				let value = EmployeeList(fromDictionary: dic)
				employeeList.append(value)
			}
		}
		mainList = dictionary["mainList"] as? AnyObject
		platformList = dictionary["platformList"] as? AnyObject
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if domainList != nil{
			dictionary["domainList"] = domainList
		}
		if employeeList != nil{
			var dictionaryElements = [[String:Any]]()
			for employeeListElement in employeeList {
				dictionaryElements.append(employeeListElement.toDictionary())
			}
			dictionary["employeeList"] = dictionaryElements
		}
		if mainList != nil{
			dictionary["mainList"] = mainList
		}
		if platformList != nil{
			dictionary["platformList"] = platformList
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         domainList = aDecoder.decodeObject(forKey: "domainList") as? AnyObject
         employeeList = aDecoder.decodeObject(forKey :"employeeList") as? [EmployeeList]
         mainList = aDecoder.decodeObject(forKey: "mainList") as? AnyObject
         platformList = aDecoder.decodeObject(forKey: "platformList") as? AnyObject
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if domainList != nil{
			aCoder.encode(domainList, forKey: "domainList")
		}
		if employeeList != nil{
			aCoder.encode(employeeList, forKey: "employeeList")
		}
		if mainList != nil{
			aCoder.encode(mainList, forKey: "mainList")
		}
		if platformList != nil{
			aCoder.encode(platformList, forKey: "platformList")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
