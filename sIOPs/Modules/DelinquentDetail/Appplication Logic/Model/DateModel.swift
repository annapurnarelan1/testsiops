//
//	Date.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DateModel : NSObject, NSCoding{

	var dates : String!
	var hrs : String!
	var status : String!
	var swapStatus : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		dates = dictionary["dates"] as? String
		hrs = dictionary["hrs"] as? String
		status = dictionary["status"] as? String
		swapStatus = dictionary["swapStatus"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if dates != nil{
			dictionary["dates"] = dates
		}
		if hrs != nil{
			dictionary["hrs"] = hrs
		}
		if status != nil{
			dictionary["status"] = status
		}
		if swapStatus != nil{
			dictionary["swapStatus"] = swapStatus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dates = aDecoder.decodeObject(forKey: "dates") as? String
         hrs = aDecoder.decodeObject(forKey: "hrs") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         swapStatus = aDecoder.decodeObject(forKey: "swapStatus") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if dates != nil{
			aCoder.encode(dates, forKey: "dates")
		}
		if hrs != nil{
			aCoder.encode(hrs, forKey: "hrs")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if swapStatus != nil{
			aCoder.encode(swapStatus, forKey: "swapStatus")
		}

	}

}
