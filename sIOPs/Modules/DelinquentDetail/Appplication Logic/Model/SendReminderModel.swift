//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SendReminderModel : NSObject, NSCoding{

	var ackBy : AnyObject!
	var ackCode : AnyObject!
	var ackDescription : AnyObject!
	var ackName : AnyObject!
	var ackRequired : AnyObject!
	var ackStatus : Int!
	var channel : AnyObject!
	var channelType : AnyObject!
	var count : Int!
	var createdBy : AnyObject!
	var currentNotificationCount : Int!
	var domainGroupId : AnyObject!
	var domainId : String!
	var fcmAuthKey : AnyObject!
	var fcmUrl : AnyObject!
	var fcmUserDeviceKey : AnyObject!
	var featureCategory : AnyObject!
	var featureId : Int!
	var featureName : AnyObject!
	var functionId : Int!
	var functionName : AnyObject!
	var id : Int!
	var insertTime : AnyObject!
	var message : String!
	var notifyReadStatus : Int!
	var responseMessage : AnyObject!
	var responseStatus : AnyObject!
	var sendReminderCode : String!
	var source : AnyObject!
	var status : Int!
	var title : String!
	var toggleView : AnyObject!
	var userName : String!
    var apkInstallationStatus : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		ackBy = dictionary["ackBy"] as? AnyObject
		ackCode = dictionary["ackCode"] as? AnyObject
		ackDescription = dictionary["ackDescription"] as? AnyObject
		ackName = dictionary["ackName"] as? AnyObject
		ackRequired = dictionary["ackRequired"] as? AnyObject
		ackStatus = dictionary["ackStatus"] as? Int
		channel = dictionary["channel"] as? AnyObject
		channelType = dictionary["channelType"] as? AnyObject
		count = dictionary["count"] as? Int
		createdBy = dictionary["createdBy"] as? AnyObject
		currentNotificationCount = dictionary["currentNotificationCount"] as? Int
		domainGroupId = dictionary["domainGroupId"] as? AnyObject
		domainId = dictionary["domainId"] as? String
		fcmAuthKey = dictionary["fcmAuthKey"] as? AnyObject
		fcmUrl = dictionary["fcmUrl"] as? AnyObject
		fcmUserDeviceKey = dictionary["fcmUserDeviceKey"] as? AnyObject
		featureCategory = dictionary["featureCategory"] as? AnyObject
		featureId = dictionary["featureId"] as? Int
		featureName = dictionary["featureName"] as? AnyObject
		functionId = dictionary["functionId"] as? Int
		functionName = dictionary["functionName"] as? AnyObject
		id = dictionary["id"] as? Int
		insertTime = dictionary["insertTime"] as? AnyObject
		message = dictionary["message"] as? String
		notifyReadStatus = dictionary["notifyReadStatus"] as? Int
		responseMessage = dictionary["responseMessage"] as? AnyObject
		responseStatus = dictionary["responseStatus"] as? AnyObject
		sendReminderCode = dictionary["sendReminderCode"] as? String
		source = dictionary["source"] as? AnyObject
		status = dictionary["status"] as? Int
		title = dictionary["title"] as? String
		toggleView = dictionary["toggleView"] as? AnyObject
		userName = dictionary["userName"] as? String
        apkInstallationStatus = dictionary["apkInstallationStatus"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if ackBy != nil{
			dictionary["ackBy"] = ackBy
		}
		if ackCode != nil{
			dictionary["ackCode"] = ackCode
		}
		if ackDescription != nil{
			dictionary["ackDescription"] = ackDescription
		}
		if ackName != nil{
			dictionary["ackName"] = ackName
		}
		if ackRequired != nil{
			dictionary["ackRequired"] = ackRequired
		}
		if ackStatus != nil{
			dictionary["ackStatus"] = ackStatus
		}
		if channel != nil{
			dictionary["channel"] = channel
		}
		if channelType != nil{
			dictionary["channelType"] = channelType
		}
		if count != nil{
			dictionary["count"] = count
		}
		if createdBy != nil{
			dictionary["createdBy"] = createdBy
		}
		if currentNotificationCount != nil{
			dictionary["currentNotificationCount"] = currentNotificationCount
		}
		if domainGroupId != nil{
			dictionary["domainGroupId"] = domainGroupId
		}
		if domainId != nil{
			dictionary["domainId"] = domainId
		}
		if fcmAuthKey != nil{
			dictionary["fcmAuthKey"] = fcmAuthKey
		}
		if fcmUrl != nil{
			dictionary["fcmUrl"] = fcmUrl
		}
		if fcmUserDeviceKey != nil{
			dictionary["fcmUserDeviceKey"] = fcmUserDeviceKey
		}
		if featureCategory != nil{
			dictionary["featureCategory"] = featureCategory
		}
		if featureId != nil{
			dictionary["featureId"] = featureId
		}
		if featureName != nil{
			dictionary["featureName"] = featureName
		}
		if functionId != nil{
			dictionary["functionId"] = functionId
		}
		if functionName != nil{
			dictionary["functionName"] = functionName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if insertTime != nil{
			dictionary["insertTime"] = insertTime
		}
		if message != nil{
			dictionary["message"] = message
		}
		if notifyReadStatus != nil{
			dictionary["notifyReadStatus"] = notifyReadStatus
		}
		if responseMessage != nil{
			dictionary["responseMessage"] = responseMessage
		}
		if responseStatus != nil{
			dictionary["responseStatus"] = responseStatus
		}
		if sendReminderCode != nil{
			dictionary["sendReminderCode"] = sendReminderCode
		}
		if source != nil{
			dictionary["source"] = source
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if toggleView != nil{
			dictionary["toggleView"] = toggleView
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ackBy = aDecoder.decodeObject(forKey: "ackBy") as? AnyObject
         ackCode = aDecoder.decodeObject(forKey: "ackCode") as? AnyObject
         ackDescription = aDecoder.decodeObject(forKey: "ackDescription") as? AnyObject
         ackName = aDecoder.decodeObject(forKey: "ackName") as? AnyObject
         ackRequired = aDecoder.decodeObject(forKey: "ackRequired") as? AnyObject
         ackStatus = aDecoder.decodeObject(forKey: "ackStatus") as? Int
         channel = aDecoder.decodeObject(forKey: "channel") as? AnyObject
         channelType = aDecoder.decodeObject(forKey: "channelType") as? AnyObject
         count = aDecoder.decodeObject(forKey: "count") as? Int
         createdBy = aDecoder.decodeObject(forKey: "createdBy") as? AnyObject
         currentNotificationCount = aDecoder.decodeObject(forKey: "currentNotificationCount") as? Int
         domainGroupId = aDecoder.decodeObject(forKey: "domainGroupId") as? AnyObject
         domainId = aDecoder.decodeObject(forKey: "domainId") as? String
         fcmAuthKey = aDecoder.decodeObject(forKey: "fcmAuthKey") as? AnyObject
         fcmUrl = aDecoder.decodeObject(forKey: "fcmUrl") as? AnyObject
         fcmUserDeviceKey = aDecoder.decodeObject(forKey: "fcmUserDeviceKey") as? AnyObject
         featureCategory = aDecoder.decodeObject(forKey: "featureCategory") as? AnyObject
         featureId = aDecoder.decodeObject(forKey: "featureId") as? Int
         featureName = aDecoder.decodeObject(forKey: "featureName") as? AnyObject
         functionId = aDecoder.decodeObject(forKey: "functionId") as? Int
         functionName = aDecoder.decodeObject(forKey: "functionName") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? Int
         insertTime = aDecoder.decodeObject(forKey: "insertTime") as? AnyObject
         message = aDecoder.decodeObject(forKey: "message") as? String
         notifyReadStatus = aDecoder.decodeObject(forKey: "notifyReadStatus") as? Int
         responseMessage = aDecoder.decodeObject(forKey: "responseMessage") as? AnyObject
         responseStatus = aDecoder.decodeObject(forKey: "responseStatus") as? AnyObject
         sendReminderCode = aDecoder.decodeObject(forKey: "sendReminderCode") as? String
         source = aDecoder.decodeObject(forKey: "source") as? AnyObject
         status = aDecoder.decodeObject(forKey: "status") as? Int
         title = aDecoder.decodeObject(forKey: "title") as? String
         toggleView = aDecoder.decodeObject(forKey: "toggleView") as? AnyObject
         userName = aDecoder.decodeObject(forKey: "userName") as? String
        apkInstallationStatus = aDecoder.decodeObject(forKey: "apkInstallationStatus") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if ackBy != nil{
			aCoder.encode(ackBy, forKey: "ackBy")
		}
		if ackCode != nil{
			aCoder.encode(ackCode, forKey: "ackCode")
		}
		if ackDescription != nil{
			aCoder.encode(ackDescription, forKey: "ackDescription")
		}
		if ackName != nil{
			aCoder.encode(ackName, forKey: "ackName")
		}
		if ackRequired != nil{
			aCoder.encode(ackRequired, forKey: "ackRequired")
		}
		if ackStatus != nil{
			aCoder.encode(ackStatus, forKey: "ackStatus")
		}
		if channel != nil{
			aCoder.encode(channel, forKey: "channel")
		}
		if channelType != nil{
			aCoder.encode(channelType, forKey: "channelType")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "createdBy")
		}
		if currentNotificationCount != nil{
			aCoder.encode(currentNotificationCount, forKey: "currentNotificationCount")
		}
		if domainGroupId != nil{
			aCoder.encode(domainGroupId, forKey: "domainGroupId")
		}
		if domainId != nil{
			aCoder.encode(domainId, forKey: "domainId")
		}
		if fcmAuthKey != nil{
			aCoder.encode(fcmAuthKey, forKey: "fcmAuthKey")
		}
		if fcmUrl != nil{
			aCoder.encode(fcmUrl, forKey: "fcmUrl")
		}
		if fcmUserDeviceKey != nil{
			aCoder.encode(fcmUserDeviceKey, forKey: "fcmUserDeviceKey")
		}
		if featureCategory != nil{
			aCoder.encode(featureCategory, forKey: "featureCategory")
		}
		if featureId != nil{
			aCoder.encode(featureId, forKey: "featureId")
		}
		if featureName != nil{
			aCoder.encode(featureName, forKey: "featureName")
		}
		if functionId != nil{
			aCoder.encode(functionId, forKey: "functionId")
		}
		if functionName != nil{
			aCoder.encode(functionName, forKey: "functionName")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if insertTime != nil{
			aCoder.encode(insertTime, forKey: "insertTime")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if notifyReadStatus != nil{
			aCoder.encode(notifyReadStatus, forKey: "notifyReadStatus")
		}
		if responseMessage != nil{
			aCoder.encode(responseMessage, forKey: "responseMessage")
		}
		if responseStatus != nil{
			aCoder.encode(responseStatus, forKey: "responseStatus")
		}
		if sendReminderCode != nil{
			aCoder.encode(sendReminderCode, forKey: "sendReminderCode")
		}
		if source != nil{
			aCoder.encode(source, forKey: "source")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if toggleView != nil{
			aCoder.encode(toggleView, forKey: "toggleView")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}
        if apkInstallationStatus != nil{
            aCoder.encode(apkInstallationStatus, forKey: "apkInstallationStatus")
        }

	}

}
