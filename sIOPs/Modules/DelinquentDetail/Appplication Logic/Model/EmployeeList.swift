//
//	EmployeeList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EmployeeList : NSObject, NSCoding{

	var dates : [DateModel]!
	var delinquentCount : Int!
	var domainId : String!
	var swapStatus : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		dates = [DateModel]()
		if let datesArray = dictionary["dates"] as? [[String:Any]]{
			for dic in datesArray{
				let value = DateModel(fromDictionary: dic)
				dates.append(value)
			}
		}
		delinquentCount = dictionary["delinquentCount"] as? Int
		domainId = dictionary["domainId"] as? String
		swapStatus = dictionary["swapStatus"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if dates != nil{
			var dictionaryElements = [[String:Any]]()
			for datesElement in dates {
				dictionaryElements.append(datesElement.toDictionary())
			}
			dictionary["dates"] = dictionaryElements
		}
		if delinquentCount != nil{
			dictionary["delinquentCount"] = delinquentCount
		}
		if domainId != nil{
			dictionary["domainId"] = domainId
		}
		if swapStatus != nil{
			dictionary["swapStatus"] = swapStatus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dates = aDecoder.decodeObject(forKey :"dates") as? [DateModel]
         delinquentCount = aDecoder.decodeObject(forKey: "delinquentCount") as? Int
         domainId = aDecoder.decodeObject(forKey: "domainId") as? String
         swapStatus = aDecoder.decodeObject(forKey: "swapStatus") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if dates != nil{
			aCoder.encode(dates, forKey: "dates")
		}
		if delinquentCount != nil{
			aCoder.encode(delinquentCount, forKey: "delinquentCount")
		}
		if domainId != nil{
			aCoder.encode(domainId, forKey: "domainId")
		}
		if swapStatus != nil{
			aCoder.encode(swapStatus, forKey: "swapStatus")
		}

	}

}
