//
//  DelinquentDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol DelinquentDetailViewProtocol: class {
    var presenter: DelinquentDetailPresenterProtocol? { get set }
    //func DelinquentDetailDone(DelinquentDetailRes :DelinquentDetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(detail:EmployeeModel)
    func reloadSendReminder(detail:SendReminderModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol DelinquentDetailWireFrameProtocol: class {
    static func presentDelinquentDetailModule(fromView:AnyObject,domainName:String,platformDomain:String,ngoResponse:NGO,count:String)

}

/// Method contract between VIEW -> PRESENTER
protocol DelinquentDetailPresenterProtocol: class {
    var view: DelinquentDetailViewProtocol? { get set }
    var interactor: DelinquentDetailInteractorInputProtocol? { get set }
    var wireFrame: DelinquentDetailWireFrameProtocol? { get set }
    
     func requestData(domainName:String,platformName:String)
    func sendReminderRequest(employlist:EmployeeList)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol DelinquentDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:EmployeeModel)
    func reloadSendReminder(detail:SendReminderModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol DelinquentDetailInteractorInputProtocol: class
{
    var presenter: DelinquentDetailInteractorOutputProtocol? { get set }
    func requestData(domainName:String,platformName:String)
    func sendReminderRequest(employlist:EmployeeList)
    
}
