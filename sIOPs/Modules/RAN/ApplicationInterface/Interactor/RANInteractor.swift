//
//  RANInteractor.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//




import Foundation

class RANInteractor: RANInteractorInputProtocol {
    
    
    
    public enum RANError {
        case internetError(String)
        case serverMessage(String)
    }
    
    // MARK: Variables
    weak var presenter: RANInteractorOutputProtocol?
    
    
    func requestAvailableData(application:LoginApplication){
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "appRoleCode":"800",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        
        let  busicode = Constants.BusiCode.AvailabilityRAN
        
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let detail = AvailabilityModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                    self.presenter?.reloadAvailabilityData(response: detail)
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.presenter?.showAvailabilityError(error:message)
                default:
                    self.presenter?.showAvailabilityError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showAvailabilityError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showAvailabilityError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showAvailabilityError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showAvailabilityError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestPerformanceData(application:LoginApplication){
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
             "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
             "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
             "osType": "ios",
             "lang": "en_US",
             "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "appRoleCode":"800",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
        
        
        let  busicode = Constants.BusiCode.PerformanceRAN
        
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                    let detail = PerformanceModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                    self.presenter?.reloadPerformanecData(response: detail)
                case Constants.status.NOK:
                    let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                    self.presenter?.showPerformanceError(error:message)
                default:
                    self.presenter?.showPerformanceError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showPerformanceError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showPerformanceError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showPerformanceError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showPerformanceError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestWorkOrderData(application:LoginApplication){
           
           
           let pubInfo: [String : Any] =
               ["timestamp": String(Date().ticks),
                "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
                "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
                "osType": "ios",
                "lang": "en_US",
                "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
           
           let requestHeader:[String :Any] = [
               "serviceName": "userInfo"
           ]
           
           let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               "appRoleCode":"800",
               "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
               "type": "userInfo",
               
               
           ]
           
          let busiParams: [String : Any] = [
               "requestHeader": requestHeader,
               "requestBody": requestBody,
           ]
           
           
           let  busicode = Constants.BusiCode.WorkOrder
           
           
           let requestList: [String: Any] = [
               "busiCode": busicode,
               "isEncrypt": false,
               "transactionId": "0001574162779054",
               "busiParams":busiParams
           ]
           
           let jsonParameter = [
               "pubInfo" : pubInfo,
               "requestList" : [requestList]
               ] as [String : Any]
           
           APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
               switch result {
               case .success(let returnJson) :
                   print(returnJson)
                   
                   
                   let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                   
                   switch status {
                   case Constants.status.OK:
                       let detail = WorkOrderModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                       self.presenter?.reloadWorkOrderData(response:detail)
                   case Constants.status.NOK:
                       let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                       self.presenter?.showPerformanceError(error:message)
                   default:
                       self.presenter?.showWorkOrderError(error: APIManager.NetworkError.unKnownError.rawValue)
                   }
                   
                   
                   
                   
               case .failure(let failure) :
                   switch failure {
                   case .connectionError:
                       self.presenter?.showWorkOrderError(error:APIManager.NetworkError.internetError.rawValue)
                       
                   case .authorizationError(let errorJson):
                       self.presenter?.showWorkOrderError(error: APIManager.NetworkError.unKnownError.rawValue)
                   case .serverError:
                       self.presenter?.showWorkOrderError(error: APIManager.NetworkError.unKnownError.rawValue)
                   case .newUpdate:
                       self.presenter?.stopLoader()
                   default:
                       self.presenter?.showWorkOrderError(error:APIManager.NetworkError.unKnownError.rawValue)
                       
                   }
               }
           })
           
       }
    
    
}
