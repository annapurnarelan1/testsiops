//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class WorkOrderModel : NSObject, NSCoding{

	var deployment : Int!
	var operations : Int!
	var performance : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		deployment = dictionary["deployment"] as? Int
		operations = dictionary["operations"] as? Int
		performance = dictionary["performance"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if deployment != nil{
			dictionary["deployment"] = deployment
		}
		if operations != nil{
			dictionary["operations"] = operations
		}
		if performance != nil{
			dictionary["performance"] = performance
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         deployment = aDecoder.decodeObject(forKey: "deployment") as? Int
         operations = aDecoder.decodeObject(forKey: "operations") as? Int
         performance = aDecoder.decodeObject(forKey: "performance") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if deployment != nil{
			aCoder.encode(deployment, forKey: "deployment")
		}
		if operations != nil{
			aCoder.encode(operations, forKey: "operations")
		}
		if performance != nil{
			aCoder.encode(performance, forKey: "performance")
		}

	}

}
