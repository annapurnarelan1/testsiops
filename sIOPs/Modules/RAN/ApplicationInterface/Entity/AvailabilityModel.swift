//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AvailabilityModel : NSObject, NSCoding{

	var avalMins : Double!
	var avalMinsUnit : String!
	var avalPercent : Double!
	var unAvalMins : Int!
	var unAvalMinsUnit : String!
	var unAvalPercent : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		avalMins = dictionary["avalMins"] as? Double
		avalMinsUnit = dictionary["avalMinsUnit"] as? String
		avalPercent = dictionary["avalPercent"] as? Double
		unAvalMins = dictionary["unAvalMins"] as? Int
		unAvalMinsUnit = dictionary["unAvalMinsUnit"] as? String
		unAvalPercent = dictionary["unAvalPercent"] as? Double
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if avalMins != nil{
			dictionary["avalMins"] = avalMins
		}
		if avalMinsUnit != nil{
			dictionary["avalMinsUnit"] = avalMinsUnit
		}
		if avalPercent != nil{
			dictionary["avalPercent"] = avalPercent
		}
		if unAvalMins != nil{
			dictionary["unAvalMins"] = unAvalMins
		}
		if unAvalMinsUnit != nil{
			dictionary["unAvalMinsUnit"] = unAvalMinsUnit
		}
		if unAvalPercent != nil{
			dictionary["unAvalPercent"] = unAvalPercent
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         avalMins = aDecoder.decodeObject(forKey: "avalMins") as? Double
         avalMinsUnit = aDecoder.decodeObject(forKey: "avalMinsUnit") as? String
         avalPercent = aDecoder.decodeObject(forKey: "avalPercent") as? Double
         unAvalMins = aDecoder.decodeObject(forKey: "unAvalMins") as? Int
         unAvalMinsUnit = aDecoder.decodeObject(forKey: "unAvalMinsUnit") as? String
         unAvalPercent = aDecoder.decodeObject(forKey: "unAvalPercent") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if avalMins != nil{
			aCoder.encode(avalMins, forKey: "avalMins")
		}
		if avalMinsUnit != nil{
			aCoder.encode(avalMinsUnit, forKey: "avalMinsUnit")
		}
		if avalPercent != nil{
			aCoder.encode(avalPercent, forKey: "avalPercent")
		}
		if unAvalMins != nil{
			aCoder.encode(unAvalMins, forKey: "unAvalMins")
		}
		if unAvalMinsUnit != nil{
			aCoder.encode(unAvalMinsUnit, forKey: "unAvalMinsUnit")
		}
		if unAvalPercent != nil{
			aCoder.encode(unAvalPercent, forKey: "unAvalPercent")
		}

	}

}
