//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PerformanceModel : NSObject, NSCoding{

	var cellThroughput : Double!
	var cellThroughputUnit : String!
	var consumptionData : Double!
	var consumptionDataUnit : String!
	var consumptionVoice : Int!
	var consumptionVoiceUnit : String!
	var dcr : Double!
	var dead : Int!
	var deadUnit : String!
	var healthy : Double!
	var healthyUnit : String!
	var hosp : Int!
	var hospUnit : String!
	var icu : Int!
	var icuUnit : String!
	var ipThroughput : Double!
	var ipThroughputUnit : String!
	var mcr : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		cellThroughput = dictionary["cellThroughput"] as? Double
		cellThroughputUnit = dictionary["cellThroughputUnit"] as? String
		consumptionData = dictionary["consumptionData"] as? Double
		consumptionDataUnit = dictionary["consumptionDataUnit"] as? String
		consumptionVoice = dictionary["consumptionVoice"] as? Int
		consumptionVoiceUnit = dictionary["consumptionVoiceUnit"] as? String
		dcr = dictionary["dcr"] as? Double
		dead = dictionary["dead"] as? Int
		deadUnit = dictionary["deadUnit"] as? String
		healthy = dictionary["healthy"] as? Double
		healthyUnit = dictionary["healthyUnit"] as? String
		hosp = dictionary["hosp"] as? Int
		hospUnit = dictionary["hospUnit"] as? String
		icu = dictionary["icu"] as? Int
		icuUnit = dictionary["icuUnit"] as? String
		ipThroughput = dictionary["ipThroughput"] as? Double
		ipThroughputUnit = dictionary["ipThroughputUnit"] as? String
		mcr = dictionary["mcr"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cellThroughput != nil{
			dictionary["cellThroughput"] = cellThroughput
		}
		if cellThroughputUnit != nil{
			dictionary["cellThroughputUnit"] = cellThroughputUnit
		}
		if consumptionData != nil{
			dictionary["consumptionData"] = consumptionData
		}
		if consumptionDataUnit != nil{
			dictionary["consumptionDataUnit"] = consumptionDataUnit
		}
		if consumptionVoice != nil{
			dictionary["consumptionVoice"] = consumptionVoice
		}
		if consumptionVoiceUnit != nil{
			dictionary["consumptionVoiceUnit"] = consumptionVoiceUnit
		}
		if dcr != nil{
			dictionary["dcr"] = dcr
		}
		if dead != nil{
			dictionary["dead"] = dead
		}
		if deadUnit != nil{
			dictionary["deadUnit"] = deadUnit
		}
		if healthy != nil{
			dictionary["healthy"] = healthy
		}
		if healthyUnit != nil{
			dictionary["healthyUnit"] = healthyUnit
		}
		if hosp != nil{
			dictionary["hosp"] = hosp
		}
		if hospUnit != nil{
			dictionary["hospUnit"] = hospUnit
		}
		if icu != nil{
			dictionary["icu"] = icu
		}
		if icuUnit != nil{
			dictionary["icuUnit"] = icuUnit
		}
		if ipThroughput != nil{
			dictionary["ipThroughput"] = ipThroughput
		}
		if ipThroughputUnit != nil{
			dictionary["ipThroughputUnit"] = ipThroughputUnit
		}
		if mcr != nil{
			dictionary["mcr"] = mcr
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cellThroughput = aDecoder.decodeObject(forKey: "cellThroughput") as? Double
         cellThroughputUnit = aDecoder.decodeObject(forKey: "cellThroughputUnit") as? String
         consumptionData = aDecoder.decodeObject(forKey: "consumptionData") as? Double
         consumptionDataUnit = aDecoder.decodeObject(forKey: "consumptionDataUnit") as? String
         consumptionVoice = aDecoder.decodeObject(forKey: "consumptionVoice") as? Int
         consumptionVoiceUnit = aDecoder.decodeObject(forKey: "consumptionVoiceUnit") as? String
         dcr = aDecoder.decodeObject(forKey: "dcr") as? Double
         dead = aDecoder.decodeObject(forKey: "dead") as? Int
         deadUnit = aDecoder.decodeObject(forKey: "deadUnit") as? String
         healthy = aDecoder.decodeObject(forKey: "healthy") as? Double
         healthyUnit = aDecoder.decodeObject(forKey: "healthyUnit") as? String
         hosp = aDecoder.decodeObject(forKey: "hosp") as? Int
         hospUnit = aDecoder.decodeObject(forKey: "hospUnit") as? String
         icu = aDecoder.decodeObject(forKey: "icu") as? Int
         icuUnit = aDecoder.decodeObject(forKey: "icuUnit") as? String
         ipThroughput = aDecoder.decodeObject(forKey: "ipThroughput") as? Double
         ipThroughputUnit = aDecoder.decodeObject(forKey: "ipThroughputUnit") as? String
         mcr = aDecoder.decodeObject(forKey: "mcr") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cellThroughput != nil{
			aCoder.encode(cellThroughput, forKey: "cellThroughput")
		}
		if cellThroughputUnit != nil{
			aCoder.encode(cellThroughputUnit, forKey: "cellThroughputUnit")
		}
		if consumptionData != nil{
			aCoder.encode(consumptionData, forKey: "consumptionData")
		}
		if consumptionDataUnit != nil{
			aCoder.encode(consumptionDataUnit, forKey: "consumptionDataUnit")
		}
		if consumptionVoice != nil{
			aCoder.encode(consumptionVoice, forKey: "consumptionVoice")
		}
		if consumptionVoiceUnit != nil{
			aCoder.encode(consumptionVoiceUnit, forKey: "consumptionVoiceUnit")
		}
		if dcr != nil{
			aCoder.encode(dcr, forKey: "dcr")
		}
		if dead != nil{
			aCoder.encode(dead, forKey: "dead")
		}
		if deadUnit != nil{
			aCoder.encode(deadUnit, forKey: "deadUnit")
		}
		if healthy != nil{
			aCoder.encode(healthy, forKey: "healthy")
		}
		if healthyUnit != nil{
			aCoder.encode(healthyUnit, forKey: "healthyUnit")
		}
		if hosp != nil{
			aCoder.encode(hosp, forKey: "hosp")
		}
		if hospUnit != nil{
			aCoder.encode(hospUnit, forKey: "hospUnit")
		}
		if icu != nil{
			aCoder.encode(icu, forKey: "icu")
		}
		if icuUnit != nil{
			aCoder.encode(icuUnit, forKey: "icuUnit")
		}
		if ipThroughput != nil{
			aCoder.encode(ipThroughput, forKey: "ipThroughput")
		}
		if ipThroughputUnit != nil{
			aCoder.encode(ipThroughputUnit, forKey: "ipThroughputUnit")
		}
		if mcr != nil{
			aCoder.encode(mcr, forKey: "mcr")
		}

	}

}
