//
//  RANPresenter.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class RANPresenter:BasePresenter, RANPresenterProtocol, RANInteractorOutputProtocol {
    
    
   
    // MARK: Variables
    weak var view: RANViewProtocol?
    var interactor: RANInteractorInputProtocol?
    var wireFrame: RANWireFrameProtocol?
    let stringsTableName = "RAN"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
    func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    
    
    
    func requestAvailableData(application:LoginApplication)
    {
        self.interactor?.requestAvailableData(application:application)
    }
    func requestPerformanceData(application:LoginApplication)
    {
        self.interactor?.requestPerformanceData(application:application)
    }
    
    
    func reloadAvailabilityData(response:AvailabilityModel)
    {
        self.view?.reloadAvailabilityData(response:response)
    }
    func reloadPerformanecData(response:PerformanceModel)
    {
        self.view?.reloadPerformanecData(response:response)
    }
    

    func showAvailabilityError(error:String)
    {
         self.view?.showAvailabilityError(error:error)
    }
    func showPerformanceError(error:String)
    {
        self.view?.showPerformanceError(error:error)
    }
    
    
     func requestWorkOrderData(application:LoginApplication)
     {
        self.interactor?.requestWorkOrderData(application:application)
    }
    
    func reloadWorkOrderData(response:WorkOrderModel)
    {
         self.view?.reloadWorkOrderData(response:response)
    }
   
     func showWorkOrderError(error:String)
     {
        self.view?.showWorkOrderError(error:error)
    }
}
