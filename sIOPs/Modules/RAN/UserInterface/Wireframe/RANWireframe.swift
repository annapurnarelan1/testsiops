//
//  RANWireframe.swift
//  sIOPs
//
//  Created by ASM ESPL on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class RANWireFrame: RANWireFrameProtocol {
    
    
    
    // MARK: RANWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
  class func presentRANModule(fromView:AnyObject,application:LoginApplication)  {

        // Generating module components
        let view: RANViewProtocol = RANViewController.instantiate()
        let presenter: RANPresenterProtocol & RANInteractorOutputProtocol = RANPresenter()
        let interactor: RANInteractorInputProtocol = RANInteractor()
       
        let wireFrame: RANWireFrameProtocol = RANWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! RANViewController
        viewController.application = application
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    

    // MARK: RANWireFrameProtocol
      
      /// Static method that initializes every class needed
      ///
      /// - Parameter fromView: default parameter
//    class func presentRANDetailModule(fromView:AnyObject,application:LoginApplication)  {
//
//          // Generating module components
//          let view: RANViewProtocol = RANViewController.instantiate()
//          let presenter: RANPresenterProtocol & RANInteractorOutputProtocol = RANPresenter()
//          let interactor: RANInteractorInputProtocol = RANInteractor()
//         
//          let wireFrame: RANWireFrameProtocol = RANWireFrame()
//
//          // Connecting
//          view.presenter = presenter
//          presenter.view = view
//          presenter.wireFrame = wireFrame
//          presenter.interactor = interactor
//          interactor.presenter = presenter
//          
//          let viewController = view as! RANViewController
//          viewController.application = application
//         
//          NavigationHelper.pushViewController(viewController: viewController)
//      }
   

}

