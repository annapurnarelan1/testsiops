//
//  RANViewController.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class RANViewController: BaseViewController,RANViewProtocol{
    
    var presenter: RANPresenterProtocol?
    @IBOutlet weak var RANCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    
    var btn = UIButton()
    var application:LoginApplication?
    var refresher:UIRefreshControl!
    var isFirstLoading = true
    var isSecondLoading = true
    var isThirdLoading = true
    var availabilityResponse:AvailabilityModel?
    var performanceResponse:PerformanceModel?
    var workOrderResponse:WorkOrderModel?
    var availableError = ""
    var performanceError = ""
    var workOrderError = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        RANCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        self.refresher = UIRefreshControl()
        self.RANCollectionView.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.init(red: 44, green: 88, blue: 156)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.RANCollectionView.addSubview(refresher)
        
        
        self.presenter?.requestAvailableData(application: application!)
        self.presenter?.requestPerformanceData(application: application!)
        self.presenter?.requestWorkOrderData(application: application!)
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    
    @objc func loadData() {
        // self.NGOCollectionView.refreshControl?.beginRefreshing()
        stopRefresher()
        availableError = ""
        performanceError = ""
        workOrderError = ""
        isFirstLoading = true
        isSecondLoading = true
        isThirdLoading = true
        RANCollectionView.reloadData()
        self.presenter?.requestAvailableData(application: application!)
        self.presenter?.requestPerformanceData(application: application!)
        self.presenter?.requestWorkOrderData(application: application!)
        //Call this to stop refresher
    }
    
    
    
    func reloadAvailabilityData(response:AvailabilityModel)
    {
        isFirstLoading = false
        self.availabilityResponse = response
        RANCollectionView.reloadData()
    }
    func reloadPerformanecData(response:PerformanceModel)
    {
        isSecondLoading = false
        self.performanceResponse = response
        RANCollectionView.reloadData()
    }
    
    func reloadWorkOrderData(response:WorkOrderModel)
    {
        isThirdLoading = false
        self.workOrderResponse = response
        RANCollectionView.reloadData()
    }
    
    
    func showAvailabilityError(error:String)
    {
        isFirstLoading = false
        availableError = error
        RANCollectionView.reloadData()
    }
    func showPerformanceError(error:String)
    {
        isSecondLoading = false
        performanceError = error
        RANCollectionView.reloadData()
    }
    func showWorkOrderError(error:String)
    {
        isThirdLoading = false
        workOrderError = error
        RANCollectionView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //RANCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    func stopRefresher() {
        refresher.endRefreshing()
    }
    
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> RANViewProtocol{
        return UIStoryboard(name: "RAN", bundle: nil).instantiateViewController(withIdentifier: "RAN") as! RANViewController
    }
    
    
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        
    }
    
    
    
    
}

extension RANViewController :UICollectionViewDataSource
{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        
        return 1
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.row == 0)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Availability", for: indexPath) as! AvailabilityCellCollectionViewCell
            
            if(isFirstLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.mainView.isHidden = true
                cell.loader.startAnimating()
                
            }
            else
            {
                if(availableError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = availableError
                    cell.mainView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.mainView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            
            
            
            cell.availabilityValue.setTitle("\(availabilityResponse?.avalPercent ?? 0.0)%", for: .normal)
            cell.availabilitySubLabel.text = "\(availabilityResponse?.avalMins ?? 0.0) \(availabilityResponse?.avalMinsUnit ?? "")"
            cell.unavailableTitleLabel.setTitle("\(availabilityResponse?.unAvalPercent ?? 0.0)%", for: .normal)
            cell.unavailableSubLabel.text = "\(availabilityResponse?.unAvalMins ?? 0) \(availabilityResponse?.unAvalMinsUnit ?? "")"
            //
            
            
            return cell
        }
        else  if(indexPath.row == 1)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Performance", for: indexPath) as! PerformanceCollectionViewCell
            
            
            if(isSecondLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.mainView.isHidden = true
                cell.loader.startAnimating()
                
                
                
            }
            else
            {
                if(performanceError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = performanceError
                    cell.mainView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.mainView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            
            
            cell.deadValueLabel.setTitle("\(performanceResponse?.dead ?? 0 ) \(performanceResponse?.deadUnit ?? "")", for: .normal)
            cell.icuValueLabel.setTitle("\(performanceResponse?.icu ?? 0) \(performanceResponse?.icuUnit ?? "")", for: .normal)
            cell.hospValueLabel.setTitle("\(performanceResponse?.hosp ?? 0) \(performanceResponse?.hospUnit ?? "")", for: .normal)
            cell.healthyValueLabel.setTitle("\(performanceResponse?.healthy ?? 0 ) \(performanceResponse?.hospUnit ?? "")", for: .normal)
            
            // let value = performanceResponse?.ipThroughput == 0 ? "-" :
            
            cell.ipValueLabel.setTitle("\(performanceResponse?.ipThroughput ?? 0) \(performanceResponse?.ipThroughputUnit ?? "")", for: .normal)
            cell.cellEffectiveValueLabel.setTitle("\(performanceResponse?.cellThroughput ?? 0) \(performanceResponse?.cellThroughputUnit ?? "")", for: .normal)
            cell.consumptionDataValueLabel.setTitle("\(performanceResponse?.consumptionData ?? 0) \(performanceResponse?.consumptionDataUnit ?? "")", for: .normal)
            cell.dcrValueLabel.setTitle("\(performanceResponse?.dcr ?? 0) %", for: .normal)
            
            let mcrVlaue = performanceResponse?.mcr == 0 ? "-" : "\(performanceResponse?.mcr ?? 0) %"
            cell.mcrValueLabel.setTitle(mcrVlaue, for: .normal)
            let consumptionValue = performanceResponse?.consumptionVoice == 0 ? "-" : "\(performanceResponse?.consumptionVoice ?? 0) \(performanceResponse?.consumptionVoiceUnit ?? "")"
            cell.consumptionVoiceValueLabel.setTitle(consumptionValue, for: .normal)
            
            
            
            
            
            
            
            
            return cell
        }
        else  if(indexPath.row == 2)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkOrder", for: indexPath) as! WorkOrderCollectionViewCell
            
            
            
            
            if(isThirdLoading)
            {
                
                cell.errorLabel.isHidden = true
                cell.mainView.isHidden = true
                cell.loader.startAnimating()
                
            }
            else
            {
                if(workOrderError != "")
                {
                    cell.errorLabel.isHidden = false
                    cell.errorLabel.text = workOrderError
                    cell.mainView.isHidden = true
                }
                else
                {
                    cell.errorLabel.isHidden = true
                    cell.mainView.isHidden = false
                }
                
                
                cell.loader.stopAnimating()
            }
            
            let performance =  "\(workOrderResponse?.performance ?? 0)"
            let deployment =  "\(workOrderResponse?.deployment ?? 0)"
            let operations = "\(workOrderResponse?.operations ?? 0)"
            cell.performanceValueLabel.setTitle(performance, for: .normal)
            cell.deploymentValueLabel.setTitle(deployment, for: .normal)
            cell.operationsValueLabel.setTitle(operations, for: .normal)
            
            
            return cell
        }
        
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
        
        
        
        
        
        headerView?.headerTitleLbl.text = "Radio Access Network"
        headerView?.alarmLbl.isHidden = true
        
        
        
        
        
        
        
        return headerView ?? UICollectionReusableView()
    }
    
    
}

extension RANViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        itemsPerRow = 1
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 1
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        
        if(indexPath.row == 0)
        {
            return CGSize(width: collectionView.frame.width - 20, height: 150)
        }
        else if(indexPath.row == 1)
        {
            return CGSize(width: collectionView.frame.width - 20, height: 240)
        }
        else
        {
            return CGSize(width: collectionView.frame.width - 20, height: 200)
        }
        
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        
        return CGSize(width: collectionView.frame.width, height:40)
        
        
        
    }
    
    
}
