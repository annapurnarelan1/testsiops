//
//  AvailabilityCellCollectionViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class AvailabilityCellCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var availabilityHeaderLabel: UILabel!
    @IBOutlet weak var impactedHeaderLabel: UILabel!
    @IBOutlet weak var availabilityTitle: UILabel!
    @IBOutlet weak var availabilityValue: UIButton!
    @IBOutlet weak var availabilitySubLabel: UILabel!
    @IBOutlet weak var unavailableTitle: UILabel!
    @IBOutlet weak var unavailableTitleLabel: UIButton!
    @IBOutlet weak var unavailableSubLabel: UILabel!
    @IBOutlet weak var unavailabilityHeaderLabel: UILabel!
    @IBOutlet weak var rrcLabel: UILabel!
    @IBOutlet weak var infraLabel: UILabel!
    @IBOutlet weak var ranLabel: UILabel!
    @IBOutlet weak var backHaulLabel: UILabel!
    @IBOutlet weak var unknownLabel: UILabel!
    @IBOutlet weak var infraValueLabel: UIButton!
    @IBOutlet weak var ranValueLabel: UIButton!
    @IBOutlet weak var backhaulValueLabel: UIButton!
    @IBOutlet weak var unknownValueLabel: UIButton!
    @IBOutlet weak var affectedRRCLabel: NSLayoutConstraint!
    @IBOutlet weak var infraSubTitle: UILabel!
    @IBOutlet weak var ranSubTitle: UILabel!
    @IBOutlet weak var backHaulSubTitle: UILabel!
    @IBOutlet weak var unknownSubTitle: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setUP()
        // Initialization code
    }
    
    
    
}
