//
//  WorkOrderCollectionViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class WorkOrderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var workOrderHeaderLabel: UILabel!
    @IBOutlet weak var IANLTitleLabel: UILabel!
    @IBOutlet weak var TRTTitleLabel: UILabel!
    @IBOutlet weak var FRTTitleLabel: UILabel!
    @IBOutlet weak var IANLValueLabel: UIButton!
    @IBOutlet weak var TRTValueLabel: UIButton!
    @IBOutlet weak var FRTValueLabel: UIButton!
    @IBOutlet weak var productivityHeaderLabel: UILabel!
    
    @IBOutlet weak var performanceTitleLabel: UILabel!
    @IBOutlet weak var deploymentTitleLabel: UILabel!
    @IBOutlet weak var operationsTitleLabel: UILabel!
    @IBOutlet weak var performanceValueLabel: UIButton!
    @IBOutlet weak var deploymentValueLabel: UIButton!
    @IBOutlet weak var operationsValueLabel: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func awakeFromNib() {
              super.awakeFromNib()
              
              self.setUP()
              // Initialization code
          }
    
}
