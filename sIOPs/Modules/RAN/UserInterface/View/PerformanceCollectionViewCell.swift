//
//  PerformanceCollectionViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class PerformanceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var performanceHeaderTitle: UILabel!
    @IBOutlet weak var cellDownHeaderTitle: UILabel!
    @IBOutlet weak var deadTitleLabel: UILabel!
    @IBOutlet weak var icuTitleLabel: UILabel!
    @IBOutlet weak var hospTitleLabel: UILabel!
    @IBOutlet weak var healthyTitleLabel: UILabel!
    @IBOutlet weak var deadValueLabel: UIButton!
    @IBOutlet weak var icuValueLabel: UIButton!
    @IBOutlet weak var hospValueLabel: UIButton!
    @IBOutlet weak var healthyValueLabel: UIButton!
    @IBOutlet weak var ipTitleLabel: UILabel!
    @IBOutlet weak var cellEffectiveTitleLabel: UILabel!
    @IBOutlet weak var consumptionDataTitleLabel: UILabel!
    @IBOutlet weak var ipValueLabel: UIButton!
    @IBOutlet weak var cellEffectiveValueLabel: UIButton!
    @IBOutlet weak var consumptionDataValueLabel: UIButton!
    @IBOutlet weak var dcrTitleLabel: UILabel!
    @IBOutlet weak var mcrTitleLabel: UILabel!
    @IBOutlet weak var consumptionVoiceTitleLabel: UILabel!
    @IBOutlet weak var dcrValueLabel: UIButton!
    @IBOutlet weak var mcrValueLabel: UIButton!
    @IBOutlet weak var consumptionVoiceValueLabel: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    

    override func awakeFromNib() {
           super.awakeFromNib()
           
           self.setUP()
           // Initialization code
       }


}
