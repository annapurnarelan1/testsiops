//
//  RANProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 23/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//




import Foundation


/// Method contract between PRESENTER -> VIEW
protocol RANViewProtocol: class {
    var presenter: RANPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadAvailabilityData(response:AvailabilityModel)
    func reloadPerformanecData(response:PerformanceModel)
    func showAvailabilityError(error:String)
    func showPerformanceError(error:String)
    func reloadWorkOrderData(response:WorkOrderModel)
     func showWorkOrderError(error:String)
    
}

/// Method contract between PRESENTER -> WIREFRAME
protocol RANWireFrameProtocol: class {
    static   func presentRANModule(fromView:AnyObject,application:LoginApplication)
    
    
}

/// Method contract between VIEW -> PRESENTER
protocol RANPresenterProtocol: class {
    var view: RANViewProtocol? { get set }
    var interactor: RANInteractorInputProtocol? { get set }
    var wireFrame: RANWireFrameProtocol? { get set }
    
    
    
    
    
    func requestAvailableData(application:LoginApplication)
    func requestPerformanceData(application:LoginApplication)
    func requestWorkOrderData(application:LoginApplication)
    
    
}

/// Method contract between INTERACTOR -> PRESENTER
protocol RANInteractorOutputProtocol: class {
    
    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func showAvailabilityError(error:String)
    func showPerformanceError(error:String)
    func reloadAvailabilityData(response:AvailabilityModel)
    func reloadPerformanecData(response:PerformanceModel)
    func reloadWorkOrderData(response:WorkOrderModel)
     func showWorkOrderError(error:String)
    
    
}

/// Method contract between PRESENTER -> INTERACTOR
protocol RANInteractorInputProtocol: class
{
    var presenter: RANInteractorOutputProtocol? { get set }
    func requestAvailableData(application:LoginApplication)
    func requestPerformanceData(application:LoginApplication)
    func requestWorkOrderData(application:LoginApplication)
    
    
}
