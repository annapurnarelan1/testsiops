//
//  OpenAlertsProtocol.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol OpenAlertsViewProtocol: class {
    var presenter: OpenAlertsPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    func reloadOpenYesterdayALerts(response:OpenAlertYesterdayModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol OpenAlertsWireFrameProtocol: class {
    static func presentOpenAlertsModule(fromView:AnyObject,type:String,selected:String,response:NGOOpenALertsModel)
    func presentYesterdayOpenAlertsModule(type:String,selected:String,response:OpenAlertYesterdayModel,date:String)
    func presentOpenAlertsHistoryModule(type:String,date:String,count: String, isFromYesterday: Bool , response:Any)
    func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
}

/// Method contract between VIEW -> PRESENTER
protocol OpenAlertsPresenterProtocol: class {
    var view: OpenAlertsViewProtocol? { get set }
    var interactor: OpenAlertsInteractorInputProtocol? { get set }
    var wireFrame: OpenAlertsWireFrameProtocol? { get set }
    func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
    func requestData(date:String)
    func presentYesterdayOpenAlertsModule(type:String,selected:String,response:OpenAlertYesterdayModel,date:String)
    func presentOpenAlertsHistoryModule(selectedHistoryType:String,date:String,count: String ,isFromYesterday: Bool,response:Any)
   // func requestData()
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol OpenAlertsInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
    func reloadOpenYesterdayALerts(response:OpenAlertYesterdayModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol OpenAlertsInteractorInputProtocol: class
{
    var presenter: OpenAlertsInteractorOutputProtocol? { get set }
    func requestData(date:String)
    
}
