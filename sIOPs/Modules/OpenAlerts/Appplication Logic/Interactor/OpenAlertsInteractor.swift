//
//  NGODetailDetailInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class OpenAlertsInteractor: OpenAlertsInteractorInputProtocol {

public enum OpenAlertsError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: OpenAlertsInteractorOutputProtocol?
    
    
    func requestData(date:String){
        
        
        let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
           // "appRoleCode": NGODetailParams.applicationCode ?? "",
           // "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            "appRoleCode": "726",
            "date": date
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
//        var busicode = ""
//        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGODetail")
//        {
//            busicode = Constants.BusiCode.NGODetailSummary
//        }
        
        let busicode = Constants.BusiCode.NGOOpenAlertHistory
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print("check data",returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK

                switch status {
                case Constants.status.OK:
                        let openAlertsYesterdaydata = OpenAlertYesterdayModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                        self.presenter?.reloadOpenYesterdayALerts(response: openAlertsYesterdaydata)
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                

           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
}
