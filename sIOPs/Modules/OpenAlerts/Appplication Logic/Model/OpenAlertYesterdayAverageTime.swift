//
//	AverageTime.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenAlertYesterdayAverageTime : NSObject, NSCoding{

	var featureName : String!
	var outlierCount : String!
    var value        : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		featureName = dictionary["featureName"] as? String
		outlierCount = dictionary["outlierCount"] as? String
        value  = dictionary["value"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if featureName != nil{
			dictionary["featureName"] = featureName
		}
		if outlierCount != nil{
			dictionary["outlierCount"] = outlierCount
		}
        if value != nil{
            dictionary["value"] = outlierCount
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         featureName = aDecoder.decodeObject(forKey: "featureName") as? String
         outlierCount = aDecoder.decodeObject(forKey: "outlierCount") as? String
         value = aDecoder.decodeObject(forKey: "value") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if featureName != nil{
			aCoder.encode(featureName, forKey: "featureName")
		}
		if outlierCount != nil{
			aCoder.encode(outlierCount, forKey: "outlierCount")
		}
        if value != nil{
            aCoder.encode(outlierCount, forKey: "value")
        }

	}

}
