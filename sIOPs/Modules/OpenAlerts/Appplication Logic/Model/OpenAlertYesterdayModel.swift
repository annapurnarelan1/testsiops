//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OpenAlertYesterdayModel : NSObject, NSCoding{

	var acknowledement : [OpenAlertYesterdayAcknowledement]!
	var acknowledementCount : Int!
	var acknowledementSLA : [OpenAlertYesterdayAcknowledement]!
	var acknowledementSLACount : Int!
	var acknowledged : AnyObject!
	var application : [OpenAlertYesterdayApplication]!
	var averageTime : [OpenAlertYesterdayAverageTime]!
	var closed : [OpenAlertYesterdayApplication]!
	var critical : Int!
	var engineer : [OpenAlertYesterdayApplication]!
	var fatal : Int!
	var infrastructure : [OpenAlertYesterdayApplication]!
	var met : [OpenAlertYesterdayApplication]!
	var missed : [OpenAlertYesterdayApplication]!
	var open : [OpenAlertYesterdayApplication]!
	var openAlertCount : Int!
	var openAlerts : [OpenAlertYesterdayAcknowledement]!
	var status : [OpenAlertYesterdayAcknowledement]!
	var statusCount : Int!
	var system : [OpenAlertYesterdayApplication]!
	var tools : [OpenAlertYesterdayApplication]!
	var unAcknowledged : [OpenAlertYesterdayApplication]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		acknowledement = [OpenAlertYesterdayAcknowledement]()
		if let acknowledementArray = dictionary["acknowledement"] as? [[String:Any]]{
			for dic in acknowledementArray{
				let value = OpenAlertYesterdayAcknowledement(fromDictionary: dic)
				acknowledement.append(value)
			}
		}
		acknowledementCount = dictionary["acknowledementCount"] as? Int
		acknowledementSLA = [OpenAlertYesterdayAcknowledement]()
		if let acknowledementSLAArray = dictionary["acknowledementSLA"] as? [[String:Any]]{
			for dic in acknowledementSLAArray{
				let value = OpenAlertYesterdayAcknowledement(fromDictionary: dic)
				acknowledementSLA.append(value)
			}
		}
		acknowledementSLACount = dictionary["acknowledementSLACount"] as? Int
		acknowledged = dictionary["acknowledged"] as? AnyObject
		application = [OpenAlertYesterdayApplication]()
		if let applicationArray = dictionary["application"] as? [[String:Any]]{
			for dic in applicationArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				application.append(value)
			}
		}
		averageTime = [OpenAlertYesterdayAverageTime]()
		if let averageTimeArray = dictionary["averageTime"] as? [[String:Any]]{
			for dic in averageTimeArray{
				let value = OpenAlertYesterdayAverageTime(fromDictionary: dic)
				averageTime.append(value)
			}
		}
		closed = [OpenAlertYesterdayApplication]()
		if let closedArray = dictionary["closed"] as? [[String:Any]]{
			for dic in closedArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				closed.append(value)
			}
		}
		critical = dictionary["critical"] as? Int
		engineer = [OpenAlertYesterdayApplication]()
		if let engineerArray = dictionary["engineer"] as? [[String:Any]]{
			for dic in engineerArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				engineer.append(value)
			}
		}
		fatal = dictionary["fatal"] as? Int
		infrastructure = [OpenAlertYesterdayApplication]()
		if let infrastructureArray = dictionary["infrastructure"] as? [[String:Any]]{
			for dic in infrastructureArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				infrastructure.append(value)
			}
		}
		met = [OpenAlertYesterdayApplication]()
		if let metArray = dictionary["met"] as? [[String:Any]]{
			for dic in metArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				met.append(value)
			}
		}
		missed = [OpenAlertYesterdayApplication]()
		if let missedArray = dictionary["missed"] as? [[String:Any]]{
			for dic in missedArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				missed.append(value)
			}
		}
		open = [OpenAlertYesterdayApplication]()
        if let missedArray = dictionary["open"] as? [[String:Any]]{
        for dic in missedArray{
            let value = OpenAlertYesterdayApplication(fromDictionary: dic)
            open.append(value)
        }
    }
        
		openAlertCount = dictionary["openAlertCount"] as? Int
		openAlerts = [OpenAlertYesterdayAcknowledement]()
		if let openAlertsArray = dictionary["openAlerts"] as? [[String:Any]]{
			for dic in openAlertsArray{
				let value = OpenAlertYesterdayAcknowledement(fromDictionary: dic)
				openAlerts.append(value)
			}
		}
		status = [OpenAlertYesterdayAcknowledement]()
		if let statusArray = dictionary["status"] as? [[String:Any]]{
			for dic in statusArray{
				let value = OpenAlertYesterdayAcknowledement(fromDictionary: dic)
				status.append(value)
			}
		}
		statusCount = dictionary["statusCount"] as? Int
		system = [OpenAlertYesterdayApplication]()
		if let systemArray = dictionary["system"] as? [[String:Any]]{
			for dic in systemArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				system.append(value)
			}
		}
		tools = [OpenAlertYesterdayApplication]()
		if let toolsArray = dictionary["tools"] as? [[String:Any]]{
			for dic in toolsArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				tools.append(value)
			}
		}
		unAcknowledged = [OpenAlertYesterdayApplication]()
		if let unAcknowledgedArray = dictionary["unAcknowledged"] as? [[String:Any]]{
			for dic in unAcknowledgedArray{
				let value = OpenAlertYesterdayApplication(fromDictionary: dic)
				unAcknowledged.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if acknowledement != nil{
			var dictionaryElements = [[String:Any]]()
			for acknowledementElement in acknowledement {
				dictionaryElements.append(acknowledementElement.toDictionary())
			}
			dictionary["acknowledement"] = dictionaryElements
		}
		if acknowledementCount != nil{
			dictionary["acknowledementCount"] = acknowledementCount
		}
		if acknowledementSLA != nil{
			var dictionaryElements = [[String:Any]]()
			for acknowledementSLAElement in acknowledementSLA {
				dictionaryElements.append(acknowledementSLAElement.toDictionary())
			}
			dictionary["acknowledementSLA"] = dictionaryElements
		}
		if acknowledementSLACount != nil{
			dictionary["acknowledementSLACount"] = acknowledementSLACount
		}
		if acknowledged != nil{
			dictionary["acknowledged"] = acknowledged
		}
		if application != nil{
			var dictionaryElements = [[String:Any]]()
			for applicationElement in application {
				dictionaryElements.append(applicationElement.toDictionary())
			}
			dictionary["application"] = dictionaryElements
		}
		if averageTime != nil{
			var dictionaryElements = [[String:Any]]()
			for averageTimeElement in averageTime {
				dictionaryElements.append(averageTimeElement.toDictionary())
			}
			dictionary["averageTime"] = dictionaryElements
		}
		if closed != nil{
			var dictionaryElements = [[String:Any]]()
			for closedElement in closed {
				dictionaryElements.append(closedElement.toDictionary())
			}
			dictionary["closed"] = dictionaryElements
		}
		if critical != nil{
			dictionary["critical"] = critical
		}
		if engineer != nil{
			var dictionaryElements = [[String:Any]]()
			for engineerElement in engineer {
				dictionaryElements.append(engineerElement.toDictionary())
			}
			dictionary["engineer"] = dictionaryElements
		}
		if fatal != nil{
			dictionary["fatal"] = fatal
		}
		if infrastructure != nil{
			var dictionaryElements = [[String:Any]]()
			for infrastructureElement in infrastructure {
				dictionaryElements.append(infrastructureElement.toDictionary())
			}
			dictionary["infrastructure"] = dictionaryElements
		}
		if met != nil{
			var dictionaryElements = [[String:Any]]()
			for metElement in met {
				dictionaryElements.append(metElement.toDictionary())
			}
			dictionary["met"] = dictionaryElements
		}
		if missed != nil{
			var dictionaryElements = [[String:Any]]()
			for missedElement in missed {
				dictionaryElements.append(missedElement.toDictionary())
			}
			dictionary["missed"] = dictionaryElements
		}
		if open != nil{
			var dictionaryElements = [[String:Any]]()
            for missedElement in missed {
                dictionaryElements.append(missedElement.toDictionary())
            }
            dictionary["open"] = dictionaryElements
		}
		if openAlertCount != nil{
			dictionary["openAlertCount"] = openAlertCount
		}
		if openAlerts != nil{
			var dictionaryElements = [[String:Any]]()
			for openAlertsElement in openAlerts {
				dictionaryElements.append(openAlertsElement.toDictionary())
			}
			dictionary["openAlerts"] = dictionaryElements
		}
		if status != nil{
			var dictionaryElements = [[String:Any]]()
			for statusElement in status {
				dictionaryElements.append(statusElement.toDictionary())
			}
			dictionary["status"] = dictionaryElements
		}
		if statusCount != nil{
			dictionary["statusCount"] = statusCount
		}
		if system != nil{
			var dictionaryElements = [[String:Any]]()
			for systemElement in system {
				dictionaryElements.append(systemElement.toDictionary())
			}
			dictionary["system"] = dictionaryElements
		}
		if tools != nil{
			var dictionaryElements = [[String:Any]]()
			for toolsElement in tools {
				dictionaryElements.append(toolsElement.toDictionary())
			}
			dictionary["tools"] = dictionaryElements
		}
		if unAcknowledged != nil{
			var dictionaryElements = [[String:Any]]()
			for unAcknowledgedElement in unAcknowledged {
				dictionaryElements.append(unAcknowledgedElement.toDictionary())
			}
			dictionary["unAcknowledged"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         acknowledement = aDecoder.decodeObject(forKey :"acknowledement") as? [OpenAlertYesterdayAcknowledement]
         acknowledementCount = aDecoder.decodeObject(forKey: "acknowledementCount") as? Int
         acknowledementSLA = aDecoder.decodeObject(forKey :"acknowledementSLA") as? [OpenAlertYesterdayAcknowledement]
         acknowledementSLACount = aDecoder.decodeObject(forKey: "acknowledementSLACount") as? Int
         acknowledged = aDecoder.decodeObject(forKey: "acknowledged") as? AnyObject
         application = aDecoder.decodeObject(forKey :"application") as? [OpenAlertYesterdayApplication]
         averageTime = aDecoder.decodeObject(forKey :"averageTime") as? [OpenAlertYesterdayAverageTime]
         closed = aDecoder.decodeObject(forKey :"closed") as? [OpenAlertYesterdayApplication]
         critical = aDecoder.decodeObject(forKey: "critical") as? Int
         engineer = aDecoder.decodeObject(forKey :"engineer") as? [OpenAlertYesterdayApplication]
         fatal = aDecoder.decodeObject(forKey: "fatal") as? Int
         infrastructure = aDecoder.decodeObject(forKey :"infrastructure") as? [OpenAlertYesterdayApplication]
         met = aDecoder.decodeObject(forKey :"met") as? [OpenAlertYesterdayApplication]
         missed = aDecoder.decodeObject(forKey :"missed") as? [OpenAlertYesterdayApplication]
         open = aDecoder.decodeObject(forKey: "open") as? [OpenAlertYesterdayApplication]
         openAlertCount = aDecoder.decodeObject(forKey: "openAlertCount") as? Int
         openAlerts = aDecoder.decodeObject(forKey :"openAlerts") as? [OpenAlertYesterdayAcknowledement]
         status = aDecoder.decodeObject(forKey :"status") as? [OpenAlertYesterdayAcknowledement]
         statusCount = aDecoder.decodeObject(forKey: "statusCount") as? Int
         system = aDecoder.decodeObject(forKey :"system") as? [OpenAlertYesterdayApplication]
         tools = aDecoder.decodeObject(forKey :"tools") as? [OpenAlertYesterdayApplication]
         unAcknowledged = aDecoder.decodeObject(forKey :"unAcknowledged") as? [OpenAlertYesterdayApplication]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if acknowledement != nil{
			aCoder.encode(acknowledement, forKey: "acknowledement")
		}
		if acknowledementCount != nil{
			aCoder.encode(acknowledementCount, forKey: "acknowledementCount")
		}
		if acknowledementSLA != nil{
			aCoder.encode(acknowledementSLA, forKey: "acknowledementSLA")
		}
		if acknowledementSLACount != nil{
			aCoder.encode(acknowledementSLACount, forKey: "acknowledementSLACount")
		}
		if acknowledged != nil{
			aCoder.encode(acknowledged, forKey: "acknowledged")
		}
		if application != nil{
			aCoder.encode(application, forKey: "application")
		}
		if averageTime != nil{
			aCoder.encode(averageTime, forKey: "averageTime")
		}
		if closed != nil{
			aCoder.encode(closed, forKey: "closed")
		}
		if critical != nil{
			aCoder.encode(critical, forKey: "critical")
		}
		if engineer != nil{
			aCoder.encode(engineer, forKey: "engineer")
		}
		if fatal != nil{
			aCoder.encode(fatal, forKey: "fatal")
		}
		if infrastructure != nil{
			aCoder.encode(infrastructure, forKey: "infrastructure")
		}
		if met != nil{
			aCoder.encode(met, forKey: "met")
		}
		if missed != nil{
			aCoder.encode(missed, forKey: "missed")
		}
		if open != nil{
			aCoder.encode(open, forKey: "open")
		}
		if openAlertCount != nil{
			aCoder.encode(openAlertCount, forKey: "openAlertCount")
		}
		if openAlerts != nil{
			aCoder.encode(openAlerts, forKey: "openAlerts")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if statusCount != nil{
			aCoder.encode(statusCount, forKey: "statusCount")
		}
		if system != nil{
			aCoder.encode(system, forKey: "system")
		}
		if tools != nil{
			aCoder.encode(tools, forKey: "tools")
		}
		if unAcknowledged != nil{
			aCoder.encode(unAcknowledged, forKey: "unAcknowledged")
		}

	}

}
