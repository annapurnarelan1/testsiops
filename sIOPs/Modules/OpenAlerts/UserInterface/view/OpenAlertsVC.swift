//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class OpenAlertsVC: BaseViewController,OpenAlertsViewProtocol, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance
{
    
    var presenter: OpenAlertsPresenterProtocol?
    
    @IBOutlet weak var descriptionThirdLbl: UILabel!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var descriptionSecondLbl: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var secondlbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
//    @IBOutlet weak var filteBtn: UIButton!
//    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var OpenAlertsCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    var selectedIndexPath : IndexPath?
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cnstHeightCalender: NSLayoutConstraint!
    
    var type:String?
    var selected:String?
    var NGOResponse:NGOOpenALertsModel?
    var NGOYesterdayResp : OpenAlertYesterdayModel?
    var selectedArray:[NGOApplication]?
    
    let date = Date()
    var selectedDate:String?
    let formatter = DateFormatter()
     @IBOutlet weak var calendarView: FSCalendar!
    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> OpenAlertsViewProtocol{
        return UIStoryboard(name: "OpenAlerts", bundle: nil).instantiateViewController(withIdentifier: "OpenAlerts") as! OpenAlertsVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatter.dateFormat = "yyyy-MM-dd"
        if UIDevice.current.model.hasPrefix("iPad") {
            self.cnstHeightCalender.constant = 400
        }
        selectedDate = formatter.string(from: date)
        self.calendarView.select(Date())
        self.calendarView.scope = .week
        self.calendarView.setScope(.week, animated: false)

        // For UITest
        self.calendarView.accessibilityIdentifier = "calendar"
        
        //tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        OpenAlertsCollectionView?.register(UINib(nibName: "ThreeButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ThreeButtonCollectionView")
        OpenAlertsCollectionView?.register(UINib(nibName: "TwoButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoButtonCollectionViewCell")
        OpenAlertsCollectionView?.register(UINib(nibName: "TwoButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoButtonCollectionViewCell")
         OpenAlertsCollectionView?.register(UINib(nibName: "dummyCollectionView", bundle: nil), forCellWithReuseIdentifier:"dummyCollectionView")
        // Do any additional setup after loading the view.
    }
    

  
    func setSelectedArray() {
        switch selected {
        case Constants.openAlerts.infra :
           // selectedArray = NGOResponse?.infra
            firstLabel.font =  UIFont(name: "JioType-Medium", size: 22)
        case Constants.openAlerts.application :
           // selectedArray = NGOResponse?.application
            secondlbl.font =  UIFont(name: "JioType-Medium", size: 22)
        case Constants.openAlerts.tools :
          // selectedArray = NGOResponse?.tools
           thirdLbl.font =  UIFont(name: "JioType-Medium", size: 22)
        default:
            print("nothing selected")
        }
    }
    
    @objc func ngoDetailOpenAlerts(sender:Any)
    {
        //let cell =  self.OpenAlertsCollectionView.cellForItem(at: self.selectedIndexPath ?? IndexPath(row: 0, section: 0))
        var selectedDetailType = ""
        var count = "0"
        let btn = (sender as? UIButton)!
        let section = btn.tag/100
        let index = btn.tag%100
        let cell =  self.OpenAlertsCollectionView.cellForItem(at: IndexPath(row: 0, section: section))
        if let selectedCell = cell as? ThreeButtonCollectionViewCell {
            //let btn = (sender as? UIButton)!
           // let tag = btn.tag
            switch index {
            case 1:
                selectedDetailType =   selectedCell.descriptionFirstLbl.text ?? ""
                count =  selectedCell.firstLabel.text ?? ""
                break
            case 2:
                selectedDetailType =   selectedCell.descriptionSecondLbl.text ?? ""
                count =  selectedCell.secondlbl.text ?? ""
                break
             case 3:
                selectedDetailType = selectedCell.descriptionThirdLbl.text ?? ""
                count = selectedCell.thirdLbl.text ?? ""
                 break
            default:
                print("in default")
            }
        }
        
        if let selectedCell = cell as? TwoButtonCollectionViewCell
        {
           //let btn = (sender as? UIButton)!
            //let tag = btn.tag
            switch index {
            case 1:
             selectedDetailType =  selectedCell.descriptionFirstLbl.text ?? ""
             count = selectedCell.firstLabel.text ?? ""
                break
            case 2:
                selectedDetailType = selectedCell.descriptionSecondLbl.text ?? ""
                count = selectedCell.secondlbl.text ?? ""
                break
            default:
                print("in default")
            }
        }
                UserDefaults.standard.set(selected, forKey: "openAlertType")
                    var isFromYes = true
                    if selectedDate == formatter.string(from: date){
                        isFromYes = false
                        self.presenter?.presentOpenAlertsHistoryModule(selectedHistoryType: selectedDetailType, date: selectedDate ?? "" , count: count, isFromYesterday: isFromYes, response: self.NGOResponse as Any )
                    }
                    else{
                        self.presenter?.presentOpenAlertsHistoryModule(selectedHistoryType: selectedDetailType, date: selectedDate ?? "" , count: count, isFromYesterday: isFromYes, response: self.NGOYesterdayResp as Any )
                    }
    }

//        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "dummyHeader", for: indexPath) as? dummyHeaderCollectionView
//
//           selectedIndexPath = indexPath
//
//
//
//
//            return headerView ?? UICollectionReusableView()
//        }
    
    
   override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated);
    
    
        
       HelperMethods().removeCustomBarButtons(self)
       HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       self.navigationController?.setNavigationBarHidden(false, animated: false)
       self.navigationItem.setHidesBackButton(false, animated:true)
       selectedDate = formatter.string(from: date)
       self.calendarView.select(Date())
       self.calendarView.scope = .week
         let titleLabel = String(UserDefaults.standard.string(forKey: "openAlertType") ?? "")
          self.titleLbl.text = "Service Glance - \(titleLabel)  Alerts"
    }
  
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadOpenYesterdayALerts(response:OpenAlertYesterdayModel)
    {
        stopAnimating()
        //self.NGOYesterdayResp = response
        self.presenter?.presentYesterdayOpenAlertsModule(type:type ?? "",selected:selected ?? "",response:response,date:selectedDate ?? "")
        self.calendarView.appearance.todaySelectionColor = UIColor(red: 31.0/255.0, green: 119.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        self.calendarView.appearance.selectionColor = UIColor.clear
        
    }
    

//}

    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.cnstHeightCalender.constant = bounds.height
        self.view.layoutIfNeeded()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        selectedDate = "\(self.dateFormatter.string(from: date))"
        print("did select date \(self.dateFormatter.string(from: date))")
        
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        startAnimating()
        self.presenter?.requestData(date: selectedDate ?? "")
        
    }
    
    private func calendar(calendar: FSCalendar, appearance: FSCalendarAppearance, selectionColorForDate date: NSDate) -> UIColor? {
        if (selectedDate == "\(self.dateFormatter.string(from: date as Date))") {
            return UIColor.red

        }
        return UIColor.clear
    }
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        let fromDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        return self.dateFormatter.date(from: self.dateFormatter.string(from: fromDate!))!
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return self.dateFormatter.date(from: self.dateFormatter.string(from:Date()))!
    }
}

extension OpenAlertsVC :UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //selectedIndexPath = NSIndexPath(row: 0, section: -1) as IndexPath
        switch indexPath.section
        {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            
            let normalText = "Open Alerts "
            let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            attributedString.append(boldString)
            cell.titleLbl.attributedText = attributedString
            cell.descriptionFirstLbl.attributedText = attributedString
            
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                cell.firstLabel.text = "\(NGOResponse?.openAlerts[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = NGOResponse?.openAlerts[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.openAlerts[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = NGOResponse?.openAlerts[1].featureName ?? ""
                cell.thirdLbl.text = "\(NGOResponse?.openAlerts[2].outlierCount ?? 0)"
                cell.descriptionThirdLbl.text = NGOResponse?.openAlerts[2].featureName ?? ""
                cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                cell.thirdBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 3
                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action:#selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.thirdBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                //selectedIndexPath = indexPath
            }
            
            return cell

        case 1:
            //switch indexPath.row {
            //case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            let normalText = "Acknowledgement "
            //let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
            //let attributedString = NSMutableAttributedString(string:normalText)
            
            //let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            //let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
            
           // attributedString.append(boldString)
            cell.titleLbl.text = normalText
            
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                cell.firstLabel.text = "\(NGOResponse?.acknowledement[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = NGOResponse?.acknowledement[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.acknowledement[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = NGOResponse?.acknowledement[1].featureName ?? ""
                if cell.descriptionSecondLbl.text == "Unacknowledged"{
                    cell.secondlbl.textColor = UIColor.red
                }
                cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action:#selector(ngoDetailOpenAlerts), for: .touchUpInside)
            }
            return cell
            
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            let normalText = "Acknowledgement SLA "
//            let boldText  = "(" + "\(NGOResponse?.openAlertCount ?? 0)" + ")"
//            let attributedString = NSMutableAttributedString(string:normalText)
//
//            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
//            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs)
//
//            attributedString.append(boldString)
            cell.titleLbl.text = normalText
            if((NGOResponse?.openAlerts.count ?? 0) > 0)
            {
                cell.firstLabel.text = "\(NGOResponse?.acknowledementSLA[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = NGOResponse?.acknowledementSLA[0].featureName ?? ""
                cell.secondlbl.text = "\(NGOResponse?.acknowledementSLA[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = NGOResponse?.acknowledementSLA[1].featureName ?? ""
                if cell.descriptionSecondLbl.text == "Missed"{
                    cell.secondlbl.textColor = UIColor.red
                }
                cell.firstBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 1
                cell.secondBtn.tag = indexPath.section * 100 + indexPath.item * 10 + 2
                cell.firstBtn.addTarget(self, action: #selector(ngoDetailOpenAlerts), for: .touchUpInside)
                cell.secondBtn.addTarget(self, action:#selector(ngoDetailOpenAlerts), for: .touchUpInside)
            }
            return cell
        default:
            return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(NGOResponse != nil)
        {
            return 3
        }
        else
        {
            return 0
        }
    }
}

extension OpenAlertsVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        itemsPerRow = 1
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 1
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        return CGSize(width: widthPerItem, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height:30)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //heightConstraint.constant = self.OpenAlertsCollectionView.contentSize.height + 40
        self.OpenAlertsCollectionView.setNeedsLayout()
    }
}


