//
//  OpenAlertsWireframe.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class OpenAlertsWireFrame: OpenAlertsWireFrameProtocol {
   

    // MARK: NGODetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentOpenAlertsModule(fromView: AnyObject,type:String,selected:String, response:NGOOpenALertsModel) {

        // Generating module components
        let view: OpenAlertsViewProtocol = OpenAlertsVC.instantiate()
        let presenter: OpenAlertsPresenterProtocol & OpenAlertsInteractorOutputProtocol = OpenAlertsPresenter()
        let interactor: OpenAlertsInteractorInputProtocol = OpenAlertsInteractor()
       
        let wireFrame: OpenAlertsWireFrameProtocol = OpenAlertsWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
    
        let viewController = view as! OpenAlertsVC
        viewController.type = type
        viewController.selected = selected
        viewController.NGOResponse = response
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
   {
      OpenAlertsDetailWireFrame.presentOpenAlertsDetailModule(fromView:self,type:type,ngoResponse:ngoResponse,count:count)
    }
    
    func presentYesterdayOpenAlertsModule(type:String,selected:String,response:OpenAlertYesterdayModel,date:String)
        {
    YesterdayOpenAlertsWireFrame.presentYesterdayOpenAlertsModule(fromView:self,type:type,selected:selected,response:response,date:date)
       }

    func presentOpenAlertsHistoryModule(type: String, date: String,count: String, isFromYesterday: Bool, response: Any) {
        
    OpenAlertsHistoryWireFrame.presentOpenAlertsHistoryModule(fromView:self,type:type,date: date, count: count, isFromYesterday: isFromYesterday, response: response) 
      }
}
