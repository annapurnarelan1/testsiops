//
//  OpenAlertsPresenter.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class OpenAlertsPresenter:BasePresenter, OpenAlertsPresenterProtocol, OpenAlertsInteractorOutputProtocol {
  
    // MARK: Variables
    weak var view: OpenAlertsViewProtocol?
    var interactor: OpenAlertsInteractorInputProtocol?
    var wireFrame: OpenAlertsWireFrameProtocol?
    let stringsTableName = "NGODetail"
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       /// Called when there is no network
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
      func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
    func requestData(date:String)
    {
        self.interactor?.requestData(date:date)
    }
    
func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
{
    self.wireFrame?.presentOpenAlertsDetailModule(type:type,ngoResponse:ngoResponse,count:count)
    }
    
    func reloadOpenYesterdayALerts(response:OpenAlertYesterdayModel)
    {
        self.view?.reloadOpenYesterdayALerts(response: response)
    }
    
     func presentYesterdayOpenAlertsModule(type:String,selected:String,response:OpenAlertYesterdayModel,date:String)
     {
      self.wireFrame?.presentYesterdayOpenAlertsModule(type:type,selected:selected,response:response,date:date)
    }
   func presentOpenAlertsHistoryModule(selectedHistoryType: String, date: String, count: String, isFromYesterday: Bool, response: Any) {
    self.wireFrame?.presentOpenAlertsHistoryModule(type:selectedHistoryType,date:date, count: count,
                                                   isFromYesterday: isFromYesterday,response:response)
      }
}
