//
//  OpenAlertsWireframe.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class RearViewControllerrWireFrame: RearViewControllerWireFrameProtocol {
    
    // MARK: NGODetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentRearViewControllerModule(fromView:AnyObject , application:LoginApplication) -> BaseViewController {

        // Generating module components
        let view: RearViewControllerViewProtocol = RearViewController.instantiate()
        let presenter: RearViewControllerPresenterProtocol & RearViewControllerInteractorOutputProtocol = RearViewControllerPresenter()
        //let interactor: ViewControllerInteractorInputProtocol = ViewControllerInteractor()
       
        let wireFrame: RearViewControllerWireFrameProtocol = RearViewControllerrWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        //presenter.interactor = interactor
        //interactor.presenter = presenter
        
        let viewController = view as! RearViewController
        viewController.application = application
        //viewController.type = type
        //viewController.selected = selected
        //viewController.selectedDate = date
        //viewController.NGOResponse = response
           return viewController
       // NavigationHelper.pushViewController(viewController: viewController)
    }
    
//    func presentEnergyModule(applicationRes:LoginApplication)
//    {
//        EnergyWireFrame.presentEnergyModule(fromView: self, application: applicationRes)
//    }
    
    
//   func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String, type: String, date: String, ngoResponse: OpenAlertYesterdayModel, count: String)
//   {
//       OpenAlertsHistoryWireFrame.presentOpenAlertsHistoryModule(fromView:self,type:selectedHistoryType,date: date, count: count, isFromYesterday: true, response: ngoResponse as Any)
//
//      // OpenAlertsDetailWireFrame.presentOpenAlertsDetailModule(fromView:self,type:type,ngoResponse:ngoResponse,count:count)
//    }

}
