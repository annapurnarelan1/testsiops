//
//  RearViewController.swift
//  HamburgerMenu
//
//  Created by Prathamesh Salvi on 05/10/18.
//  Copyright © 2018 Big Rattle Technologies Private Limited. All rights reserved.
//

import UIKit
import iOSDropDown
class RearViewController: BaseViewController,RearViewControllerViewProtocol {
    var presenter: RearViewControllerPresenterProtocol?
    
    @IBOutlet var customDropdown: [DropDown]!
    var value1 = ""
    var value2 = ""
    var value3 = ""
    var value4 = ""
    
    func show(image: String, error: String, description: String) {
        
    }
    
    func showError(error: String, description: String) {
        
    }
    
    func stopLoader() {
        
    }
    
    func showFailError(error: String) {
        
    }
    

    var application:LoginApplication?
    override func viewDidLoad() {
        super.viewDidLoad()
        let option =  Options()
        
        self.customDropdown[0].optionArray = option.boolData
         self.customDropdown[1].optionArray = option.countries
            self.customDropdown[2].optionArray = option.color
            self.customDropdown[3].optionArray = option.color

        self.customDropdown[0].isSearchEnable = false
        self.customDropdown[1].isSearchEnable = false
        self.customDropdown[2].isSearchEnable = false
        self.customDropdown[3].isSearchEnable = false

         self.customDropdown[0].didSelect{(selectedText , index , id) in
              self.value1 = "Selected String: \(selectedText) \n index: \(index) \n Id: \(id)"
               print(self.value1)
          }
        self.customDropdown[1].didSelect{(selectedText , index , id) in
            self.value2 = "Selected String: \(selectedText) \n index: \(index) \n Id: \(id)"
             print(self.value2)
        }
        self.customDropdown[2].didSelect{(selectedText , index , id) in
            self.value3 = "Selected String: \(selectedText) \n index: \(index) \n Id: \(id)"
             print(self.value3)
        }
        self.customDropdown[3].didSelect{(selectedText , index , id) in
            self.value4 = "Selected String: \(selectedText) \n index: \(index) \n Id: \(id)"
             print(self.value3)
        }
        // Do any additional setup after loading the view.
    }
    static func instantiate() -> RearViewControllerViewProtocol{
         return UIStoryboard(name: "RearViewController", bundle: nil).instantiateViewController(withIdentifier: "rearVC") as! RearViewController
     }
//    
//    @IBAction func openViewBtnAction(_ sender: UIButton) {
//        self.performSegue(withIdentifier: "openViewOneSegue", sender: self)
//        HamburgerMenu().closeSideMenu()
//
//    }
//    
//    @IBAction func openViewTwoBtnAction(_ sender: UIButton) {
//        HamburgerMenu().closeSideMenu()
//       self.performSegue(withIdentifier: "openViewOneSegue", sender: self)
//        
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
