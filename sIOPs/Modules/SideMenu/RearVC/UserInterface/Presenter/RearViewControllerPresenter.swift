//
//  OpenAlertsPresenter.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class RearViewControllerPresenter:BasePresenter, RearViewControllerPresenterProtocol,RearViewControllerInteractorOutputProtocol {
   
    // MARK: Variables
    weak var view: RearViewControllerViewProtocol?
    var interactor: RearViewControllerInteractorInputProtocol?
    var wireFrame:RearViewControllerWireFrameProtocol?
    let stringsTableName = "SideMEnu"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
//    func presentEnergyModule(applicationRes:LoginApplication)
//      {
//           self.wireFrame?.presentEnergyModule(applicationRes:applicationRes)
//      }
      
//    func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel) {
//        self.view?.reloadData(yesterdayOpenAlertsResponse: yesterdayOpenAlertsResponse)
//    }
//    
//    func requestData(date:String)
//    {
//        self.interactor?.requestData(date:date)
//    }
//    
//func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
//{
//    self.wireFrame?.presentYesterdayOpenAlertsDetailModule(selectedHistoryType: selectedHistoryType, type: type, date: date, ngoResponse: ngoResponse, count: count)
////    self.wireFrame?.presentYesterdayOpenAlertsDetailModule(selectedHistoryType: selectedHistoryType,date: date,type:type,count:count,ngoResponse:ngoResponse)
//    
//    }
}
