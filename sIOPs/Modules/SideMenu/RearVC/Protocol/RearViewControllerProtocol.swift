//
//  OpenAlertsProtocol.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol RearViewControllerViewProtocol: class {
    var presenter: RearViewControllerPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    //func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol RearViewControllerWireFrameProtocol: class {
    static func presentRearViewControllerModule(fromView:AnyObject , application:LoginApplication) -> BaseViewController
    //func presentEnergyModule(applicationRes:LoginApplication)
//    func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
  // func presentYesterdayOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
}

/// Method contract between VIEW -> PRESENTER
protocol RearViewControllerPresenterProtocol: class {
    var view: RearViewControllerViewProtocol? { get set }
    var interactor: RearViewControllerInteractorInputProtocol? { get set }
    var wireFrame: RearViewControllerWireFrameProtocol? { get set }
    
    //func presentEnergyModule(applicationRes:LoginApplication)
//    func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
   // func presentYesterdayOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
  //  func requestData(date:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol RearViewControllerInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
   //func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol RearViewControllerInteractorInputProtocol: class
{
    var presenter: ViewControllerInteractorOutputProtocol? { get set }
   // func requestData(date:String)
}
