//
//  OpenAlertsProtocol.swift
//  sIOPs
//
//  Created by Neha Mishra on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol ViewControllerViewProtocol: class {
    var presenter: ViewControllerPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
    //func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol ViewControllerWireFrameProtocol: class {
    static func presentViewControllerModule(fromView:AnyObject , application:LoginApplication)
    func presentEnergyModule(applicationRes:LoginApplication)  //presentRearViewControllerModule
    func presentRearViewControllerModule(applicationRes:LoginApplication)
//    func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
  // func presentYesterdayOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
}

/// Method contract between VIEW -> PRESENTER
protocol ViewControllerPresenterProtocol: class {
    var view: ViewControllerViewProtocol? { get set }
    var interactor: ViewControllerInteractorInputProtocol? { get set }
    var wireFrame: ViewControllerWireFrameProtocol? { get set }
    
    func presentEnergyModule(applicationRes:LoginApplication)
    func presentRearViewControllerModule(applicationRes:LoginApplication)
//    func presentYesterdayOpenAlertsDetailModule(selectedHistoryType: String,type:String,date: String,ngoResponse:OpenAlertYesterdayModel,count:String)
   // func presentYesterdayOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
  //  func requestData(date:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol ViewControllerInteractorOutputProtocol: class {

    func show(error: String,description:String)
    func stopLoader()
    func showFailError(error:String)
   //func reloadData(yesterdayOpenAlertsResponse:OpenAlertYesterdayModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol ViewControllerInteractorInputProtocol: class
{
    var presenter: ViewControllerInteractorOutputProtocol? { get set }
   // func requestData(date:String)
}
