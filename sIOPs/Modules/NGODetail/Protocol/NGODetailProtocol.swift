//
//  NGODetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol NGODetailViewProtocol: class {
    var presenter: NGODetailPresenterProtocol? { get set }
    //func NGODetailDone(NGODetailRes :NGODetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
    
    func stopLoader()
    func showFailError(error:String)
   // func reloadData(NGODetailResponse:NGODetail)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol NGODetailWireFrameProtocol: class {
    static func presentNGODetailModule(fromView:AnyObject,type:String,selected:String,ngoResponse:NGO)
   func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
}

/// Method contract between VIEW -> PRESENTER
protocol NGODetailPresenterProtocol: class {
    var view: NGODetailViewProtocol? { get set }
    var interactor: NGODetailInteractorInputProtocol? { get set }
    var wireFrame: NGODetailWireFrameProtocol? { get set }
    func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
    func requestData()
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol NGODetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    
   // func reloadData(NGODetailResponse:NGODetail)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol NGODetailInteractorInputProtocol: class
{
    var presenter: NGODetailInteractorOutputProtocol? { get set }
    func requestData()
    
}
