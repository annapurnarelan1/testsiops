//
//  NGODetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class NGODetailVC: BaseViewController,NGODetailViewProtocol {
    
    var presenter: NGODetailPresenterProtocol?
    
    @IBOutlet weak var descriptionThirdLbl: UILabel!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var descriptionSecondLbl: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var secondlbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var filteBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var type:String?
    var selected:String?
    var NGOResponse:NGO?
    var selectedArray:[NGOApplication]?
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> NGODetailViewProtocol{
        return UIStoryboard(name: "NGODetail", bundle: nil).instantiateViewController(withIdentifier: "NGODetail") as! NGODetailVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        
        if(type?.contains("History") ?? false)
        {
            titleLbl.text = type?.replacingOccurrences(of: "History", with: "")
        }
        else
        {
            titleLbl.text = type
        }
        
        firstLabel.text = "\(NGOResponse?.openAlerts[0].outlierCount ?? 0)"
        descriptionFirstLbl.text = NGOResponse?.openAlerts[0].featureName ?? ""
        secondlbl.text = "\(NGOResponse?.openAlerts[1].outlierCount ?? 0)"
        descriptionSecondLbl.text = NGOResponse?.openAlerts[1].featureName ?? ""
        thirdLbl.text = "\(NGOResponse?.openAlerts[2].outlierCount ?? 0)"
        descriptionThirdLbl.text = NGOResponse?.openAlerts[2].featureName ?? ""
        setSelectedArray()
        

        // Do any additional setup after loading the view.
    }
    
  
    func setSelectedArray() {
        switch selected {
        case Constants.openAlerts.infra :
            selectedArray = NGOResponse?.infra
            firstLabel.font =  UIFont(name: "JioType-Medium", size: 22)
        case Constants.openAlerts.application :
            selectedArray = NGOResponse?.application
            secondlbl.font =  UIFont(name: "JioType-Medium", size: 22)
        case Constants.openAlerts.tools :
           selectedArray = NGOResponse?.tools
           thirdLbl.font =  UIFont(name: "JioType-Medium", size: 22)
        default:
            print("nothing selected")
        }
    }
    
    @IBAction func ngoDetailOpenAlerts(sender:Any)
    {
        firstLabel.font =  UIFont(name: "JioType-Light", size: 22)
        secondlbl.font =  UIFont(name: "JioType-Light", size: 22)
        thirdLbl.font =  UIFont(name: "JioType-Light", size: 22)
        
        let btn:UIButton = (sender as? UIButton)!
        switch btn.tag {
        case 0:
           selectedArray = NGOResponse?.infra
           firstLabel.font =  UIFont(name: "JioType-Medium", size: 22)
        case 1:
            selectedArray = NGOResponse?.application
            secondlbl.font =  UIFont(name: "JioType-Medium", size: 22)
        case 2:
            selectedArray = NGOResponse?.tools
            thirdLbl.font =  UIFont(name: "JioType-Medium", size: 22)
        default:
            print("nothing selected")
        }
        
       
        tableView.reloadData()
    }

   override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated);
        
       HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       self.navigationController?.setNavigationBarHidden(false, animated: false)
       self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
     
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    

}

extension NGODetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell:OutlierTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell", for: indexPath as IndexPath) as! OutlierTableViewCell
        cell.titleLbl.text = selectedArray?[indexPath.section].featureName ?? ""
        cell.countLbl.text = "\(selectedArray?[indexPath.section].outlierCount ?? 0)"
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
             if(type?.contains("History") ?? false)
             {
                self.presenter?.presentOpenAlertsDetailModule(type:(selectedArray?[indexPath.section].featureName ?? "") + "History",ngoResponse:NGOResponse!,count:"\(selectedArray?[indexPath.section].outlierCount ?? 0)")
        }
        else
             {
                self.presenter?.presentOpenAlertsDetailModule(type:selectedArray?[indexPath.section].featureName ?? "",ngoResponse:NGOResponse!,count:"\(selectedArray?[indexPath.section].outlierCount ?? 0)")
        }
        tableView.deselectRow(at: indexPath, animated: false)
          
    }
}
