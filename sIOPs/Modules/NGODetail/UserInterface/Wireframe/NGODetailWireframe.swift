//
//  NGODetailDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class NGODetailWireFrame: NGODetailWireFrameProtocol {
    
    
    
    // MARK: NGODetailDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentNGODetailModule(fromView:AnyObject,type:String,selected:String,ngoResponse:NGO) {

        // Generating module components
        let view: NGODetailViewProtocol = NGODetailVC.instantiate()
        let presenter: NGODetailPresenterProtocol & NGODetailInteractorOutputProtocol = NGODetailPresenter()
        let interactor: NGODetailInteractorInputProtocol = NGODetailInteractor()
       
        let wireFrame: NGODetailWireFrameProtocol = NGODetailWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! NGODetailVC
        viewController.type = type
        viewController.selected = selected
        viewController.NGOResponse =  ngoResponse
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
    
   func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
   {
    OpenAlertsDetailWireFrame.presentOpenAlertsDetailModule(fromView:self,type:type,ngoResponse:ngoResponse,count:count)
    }

}
