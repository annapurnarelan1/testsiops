//
//  NGODetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class NGODetailPresenter:BasePresenter, NGODetailPresenterProtocol, NGODetailInteractorOutputProtocol {
   
    
    

    // MARK: Variables
    weak var view: NGODetailViewProtocol?
    var interactor: NGODetailInteractorInputProtocol?
    var wireFrame: NGODetailWireFrameProtocol?
    let stringsTableName = "NGODetail"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
        func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
    func requestData()
    {
        self.interactor?.requestData()
    }
    
func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
{
    self.wireFrame?.presentOpenAlertsDetailModule(type:type,ngoResponse:ngoResponse,count:count)
    
    }
}
