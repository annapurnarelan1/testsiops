//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EnergyModel : NSObject, NSCoding{

	var co2 : [Co2]!
	var ebBills : [EbBill]!
	var highCPH : [EbBill]!
	var list : AnyObject!
	var power : [EbBill]!
	var siteLoad : [EbBill]!
	var sites : [EbBill]!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		co2 = [Co2]()
		if let co2Array = dictionary["co2"] as? [[String:Any]]{
			for dic in co2Array{
				let value = Co2(fromDictionary: dic)
				co2.append(value)
			}
		}
		ebBills = [EbBill]()
		if let ebBillsArray = dictionary["ebBills"] as? [[String:Any]]{
			for dic in ebBillsArray{
				let value = EbBill(fromDictionary: dic)
				ebBills.append(value)
			}
		}
		highCPH = [EbBill]()
		if let highCPHArray = dictionary["highCPH"] as? [[String:Any]]{
			for dic in highCPHArray{
				let value = EbBill(fromDictionary: dic)
				highCPH.append(value)
			}
		}
		list = dictionary["list"] as? AnyObject
		power = [EbBill]()
		if let powerArray = dictionary["power"] as? [[String:Any]]{
			for dic in powerArray{
				let value = EbBill(fromDictionary: dic)
				power.append(value)
			}
		}
		siteLoad = [EbBill]()
		if let siteLoadArray = dictionary["siteLoad"] as? [[String:Any]]{
			for dic in siteLoadArray{
				let value = EbBill(fromDictionary: dic)
				siteLoad.append(value)
			}
		}
		sites = [EbBill]()
		if let sitesArray = dictionary["sites"] as? [[String:Any]]{
			for dic in sitesArray{
				let value = EbBill(fromDictionary: dic)
				sites.append(value)
			}
		}
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if co2 != nil{
			var dictionaryElements = [[String:Any]]()
			for co2Element in co2 {
				dictionaryElements.append(co2Element.toDictionary())
			}
			dictionary["co2"] = dictionaryElements
		}
		if ebBills != nil{
			var dictionaryElements = [[String:Any]]()
			for ebBillsElement in ebBills {
				dictionaryElements.append(ebBillsElement.toDictionary())
			}
			dictionary["ebBills"] = dictionaryElements
		}
		if highCPH != nil{
			var dictionaryElements = [[String:Any]]()
			for highCPHElement in highCPH {
				dictionaryElements.append(highCPHElement.toDictionary())
			}
			dictionary["highCPH"] = dictionaryElements
		}
		if list != nil{
			dictionary["list"] = list
		}
		if power != nil{
			var dictionaryElements = [[String:Any]]()
			for powerElement in power {
				dictionaryElements.append(powerElement.toDictionary())
			}
			dictionary["power"] = dictionaryElements
		}
		if siteLoad != nil{
			var dictionaryElements = [[String:Any]]()
			for siteLoadElement in siteLoad {
				dictionaryElements.append(siteLoadElement.toDictionary())
			}
			dictionary["siteLoad"] = dictionaryElements
		}
		if sites != nil{
			var dictionaryElements = [[String:Any]]()
			for sitesElement in sites {
				dictionaryElements.append(sitesElement.toDictionary())
			}
			dictionary["sites"] = dictionaryElements
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         co2 = aDecoder.decodeObject(forKey :"co2") as? [Co2]
         ebBills = aDecoder.decodeObject(forKey :"ebBills") as? [EbBill]
         highCPH = aDecoder.decodeObject(forKey :"highCPH") as? [EbBill]
         list = aDecoder.decodeObject(forKey: "list") as? AnyObject
         power = aDecoder.decodeObject(forKey :"power") as? [EbBill]
         siteLoad = aDecoder.decodeObject(forKey :"siteLoad") as? [EbBill]
         sites = aDecoder.decodeObject(forKey :"sites") as? [EbBill]
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if co2 != nil{
			aCoder.encode(co2, forKey: "co2")
		}
		if ebBills != nil{
			aCoder.encode(ebBills, forKey: "ebBills")
		}
		if highCPH != nil{
			aCoder.encode(highCPH, forKey: "highCPH")
		}
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if power != nil{
			aCoder.encode(power, forKey: "power")
		}
		if siteLoad != nil{
			aCoder.encode(siteLoad, forKey: "siteLoad")
		}
		if sites != nil{
			aCoder.encode(sites, forKey: "sites")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
