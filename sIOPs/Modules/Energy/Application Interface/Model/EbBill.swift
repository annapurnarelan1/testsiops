//
//	EbBill.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EbBill : NSObject, NSCoding{

	var featureId : AnyObject!
	var featureName : String!
	var formattedCount : Double!
	var outlierCount : Int!
	var unit : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		featureId = dictionary["featureId"] as? AnyObject
		featureName = dictionary["featureName"] as? String
		formattedCount = dictionary["formattedCount"] as? Double
		outlierCount = dictionary["outlierCount"] as? Int
		unit = dictionary["unit"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if featureId != nil{
			dictionary["featureId"] = featureId
		}
		if featureName != nil{
			dictionary["featureName"] = featureName
		}
		if formattedCount != nil{
			dictionary["formattedCount"] = formattedCount
		}
		if outlierCount != nil{
			dictionary["outlierCount"] = outlierCount
		}
		if unit != nil{
			dictionary["unit"] = unit
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         featureId = aDecoder.decodeObject(forKey: "featureId") as? AnyObject
         featureName = aDecoder.decodeObject(forKey: "featureName") as? String
         formattedCount = aDecoder.decodeObject(forKey: "formattedCount") as? Double
         outlierCount = aDecoder.decodeObject(forKey: "outlierCount") as? Int
         unit = aDecoder.decodeObject(forKey: "unit") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if featureId != nil{
			aCoder.encode(featureId, forKey: "featureId")
		}
		if featureName != nil{
			aCoder.encode(featureName, forKey: "featureName")
		}
		if formattedCount != nil{
			aCoder.encode(formattedCount, forKey: "formattedCount")
		}
		if outlierCount != nil{
			aCoder.encode(outlierCount, forKey: "outlierCount")
		}
		if unit != nil{
			aCoder.encode(unit, forKey: "unit")
		}

	}

}
