//
//  EnergyProtocol.swift
//  sIOPs
//
//  Created by ASM ESPL on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol EnergyViewProtocol: class {
    var presenter: EnergyPresenterProtocol? { get set }
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(EnergyResponse:EnergyModel)

  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol EnergyWireFrameProtocol: class {
 static func presentEnergyModule(fromView:AnyObject,application:LoginApplication) -> BaseViewController
}

/// Method contract between VIEW -> PRESENTER
protocol EnergyPresenterProtocol: class {
    var view: EnergyViewProtocol? { get set }
    var interactor: EnergyInteractorInputProtocol? { get set }
    var wireFrame: EnergyWireFrameProtocol? { get set }

    func requestData(application:LoginApplication)

  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol EnergyInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(EnergyResponse:EnergyModel)


}

/// Method contract between PRESENTER -> INTERACTOR
protocol EnergyInteractorInputProtocol: class
{
    var presenter: EnergyInteractorOutputProtocol? { get set }
    func requestData(application:LoginApplication)

    
}
