//
//  EnergyPresenter.swift
//  sIOPs
//
//  Created by ASM ESPL on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class EnergyPresenter:BasePresenter, EnergyPresenterProtocol, EnergyInteractorOutputProtocol {
    
    
   
    // MARK: Variables
    weak var view: EnergyViewProtocol?
    var interactor: EnergyInteractorInputProtocol?
    var wireFrame: EnergyWireFrameProtocol?
    let stringsTableName = "Energy"
    
    
    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
     func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
    
    func requestData(application:LoginApplication)
    {
        self.interactor?.requestData(application:application)
    }
    
    func reloadData(EnergyResponse:EnergyModel)
    {
        self.view?.reloadData(EnergyResponse:EnergyResponse)
    }
    

    
   
}
