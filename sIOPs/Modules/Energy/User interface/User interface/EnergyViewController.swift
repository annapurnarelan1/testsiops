//
//  EnergyViewController.swift
//  sIOPs
//
//  Created by ASM ESPL on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation
import UIKit




class EnergyViewController: BaseViewController,EnergyViewProtocol{
    
    var presenter: EnergyPresenterProtocol?
    @IBOutlet weak var EnergyCollectionView: UICollectionView!
    var itemsPerRow: CGFloat = 1
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let collectionViewItemHeight: CGFloat = 99
    var energyResponse :EnergyModel?
    var btn = UIButton()
    var application:LoginApplication?
    var refresher:UIRefreshControl!
   // @IBOutlet weak var sideMenuView: UIView!
   // @IBOutlet var tanspContainerView: UIView!
    
//    var initialPos: CGPoint?
//    var touchPos: CGPoint?
//    let blackTransparentViewTag = 02271994
//    var openFlag: Bool = false
    
//    lazy var rearVC: BaseViewController? = {
//        return RearViewControllerrWireFrame.presentRearViewControllerModule(fromView: self, application: self.application!)
//       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        addShadowToView()
//        setUpNotifications()
//        setUpGestures()
        
        self.refresher = UIRefreshControl()
        self.EnergyCollectionView.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.init(red: 44, green: 88, blue: 156)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.EnergyCollectionView.addSubview(refresher)
        
        EnergyCollectionView?.register(UINib(nibName: "TwoButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"TwoButtonCollectionViewCell")
        EnergyCollectionView?.register(UINib(nibName: "ThreeButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"ThreeButtonCollectionView")
//        EnergyCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        EnergyCollectionView?.register(UINib(nibName: "SingleButtonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:"SingleButtonCollectionViewCell")
        
        
        
        startAnimating()
        self.presenter?.requestData(application: application!)
        
        // Do any additional setup after loading the view.
    }
    
//    func displaySideMenu(){
//        // To display RearViewController in Side Menu View
//        if !self.children.contains(rearVC!){
//            if let vc = rearVC {
//                self.addChild(vc)
//                vc.didMove(toParent: self)
//
//                vc.view.frame = self.sideMenuView.bounds
//                self.sideMenuView.addSubview(vc.view)
//                self.sideMenuView.bringSubviewToFront(vc.view)
//            }
//        }
//    }
    
//    //MARK: - Shadow View
//    func addBlackTransparentView() -> UIView{
//        //Black Shadow on MainView(i.e on TabBarController) when side menu is opened.
//        let blackView = self.view.viewWithTag(blackTransparentViewTag)
//        if blackView != nil{
//            return blackView!
//        }else{
//            let sView = UIView(frame: self.view.bounds)
//            sView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            sView.tag = blackTransparentViewTag
//            sView.alpha = 0
//            sView.backgroundColor = UIColor.black
//            let recognizer = UITapGestureRecognizer(target: self, action: #selector(closeSideMenu))
//            sView.addGestureRecognizer(recognizer)
//            return sView
//        }
//
//
//    }

//    func addShadowToView(){
//        //Gives Illusion that main view is above the side menu
//        self.view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
//        self.view.layer.shadowOffset = CGSize(width: -1, height: 1)
//        self.view.layer.shadowRadius = 1
//        self.view.layer.shadowOpacity = 1
//        self.view.layer.borderColor = UIColor.lightGray.cgColor
//        self.view.layer.borderWidth = 0.2
//    }
    
    
//    //MARK: - Selector Methods
//     @objc func openOrCloseSideMenu(){
//         //Opens or Closes Side Menu On Click of Button
//         if openFlag{
//             //This closes Rear View
//             let blackTransparentView = self.view.viewWithTag(self.blackTransparentViewTag)
//             UIView.animate(withDuration: 0.3, animations: {
//                 self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//                 blackTransparentView?.alpha = 0
//
//             }) { (_) in
//                 blackTransparentView?.removeFromSuperview()
//                 self.openFlag = false
//             }
//         }else{
//             //This opens Rear View
//             UIView.animate(withDuration: 0.0, animations: {
//                 self.displaySideMenu()
//                 //let blackTransparentView = self.addBlackTransparentView()
//
//                 //self.view.addSubview(blackTransparentView)
//
//             }) { (_) in
//                 UIView.animate(withDuration: 0.3, animations: {
//
////                 self.addBlackTransparentView().alpha = self.view.bounds.width * 0.8/(self.view.bounds.width * 1.8)
//                 self.view.frame = CGRect(x: -(self.view.bounds.size.width * 0.8), y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//                     }) { (_) in
//                     self.openFlag = true
//                     }
//             }
//         }
//     }
     
//     @objc func closeSideMenu(){
//         //To close Side Menu
//         let blackTransparentView = self.view.viewWithTag(self.blackTransparentViewTag)
//             UIView.animate(withDuration: 0.3, animations: {
//                 self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//                 blackTransparentView?.alpha = 0.0
//
//             }) { (_) in
//                 blackTransparentView?.removeFromSuperview()
//                 self.openFlag = false
//             }
//
//     }

//     @objc func closeWithoutAnimation(){
//         //To close Side Menu without animation
//         let blackTransparentView = self.view.viewWithTag(self.blackTransparentViewTag)
//         blackTransparentView?.alpha = 0
//         blackTransparentView?.removeFromSuperview()
//        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//        self.openFlag = false
//     }
//
//
//
//     //MARK: - Pan Gesture
//     @objc func handlePanGesture(panGesture: UIPanGestureRecognizer){
//         //For Pan Gesture
//
//         touchPos = panGesture.location(in: self.view)
//         let translation = panGesture.translation(in: self.view)
//
//         //Add BlackShadowView
//       //  let blackTransparentView = self.addBlackTransparentView()
//        // self.view.addSubview(blackTransparentView)
//
//
//         if panGesture.state == .began{
//             initialPos = touchPos
//         }else if panGesture.state == .changed{
//             let touchPosition = self.view.bounds.width * 0.8
//             if (initialPos?.x)! > touchPosition && openFlag{
//                 //To Close Rear View
//                 if self.view.frame.minX > 0{
//                     self.view.center = CGPoint(x: self.view.center.x + translation.x, y: self.view.bounds.midY)
//                     panGesture.setTranslation(CGPoint.zero, in: self.view)
//
//                     //blackTransparentView.alpha = self.view.frame.minX/(self.view.bounds.width * 1.8)
//                 }
//             }else if !openFlag{
//                 //To Open Rear View
//                 if translation.x > 0.0{
//                     displaySideMenu()
//
//                     self.view.center = CGPoint(x: translation.x + self.view.center.x, y: self.view.bounds.midY)
//                     panGesture.setTranslation(CGPoint.zero, in: self.view)
//
//                    // blackTransparentView.alpha = self.view.frame.minX/(self.view.bounds.width * 1.8)
//                 }
//
//             }
//
//         }else if panGesture.state == .ended{
//             if self.view.frame.minX > self.view.frame.midX{
//                 //Opens Rear View
//                 UIView.animate(withDuration: 0.2, animations: {
//
//                     self.view.frame = CGRect(x: self.view.frame.width * 0.8, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
//                    // blackTransparentView.alpha = self.view.frame.minX/(self.view.bounds.width * 1.8)
//                 }) { (_) in
//                     self.openFlag = true
//                 }
//             }else{
//                 //Closes Rear View
//                 UIView.animate(withDuration: 0.2, animations: {
//                     self.view.center = CGPoint(x: self.view.center.x, y: self.view.bounds.midY)
//                    // blackTransparentView.alpha = 0
//                 }) { (_) in
//                   //  blackTransparentView.removeFromSuperview()
//                     self.openFlag = false
//
//                 }
//             }
//         }
//     }
     
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
//    func setUpNotifications(){
//        let notificationOpenOrCloseSideMenu = Notification.Name("notificationOpenOrCloseSideMenu")
//        NotificationCenter.default.addObserver(self, selector: #selector(openOrCloseSideMenu), name: notificationOpenOrCloseSideMenu, object: nil)
//
//        let notificationCloseSideMenu = Notification.Name("notificationCloseSideMenu")
//        NotificationCenter.default.addObserver(self, selector: #selector(closeSideMenu), name: notificationCloseSideMenu, object: nil)
//
//        let notificationCloseSideMenuWithoutAnimation = Notification.Name("notificationCloseSideMenuWithoutAnimation")
//        NotificationCenter.default.addObserver(self, selector: #selector(closeWithoutAnimation), name: notificationCloseSideMenuWithoutAnimation, object: nil)
//    }
//
//    func setUpGestures(){
//        let panGestureContainerView = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(panGesture:)))
//        self.view.addGestureRecognizer(panGestureContainerView)
//    }
    
    
    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
        HamburgerMenu().triggerSideMenu()
      }
      
      @objc func hideHamburger(){
          HamburgerMenu().closeSideMenu()
      }
      
    
    @objc func loadData() {
        // self.NGOCollectionView.refreshControl?.beginRefreshing()
        stopRefresher()
        self.presenter?.requestData(application: application!)
        //Call this to stop refresher
    }

    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        EnergyCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    func stopRefresher() {
        refresher.endRefreshing()
    }
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> EnergyViewProtocol{
        return UIStoryboard(name: "EnergyVC", bundle: nil).instantiateViewController(withIdentifier: "EnergyVC") as! EnergyViewController
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
       
    }
    
    func reloadData(EnergyResponse:EnergyModel)
    {
        stopAnimating()
        energyResponse = EnergyResponse
        EnergyCollectionView.reloadData()
 
    }

}

extension EnergyViewController :UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if(energyResponse != nil)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        
        if(energyResponse?.sites.count == 0 && energyResponse?.siteLoad.count == 0  && energyResponse?.siteLoad.count == 0 && energyResponse?.co2.count == 0 && energyResponse?.ebBills.count == 0 && energyResponse?.power.count == 0)
        {
            return 0
        }
        
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.row == 0)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            
            cell.titleLbl.text  = "Sites"
            
            if(energyResponse?.sites.count ?? 0 > 0)
            {
                
                cell.firstLabel.text = "\(energyResponse?.sites[0].outlierCount ?? 0)" == "0" ? "-" : "\(energyResponse?.sites[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = energyResponse?.sites[0].featureName ?? ""
                cell.secondlbl.text = "\(energyResponse?.sites[1].outlierCount ?? 0)" == "0" ? "-" : "\(energyResponse?.sites[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = energyResponse?.sites[1].featureName ?? ""
                cell.thirdLbl.text = "\(energyResponse?.sites[2].outlierCount ?? 0)" == "0" ? "-" : "\(energyResponse?.sites[2].outlierCount ?? 0)"
                cell.descriptionThirdLbl.text = energyResponse?.sites[2].featureName ?? ""
            }
            
            
            
            return cell
        }
        else  if(indexPath.row == 1)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            
            
            cell.titleLbl.text = "Site Load"
            
            
            if(energyResponse?.siteLoad.count ?? 0 > 0)
            {
                
                
                
                let normalText1 = "\(energyResponse?.siteLoad[0].formattedCount ?? 0)"
                let boldText1  = energyResponse?.siteLoad[0].unit ?? ""
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                let dashAttributed = NSMutableAttributedString(string:"-")
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                attributedString1.append(boldString1)
                cell.firstLabel.attributedText =  attributedString1
                
               // "\(energyResponse?.siteLoad[0].formattedCount ?? 0)" == "0.0" ? dashAttributed : attributedString1

                cell.descriptionFirstLbl.text = energyResponse?.siteLoad[0].featureName ?? ""
                let normalText2 = "\(energyResponse?.siteLoad[1].formattedCount ?? 0)"
                let boldText2  = energyResponse?.siteLoad[1].unit ?? ""
                let attributedString2 = NSMutableAttributedString(string:normalText2)
                let boldString2 = NSMutableAttributedString(string: boldText2, attributes:attrs1)
                attributedString2.append(boldString2)
                cell.secondlbl.attributedText =  attributedString2
                cell.descriptionSecondLbl.text = energyResponse?.siteLoad[1].featureName ?? ""
                let normalText3 = "\(energyResponse?.siteLoad[2].formattedCount ?? 0)"
                let boldText3  = energyResponse?.siteLoad[2].unit ?? ""
                let attributedString3 = NSMutableAttributedString(string:normalText3)
                let boldString3 = NSMutableAttributedString(string: boldText3, attributes:attrs1)
                attributedString3.append(boldString3)
                cell.thirdLbl.attributedText =  attributedString3
                cell.descriptionThirdLbl.text = energyResponse?.siteLoad[2].featureName ?? ""
            }
            
            
            
            return cell
        }
        else  if(indexPath.row == 2)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            
            
            cell.titleLbl.text = "Power"
            
            if(energyResponse?.power.count ?? 0 > 0)
            {
                cell.firstLabel.text = "\(energyResponse?.power[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = energyResponse?.power[0].featureName ?? ""
                cell.secondlbl.text = "\(energyResponse?.power[1].outlierCount ?? 0)"
                cell.descriptionSecondLbl.text = energyResponse?.power[1].featureName ?? ""
                cell.thirdLbl.text = "\(energyResponse?.power[2].outlierCount ?? 0)"
                cell.descriptionThirdLbl.text = energyResponse?.power[2].featureName ?? ""
                
                
                
                
                
            }
            
            
            
            return cell
        }
        else  if(indexPath.row == 3)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeButtonCollectionView", for: indexPath) as! ThreeButtonCollectionViewCell
            
            
            cell.titleLbl.text = "CO2"
            
            
            if(energyResponse?.co2.count ?? 0 > 0)
            {
                
                let normalText1 = "\(energyResponse?.co2[0].formattedCount ?? 0)"
                let boldText1  = energyResponse?.co2[0].unit ?? ""
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                attributedString1.append(boldString1)
                cell.firstLabel.attributedText = attributedString1
                cell.descriptionFirstLbl.text = energyResponse?.co2[0].featureName ?? ""
                let normalText2 = "\(energyResponse?.co2[1].formattedCount ?? 0)"
                let boldText2  = energyResponse?.co2[1].unit ?? ""
                let attributedString2 = NSMutableAttributedString(string:normalText2)
                let boldString2 = NSMutableAttributedString(string: boldText2, attributes:attrs1)
                attributedString2.append(boldString2)
                cell.secondlbl.attributedText = attributedString2
                cell.descriptionSecondLbl.text = energyResponse?.co2[1].featureName ?? ""
                let normalText3 = "\(energyResponse?.co2[2].formattedCount ?? 0)"
                let boldText3  = energyResponse?.co2[2].unit ?? ""
                let attributedString3 = NSMutableAttributedString(string:normalText3)
                let boldString3 = NSMutableAttributedString(string: boldText3, attributes:attrs1)
                attributedString3.append(boldString3)
                cell.thirdLbl.attributedText = attributedString3
                cell.descriptionThirdLbl.text = energyResponse?.co2[2].featureName ?? ""
            }
            
            
            return cell
        }
        else  if(indexPath.row == 4)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            
            
            
            cell.titleLbl.text = "High CPH"
            
            if(energyResponse?.highCPH.count ?? 0 > 0)
            {
                
                let normalText1 = "\(energyResponse?.highCPH[0].outlierCount ?? 0)"
                let boldText1  = energyResponse?.highCPH[0].unit ?? ""
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                attributedString1.append(boldString1)
                
                cell.firstLabel.text = "\(energyResponse?.highCPH[0].outlierCount ?? 0)" == "0" ? "-" : "\(energyResponse?.highCPH[0].outlierCount ?? 0)"
                // cell.firstLabel.text = "\(energyResponse?.sites[0].outlierCount ?? 0)" == "0" ? "-" : "\(energyResponse?.sites[0].outlierCount ?? 0)"
                cell.descriptionFirstLbl.text = energyResponse?.highCPH[0].featureName ?? ""
                let normalText2 = "\(energyResponse?.highCPH[1].outlierCount ?? 0)"
                let boldText2  = energyResponse?.highCPH[1].unit ?? ""
                let attributedString2 = NSMutableAttributedString(string:normalText2)
                let boldString2 = NSMutableAttributedString(string: boldText2, attributes:attrs1)
                attributedString2.append(boldString2)
                
                cell.secondlbl.text = "\(energyResponse?.highCPH[1].outlierCount ?? 0)" == "0" ? "-" : "\(energyResponse?.highCPH[1].outlierCount ?? 0)"
                // cell.secondlbl.attributedText = attributedString2
                cell.descriptionSecondLbl.text = energyResponse?.highCPH[1].featureName ?? ""
                
            }
            
            
            
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoButtonCollectionViewCell", for: indexPath) as! TwoButtonCollectionViewCell
            
            
            
            cell.titleLbl.text = "EB Bills"
            
            if(energyResponse?.ebBills.count ?? 0 > 0)
            {
                
                let normalText1 = "\(energyResponse?.ebBills[0].outlierCount ?? 0)"
                let boldText1  = energyResponse?.ebBills[0].unit ?? ""
                let attributedString1 = NSMutableAttributedString(string:normalText1)
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
                let boldString1 = NSMutableAttributedString(string: boldText1, attributes:attrs1)
                attributedString1.append(boldString1)
                cell.firstLabel.attributedText = attributedString1
                cell.descriptionFirstLbl.text = energyResponse?.ebBills[0].featureName ?? ""
                let normalText2 = "\(energyResponse?.ebBills[1].outlierCount ?? 0)"
                let boldText2  = energyResponse?.ebBills[1].unit ?? ""
                let attributedString2 = NSMutableAttributedString(string:normalText2)
                let boldString2 = NSMutableAttributedString(string: boldText2, attributes:attrs1)
                attributedString2.append(boldString2)
                cell.secondlbl.attributedText = attributedString2
                cell.descriptionSecondLbl.text = energyResponse?.ebBills[1].featureName ?? ""
                
            }
            
            
            return cell
        }
        
        
        
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as? HeaderCollectionReusableView
//
//
//
//
//
//        headerView?.headerTitleLbl.text = "Energy Dashboard"
//        headerView?.alarmLbl.isHidden = true
//
//
//
//
//
//
//
//        return headerView ?? UICollectionReusableView()
//    }
    
    
}

extension EnergyViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        if(indexPath.row  <= 3)
        //        {
        itemsPerRow = 1
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        var widthPerItem: CGFloat
        if(self.iPadUI!){
            itemsPerRow = 1
            widthPerItem = availableWidth / itemsPerRow
        }else{
            widthPerItem = availableWidth / itemsPerRow
        }
        
        return CGSize(width: widthPerItem, height: 120)
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
//    {
//
//        return CGSize(width: collectionView.frame.width, height:30)
//
//
//
//    }
    
    
}
//class HamburgerMenu{
//    //Class To Implement Easy Functions To Open Or Close RearView
//    //Make object of this class and call functions
//    func triggerSideMenu(){
//        let notificationOpenOrCloseSideMenu = Notification.Name("notificationOpenOrCloseSideMenu")
//        NotificationCenter.default.post(name: notificationOpenOrCloseSideMenu, object: nil)
//    }
//
//    func closeSideMenu(){
//        let notificationCloseSideMenu = Notification.Name("notificationCloseSideMenu")
//        NotificationCenter.default.post(name: notificationCloseSideMenu, object: nil)
//    }
//
//    func closeSideMenuWithoutAnimation(){
//        let notificationCloseSideMenuWithoutAnimation = Notification.Name("notificationCloseSideMenuWithoutAnimation")
//        NotificationCenter.default.post(name: notificationCloseSideMenuWithoutAnimation, object: nil)
//    }
//
//}
