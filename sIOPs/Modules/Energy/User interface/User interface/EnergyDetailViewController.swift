//
//  ViewController.swift
//  sIOPs
//
//  Created by ASM ESPL on 19/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class EnergyDetailViewController: BaseViewController {

    @IBOutlet weak var filterBtn: BadgeButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var energyDetailtableView:UITableView!
    var energyDetailArray:NSArray?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func filterBtnAction(_ sender: Any) {
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> EnergyViewProtocol{
        return UIStoryboard(name: "EnergyVC", bundle: nil).instantiateViewController(withIdentifier: "EnergyDetail") as! EnergyViewController
    }
    
    
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(EnergyResponse:EnergyModel)
    {
        stopAnimating()
        
        energyDetailtableView.reloadData()
        
        
        
    }
    

}

extension EnergyDetailViewController  : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SiteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SiteTableViewCell", for: indexPath as IndexPath) as! SiteTableViewCell
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return selectedArray?.count ?? 0
        return energyDetailArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
   
    
}
