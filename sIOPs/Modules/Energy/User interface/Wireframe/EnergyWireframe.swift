//
//  EnergyWireframe.swift
//  sIOPs
//
//  Created by ASM ESPL on 18/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class EnergyWireFrame: EnergyWireFrameProtocol {
    
    
    
    // MARK: EnergyWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
  class func presentEnergyModule(fromView:AnyObject,application:LoginApplication) -> BaseViewController  {

        // Generating module components
        let view: EnergyViewProtocol = EnergyViewController.instantiate()
        let presenter: EnergyPresenterProtocol & EnergyInteractorOutputProtocol = EnergyPresenter()
        let interactor: EnergyInteractorInputProtocol = EnergyInteractor()
       
        let wireFrame: EnergyWireFrameProtocol = EnergyWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! EnergyViewController
        viewController.application = application
        return viewController
        //NavigationHelper.pushViewController(viewController: viewController)
    }
    

    // MARK: EnergyWireFrameProtocol
      
      /// Static method that initializes every class needed
      ///
      /// - Parameter fromView: default parameter
    class func presentEnergyDetailModule(fromView:AnyObject,application:LoginApplication)  {

          // Generating module components
          let view: EnergyViewProtocol = EnergyViewController.instantiate()
          let presenter: EnergyPresenterProtocol & EnergyInteractorOutputProtocol = EnergyPresenter()
          let interactor: EnergyInteractorInputProtocol = EnergyInteractor()
         
          let wireFrame: EnergyWireFrameProtocol = EnergyWireFrame()

          // Connecting
          view.presenter = presenter
          presenter.view = view
          presenter.wireFrame = wireFrame
          presenter.interactor = interactor
          interactor.presenter = presenter
          
          let viewController = view as! EnergyViewController
          viewController.application = application
         
        
          NavigationHelper.pushViewController(viewController: viewController)
      }
   

}

