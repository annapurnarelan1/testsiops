//
//  InfraDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol InfraDetailViewProtocol: class {
    var presenter: InfraDetailPresenterProtocol? { get set }
    //func InfraDetailDone(InfraDetailRes :InfraDetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadHistoryData(response:AlarmModel)
    func reloadsiteDownData(response:SiteDownModel)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol InfraDetailWireFrameProtocol: class {
    static func presentAlarmDetailModule(fromView:AnyObject,type:String,selected:String,response:Any,applicationDic:LoginApplication)
     static func presentInfraDetailModule(fromView:AnyObject,type:String,selected:String,response:Any,applicationDic:LoginApplication,selectedSite:InfraCellImpacted)
 //  func presentOpenAlertsDetailModule(type:String,infraResponse:InfraModel)
    func presentFilterScreen(type:String,selected:String,previousView:Any)
    func presentAlarmDetailModule(alarmOutlierList:AlarmReasonList,applicationCode:String,response:Any,filterSelected:[String:Any])
     func presentSiteDownDetailModule(type:String,selected:String,response:DownList,selectedSite:InfraCellImpacted,filterSelected:[String:Any])
}

/// Method contract between VIEW -> PRESENTER
protocol InfraDetailPresenterProtocol: class {
    var view: InfraDetailViewProtocol? { get set }
    var interactor: InfraDetailInteractorInputProtocol? { get set }
    var wireFrame: InfraDetailWireFrameProtocol? { get set }
  //  func presentInfraDetailModule(type:String,infraResponse:InfraModel)
    
    func presentFilterScreen(type:String,selected:String,previousView:Any)
    func requestAlarmData(category:String)
    func requestSiteDownData(category:String,id:String)
    func presentAlarmDetailModule(alarmOutlierList:AlarmReasonList,applicationCode:String,response:Any,filterSelected:[String:Any])
     func presentSiteDownDetailModule(type:String,selected:String,response:DownList,selectedSite:InfraCellImpacted,filterSelected:[String:Any])
    func requestAlarmDataFilter(category:String,filter:[String:Any])
    func requestSiteDownDataFilter(category:String,id:String,filter:[String:Any])
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol InfraDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadHistoryData(response:AlarmModel)
    func reloadsiteDownData(response:SiteDownModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol InfraDetailInteractorInputProtocol: class
{
    var presenter: InfraDetailInteractorOutputProtocol? { get set }
    func requestAlarmData(category:String)
    func requestSiteDownData(category:String,id:String)
    func requestAlarmDataFilter(category:String,filter:[String:Any])
    func requestSiteDownDataFilter(category:String,id:String,filter:[String:Any])
    
}
