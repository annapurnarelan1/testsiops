//
//  InfraDetailVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class InfraDetailVC: BaseViewController,InfraDetailViewProtocol {
    
    var presenter: InfraDetailPresenterProtocol?
    
    @IBOutlet weak var fourthView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var descriptionThirdLbl: UILabel!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var descriptionSecondLbl: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var secondlbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var filteBtn: BadgeButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var descriptionfourthLbl: UILabel!
    @IBOutlet weak var fourthBtn: UIButton!
    
    var type:String?
    var selected:String?
    var infraResponse:Any?
    var selectedArray:[Any]?
    var applicationCode:String?
    var selectedSite:InfraCellImpacted?
    var selectedCategory :String = "All"
    
    var selectedFilter = [String:Any]()
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> InfraDetailViewProtocol{
        return UIStoryboard(name: "InfraDetail", bundle: nil).instantiateViewController(withIdentifier: "InfraDetail") as! InfraDetailVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        
        
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setAlarmDetail()
    {
        if let alarmResponse = infraResponse as? AlarmModel
        {
            let normalText = selected ?? ""
            
            
            let boldText  = "  \(alarmResponse.alarmCount ?? 0)"
            
            let attributedString = NSMutableAttributedString(string:normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs as [NSAttributedString.Key : Any])
            
            attributedString.append(boldString)
            
            
            
            titleLbl.attributedText = attributedString
            firstLabel.text = "\(alarmResponse.alarmsList[0].count ?? "0")"
            descriptionFirstLbl.text = alarmResponse.alarmsList[0].name ?? ""
            secondlbl.text = "\(alarmResponse.alarmsList[1].count ?? "0")"
            descriptionSecondLbl.text = alarmResponse.alarmsList[1].name ?? ""
            thirdLbl.text = "\(alarmResponse.alarmsList[2].count ?? "0")"
            descriptionThirdLbl.text = alarmResponse.alarmsList[2].name ?? ""
            
            self.selectedArray = alarmResponse.list
            // setSelectedArray()
        }
        
        
    }
    
    func setSiteDownDetail()
    {
        if let response = infraResponse as? SiteDownModel
        {
            let normalText = selected ?? ""
            
            
            let boldText  = "  \(selectedSite?.outlierCount ?? 0)"
            
            let attributedString = NSMutableAttributedString(string:" " + normalText)
            
            let attrs = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Bold", size: 12.0)]
            let boldString = NSMutableAttributedString(string: boldText, attributes:attrs as [NSAttributedString.Key : Any])
            
            attributedString.append(boldString)
            
            
            
            titleLbl.attributedText = attributedString
            firstLabel.text = "\(response.sitesList[0].count ?? "0")"
            descriptionFirstLbl.text = response.sitesList[0].name ?? ""
            secondlbl.text = "\(response.sitesList[1].count ?? "0")"
            descriptionSecondLbl.text = response.sitesList[1].name ?? ""
            if(response.sitesList.count > 2)
            {
                thirdView.isHidden = false
                thirdLbl.text = "\(response.sitesList[2].count ?? "0")"
                descriptionThirdLbl.text = response.sitesList[2].name ?? ""
                
            }
            else
            {
                thirdView.isHidden = true
                thirdLbl.isHidden = true
                descriptionThirdLbl.isHidden = true
               
            }
            
            if(response.sitesList.count > 3)
            {
                fourthView.isHidden = false
                fourthLabel.text = "\(response.sitesList[3].count ?? "0")"
                descriptionfourthLbl.text =  response.sitesList[3].name ?? ""
            }
            else
            {
                fourthView.isHidden = true
                fourthLabel.isHidden = true
                descriptionfourthLbl.isHidden = true
            }
            
            
            self.selectedArray = response.list
            // setSelectedArray()
        }
        
        
    }
    
    
    
    func setSelectedArray() {
        //        switch selected {
        //        case Constants.openAlerts.infra :
        //            selectedArray = NGOResponse?.infra
        //            firstLabel.font =  UIFont(name: "JioType-Medium", size: 22)
        //        case Constants.openAlerts.application :
        //            selectedArray = NGOResponse?.application
        //            secondlbl.font =  UIFont(name: "JioType-Medium", size: 22)
        //        case Constants.openAlerts.tools :
        //           selectedArray = NGOResponse?.tools
        //           thirdLbl.font =  UIFont(name: "JioType-Medium", size: 22)
        //        default:
        //            print("nothing selected")
        //        }
    }
    
    func reloadHistoryData(response:AlarmModel)
    {
        stopAnimating()
        
        
        self.selectedArray = response.list
        tableView.reloadData()
        
        
    }
    
    func reloadsiteDownData(response:SiteDownModel)
    {
        stopAnimating()
        self.selectedArray = response.list
        tableView.reloadData()
    }
    
    @IBAction func InfraDetailOpenAlerts(sender:Any)
    {
        firstLabel.font =  UIFont(name: "JioType-Light", size: 22)
        secondlbl.font =  UIFont(name: "JioType-Light", size: 22)
        thirdLbl.font =  UIFont(name: "JioType-Light", size: 22)
        fourthLabel.font = UIFont(name: "JioType-Light", size: 22)
        
        let btn:UIButton = (sender as? UIButton)!
        filteBtn.badge = nil
        switch btn.tag {
        case 0:
            
            firstLabel.font =  UIFont(name: "JioType-Medium", size: 22)
            if(type == Constants.InfraDetail.alarm)
            {
                if let alarmResponse = infraResponse as? AlarmModel
                {
                    startAnimating()
                    selectedCategory = alarmResponse.alarmsList[0].name ?? ""
                    self.presenter?.requestAlarmData(category:alarmResponse.alarmsList[0].name)
                }
                
            }
            else
            {
                if let response = infraResponse as? SiteDownModel
                {
                    
                    startAnimating()
                    selectedCategory = response.sitesList[0].name ?? ""
                    
                    self.presenter?.requestSiteDownData(category:response.sitesList[0].name ?? "",id:selectedSite?.featureId ?? "")
                    
                }
            }
        case 1:
            secondlbl.font =  UIFont(name: "JioType-Medium", size: 22)
            if(type == Constants.InfraDetail.alarm)
            {
                if let alarmResponse = infraResponse as? AlarmModel
                {
                    startAnimating()
                    selectedCategory = alarmResponse.alarmsList[1].name ?? ""
                    self.presenter?.requestAlarmData(category:alarmResponse.alarmsList[1].name)
                }
                
            }
            else
            {
                if let response = infraResponse as? SiteDownModel
                {
                    
                    startAnimating()
                    selectedCategory = response.sitesList[1].name ?? ""
                    self.presenter?.requestSiteDownData(category:response.sitesList[1].name ?? "",id:selectedSite?.featureId ?? "")
                    
                }
            }
        case 2:
            thirdLbl.font =  UIFont(name: "JioType-Medium", size: 22)
            if(type == Constants.InfraDetail.alarm)
            {
                startAnimating()
                if let alarmResponse = infraResponse as? AlarmModel
                {
                    selectedCategory = alarmResponse.alarmsList[2].name ?? ""
                    self.presenter?.requestAlarmData(category:alarmResponse.alarmsList[2].name)
                }
                
            }
            else
            {
                if let response = infraResponse as? SiteDownModel
                {
                    
                    startAnimating()
                    selectedCategory = response.sitesList[2].name ?? ""
                    self.presenter?.requestSiteDownData(category:response.sitesList[2].name ?? "",id:selectedSite?.featureId ?? "")
                    
                }
            }
        case 3:
            fourthLabel.font =  UIFont(name: "JioType-Medium", size: 22)
            
            if let response = infraResponse as? SiteDownModel
            {
                
                startAnimating()
                selectedCategory = response.sitesList[3].name ?? ""
                self.presenter?.requestSiteDownData(category:response.sitesList[3].name ?? "",id:selectedSite?.featureId ?? "")
                
            }
            
            
        default:
            print("nothing selected")
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
        if(type == Constants.InfraDetail.alarm)
        {
            setAlarmDetail()
        }
        else
        {
            setSiteDownDetail()
        }
    }
    
     
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    @IBAction func filterBtnAcn(_ sender: Any) {
        
        
        self.presenter?.presentFilterScreen(type:type!,selected:selected ?? "",previousView:self)
        
        
        
    }
    
}

extension InfraDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OutlierTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell", for: indexPath as IndexPath) as! OutlierTableViewCell
        if(type == "Alarms")
        {
            cell.titleLbl.text = (selectedArray?[indexPath.section] as? AlarmReasonList)?.name ?? ""
            cell.countLbl.text = "\((selectedArray?[indexPath.section] as? AlarmReasonList)?.count ?? "0")"
        }
        else
        {
            cell.titleLbl.text = (selectedArray?[indexPath.section] as? DownList)?.name ?? ""
            cell.countLbl.text = "\((selectedArray?[indexPath.section] as? DownList)?.count ?? "0")"
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if type == Constants.InfraDetail.alarm
        {
            self.presenter?.presentAlarmDetailModule(alarmOutlierList: (selectedArray?[indexPath.section])! as! AlarmReasonList, applicationCode: applicationCode ?? "", response: infraResponse,filterSelected:selectedFilter)
        }
        else
        {
            self.presenter?.presentSiteDownDetailModule(type:(selectedArray?[indexPath.section] as? DownList)?.name ?? "",selected:selectedCategory,response:(selectedArray?[indexPath.section] as? DownList)!,selectedSite:selectedSite!,filterSelected:self.selectedFilter)
        }
        
    }
}


extension InfraDetailVC :FilterControllerDelegate
{
    func filterApplied(selectedFilter:[String:Any])
    {
        
        if(type == Constants.InfraDetail.alarm)
        {
            if (infraResponse as? AlarmModel) != nil
            {
                filteBtn.badge = "\(selectedFilter.count)"
                startAnimating()
                self.presenter?.requestAlarmDataFilter(category: selectedCategory, filter: selectedFilter)
                
                
            }
            
            self.selectedFilter = selectedFilter
            
        }
        else
        {
            if let response = infraResponse as? SiteDownModel
            {
                filteBtn.badge = "\(selectedFilter.count)"
                startAnimating()
                selectedCategory = response.sitesList[0].name ?? ""
                
                self.presenter?.requestSiteDownDataFilter(category:selectedCategory,id:selectedSite?.featureId ?? "", filter: selectedFilter)
                
            }
            self.selectedFilter = selectedFilter
        }
        
    }
    
    func clearFilters()
    {
        filteBtn.badge  = nil
        
        self.selectedFilter = [:]
        if(type == Constants.InfraDetail.alarm)
        {
            if (infraResponse as? AlarmModel) != nil
            {
                // filteBtn.badge = "\(selectedFilter.count)"
                startAnimating()
                
                self.presenter?.requestAlarmData(category:selectedCategory)
                
                
            }
            
        }
        else
        {
            if let response = infraResponse as? SiteDownModel
            {
                
                startAnimating()
                
                
                selectedCategory = response.sitesList[0].name ?? ""
                
                self.presenter?.requestSiteDownData(category:response.sitesList[0].name ?? "",id:selectedSite?.featureId ?? "")
                
            }
        }
    }
}
