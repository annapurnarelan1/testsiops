//
//  InfraDetailDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class InfraDetailPresenter:BasePresenter, InfraDetailPresenterProtocol, InfraDetailInteractorOutputProtocol {
   
    
    

    // MARK: Variables
    weak var view: InfraDetailViewProtocol?
    var interactor: InfraDetailInteractorInputProtocol?
    var wireFrame: InfraDetailWireFrameProtocol?
    let stringsTableName = "InfraDetail"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
       func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
//    func requestData()
//    {
//        self.interactor?.requestData()
//    }
    
//func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO)
//{
//    self.wireFrame?.presentOpenAlertsDetailModule(type:type,ngoResponse:ngoResponse)
//
//    }
    
    func presentFilterScreen(type:String,selected:String,previousView:Any)
    {
        self.wireFrame?.presentFilterScreen(type:type,selected:selected,previousView:previousView)
    }
    
    func presentAlarmDetailModule(alarmOutlierList:AlarmReasonList,applicationCode:String,response:Any,filterSelected:[String:Any])
    {
         self.wireFrame?.presentAlarmDetailModule(alarmOutlierList:alarmOutlierList,applicationCode:applicationCode,response:response,filterSelected:filterSelected)
    }
    
    func reloadHistoryData(response:AlarmModel)
    {
        self.view?.reloadHistoryData(response:response)
    }
    func requestAlarmData(category:String)
    {
        self.interactor?.requestAlarmData(category:category)
    }
    func requestSiteDownData(category:String,id:String)
    {
        self.interactor?.requestSiteDownData(category:category,id:id)
    }
    
    func reloadsiteDownData(response:SiteDownModel)
    {
        self.view?.reloadsiteDownData(response:response)
    }
    
    func presentSiteDownDetailModule(type: String, selected: String, response: DownList, selectedSite: InfraCellImpacted,filterSelected:[String:Any])
     {
        self.wireFrame?.presentSiteDownDetailModule(type:type,selected:selected,response:response,selectedSite:selectedSite,filterSelected:filterSelected)
    }
    
    func requestAlarmDataFilter(category:String,filter:[String:Any])
    {
        self.interactor?.requestAlarmDataFilter(category:category,filter:filter)
    }
    
    func requestSiteDownDataFilter(category:String,id:String,filter:[String:Any])
    {
        self.interactor?.requestSiteDownDataFilter(category:category,id:id,filter:filter)
    }
}
