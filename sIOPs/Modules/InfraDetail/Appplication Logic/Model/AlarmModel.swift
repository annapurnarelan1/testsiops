//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AlarmModel : NSObject, NSCoding{

	var alarmsList : [AlarmsList]!
	var list : [AlarmReasonList]!
	var userName : String!
    var alarmCount : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
        
        alarmCount = dictionary["alarmCount"] as? Int
		alarmsList = [AlarmsList]()
		if let alarmsListArray = dictionary["alarmsList"] as? [[String:Any]]{
			for dic in alarmsListArray{
				let value = AlarmsList(fromDictionary: dic)
				alarmsList.append(value)
			}
		}
		list = [AlarmReasonList]()
		if let listArray = dictionary["list"] as? [[String:Any]]{
			for dic in listArray{
				let value = AlarmReasonList(fromDictionary: dic)
				list.append(value)
			}
		}
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if alarmCount != nil{
            dictionary["alarmCount"] = alarmCount
        }
		if alarmsList != nil{
			var dictionaryElements = [[String:Any]]()
			for alarmsListElement in alarmsList {
				dictionaryElements.append(alarmsListElement.toDictionary())
			}
			dictionary["alarmsList"] = dictionaryElements
		}
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         alarmsList = aDecoder.decodeObject(forKey :"alarmsList") as? [AlarmsList]
         list = aDecoder.decodeObject(forKey :"list") as? [AlarmReasonList]
         userName = aDecoder.decodeObject(forKey: "userName") as? String
        alarmCount = aDecoder.decodeObject(forKey: "alarmCount") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if alarmsList != nil{
			aCoder.encode(alarmsList, forKey: "alarmsList")
		}
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}
        if alarmCount != nil{
            aCoder.encode(alarmCount, forKey: "alarmCount")
        }

	}

}
