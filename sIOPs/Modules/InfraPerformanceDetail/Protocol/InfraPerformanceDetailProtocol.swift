//
//  InfraPerformanceDetailDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol InfraPerformanceDetailViewProtocol: class {
    var presenter: InfraPerformanceDetailPresenterProtocol? { get set }
    //func InfraPerformanceDetailDone(InfraPerformanceDetailRes :InfraPerformanceDetailModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
   // func reloadData(InfraPerformanceDetailResponse:InfraPerformanceDetail)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol InfraPerformanceDetailWireFrameProtocol: class {
    static func presentInfraPerformanceDetailModule(fromView:AnyObject,type:String,selected:String,ngoResponse:NGO)
   func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
}

/// Method contract between VIEW -> PRESENTER
protocol InfraPerformanceDetailPresenterProtocol: class {
    var view: InfraPerformanceDetailViewProtocol? { get set }
    var interactor: InfraPerformanceDetailInteractorInputProtocol? { get set }
    var wireFrame: InfraPerformanceDetailWireFrameProtocol? { get set }
    func presentOpenAlertsDetailModule(type:String,ngoResponse:NGO,count:String)
    func requestData()
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol InfraPerformanceDetailInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
   // func reloadData(InfraPerformanceDetailResponse:InfraPerformanceDetail)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol InfraPerformanceDetailInteractorInputProtocol: class
{
    var presenter: InfraPerformanceDetailInteractorOutputProtocol? { get set }
    func requestData()
    
}
