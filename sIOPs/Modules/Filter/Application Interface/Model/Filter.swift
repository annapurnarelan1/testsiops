//
//	Filter.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Filter : NSObject, NSCoding{

	var code : String!
	var endRang : String!
	var name : String!
	var startRang : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		code = dictionary["code"] as? String
		endRang = dictionary["end_rang"] as? String
		name = dictionary["name"] as? String
		startRang = dictionary["start_rang"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if code != nil{
			dictionary["code"] = code
		}
		if endRang != nil{
			dictionary["end_rang"] = endRang
		}
		if name != nil{
			dictionary["name"] = name
		}
		if startRang != nil{
			dictionary["start_rang"] = startRang
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         code = aDecoder.decodeObject(forKey: "code") as? String
         endRang = aDecoder.decodeObject(forKey: "end_rang") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         startRang = aDecoder.decodeObject(forKey: "start_rang") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if endRang != nil{
			aCoder.encode(endRang, forKey: "end_rang")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if startRang != nil{
			aCoder.encode(startRang, forKey: "start_rang")
		}

	}

}