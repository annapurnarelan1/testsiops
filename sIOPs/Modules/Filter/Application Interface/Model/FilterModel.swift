//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FilterModel : NSObject, NSCoding{

	var filter : [Filter]!
	var filterName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		filter = [Filter]()
		if let filterArray = dictionary["filter"] as? [[String:Any]]{
			for dic in filterArray{
				let value = Filter(fromDictionary: dic)
				filter.append(value)
			}
		}
		filterName = dictionary["filterName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if filter != nil{
			var dictionaryElements = [[String:Any]]()
			for filterElement in filter {
				dictionaryElements.append(filterElement.toDictionary())
			}
			dictionary["filter"] = dictionaryElements
		}
		if filterName != nil{
			dictionary["filterName"] = filterName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         filter = aDecoder.decodeObject(forKey :"filter") as? [Filter]
         filterName = aDecoder.decodeObject(forKey: "filterName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if filter != nil{
			aCoder.encode(filter, forKey: "filter")
		}
		if filterName != nil{
			aCoder.encode(filterName, forKey: "filterName")
		}

	}

}
