//
//  FilterInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class FilterInteractor: FilterInteractorInputProtocol {

public enum FilterError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: FilterInteractorOutputProtocol?
    
    
    func requestData(filterType:String){
        
        
        let pubInfo: [String : Any] =
        ["timestamp": String(Date().ticks),
        "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
        "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
        "osType": "ios",
        "lang": "en_US",
        "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
           // "appRoleCode": InfraParams.applicationCode ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            "ddqCode":filterType
            
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        let busicode = Constants.BusiCode.Filter
//        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
//        {
//            busicode = "DashSummary"
//        }
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                    switch result {
                    case .success(let returnJson) :
                        print(returnJson)
                        
                       
                        let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? "NOK" : "OK"
                        
                        switch status {
                        case "OK":
                                 let filterData = FilterModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                 self.presenter?.reloadData(filterResponse:filterData)
                          case "NOK":
                                 let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                self.presenter?.showFailError(error:message)
                        default:
                            self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                        }
                        
        //                let loginRes: LoginModel = LoginModel.sharedInstance
        //
        //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
        //                LoginModel.sharedInstance.saveUser()
                        
                       // self.presenter?.loginDone(loginRes:loginRes)
                        
                       
                   case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
                    }
                })
        
    }
    
    
    func requestDataWithSelectedFilter(filterType:String,filterSelected:Filter,parentFilterSelected:Filter){
            
            
            let pubInfo: [String : Any] =
            ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
            
            let requestHeader:[String :Any] = [
                "serviceName": "userInfo"
            ]
            
            let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               // "appRoleCode": InfraParams.applicationCode ?? "",
                "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
                "type": "userInfo",
                "ddqCode":filterType,
                "category":filterSelected.name ?? "",
                "parentCode":parentFilterSelected.code ?? ""
                
            ]
            
           let busiParams: [String : Any] = [
                "requestHeader": requestHeader,
                "requestBody": requestBody,
            ]
           
            let busicode = Constants.BusiCode.Filter
    //        if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO")
    //        {
    //            busicode = "DashSummary"
    //        }
            
            let requestList: [String: Any] = [
                "busiCode": busicode,
                "isEncrypt": false,
                "transactionId": "0001574162779054",
                "busiParams":busiParams
            ]
            
            let jsonParameter = [
                "pubInfo" : pubInfo,
                "requestList" : [requestList]
                ] as [String : Any]
            
            APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
                        switch result {
                        case .success(let returnJson) :
                            print(returnJson)
                            
                           
                            let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? "NOK" : "OK"
                            
                            switch status {
                            case "OK":
                                     let filterData = FilterModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                     self.presenter?.reloadData(filterResponse:filterData)
                              case "NOK":
                                     let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                                    self.presenter?.showFailError(error:message)
                            default:
                                self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                            }
                            
            //                let loginRes: LoginModel = LoginModel.sharedInstance
            //
            //                loginRes.initData(fromDictionary: returnJson as? [String : Any] ?? [:])
            //                LoginModel.sharedInstance.saveUser()
                            
                           // self.presenter?.loginDone(loginRes:loginRes)
                            
                           
                        case .failure(let failure) :
                            switch failure {
                            case .connectionError:
                            self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                                
                            case .authorizationError(let errorJson):
                               self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                            default:
                                self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                                
                            }
                        }
                    })
            
        }
    
}
