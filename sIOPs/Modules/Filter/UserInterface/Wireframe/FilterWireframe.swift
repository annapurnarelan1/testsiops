//
//  FilterWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class FilterWireFrame: FilterWireFrameProtocol {
    
    
    
    // MARK: FilterWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentFilterModule(fromView:AnyObject,type:String,selected:String,previousView:Any) {

        // Generating module components
        let view: FilterViewProtocol = FilterViewController.instantiate()
        let presenter: FilterPresenterProtocol & FilterInteractorOutputProtocol = FilterPresenter()
        let interactor: FilterInteractorInputProtocol = FilterInteractor()
       
        let wireFrame: FilterWireFrameProtocol = FilterWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        
        let viewController = view as! FilterViewController
        viewController.type = type
        viewController.delegate = previousView as? InfraDetailVC
       
        NavigationHelper.presentViewController(viewController: viewController)
    }
    
    func filterDetailOpenAlerts(type:String,selected:String,filterModelResponse filterResponse:InfraModel)
    {
      //  InfraWireFrame.presentInfraModule(fromView: self, type: type, selected: selected, infraResponse: infraResponse)
        
    }
   

}
