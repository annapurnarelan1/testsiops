//
//  FilterPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class FilterPresenter:BasePresenter, FilterPresenterProtocol, FilterInteractorOutputProtocol {
    
    
    // MARK: Variables
    weak var view: FilterViewProtocol?
    var interactor: FilterInteractorInputProtocol?
    var wireFrame: FilterWireFrameProtocol?
    let stringsTableName = "filter"
    

    /// Called to show error popup
    func show(error: String,description:String)
    {
        self.view?.show(image: "error_popup", error: error, description: description)
    }
    
    /// Called when there is no network
    
    
     func stopLoader()
     {
         self.view?.stopLoader()
     }
    
    func showFailError(error:String)
    {
        self.view?.showFailError(error:error)
    }
    
     func requestData(filterType:String)
    {
        self.interactor?.requestData(filterType:filterType)
    }
    
    func reloadData(filterResponse:FilterModel)
    {
        self.view?.reloadData(filterModelResponse:filterResponse)
    }
    
    func filterDetailOpenAlerts(type:String,selected:String,filterModelResponse filterResponse:InfraModel)
    {
      //  self.wireFrame?.filterDetailOpenAlerts(type: type, selected: selected, filterModelResponse: filterResponse)
    }
    
    func requestDataWithSelectedFilter(filterType:String,filterSelected:Filter,parentFilterSelected:Filter)
    {
        self.interactor?.requestDataWithSelectedFilter(filterType: filterType, filterSelected: filterSelected, parentFilterSelected: parentFilterSelected)
    }
}
