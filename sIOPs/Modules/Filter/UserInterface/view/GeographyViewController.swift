//
//  GeographyViewController.swift
//  sIOPs
//
//  Created by mac on 12/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

protocol GeographyControllerDelegate : NSObjectProtocol {
    func reloadTable(select:Filter,type:String,index:Int)
    
}

class GeographyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var listArray: [Filter]?
    weak var delegate: GeographyControllerDelegate?
    var type:String?
    var index:Int?
    
    @IBOutlet weak var tblVw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        cell?.textLabel?.text = listArray?[indexPath.row].name
        
        return cell ?? UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.reloadTable(select: (listArray?[indexPath.row])!, type: type ?? "", index: index!)
        self.dismiss(animated: true, completion: nil)
        
    }
    

   

}
