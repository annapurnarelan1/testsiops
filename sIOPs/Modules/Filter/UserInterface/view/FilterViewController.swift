//
//  FilterViewController.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit



protocol FilterControllerDelegate : NSObjectProtocol {
    func filterApplied(selectedFilter:[String:Any])
    func clearFilters()
    
}


class FilterViewController: BaseViewController,FilterViewProtocol{
    
    var presenter: FilterPresenterProtocol?
    
    @IBOutlet weak var categoryPickerView: UIPickerView!
    @IBOutlet weak var filterCategoryValueTableView: UITableView!
    @IBOutlet weak var filterCategoryTableView: UITableView!
    @IBOutlet weak var regionTextfield: UITextField!
    var senderSelectedIndex:Int?
    @IBOutlet weak var JCTextField: UITextField!
    @IBOutlet weak var mpTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var geographyStackView: UIStackView!
    weak var delegate: FilterControllerDelegate?
    var filterCategoryDic = ["Ageing" : Constants.filterTypes.Ageing , "Impacted customers" : Constants.filterTypes.ImpactedCustomer, "Work order" : Constants.filterTypes.WorkOrder , "Geography": Constants.filterTypes.InventoryMasterRegion]
    var keyCategoryArray = ["Ageing",  "Impacted customers" , "Work order" , "Geography"]
    var geographyArray = ["Region",  "R4G State" , "MP" , "JC"]
    
    var  geographyDic = ["Region" : "All"]
    var geographyFilterDic = [String:Filter]()
    
    var pickerData: [String] = [String]()
    var selectedGeoValue : Any = String()
    var currentTableViewIndex :Int = 0
    var selectedCategoryArray :[Any] = []
    var filterResponse :[Filter]?
    var selectedGeographyArray :[Any] = ["ALL"]
    var type:String?
    var selectedCell:Int?
    var indexGeography:Int?
    var indexpathGeo:IndexPath?
    var selectedFilters =  [String:Any]()
    //    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterCategoryTableView.register(UINib(nibName: "FilterCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterCategoryTableViewCell")
        filterCategoryValueTableView.register(UINib(nibName: "FilterCategoryValueTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterCategoryValueTableViewCell")
        filterCategoryValueTableView.register(UINib(nibName: "RegionTableViewCell", bundle: nil), forCellReuseIdentifier: "region")
      //  pickerData = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"]
        selectedGeoValue = ""
        
        
        
        if(type == Constants.InfraDetail.alarm){
            filterCategoryDic.removeValue(forKey: "Work order")
            keyCategoryArray.remove(at: 2)
            filterCategoryDic.removeValue(forKey: "Impacted customers")
            keyCategoryArray.remove(at: 1)
            
        }
        else
        {
            filterCategoryDic.removeValue(forKey: "Work order")
            keyCategoryArray.remove(at: 2)
        }
        
        
        startAnimating()
        selectedCell = 0
        senderSelectedIndex = 0

        self.presenter?.requestData(filterType: Constants.filterTypes.Ageing)
       
    }
    
   
    static func instantiate() -> FilterViewProtocol{
        return UIStoryboard(name: "Filter", bundle: nil).instantiateViewController(withIdentifier: "filter") as! FilterViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        let indexPath = IndexPath(row: 0, section: 0)
        filterCategoryTableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        let cell = filterCategoryTableView.cellForRow(at: indexPath) as! FilterCategoryTableViewCell
        cell.titleLbl.textColor = UIColor.init(hexString: "#214796")
        cell.backgroundColor = UIColor.white
    }
    
    override func viewDidLayoutSubviews()
    {
        //        heightConstraint.constant = self.InfraCollectionView.contentSize.height
    }
    
     
    
     func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(filterModelResponse:FilterModel)
    {
        stopAnimating()
        filterResponse = filterModelResponse.filter
       // filterCategoryValueTableView.reloadData()
        
         if(type == Constants.InfraDetail.alarm)
            {
                if(selectedCell == 0)
                {
                    selectedFilters["Ageing"] = filterResponse?[senderSelectedIndex ?? 0]
                }
            }
            else
            {
                if(selectedCell == 0)
                {
                    selectedFilters["Ageing"] = filterResponse?[senderSelectedIndex ?? 0]
                }
                else if(selectedCell == 1)
                {
                    selectedFilters["Impacted customers"] = filterResponse?[senderSelectedIndex ?? 0]
                }
//                else if(selectedCell == 2)
//                {
//                    selectedFilters["Work order"] = filterResponse?[senderSelectedIndex ?? 0]
//                }
            }
        
        if(selectedCell == keyCategoryArray.count - 1)
        {
            let cell = filterCategoryValueTableView.cellForRow(at: indexpathGeo!) as! RegionTableViewCell
            let storyboard = UIStoryboard(name: "Filter", bundle: nil)
            let popVC : GeographyViewController = storyboard.instantiateViewController(withIdentifier: "geography") as! GeographyViewController  // your viewcontroller's id
            popVC.modalPresentationStyle = .popover
            let popover =  popVC.popoverPresentationController
            popover?.delegate = self
            //sourceRect and sourceView is required in case of iPad
            popover?.sourceRect = cell.bounds //give anchor frame
            popover?.sourceView = cell //give anchor view
            popVC.preferredContentSize = CGSize(width: cell.separatorLine.frame.width , height: 300)
            popVC.listArray = filterResponse
            popVC.index = indexpathGeo?.row
            popVC.type = geographyArray[indexpathGeo!.row]
            popVC.delegate = self
            self.present(popVC, animated: true, completion: nil)
            
        }
        
        filterCategoryValueTableView.reloadData()
        

        
    }
    
    
    @objc func radioBtnAcn(_ sender : UIButton)
    {
        
        if(senderSelectedIndex != sender.tag)
        {
            senderSelectedIndex = sender.tag
            if(type == Constants.InfraDetail.alarm){
                
                if(selectedCell == 0)
                {
                    selectedFilters["Ageing"] = filterResponse?[senderSelectedIndex ?? 0]
                }
//                else if(selectedCell == 1)
//                {
//                    //selectedFilters["Work order"] = filterResponse?[senderSelectedIndex ?? 0]
//                }
                
                
                
            }
            else
            {
                if(selectedCell == 0)
                {
                    selectedFilters["Ageing"] = filterResponse?[senderSelectedIndex ?? 0]
                }
                else if(selectedCell == 1)
                {
                    selectedFilters["Impacted customers"] = filterResponse?[senderSelectedIndex ?? 0]
                }
//                else if(selectedCell == 2)
//                {
//                    selectedFilters["Work order"] = filterResponse?[senderSelectedIndex ?? 0]
//                }
            }
            
            
            
            
           // sender.isSelected = false
            
        }
//        else
//        {
//            senderSelectedIndex = -1
//
//            if(type == Constants.InfraDetail.alarm){
//
//                if(selectedCell == 0)
//                {
//                    selectedFilters.removeValue(forKey: "Ageing")
//
//                }
//                else if(selectedCell == 1)
//                {
//                    selectedFilters.removeValue(forKey: "Work order")
//
//                }
//
//
//
//            }
//            else
//            {
//                if(selectedCell == 0)
//                {
//                    selectedFilters.removeValue(forKey: "Ageing")
//
//                }
//                else if(selectedCell == 1)
//                {
//                    selectedFilters.removeValue(forKey: "Impacted customers")
//
//                }
//                else if(selectedCell == 2)
//                {
//                    selectedFilters.removeValue(forKey: "Work order")
//
//                }
//            }
//
//
//        }
        filterCategoryValueTableView.reloadData()
        
    }
    
    @IBAction func applyFilterAction(_ sender: Any) {
        
        self.delegate?.filterApplied(selectedFilter: selectedFilters)
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    @objc func reloadCategoryValueTable(filterType:String)
    {
        
        startAnimating()
        self.presenter?.requestData(filterType: filterType)
        
        
    }
    @objc func openPicker ()
    {categoryPickerView.reloadAllComponents()
        categoryPickerView.isHidden = false
        // Right code for picker
    }
    

    
    @IBAction func closeButtonTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearButtonTapped(_ sender: Any) {
        
        selectedFilters.removeAll()
        self.delegate?.clearFilters()
        self.dismiss(animated: true, completion: nil)
        
    }
}
extension FilterViewController :UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == filterCategoryTableView {
            return keyCategoryArray.count
        }
        else
        {
            if(type == Constants.InfraDetail.alarm){
                
                if(selectedCell == 1)
                {
                    return geographyDic.count
                }
                else
                {
                    return filterResponse?.count ?? 0
                }
            }
                
            else
            {
                if(selectedCell == 2)
                {
                    return geographyDic.count 
                }
                else
                {
                    return filterResponse?.count ?? 0
                }
            }
            
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == filterCategoryTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCategoryTableViewCell", for: indexPath)as! FilterCategoryTableViewCell
            cell.titleLbl.textColor = UIColor.black
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.white
            cell.selectedBackgroundView = bgColorView
            cell.backgroundColor =  UIColor.init(hexString: "#F4F4F4")
            cell.titleLbl.text =  keyCategoryArray[indexPath.row]
            
            return cell
        }
        else {
            
            if(type == Constants.InfraDetail.alarm){
                
                
                if(selectedCell == 1)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "region", for: indexPath) as! RegionTableViewCell
                    let bgColorView = UIView()
                    bgColorView.backgroundColor = UIColor.white
                    cell.selectedBackgroundView = bgColorView
                    cell.titleLabel.text = geographyArray[indexPath.row]
                    cell.valueLbl.text = geographyDic[geographyArray[indexPath.row]]
                    
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCategoryValueTableViewCell", for: indexPath)as! FilterCategoryValueTableViewCell
                    
                    cell.titleLbl.text = filterResponse?[indexPath.row].name ?? ""
                    cell.radioBtn.tag = indexPath.row
                    cell.radioBtn.addTarget(self, action: #selector(radioBtnAcn(_:)), for: .touchUpInside)
                    
                    if(senderSelectedIndex == cell.radioBtn.tag)
                    {
                        cell.radioBtn.setImage(UIImage(named: "selectRadio"), for: .normal)
                       
                    }
                    else
                    {
                         cell.radioBtn.setImage(UIImage(named: "unselectRadio"), for: .normal)
                    }
                    
                    return cell
                }
            }
            else
            {
                if(selectedCell == 2)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "region", for: indexPath) as! RegionTableViewCell
                    let bgColorView = UIView()
                    bgColorView.backgroundColor = UIColor.white
                    cell.selectedBackgroundView = bgColorView
                    cell.titleLabel.text = geographyArray[indexPath.row]
                    cell.valueLbl.text = geographyDic[geographyArray[indexPath.row]]
                    
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCategoryValueTableViewCell", for: indexPath)as! FilterCategoryValueTableViewCell
                    
                    cell.titleLbl.text = filterResponse?[indexPath.row].name ?? ""
                    cell.radioBtn.tag = indexPath.row
                    cell.radioBtn.addTarget(self, action: #selector(radioBtnAcn(_:)), for: .touchUpInside)
                    
                    if(senderSelectedIndex == cell.radioBtn.tag)
                    {
                        cell.radioBtn.setImage(UIImage(named: "selectRadio"), for: .normal)
                       
                    }
                    else
                    {
                         cell.radioBtn.setImage(UIImage(named: "unselectRadio"), for: .normal)
                    }
                    
                    return cell
                }
            }
            
           
            
            
        }
    }
}

extension FilterViewController :UITableViewDelegate,UIPopoverPresentationControllerDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // load second table
        if tableView == filterCategoryTableView {

            filterCategoryTableView.reloadData()
            let cell = tableView.cellForRow(at: indexPath) as! FilterCategoryTableViewCell
            cell.titleLbl.textColor = UIColor.init(hexString: "#214796")
            cell.backgroundColor = UIColor.white
            selectedCell = indexPath.row
            senderSelectedIndex = 0
            
            if(indexPath.row != keyCategoryArray.count - 1)
            {
                reloadCategoryValueTable(filterType:filterCategoryDic[keyCategoryArray[indexPath.row]] ?? "")
            }
            else
            {
                selectedFilters["geography"] = geographyDic
                filterCategoryValueTableView.reloadData()
            }
            
            

        }
        else
        {
            
            if(selectedCell == keyCategoryArray.count - 1)
                    {
                        
                        indexGeography = indexPath.row
                        indexpathGeo = indexPath
                        if(indexPath.row == 0)
                        {
                            
                            reloadCategoryValueTable(filterType:Constants.filterTypes.InventoryMasterRegion)
                        }
                        else if(indexPath.row == 1)
                        {
                            startAnimating()
                            self.presenter?.requestDataWithSelectedFilter(filterType: Constants.filterTypes.InventoryMasterCircle, filterSelected: geographyFilterDic["Region"]!, parentFilterSelected: geographyFilterDic["Region"]!)
                        }
                        else if(indexPath.row == 2)
                        {
                            startAnimating()
                            self.presenter?.requestDataWithSelectedFilter(filterType: Constants.filterTypes.InventoryMasterMP, filterSelected: geographyFilterDic["R4G State"]!, parentFilterSelected: geographyFilterDic["Region"]!)
                        }
                        else if(indexPath.row == 3)
                        {
                            startAnimating()
                            self.presenter?.requestDataWithSelectedFilter(filterType: Constants.filterTypes.InventoryMasterJC, filterSelected: geographyFilterDic["MP"]!, parentFilterSelected: geographyFilterDic["R4G State"]!)
                        }
                        
            }
           
                
                
            }

        }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    
}
extension FilterViewController : GeographyControllerDelegate {
    
    func reloadTable(select:Filter,type:String,index:Int)
    {
        if(index == 0)
        {
            geographyDic["Region"] = select.name
            geographyFilterDic["Region"] = select
            if(select.name == "All")
            {
                geographyDic.removeValue(forKey: "R4G State")
                geographyDic.removeValue(forKey: "MP")
                geographyDic.removeValue(forKey: "JC")
            }
            else
            {
                geographyDic["R4G State"] = "All"
                geographyDic.removeValue(forKey: "MP")
                geographyDic.removeValue(forKey: "JC")
            }
           
        }
        else if(index == 1)
        {
            geographyFilterDic["R4G State"] = select
            geographyDic["R4G State"] = select.name
            if(select.name == "All")
            {
               geographyDic.removeValue(forKey: "MP")
                geographyDic.removeValue(forKey: "JC")
            }
            else
            {
                geographyDic["MP"] = "All"
                geographyDic.removeValue(forKey: "JC")
            }
           
        }
        else if(index == 2)
        {
            geographyFilterDic["MP"] = select
            geographyDic["MP"] = select.name
            if(select.name == "All")
            {
                geographyDic.removeValue(forKey: "JC")
            }
            else
            {
                geographyDic["JC"] = "All"
                
            }
            
        }
        else
        {
            geographyFilterDic["JC"] = select
            geographyDic["JC"] = select.name
        }
        
        selectedFilters["geography"] = geographyDic
        
        filterCategoryValueTableView.reloadData()
    }
}
