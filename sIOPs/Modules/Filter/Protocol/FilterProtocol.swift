//
//  FilterProtocol.swift
//  sIOPs
//
//  Created by Shubhi Garg on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation


/// Method contract between PRESENTER -> VIEW
protocol FilterViewProtocol: class {
    var presenter: FilterPresenterProtocol? { get set }
    //func InfraDone(InfraRes :InfraModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadData(filterModelResponse:FilterModel)

}

/// Method contract between PRESENTER -> WIREFRAME
protocol FilterWireFrameProtocol: class {
    static func presentFilterModule(fromView:AnyObject,type:String,selected:String,previousView:Any)
    //func filterDetailOpenAlerts(type:String,selected:String,filterModelResponse:InfraModel)

}

/// Method contract between VIEW -> PRESENTER
protocol FilterPresenterProtocol: class {
    var view: FilterViewProtocol? { get set }
    var interactor: FilterInteractorInputProtocol? { get set }
    var wireFrame: FilterWireFrameProtocol? { get set }
    
   // func filterDetailOpenAlerts(type:String,selected:String)
    
     func requestData(filterType:String)
    func requestDataWithSelectedFilter(filterType:String,filterSelected:Filter,parentFilterSelected:Filter)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol FilterInteractorOutputProtocol: class {

    func show(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadData(filterResponse:FilterModel)

}

/// Method contract between PRESENTER -> INTERACTOR
protocol FilterInteractorInputProtocol: class
{
    var presenter: FilterInteractorOutputProtocol? { get set }
     func requestData(filterType:String)
    func requestDataWithSelectedFilter(filterType:String,filterSelected:Filter,parentFilterSelected:Filter)
    
}
