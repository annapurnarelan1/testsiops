//
//  DeliquencyDetailPresenter.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Presenter that acts between the interactor and the view, handles view events and interactor outputs
class DeliquencyPresenter:BasePresenter, DeliquencyPresenterProtocol, DeliquencyInteractorOutputProtocol {
  
    


    // MARK: Variables
    weak var view: DeliquencyViewProtocol?
    var interactor: DeliquencyInteractorInputProtocol?
    var wireFrame: DeliquencyWireFrameProtocol?
    let stringsTableName = "Deliquency"
    
    
    /// Called to show error popup
       func show(error: String,description:String)
       {
           self.view?.show(image: "error_popup", error: error, description: description)
       }
       
       /// Called when there is no network
       
       
       func stopLoader()
        {
            self.view?.stopLoader()
        }
    
    func showFailError(error:String)
       {
           self.view?.showFailError(error:error)
       }
    
    
     func requestData(type:String)
    {
        self.interactor?.requestData(type:type)
    }
    
    func requestDataWithTimeSheet()
    {
        self.interactor?.requestDataWithTimeSheet()
    }
    func reloadDataWithTimeSheet(detail: ResponseTimeSheetPayload) {
          self.view?.reloadDataWithTimeSheet(detail: detail)
    }
    func reloadData(detail:DeliquencyModel)
    {
        self.view?.reloadData(detail:detail)
    }
    
    func requestPlatFormData(domaninName:String)
    {
        self.interactor?.requestPlatFormData(domaninName:domaninName)
    }
    
    func reloadPlatFormData(detail:DeliquencyPlatFormModel)
    {
        self.view?.reloadPlatFormData(detail:detail)
    }
    
    func presentDelinquentDetailModule(domainName:String,platformDomain:String,ngoResponse:NGO,count:String)
    {
        self.wireFrame?.presentDelinquentDetailModule(domainName:domainName,platformDomain:platformDomain,ngoResponse:ngoResponse,count:count)
    }
}
