//
//  DeliquencyDetailWireframe.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Wireframe that handles all routing between views
class DeliquencyWireFrame: DeliquencyWireFrameProtocol {
    
    
    
    // MARK: DeliquencyDetailWireFrameProtocol
    
    /// Static method that initializes every class needed
    ///
    /// - Parameter fromView: default parameter
    class func presentDeliquencyModule(fromView:AnyObject,type:String,ngoResponse:NGO,selected:String) {

        // Generating module components
        let view: DeliquencyViewProtocol = DeliquencyVC.instantiate()
        let presenter: DeliquencyPresenterProtocol & DeliquencyInteractorOutputProtocol = DeliquencyPresenter()
        let interactor: DeliquencyInteractorInputProtocol = DeliquencyInteractor()
       
        let wireFrame: DeliquencyWireFrameProtocol = DeliquencyWireFrame()

        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let viewController = view as! DeliquencyVC
        viewController.type = type
        viewController.selected = selected
        
        viewController.NGOResponse =  ngoResponse
           
       
        NavigationHelper.pushViewController(viewController: viewController)
    }
     func presentDelinquentDetailModule(domainName:String,platformDomain:String,ngoResponse:NGO,count:String)
     {
        DelinquentDetailWireFrame.presentDelinquentDetailModule(fromView: self, domainName: domainName, platformDomain: platformDomain, ngoResponse: ngoResponse,count:count)
    }
   

}
