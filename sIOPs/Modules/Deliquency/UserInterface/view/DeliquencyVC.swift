//
//  DeliquencyVC.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit




class DeliquencyVC: BaseViewController,DeliquencyViewProtocol {
    
    var presenter: DeliquencyPresenterProtocol?
    
    @IBOutlet weak var cnstDeliquentDays: NSLayoutConstraint!
    @IBOutlet weak var cnstTitleLAbel: NSLayoutConstraint!
    @IBOutlet weak var cnstTimesheetVWHeight: NSLayoutConstraint!
    @IBOutlet weak var vwCircleFirst: UIView!
    @IBOutlet weak var lblDateFirst: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var lblHourFirst: UILabel!
    
    @IBOutlet weak var lblDateSecond: UILabel!
    @IBOutlet weak var vwCircleSecond: UIView!
    @IBOutlet weak var lblHourSecond: UILabel!
    
    @IBOutlet weak var lblDateThird: UILabel!
    @IBOutlet weak var vwCircleThird: UIView!
    @IBOutlet weak var lblHourThird: UILabel!
    
    @IBOutlet weak var lblDateFourth: UILabel!
    @IBOutlet weak var vwCircleFourth: UIView!
    @IBOutlet weak var lblHourFourth: UILabel!
    
    @IBOutlet weak var lblDateFifth: UILabel!
    @IBOutlet weak var vwCircleFifth: UIView!
    @IBOutlet weak var lblHourFifth: UILabel!
    
    @IBOutlet weak var lblDateSix: UILabel!
    @IBOutlet weak var vwCircleSix: UIView!
    @IBOutlet weak var lblHourSix: UILabel!
    
    @IBOutlet weak var lblDateSeven: UILabel!
    @IBOutlet weak var vwCircleSeven: UIView!
    @IBOutlet weak var lblHourSeven: UILabel!
    
    @IBOutlet weak var lblDeleiquentDays: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myclockingDeliquentView: UIView!
    
    var type:String?
    var selected:String?
    var sectionSelected:Int?
    var NGOResponse:NGO?
    var reponseTimeSheet : ResponseTimeSheetPayload?
    var selectedArray:[DeliquencyDomainList]?
    var platformArray = [Any]()
    var attrs = [
        NSAttributedString.Key.font : UIFont(name: "JioType-Medium", size: 10) ,
        NSAttributedString.Key.foregroundColor : UIColor.link,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    /// Static method will initialize the view
    ///
    /// - Returns: DashboardViewController instance to be presented
    static func instantiate() -> DeliquencyViewProtocol{
        return UIStoryboard(name: "Deliquency", bundle: nil).instantiateViewController(withIdentifier: "Deliquency") as! DeliquencyVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        tableView.register(UINib(nibName: "OutlierTableViewCell", bundle: nil), forCellReuseIdentifier: "OutlierTableViewCell")
        
        //        titleLbl.text =  "\(String(describing: selected)) ( \(String(describing: NGOResponse?.delinquencyCount)) ) " //selected
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No Deliquency Available")
        startAnimating()
        self.presenter?.requestData(type:type ?? "")
        self.presenter?.requestDataWithTimeSheet()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        HelperMethods().removeCustomBarButtons(self)
        HelperMethods().addCustomBarButtons(self, cartClickable: true,isMenu:false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false, animated:true)
        cnstTitleLAbel.constant = 0
        cnstDeliquentDays.constant = 0
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
    func showFailError(error:String)
    {
        stopAnimating()
        super.showToastMsg(error: error)
    }
    
    
    func show(image:String,error: String,description:String) {
        stopAnimating()
        
        super.showErrorView(image:image,error: error,description:description)
    }
    
    func reloadDataWithTimeSheet(detail: ResponseTimeSheetPayload) {
        
        if let deliquentDays = detail.delinquentDays{
            if deliquentDays != 0{
                    cnstTitleLAbel.constant = 19
                    cnstDeliquentDays.constant = 19
            }
        }
        
        if let count = detail.delinquentDate?.count{
            if count > 0 {
                 myclockingDeliquentView.isHidden = false
                //cnstTimesheetVWHeight.constant = 140
                lblDeleiquentDays.text = "My clocking - Delinquency \(detail.delinquentDays!) days"
                let deliquent1 = detail.delinquentDate![0]
                lblHourFirst.text = "\(deliquent1.hrs!)hrs"
                lblDateFirst.text = deliquent1.dates
                vwCircleFirst.backgroundColor = deliquent1.hrs != "0" ? UIColor.green : UIColor.red
                
                let deliquent2 = detail.delinquentDate![1]
                lblHourSecond.text = "\(deliquent2.hrs!)hrs"
                lblDateSecond.text = deliquent2.dates
                vwCircleSecond.backgroundColor = deliquent2.hrs != "0" ? UIColor.green : UIColor.red
                
                let deliquent3 = detail.delinquentDate![2]
                lblHourThird.text = "\(deliquent3.hrs!)hrs"
                lblDateThird.text = deliquent3.dates
                vwCircleThird.backgroundColor = deliquent3.hrs != "0" ? UIColor.green : UIColor.red
                
                let deliquent4 = detail.delinquentDate![3]
                lblHourFourth.text = "\(deliquent4.hrs!)hrs"
                lblDateFourth.text = deliquent4.dates
                vwCircleFourth.backgroundColor = deliquent4.hrs != "0" ? UIColor.green : UIColor.red
                
                let deliquent5 = detail.delinquentDate![4]
                lblHourFifth.text = "\(deliquent5.hrs!)hrs"
                lblDateFifth.text = deliquent5.dates
                vwCircleFifth.backgroundColor = deliquent5.hrs != "0" ? UIColor.green : UIColor.red
                
                let deliquent6 = detail.delinquentDate![5]
                lblHourSix.text = "\(deliquent6.hrs!)hrs"
                lblDateSix.text = deliquent6.dates
                vwCircleSix.backgroundColor = deliquent6.hrs != "0" ? UIColor.green : UIColor.red
                
                let deliquent7 = detail.delinquentDate![6]
                lblHourSeven.text = "\(deliquent7.hrs!)hrs"
                lblDateSeven.text = deliquent7.dates
                vwCircleSeven.backgroundColor = deliquent7.hrs != "0" ? UIColor.green : UIColor.red
            }
          
        }
        self.reponseTimeSheet = detail
        
    }
    
    func showError(error: String,description:String)
    {
        stopAnimating()
        //errorLabel.text = description.uppercased()
    }
    
    func reloadData(detail:DeliquencyModel)
    {
        stopAnimating()
        self.selectedArray = detail.domainList
        
        let count = detail.delinquentEmployeeCount
        titleLbl.text = "\(selected ?? "") (\(count ?? 0))"
        for dic in self.selectedArray ?? []
        {
            platformArray.append([])
        }
        
        super.setupEmptyMessageIfNeededForTable(tableView: self.tableView, array: self.selectedArray ?? [], emptyMessage: "No Deliquency Available")
        self.tableView.reloadData()
    }
    func reloadPlatFormData(detail:DeliquencyPlatFormModel)
    {
        stopAnimating()
        self.platformArray.insert(detail.platformList!, at: sectionSelected ?? 0)
        self.tableView.reloadData()
    }
    
    @objc func arrowTapped(sender:UIButton)
    {
        sectionSelected = sender.tag
        for i in (0..<(selectedArray?.count ?? 0))
        {
            if(i == sender.tag)
            {
                if((platformArray[i] as? [PlatformList])?.count ?? 0 == 0)
                {
                    startAnimating()
                    self.presenter?.requestPlatFormData(domaninName:selectedArray?[sender.tag].domainName ?? "")
                }
                else
                {
                    sectionSelected = -1
                    platformArray.remove(at: i)
                    // tableView.reloadSections([i], with: .none)
                    self.tableView.reloadData()
                    
                }
            }
            else
            {
                if((platformArray[i] as? [PlatformList])?.count ?? 0 > 0)
                {
                    platformArray.remove(at: i)
                    self.tableView.reloadData()
                }
            }
            
        }
    }
}


extension DeliquencyVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell") as! OutlierTableViewCell
        cell.titleLbl.text = "" //selectedArray?[section].domainName.capitalizingFirstLetter() ?? ""
        cell.arrowImage.image = UIImage(named: "down")
        cell.countLbl.text = "\(selectedArray?[section].count ?? 0)"
        cell.expandAction.tag = section
        cell.expandAction.addTarget(self,action:#selector(arrowTapped),
                                    for:.touchUpInside)
        
        if(sectionSelected == section)
        {
            cell.backgroundColor = UIColor.init(red: 18, green: 121, blue: 191)
            cell.countLbl.textColor = UIColor.white
            cell.titleLbl.textColor = UIColor.white
            cell.arrowImage.image = UIImage(named: "up")
        }
        else
        {
            cell.backgroundColor = UIColor.white
            cell.countLbl.textColor = UIColor.init(red: 18, green: 121, blue: 191)
            cell.titleLbl.textColor = UIColor.black
            cell.arrowImage.image = UIImage(named: "down")
        }
        
        return cell as UIView
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let list = platformArray[indexPath.section] as? [PlatformList]
        let cell:OutlierTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OutlierTableViewCell", for: indexPath as IndexPath) as! OutlierTableViewCell
        cell.titleLbl.text = list?[indexPath.row].platformName as? String
        //cell.arrowImage.image = UIImage(named: "down")
        cell.countLbl.text = "\(list?[indexPath.row].count ?? 0)"
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return selectedArray?.count ?? 0
        return self.selectedArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (platformArray[section] as? [PlatformList])?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let list = platformArray[indexPath.section] as? [PlatformList]
        tableView.deselectRow(at: indexPath, animated: false)
        self.presenter?.presentDelinquentDetailModule(domainName: selectedArray?[indexPath.section].domainName ?? "", platformDomain: list?[indexPath.row].platformName ?? "", ngoResponse: NGOResponse!,count:"\(list?[indexPath.row].count ?? 0)")
        
    }
    
}
