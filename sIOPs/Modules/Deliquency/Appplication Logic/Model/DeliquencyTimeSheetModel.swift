// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let timesheetModel = try TimesheetModel(json)

import Foundation

// MARK: - TimesheetModel
@objcMembers class DeliquencyTimeSheetModel: NSObject, Codable {
    let respInfo: RespTimeSheetInfo?
    let respData: [RespTimeSheetDatum]?

    init(respInfo: RespTimeSheetInfo?, respData: [RespTimeSheetDatum]?) {
        self.respInfo = respInfo
        self.respData = respData
    }
}

// MARK: TimesheetModel convenience initializers and mutators

extension DeliquencyTimeSheetModel {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(DeliquencyTimeSheetModel.self, from: data)
        self.init(respInfo: me.respInfo, respData: me.respData)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        respInfo: RespTimeSheetInfo?? = nil,
        respData: [RespTimeSheetDatum]?? = nil
    ) -> DeliquencyTimeSheetModel {
        return DeliquencyTimeSheetModel(
            respInfo: respInfo ?? self.respInfo,
            respData: respData ?? self.respData
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespDatum
@objcMembers class RespTimeSheetDatum: NSObject, Codable {
    let busiCode, transactionID, code, message: String?
    let isEncrypt: Bool?
    let respMsg: RespTimeSheetMsg?

    enum CodingKeys: String, CodingKey {
        case busiCode
        case transactionID
        case code, message, isEncrypt, respMsg
    }

    init(busiCode: String?, transactionID: String?, code: String?, message: String?, isEncrypt: Bool?, respMsg: RespTimeSheetMsg?) {
        self.busiCode = busiCode
        self.transactionID = transactionID
        self.code = code
        self.message = message
        self.isEncrypt = isEncrypt
        self.respMsg = respMsg
    }
}

// MARK: RespDatum convenience initializers and mutators

extension RespTimeSheetDatum {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(RespTimeSheetDatum.self, from: data)
        self.init(busiCode: me.busiCode, transactionID: me.transactionID, code: me.code, message: me.message, isEncrypt: me.isEncrypt, respMsg: me.respMsg)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        busiCode: String?? = nil,
        transactionID: String?? = nil,
        code: String?? = nil,
        message: String?? = nil,
        isEncrypt: Bool?? = nil,
        respMsg: RespTimeSheetMsg?? = nil
    ) -> RespTimeSheetDatum {
        return RespTimeSheetDatum(
            busiCode: busiCode ?? self.busiCode,
            transactionID: transactionID ?? self.transactionID,
            code: code ?? self.code,
            message: message ?? self.message,
            isEncrypt: isEncrypt ?? self.isEncrypt,
            respMsg: respMsg ?? self.respMsg
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespMsg
@objcMembers class RespTimeSheetMsg: NSObject, Codable {
    let responseHeader: ResponseTimeSheetHeader?
    let responsePayload: ResponseTimeSheetPayload?

    init(responseHeader: ResponseTimeSheetHeader?, responsePayload: ResponseTimeSheetPayload?) {
        self.responseHeader = responseHeader
        self.responsePayload = responsePayload
    }
}

// MARK: RespMsg convenience initializers and mutators

extension RespTimeSheetMsg {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(RespTimeSheetMsg.self, from: data)
        self.init(responseHeader: me.responseHeader, responsePayload: me.responsePayload)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        responseHeader: ResponseTimeSheetHeader?? = nil,
        responsePayload: ResponseTimeSheetPayload?? = nil
    ) -> RespTimeSheetMsg {
        return RespTimeSheetMsg(
            responseHeader: responseHeader ?? self.responseHeader,
            responsePayload: responsePayload ?? self.responsePayload
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponseHeader
@objcMembers class ResponseTimeSheetHeader: NSObject, Codable {
    let timestamp, message: String?
    let status: Int?
    let title, path: String?

    init(timestamp: String?, message: String?, status: Int?, title: String?, path: String?) {
        self.timestamp = timestamp
        self.message = message
        self.status = status
        self.title = title
        self.path = path
    }
}

// MARK: ResponseHeader convenience initializers and mutators

extension ResponseTimeSheetHeader {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(ResponseTimeSheetHeader.self, from: data)
        self.init(timestamp: me.timestamp, message: me.message, status: me.status, title: me.title, path: me.path)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        timestamp: String?? = nil,
        message: String?? = nil,
        status: Int?? = nil,
        title: String?? = nil,
        path: String?? = nil
    ) -> ResponseTimeSheetHeader {
        return ResponseTimeSheetHeader(
            timestamp: timestamp ?? self.timestamp,
            message: message ?? self.message,
            status: status ?? self.status,
            title: title ?? self.title,
            path: path ?? self.path
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - ResponsePayload
@objcMembers class ResponseTimeSheetPayload: NSObject, Codable {
    let userName: String?
    let domainList, platformList: JSONNull?
    let delinquentDate: [DelinquentDate]?
    let delinquentDays, delinquentEmployeeCount: Int?
    let employeeList: JSONNull?

    init(userName: String?, domainList: JSONNull?, platformList: JSONNull?, delinquentDate: [DelinquentDate]?, delinquentDays: Int?, delinquentEmployeeCount: Int?, employeeList: JSONNull?) {
        self.userName = userName
        self.domainList = domainList
        self.platformList = platformList
        self.delinquentDate = delinquentDate
        self.delinquentDays = delinquentDays
        self.delinquentEmployeeCount = delinquentEmployeeCount
        self.employeeList = employeeList
    }
}

// MARK: ResponsePayload convenience initializers and mutators

extension ResponseTimeSheetPayload {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(ResponseTimeSheetPayload.self, from: data)
        self.init(userName: me.userName, domainList: me.domainList, platformList: me.platformList, delinquentDate: me.delinquentDate, delinquentDays: me.delinquentDays, delinquentEmployeeCount: me.delinquentEmployeeCount, employeeList: me.employeeList)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        userName: String?? = nil,
        domainList: JSONNull?? = nil,
        platformList: JSONNull?? = nil,
        delinquentDate: [DelinquentDate]?? = nil,
        delinquentDays: Int?? = nil,
        delinquentEmployeeCount: Int?? = nil,
        employeeList: JSONNull?? = nil
    ) -> ResponseTimeSheetPayload {
        return ResponseTimeSheetPayload(
            userName: userName ?? self.userName,
            domainList: domainList ?? self.domainList,
            platformList: platformList ?? self.platformList,
            delinquentDate: delinquentDate ?? self.delinquentDate,
            delinquentDays: delinquentDays ?? self.delinquentDays,
            delinquentEmployeeCount: delinquentEmployeeCount ?? self.delinquentEmployeeCount,
            employeeList: employeeList ?? self.employeeList
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - DelinquentDate
@objcMembers class DelinquentDate: NSObject, Codable {
    let dates, status, hrs: String?
    let swapStatus: JSONNull?

    init(dates: String?, status: String?, hrs: String?, swapStatus: JSONNull?) {
        self.dates = dates
        self.status = status
        self.hrs = hrs
        self.swapStatus = swapStatus
    }
}

// MARK: DelinquentDate convenience initializers and mutators

extension DelinquentDate {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(DelinquentDate.self, from: data)
        self.init(dates: me.dates, status: me.status, hrs: me.hrs, swapStatus: me.swapStatus)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        dates: String?? = nil,
        status: String?? = nil,
        hrs: String?? = nil,
        swapStatus: JSONNull?? = nil
    ) -> DelinquentDate {
        return DelinquentDate(
            dates: dates ?? self.dates,
            status: status ?? self.status,
            hrs: hrs ?? self.hrs,
            swapStatus: swapStatus ?? self.swapStatus
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - RespInfo
@objcMembers class RespTimeSheetInfo: NSObject, Codable {
    let code, message: String?

    init(code: String?, message: String?) {
        self.code = code
        self.message = message
    }
}

// MARK: RespInfo convenience initializers and mutators

extension RespTimeSheetInfo {
    convenience init(data: Data) throws {
        let me = try newTimeSheetJSONDecoder().decode(RespInfo.self, from: data)
        self.init(code: me.code, message: me.message)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        code: String?? = nil,
        message: String?? = nil
    ) -> RespInfo {
        return RespInfo(
            code: code ?? self.code,
            message: message ?? self.message
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newTimeSheetJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newTimeSheetJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Encode/decode helpers
@objcMembers class newJSONNull: NSObject, Codable {

    public static func == (lhs: newJSONNull, rhs: newJSONNull) -> Bool {
        return true
    }

    override public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(newJSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

