//
//	DomainList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DeliquencyDomainList : NSObject, NSCoding{

	var activity : AnyObject!
	var count : Int!
	var deliquentdates : AnyObject!
	var domainId : AnyObject!
	var domainName : String!
	var managerName : AnyObject!
	var platformName : String!
	var subType : AnyObject!
	var task : AnyObject!
	var taskDate : AnyObject!
	var type : AnyObject!
    var sum  : Int!
    

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		activity = dictionary["activity"] as? AnyObject
		count = dictionary["count"] as? Int
		deliquentdates = dictionary["deliquentdates"] as? AnyObject
		domainId = dictionary["domainId"] as? AnyObject
		domainName = dictionary["domainName"] as? String
		managerName = dictionary["managerName"] as? AnyObject
		platformName = dictionary["platformName"] as? String
		subType = dictionary["subType"] as? AnyObject
		task = dictionary["task"] as? AnyObject
		taskDate = dictionary["taskDate"] as? AnyObject
		type = dictionary["type"] as? AnyObject
        sum = dictionary["sum"] as? Int
        
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if activity != nil{
			dictionary["activity"] = activity
		}
		if count != nil{
			dictionary["count"] = count
		}
        if sum != nil{
            dictionary["sum"] = count
        }
		if deliquentdates != nil{
			dictionary["deliquentdates"] = deliquentdates
		}
		if domainId != nil{
			dictionary["domainId"] = domainId
		}
		if domainName != nil{
			dictionary["domainName"] = domainName
		}
		if managerName != nil{
			dictionary["managerName"] = managerName
		}
		if platformName != nil{
			dictionary["platformName"] = platformName
		}
		if subType != nil{
			dictionary["subType"] = subType
		}
		if task != nil{
			dictionary["task"] = task
		}
		if taskDate != nil{
			dictionary["taskDate"] = taskDate
		}
		if type != nil{
			dictionary["type"] = type
		}
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         activity = aDecoder.decodeObject(forKey: "activity") as? AnyObject
         count = aDecoder.decodeObject(forKey: "count") as? Int
         sum = aDecoder.decodeObject(forKey: "sum") as? Int
         deliquentdates = aDecoder.decodeObject(forKey: "deliquentdates") as? AnyObject
         domainId = aDecoder.decodeObject(forKey: "domainId") as? AnyObject
         domainName = aDecoder.decodeObject(forKey: "domainName") as? String
         managerName = aDecoder.decodeObject(forKey: "managerName") as? AnyObject
         platformName = aDecoder.decodeObject(forKey: "platformName") as? String
         subType = aDecoder.decodeObject(forKey: "subType") as? AnyObject
         task = aDecoder.decodeObject(forKey: "task") as? AnyObject
         taskDate = aDecoder.decodeObject(forKey: "taskDate") as? AnyObject
         type = aDecoder.decodeObject(forKey: "type") as? AnyObject
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if activity != nil{
			aCoder.encode(activity, forKey: "activity")
		}
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
        if sum != nil{
            aCoder.encode(count, forKey: "sum")
        }
		if deliquentdates != nil{
			aCoder.encode(deliquentdates, forKey: "deliquentdates")
		}
		if domainId != nil{
			aCoder.encode(domainId, forKey: "domainId")
		}
		if domainName != nil{
			aCoder.encode(domainName, forKey: "domainName")
		}
		if managerName != nil{
			aCoder.encode(managerName, forKey: "managerName")
		}
		if platformName != nil{
			aCoder.encode(platformName, forKey: "platformName")
		}
		if subType != nil{
			aCoder.encode(subType, forKey: "subType")
		}
		if task != nil{
			aCoder.encode(task, forKey: "task")
		}
		if taskDate != nil{
			aCoder.encode(taskDate, forKey: "taskDate")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
	}

}
