//
//	DeliquencyModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DeliquencyModel : NSObject, NSCoding{

	var delinquentDates : AnyObject!
	var domainList : [DeliquencyDomainList]!
	var employeeList : AnyObject!
	var mainList : [DeliquencyDomainList]!
	var platformList : [DeliquencyDomainList]!
	var userName : String!
    var delinquentEmployeeCount : Int!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		delinquentDates = dictionary["delinquentDates"] as? AnyObject
		domainList = [DeliquencyDomainList]()
        delinquentEmployeeCount = dictionary["delinquentEmployeeCount"] as? Int
		if let domainListArray = dictionary["domainList"] as? [[String:Any]]{
			for dic in domainListArray{
				let value = DeliquencyDomainList(fromDictionary: dic)
				domainList.append(value)
			}
		}
		employeeList = dictionary["employeeList"] as? AnyObject
		mainList = [DeliquencyDomainList]()
		if let mainListArray = dictionary["mainList"] as? [[String:Any]]{
			for dic in mainListArray{
				let value = DeliquencyDomainList(fromDictionary: dic)
				mainList.append(value)
			}
		}
        
        
        platformList = [DeliquencyDomainList]()
        if let mainListArray = dictionary["platformList"] as? [[String:Any]]{
            for dic in mainListArray{
                let value = DeliquencyDomainList(fromDictionary: dic)
                mainList.append(value)
            }
        }
		//platformList = dictionary["platformList"] as? [DeliquencyDomainList]
		userName = dictionary["userName"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if delinquentDates != nil{
			dictionary["delinquentDates"] = delinquentDates
		}
		if domainList != nil{
			var dictionaryElements = [[String:Any]]()
			for domainListElement in domainList {
				dictionaryElements.append(domainListElement.toDictionary())
			}
			dictionary["domainList"] = dictionaryElements
		}
		if employeeList != nil{
			dictionary["employeeList"] = employeeList
		}
		if mainList != nil{
			var dictionaryElements = [[String:Any]]()
			for mainListElement in mainList {
				dictionaryElements.append(mainListElement.toDictionary())
			}
			dictionary["mainList"] = dictionaryElements
		}
		if platformList != nil{
            
            var dictionaryElements = [[String:Any]]()
            for platformListElement in platformList {
                dictionaryElements.append(platformListElement.toDictionary())
            }
            dictionary["platformList"] = dictionaryElements
			
		}
		if userName != nil{
			dictionary["userName"] = userName
		}
        if delinquentEmployeeCount != nil{
                dictionary["delinquentEmployeeCount"] = delinquentEmployeeCount
            }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         delinquentDates = aDecoder.decodeObject(forKey: "delinquentDates") as? AnyObject
         domainList = aDecoder.decodeObject(forKey :"domainList") as? [DeliquencyDomainList]
         employeeList = aDecoder.decodeObject(forKey: "employeeList") as? AnyObject
         mainList = aDecoder.decodeObject(forKey :"mainList") as? [DeliquencyDomainList]
         platformList = aDecoder.decodeObject(forKey: "platformList") as? [DeliquencyDomainList]
         userName = aDecoder.decodeObject(forKey: "userName") as? String
         delinquentEmployeeCount = aDecoder.decodeObject(forKey: "delinquentEmployeeCount") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if delinquentDates != nil{
			aCoder.encode(delinquentDates, forKey: "delinquentDates")
		}
		if domainList != nil{
			aCoder.encode(domainList, forKey: "domainList")
		}
		if employeeList != nil{
			aCoder.encode(employeeList, forKey: "employeeList")
		}
		if mainList != nil{
			aCoder.encode(mainList, forKey: "mainList")
		}
		if platformList != nil{
			aCoder.encode(platformList, forKey: "platformList")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}
        if delinquentEmployeeCount != nil{
            aCoder.encode(userName, forKey: "delinquentEmployeeCount")
        }

	}

}
