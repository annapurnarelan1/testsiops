//
//  DeliquencyDetailInteractor.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

class DeliquencyInteractor: DeliquencyInteractorInputProtocol {
  
    

public enum DeliquencyError {
    case internetError(String)
    case serverMessage(String)
}

// MARK: Variables
   weak var presenter: DeliquencyInteractorOutputProtocol?
    
    
    func requestData(type:String){
        
        let applicationArray = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.applications
             //  usefullLinksCollectionView.reloadData()
        let dic = applicationArray?.filter { $0.applicationUCode == "F_NGO_NextGenOPS" }
        
        let pubInfo: [String : Any] =
           ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        var application : LoginApplication?
        
        if(dic?.count ?? 0 > 0)
        {
            application = dic?[0]
        }
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            
            "appRoleCode": application?.applicationCode ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        let busicode =  Constants.BusiCode.NGOTSDomain
        
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                       let Deliquencydata =  DeliquencyModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                           
                          
                       self.presenter?.reloadData(detail:Deliquencydata)
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
                
                
                


                
               
           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    
    func requestPlatFormData(domaninName:String){
        
        let applicationArray = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.applications
             //  usefullLinksCollectionView.reloadData()
        let dic = applicationArray?.filter { $0.applicationUCode == "F_NGO_NextGenOPS" }
        
        let pubInfo: [String : Any] =
           ["timestamp": String(Date().ticks),
            "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            "osType": "ios",
            "lang": "en_US",
            "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
        
        let requestHeader:[String :Any] = [
            "serviceName": "userInfo"
        ]
        
        var application : LoginApplication?
        
        if(dic?.count ?? 0 > 0)
        {
            application = dic?[0]
        }
        let requestBody:[String : Any] = [
            "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
            
            "appRoleCode": application?.applicationCode ?? "",
            "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
            "type": "userInfo",
            "domainName":domaninName
        ]
        
       let busiParams: [String : Any] = [
            "requestHeader": requestHeader,
            "requestBody": requestBody,
        ]
       
        let busicode =  Constants.BusiCode.NGOTSPlatform
        
        
        let requestList: [String: Any] = [
            "busiCode": busicode,
            "isEncrypt": false,
            "transactionId": "0001574162779054",
            "busiParams":busiParams
        ]
        
        let jsonParameter = [
            "pubInfo" : pubInfo,
            "requestList" : [requestList]
            ] as [String : Any]
        
        APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                print(returnJson)
                
                let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                
                switch status {
                case Constants.status.OK:
                        let Deliquencydata =  DeliquencyPlatFormModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
                                          
                                         
                                      self.presenter?.reloadPlatFormData(detail:Deliquencydata)
                  case Constants.status.NOK:
                         let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                        self.presenter?.showFailError(error:message)
                default:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                }
                
               
                
                


                
               
           case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                    
                case .authorizationError(let errorJson):
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .serverError:
                    self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                case .newUpdate:
                    self.presenter?.stopLoader()
                default:
                    self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                    
                }
            }
        })
        
    }
    
    func requestDataWithTimeSheet() {
          let applicationArray = LoginModel.sharedInstance.respData[0].respMsg.responsePayload.applications
                //  usefullLinksCollectionView.reloadData()
           let dic = applicationArray?.filter { $0.applicationUCode == "F_NGO_NextGenOPS" }
           
           let pubInfo: [String : Any] =
              ["timestamp": String(Date().ticks),
               "appId": UserDefaults.standard.string(forKey: "bundleIdentifier") ?? "",
               "sessionId": UserDefaults.standard.object(forKey: "sessionId") ?? "",
               "userId": "1000016614",
               "serviceId": "702150585",
               "circleId": "MU",
               "jioroute": "WE015",
               "customerId": "1000016614",
               "osType": "ios",
               "lang": "en_US",
               "version": UserDefaults.standard.string(forKey: "buildNumber") ?? ""]
           
           let requestHeader:[String :Any] = [
               "serviceName": "userInfo"
           ]
           
           var application : LoginApplication?
           
           if(dic?.count ?? 0 > 0)
           {
               application = dic?[0]
           }
           let requestBody:[String : Any] = [
               
               "userName": LoginModel.sharedInstance.respData[0].respMsg.responsePayload.userName ?? "",
               "type": "userInfo",
             
           ]
           
           let busiParams: [String : Any] = [
               "requestHeader": requestHeader,
               "requestBody": requestBody,
           ]
          
           let busicode =  Constants.BusiCode.TimeClocking
           
           
           let requestList: [String: Any] = [
               "busiCode": busicode,
               "isEncrypt": false,
               "transactionId": "0001574162779054",
               "busiParams":busiParams
           ]
           
           let jsonParameter = [
               "pubInfo" : pubInfo,
               "requestList" : [requestList]
               ] as [String : Any]
           
           APIManager.requestData(url: Constants.APIRoutes.loginApi, method: .post, parameters: jsonParameter, completion: { (result) in
               switch result {
               case .success(let returnJson) :
                   print(returnJson)
                   
                   let status =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responseHeader"] as? [String : Any] ?? [:])["status"] as? Int ?? 0) == 0 ? Constants.status.NOK : Constants.status.OK
                   
                   switch status {
                   case Constants.status.OK:
                    
                    let jsonDict =  returnJson as? [String:Any]
                                    var respMsg : ResponseTimeSheetPayload?
                                    var model : DeliquencyTimeSheetModel?
                                    do {
                                       let json = try JSONSerialization.data(withJSONObject: jsonDict as Any)
                                       model = try DeliquencyTimeSheetModel(data: json)
                                       let arrRespData  = model?.respData
                                       if let arrayResponse = arrRespData  {
                                           for respsumObj in arrayResponse  {
                                               respMsg = ((respsumObj.respMsg?.responsePayload!)!)
                                           }
                                       }
                                                       print(model!)
                                        self.presenter?.reloadDataWithTimeSheet(detail: respMsg!)
                                                   } catch {
                                                       self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                                                       print(error)
                                                       self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                                                   }
                    
//
//                           let Deliquencydata =  DeliquencyPlatFormModel.init(fromDictionary: ((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])
//
//
//                                         self.presenter?.reloadPlatFormData(detail:Deliquencydata)
                     case Constants.status.NOK:
                            let message =  ((((((returnJson as? [String:Any] ?? [:])["respData"] as? [Any])?[0] as? [String:Any] ?? [:])["respMsg"] as? [String : Any] ?? [:])["responsePayload"] as? [String : Any] ?? [:])["messageDetails"] as? String ?? APIManager.NetworkError.unKnownError.rawValue)
                           self.presenter?.showFailError(error:message)
                   default:
                       self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                   }
                   
                  
                   
                   


                   
                  
              case .failure(let failure) :
                   switch failure {
                   case .connectionError:
                       self.presenter?.showFailError(error:APIManager.NetworkError.internetError.rawValue)
                       
                   case .authorizationError(let errorJson):
                       self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                   case .serverError:
                       self.presenter?.showFailError(error: APIManager.NetworkError.unKnownError.rawValue)
                   case .newUpdate:
                       self.presenter?.stopLoader()
                   default:
                       self.presenter?.showFailError(error:APIManager.NetworkError.unKnownError.rawValue)
                       
                   }
               }
           })
      }
    
}
