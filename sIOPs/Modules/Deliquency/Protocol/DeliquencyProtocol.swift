//
//  DeliquencyDetailProtocol.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation

/// Method contract between PRESENTER -> VIEW
protocol DeliquencyViewProtocol: class {
    var presenter: DeliquencyPresenterProtocol? { get set }
    //func DeliquencyDone(DeliquencyRes :DeliquencyModel)
    func show(image:String,error: String,description:String)
    func showError(error: String,description:String)
     
    func stopLoader()
    func showFailError(error:String)
    func reloadData(detail:DeliquencyModel)
    func reloadPlatFormData(detail:DeliquencyPlatFormModel)
    func reloadDataWithTimeSheet(detail:ResponseTimeSheetPayload)
  
}

/// Method contract between PRESENTER -> WIREFRAME
protocol DeliquencyWireFrameProtocol: class {
    
    static func presentDeliquencyModule(fromView:AnyObject,type:String,ngoResponse:NGO,selected:String)
    func presentDelinquentDetailModule(domainName:String,platformDomain:String,ngoResponse:NGO,count:String)

}

/// Method contract between VIEW -> PRESENTER
protocol DeliquencyPresenterProtocol: class {
    var view: DeliquencyViewProtocol? { get set }
    var interactor: DeliquencyInteractorInputProtocol? { get set }
    var wireFrame: DeliquencyWireFrameProtocol? { get set }
    
     func requestData(type:String)
    func requestPlatFormData(domaninName:String)
    func requestDataWithTimeSheet()
    func presentDelinquentDetailModule(domainName:String,platformDomain:String,ngoResponse:NGO,count:String)
  
}

/// Method contract between INTERACTOR -> PRESENTER
protocol DeliquencyInteractorOutputProtocol: class {

    func show(error: String,description:String)
     func stopLoader()
    
    func showFailError(error:String)
    func reloadData(detail:DeliquencyModel)
    func reloadDataWithTimeSheet(detail:ResponseTimeSheetPayload)
    func reloadPlatFormData(detail:DeliquencyPlatFormModel)
}

/// Method contract between PRESENTER -> INTERACTOR
protocol DeliquencyInteractorInputProtocol: class
{
    var presenter: DeliquencyInteractorOutputProtocol? { get set }
    func requestData(type:String)
    func requestDataWithTimeSheet()
    func requestPlatFormData(domaninName:String)
    
}
