//
//  TwoStatusCollectionViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 25/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class TwoStatusCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titlefirstLabel: UILabel!
    @IBOutlet weak var descriptionFirstLabel: UILabel!
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var headertitleLabel: UILabel!
    
    @IBOutlet weak var secondViewXContraint: NSLayoutConstraint!
    @IBOutlet weak var firstViewXContraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionSecondLabel: UILabel!
    @IBOutlet weak var titleSecondLabel: UILabel!
    override func awakeFromNib() {
        self.setUP()
        super.awakeFromNib()
        // Initialization code
    }

}
