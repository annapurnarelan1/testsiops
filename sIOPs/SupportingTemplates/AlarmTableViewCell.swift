//
//  AlarmTableViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var phone: UIButton!
    @IBOutlet weak var openAtLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var applicationLabel: UILabel!
    @IBOutlet weak var textView: UIView!
   
    @IBOutlet weak var ageingHeadingLbl: UILabel!
    @IBOutlet weak var acknoledgeByLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var acknolowledOnLbl: UILabel!
    
    @IBOutlet weak var idLbl: UILabel!
    
    @IBOutlet weak var ranLbl: UILabel!
    @IBOutlet weak var openAttextlbl: UILabel!
    @IBOutlet weak var agingTxtLbl: UILabel!
    @IBOutlet weak var dayTblView: UITableView!
    var dataArray: [SiteDownHistoryList]?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ageingInfoBtn: UIButton!
    @IBOutlet weak var icInfoBtn: UIButton!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        dayTblView.register(UINib(nibName: "AlarmHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "AlarmHeaderTableViewCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AlarmTableViewCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AlarmHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlarmHeaderTableViewCell", for: indexPath as IndexPath) as! AlarmHeaderTableViewCell
        if(indexPath.row > 0)
        {
            
            cell.ageingLbl.text = "\(self.dataArray?[indexPath.row - 1].ageing ?? 0.00)"
            cell.startDateLbl.text = "\(self.dataArray?[indexPath.row - 1].startDate ?? "")"
            cell.startDateLbl.textColor = UIColor.black
            cell.impactedCustomerLabel.text = "\(self.dataArray?[indexPath.row - 1].impactedCustomers ?? "")"
            cell.backgroundColor = UIColor.white
        }
        
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return selectedArray?.count ?? 0
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.dataArray?.count ?? 0) + 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraint.constant = self.dayTblView.contentSize.height
       // viewheightConstraint.constant = self.dayTblView.contentSize.height + 20
        self.dayTblView.layoutIfNeeded()
//        viewheightConstraint.constant = self.dayTblView.contentSize.height + 20
//        self.dayTblView.layoutIfNeeded()
    }
    
}
