//
//  JourneyCollectionViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGOMACDTrendCollectionViewCell: UICollectionViewCell {
    
   
    @IBOutlet weak var journeyTableView: UITableView!
        
    var macdTrendsArray : [NGOMACDTrendDetails]?
        
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()
        journeyTableView.register(UINib(nibName: "TrendsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendsHeaderTableViewCell")
        // Initialization code
    }
}


extension NGOMACDTrendCollectionViewCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TrendsHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TrendsHeaderTableViewCell", for: indexPath as IndexPath) as! TrendsHeaderTableViewCell

        cell.fifthLabel.isHidden = true
        if(indexPath.row > 0)
              {
                var dateFormated = ""
                if let date =  self.macdTrendsArray?[indexPath.row - 1].ddate
                {
                    dateFormated = HelperMethods().formatDateString(date: date)
                }
                  
                  cell.firstLabel.text = dateFormated
                cell.secondLabel.text = HelperMethods().nuumberFormatting(value:"\(self.macdTrendsArray?[indexPath.row - 1].simChangeTotal ?? 0)")
                cell.thirdLabel.text = HelperMethods().nuumberFormatting(value: "\(self.macdTrendsArray?[indexPath.row - 1].irWatchTotal ?? 0)")
                cell.fourthLabel.text = HelperMethods().nuumberFormatting(value: "\(self.macdTrendsArray?[indexPath.row - 1].appleIWatchCount ?? 0)")    
                  cell.backgroundColor = UIColor.white
              }
              else
              {
                 
                cell.backgroundColor = UIColor.lightGreyColor
                let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 14.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
                let boldStringfirstLable = NSMutableAttributedString(string: "Date", attributes:attrs3 as [NSAttributedString.Key : Any])
                let boldStringSecondLable = NSMutableAttributedString(string: "Sim Changed", attributes:attrs3 as [NSAttributedString.Key : Any])
                let boldStringThirdLable = NSMutableAttributedString(string: "International Roaming", attributes:attrs3 as [NSAttributedString.Key : Any])
                let boldStringFourthLable = NSMutableAttributedString(string: "Apple Watch", attributes:attrs3 as [NSAttributedString.Key : Any])

                  cell.firstLabel.attributedText = boldStringfirstLable
                  cell.secondLabel.attributedText = boldStringSecondLable
                  cell.thirdLabel.attributedText = boldStringThirdLable
                  cell.fourthLabel.attributedText = boldStringFourthLable
              }
              
              return cell

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.macdTrendsArray?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 30
       }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
        {
            heightConstraint.constant = self.journeyTableView.contentSize.height
            self.journeyTableView.layoutIfNeeded()
    //        viewheightConstraint.constant = self.dayTblView.contentSize.height + 20
    //        self.dayTblView.layoutIfNeeded()
        }
    
    
}
