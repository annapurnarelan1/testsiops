//
//  JourneyCollectionViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGORechargeJourneyTrendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var phone: UIButton!
    @IBOutlet weak var openAtLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var applicationLabel: UILabel!
    @IBOutlet weak var textView: UIView!
    
    @IBOutlet weak var ageingHeadingLbl: UILabel!
    @IBOutlet weak var acknoledgeByLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var acknolowledOnLbl: UILabel!
    
    @IBOutlet weak var idLbl: UILabel!
    
    @IBOutlet weak var ranLbl: UILabel!
    @IBOutlet weak var openAttextlbl: UILabel!
    @IBOutlet weak var agingTxtLbl: UILabel!
    @IBOutlet weak var journeyTableView: UITableView!
    
    var rechargeTableView: UITableView!
    var activationTableView: UITableView!
    
    var recharegeActivationTrendsArray : [NGORechargeTrend]?
    
    
    //    var orderActivationArray: [NGO]?
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewheightConstraint: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()

        // Initialization code
    }
    override func layoutSubviews() {
          super.layoutSubviews()

          journeyTableView.delegate = self
          journeyTableView.dataSource = self
          journeyTableView.register(UINib(nibName: "TrendsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendsHeaderTableViewCell")
        
          journeyTableView.estimatedRowHeight = 30
      }
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        // note: don't change the width
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        journeyTableView.estimatedRowHeight = 30
        return layoutAttributes
    }
}


extension NGORechargeJourneyTrendCollectionViewCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TrendsHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TrendsHeaderTableViewCell", for: indexPath as IndexPath) as! TrendsHeaderTableViewCell
        
        if(indexPath.row != 0)
        {
            var dateFormatted = ""
            if let date =  self.recharegeActivationTrendsArray?[indexPath.row - 1].rdate
            {
                dateFormatted = HelperMethods().formatDate(date: date)
            }
            cell.firstLabel.text = dateFormatted
            cell.secondLabel.text = HelperMethods().nuumberFormatting(value: "\(self.recharegeActivationTrendsArray?[indexPath.row - 1].initiated ?? 0)")
            cell.thirdLabel.text =  HelperMethods().nuumberFormatting(value:"\(self.recharegeActivationTrendsArray?[indexPath.row - 1].paymentfailed ?? 0)")
            cell.fourthLabel.text =  HelperMethods().nuumberFormatting(value: "\(self.recharegeActivationTrendsArray?[indexPath.row - 1].paymentsuccess ?? 0)" )
            cell.fifthLabel.text =  HelperMethods().nuumberFormatting(value: "\(self.recharegeActivationTrendsArray?[indexPath.row - 1].succes ?? 0)")  
            cell.backgroundColor = UIColor.white
        }
        else
        {
            cell.backgroundColor = UIColor.init(hexString: "F4F4F4")
            let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 14.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
            let boldStringfirstLable = NSMutableAttributedString(string: "Date", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringSecondLable = NSMutableAttributedString(string: "Initiated", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringThirdLable = NSMutableAttributedString(string: "Payment Aborted", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFourthLable = NSMutableAttributedString(string: "Payment Success", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFifthLable = NSMutableAttributedString(string: "Recharge Success", attributes:attrs3 as [NSAttributedString.Key : Any])
            cell.firstLabel.attributedText = boldStringfirstLable
            cell.secondLabel.attributedText = boldStringSecondLable
            cell.thirdLabel.attributedText = boldStringThirdLable
            cell.fourthLabel.attributedText = boldStringFourthLable
            cell.fifthLabel.attributedText = boldStringFifthLable
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.recharegeActivationTrendsArray?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
//        
//        
        tableView.estimatedRowHeight = 30
          tableView.rowHeight = UITableView.automaticDimension
         heightConstraint.constant = self.journeyTableView.contentSize.height 

        
        
        setNeedsLayout()
        layoutIfNeeded()
   
        }
}
