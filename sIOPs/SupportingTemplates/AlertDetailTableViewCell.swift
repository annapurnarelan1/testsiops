//
//  AlertDetailTableViewCell.swift
//  sIOPs
//
//  Created by mac on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class AlertDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var phone: UIButton!
    @IBOutlet weak var openAtLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var applicationLabel: UILabel!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var dateLabel: NSLayoutConstraint!
    @IBOutlet weak var ageingHeadingLbl: UILabel!
    @IBOutlet weak var acknoledgeByLbl: UILabel!
    @IBOutlet weak var acknoledgeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var acknolowledOnLbl: UILabel!
    
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
