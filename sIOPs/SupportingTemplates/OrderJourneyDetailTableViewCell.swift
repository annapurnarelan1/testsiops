//
//  OrderJourneyDetailTableViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 02/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import UIKit

class OrderJourneyDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var firstOutlIerCount: UILabel!
    @IBOutlet weak var firstOutlIerName: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondOutlIerCount: UILabel!
    @IBOutlet weak var secondOutlIerName: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdOutlIerCount: UILabel!
    @IBOutlet weak var thirdOutlIerName: UILabel!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var rejectedCount: UILabel!
    @IBOutlet weak var rejectedName: UILabel!
    @IBOutlet weak var rejectedBtn: UIButton!
    @IBOutlet weak var inProgressCount: UILabel!
    @IBOutlet weak var inProgressName: UILabel!
    @IBOutlet weak var inProgressBtn: UIButton!
    @IBOutlet weak var tvPendingCount: UILabel!
    @IBOutlet weak var tvPendingName: UILabel!
    @IBOutlet weak var tvPendingBtn: UIButton!
    @IBOutlet weak var detailtitleLabel: UILabel!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailFirstView: UIView!
    @IBOutlet weak var detailSecondView: UIView!
    @IBOutlet weak var detailThirdView: UIView!
    @IBOutlet weak var detailForthView: UIView!
    @IBOutlet weak var detailFifthView: UIView!
    @IBOutlet weak var detailFirstOutLierCount: UILabel!
    @IBOutlet weak var detailFirstOutlierName: UILabel!
    @IBOutlet weak var detailFirstBtnCount: UIButton!
    @IBOutlet weak var detailSecondOutLierCount: UILabel!
    @IBOutlet weak var detailSecondOutlierName: UILabel!
    @IBOutlet weak var detailSecondBtnCount: UIButton!
    @IBOutlet weak var detailThirdOutLierCount: UILabel!
    @IBOutlet weak var detailThirdOutlierName: UILabel!
    @IBOutlet weak var detailThirdBtnCount: UIButton!
    @IBOutlet weak var detailForthOutLierCount: UILabel!
    @IBOutlet weak var detailForthOutlierName: UILabel!
    @IBOutlet weak var detailForthBtnCount: UIButton!
    @IBOutlet weak var detailFifthOutLierCount: UILabel!
    @IBOutlet weak var detailFifthOutlierName: UILabel!
    @IBOutlet weak var detailFifthBtnCount: UIButton!
    @IBOutlet weak var viewdetailBtn: UIButton!
    @IBOutlet weak var circleBtn: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var initialStackView: UIStackView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var secondLastView: UIView!
    @IBOutlet weak var secondFirstView: UIView!
    @IBOutlet weak var secondSecondView: UIView!
    @IBOutlet weak var secondThirdView: UIView!
    @IBOutlet weak var fourthOutlierCount: UILabel!
    @IBOutlet weak var fourthOutlierButton: UIButton!
    @IBOutlet weak var fourthOutierName: UILabel!
    @IBOutlet weak var detailViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cnstViewDetailsHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwBackground.layer.borderWidth = 0.5
        self.vwBackground.layer.borderColor = UIColor.white.cgColor
        self.vwBackground.layer.masksToBounds = true
        self.vwBackground.layer.cornerRadius = 5
               
               
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
