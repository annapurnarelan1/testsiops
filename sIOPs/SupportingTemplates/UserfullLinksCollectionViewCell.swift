//
//  UserfullLinksCollectionViewCell.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class UserfullLinksCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgroundVw: UIView!
    @IBOutlet weak var iconImageVw: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
