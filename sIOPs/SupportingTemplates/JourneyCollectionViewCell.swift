//
//  JourneyCollectionViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class JourneyCollectionViewCell: UICollectionReusableView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var journeyTableView: UITableView!
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var btnJourneySatus: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
        
    var orderActivationArray : [NGOOrderStage]?
   
    @IBOutlet weak var viewheightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnJourney: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.journeyTableView.rowHeight = UITableView.automaticDimension
        self.journeyTableView.estimatedRowHeight = 45
        journeyTableView.register(UINib(nibName: "BSDTableViewCell", bundle: nil), forCellReuseIdentifier: "BSDTableViewCell")
        // Initialization code
    }
}


extension JourneyCollectionViewCell : UITableViewDelegate,UITableViewDataSource
{
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:BSDTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BSDTableViewCell", for: indexPath as IndexPath) as! BSDTableViewCell
            
            if(indexPath.row > 0)
            {
                let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 10.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#7B7B7B")]
                 let attrs4 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 8.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#7B7B7B")]
                let attrs2 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 10.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#0078C1")]
                
               cell.percentageWidthCOntraint.constant = 40
              // cell.thirdLabelWidtHConstraint.constant = 130
    
                let attachment = NSTextAttachment()
                
                if (self.orderActivationArray?[indexPath.row - 1].color == 0) {
                    if self.orderActivationArray?[indexPath.row - 1].status == "UP" {
                        attachment.image = UIImage(named: "green_arrrow_up")
                        
                    }
                    else {
                        attachment.image = UIImage(named: "green_arrow_down")
                        
                    }
                    
                } else if(self.orderActivationArray?[indexPath.row - 1].color == 1) {
                    if self.orderActivationArray?[indexPath.row - 1].status == "UP" {
                        attachment.image = UIImage(named: "red_arrow_up")
                    }
                    else {
                        attachment.image = UIImage(named: "red_arrow_down")
                        
                    }
                }
                    
                else{
                    attachment.image = UIImage(named: "")
                    
                }
                
                
                let attachmentString = NSAttributedString(attachment: attachment)
                let attributedString1 = NSMutableAttributedString(string:"\("\(self.orderActivationArray?[indexPath.row - 1].perc ?? 0)")", attributes: attrs1 )
                
                let yesterdayData =  HelperMethods().nuumberFormatting(value: "\(self.orderActivationArray?[indexPath.row - 1].yesCount ?? 0)")
                
                let attributedString = NSMutableAttributedString(string:"")
                attributedString.append(attachmentString)
                attributedString.append(attributedString1)
                
                let percentageAttributedString = NSMutableAttributedString(string:"%")
                attributedString.append(percentageAttributedString)
                
                let attributedString2 = NSMutableAttributedString(string:"\(yesterdayData)")
                attributedString2.append(NSAttributedString(string: "\n"))
                let attributedString3 = NSMutableAttributedString(string:"(\(HelperMethods().nuumberFormatting(value: "\(self.orderActivationArray?[indexPath.row - 1].yesTotal ?? 0)")))", attributes: attrs2 )
                                
                attributedString2.append(attributedString3)
                
                //cell.firstLblFirstiewLbl.textAlignment = .left
                cell.firstLblFirstiewLbl.text = "\( self.orderActivationArray?[indexPath.row - 1].stage ?? "")"
                //cell.secondLblFirstViewLbl.textAlignment = .left
                cell.secondLblFirstViewLbl.text = HelperMethods().nuumberFormatting(value: "\(self.orderActivationArray?[indexPath.row - 1].todayCount ?? 0)")
                cell.thirdLblFirstViewLbl.attributedText = attributedString2
                cell.backgroundColor = UIColor.white
                if (Double(self.orderActivationArray?[indexPath.row - 1].perc ?? Int(0.0)) == 0.0) {
                    cell.percentageLabel.isHidden  = true
                }
                else {
                    cell.percentageLabel.isHidden  = false
                    cell.percentageLabel.attributedText = attributedString
                    
                }
                
            }
                
                
            else
            {
               cell.backgroundColor = UIColor.init(hexString:"F4F4F4")
                      
                let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 14.0) , NSAttributedString.Key.foregroundColor : UIColor.black]

                let attributedString2 = NSMutableAttributedString(string: "Yesterday", attributes:attrs3 as [NSAttributedString.Key : Any])
                cell.percentageWidthCOntraint.constant = 0
                //cell.thirdLabelWidtHConstraint.constant = 130

                let boldStringToday = NSMutableAttributedString(string: "Today", attributes:attrs3 as [NSAttributedString.Key : Any])
                
                let boldStringStage = NSMutableAttributedString(string: "Stage", attributes:attrs3 as [NSAttributedString.Key : Any])
                
                           cell.firstLblFirstiewLbl.attributedText =   boldStringStage
                           cell.secondLblFirstViewLbl.attributedText = boldStringToday
                           cell.thirdLblFirstViewLbl.attributedText =  attributedString2

                
            }
            
            
            
            return cell
        }
        
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.orderActivationArray?.count ?? 0) + 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraint.constant = self.journeyTableView.contentSize.height
        self.journeyTableView.layoutIfNeeded()
    }
    
    
}
