//
//  JourneyCollectionViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGOOrderActivationTrendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var phone: UIButton!
    @IBOutlet weak var openAtLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var applicationLabel: UILabel!
    @IBOutlet weak var textView: UIView!
    
    @IBOutlet weak var ageingHeadingLbl: UILabel!
    @IBOutlet weak var acknoledgeByLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var acknolowledOnLbl: UILabel!
    
    @IBOutlet weak var idLbl: UILabel!
    
    @IBOutlet weak var ranLbl: UILabel!
    @IBOutlet weak var openAttextlbl: UILabel!
    @IBOutlet weak var agingTxtLbl: UILabel!
    @IBOutlet weak var journeyTableView: UITableView!
    
    var rechargeTableView: UITableView!
    var activationTableView: UITableView!
    
    
    
    var orderActivationTrendsArray : [NGOOrderTrend]?
    var portInPortOutTrendArray: [NGOPortInPortOutTrend]?
    
    
    //    var orderActivationArray: [NGO]?
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewheightConstraint: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()
        
        journeyTableView.register(UINib(nibName: "TrendsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendsHeaderTableViewCell")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        journeyTableView.delegate = self
        journeyTableView.dataSource = self
        
        //   journeyTableView.register(UINib(nibName: "TrendsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendsHeaderTableViewCell")
        journeyTableView.estimatedRowHeight = 30
    }
    
}


extension NGOOrderActivationTrendCollectionViewCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TrendsHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TrendsHeaderTableViewCell", for: indexPath as IndexPath) as! TrendsHeaderTableViewCell
        
        
        if(indexPath.row != 0)
        {
            let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 13.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
            
            
            
            var dateFormatted = ""
            if let date =  self.orderActivationTrendsArray?[indexPath.row - 1].ddate
            {
                dateFormatted = HelperMethods().formatDate(date: date)
            }
            cell.backgroundColor = UIColor.white
            let boldStringfirstLable = NSMutableAttributedString(string: dateFormatted, attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringSecondLable = NSMutableAttributedString(string:  "\(HelperMethods().nuumberFormatting(value:String(self.orderActivationTrendsArray?[indexPath.row - 1].entered ?? 0)))", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringThirdLable = NSMutableAttributedString(string: "\(HelperMethods().nuumberFormatting(value: String(self.orderActivationTrendsArray?[indexPath.row - 1].activated ?? 0)))", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFourthLable = NSMutableAttributedString(string: "\(HelperMethods().nuumberFormatting(value: String(self.orderActivationTrendsArray?[indexPath.row - 1].rejected ?? 0)))", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFifthLable = NSMutableAttributedString(string: "\(HelperMethods().nuumberFormatting(value: String(self.orderActivationTrendsArray?[indexPath.row - 1].tvpending ?? 0)))", attributes:attrs3 as [NSAttributedString.Key : Any])
            cell.firstLabel.attributedText = boldStringfirstLable
            cell.secondLabel.attributedText = boldStringSecondLable
            cell.thirdLabel.attributedText = boldStringThirdLable
            cell.fourthLabel.attributedText = boldStringFourthLable
            cell.fifthLabel.attributedText = boldStringFifthLable
            
        }
        else
        {
            
            cell.backgroundColor = UIColor.init(hexString: "F4F4F4")
            let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 14.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
            let boldStringfirstLable = NSMutableAttributedString(string: "Date", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringSecondLable = NSMutableAttributedString(string: "Entered", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringThirdLable = NSMutableAttributedString(string: "Activated", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFourthLable = NSMutableAttributedString(string: "Rejected", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFifthLable = NSMutableAttributedString(string: "AO Pending", attributes:attrs3 as [NSAttributedString.Key : Any])
            
            cell.firstLabel.attributedText = boldStringfirstLable
            cell.secondLabel.attributedText = boldStringSecondLable
            cell.thirdLabel.attributedText = boldStringThirdLable
            cell.fourthLabel.attributedText = boldStringFourthLable
            cell.fifthLabel.attributedText = boldStringFifthLable
            
            
        }
        
        
        
        return cell
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.orderActivationTrendsArray?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
//        tableView.estimatedRowHeight = 30
//        tableView.rowHeight = UITableView.automaticDimension
        heightConstraint.constant = self.journeyTableView.contentSize.height
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    
}
