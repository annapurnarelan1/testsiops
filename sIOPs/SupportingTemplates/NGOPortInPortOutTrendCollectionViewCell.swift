//
//  JourneyCollectionViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGOPortInPortOutTrendCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var journeyTableView: UITableView!
    var portInPortOutTrendArray: [NGOPortInPortOutTrend]?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()
        
        journeyTableView.register(UINib(nibName: "TrendsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendsHeaderTableViewCell")
        // Initialization code
    }
    
}


extension NGOPortInPortOutTrendCollectionViewCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TrendsHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TrendsHeaderTableViewCell", for: indexPath as IndexPath) as! TrendsHeaderTableViewCell
        
        
        if(indexPath.row > 0)
        {
            var dateFormated = ""
            if let date =  self.portInPortOutTrendArray?[indexPath.row - 1].ddate
            {
                dateFormated = HelperMethods().formatDateString(date: date)
            }
            cell.firstLabel.text = dateFormated
            cell.secondLabel.text =  HelperMethods().nuumberFormatting(value:"\(self.portInPortOutTrendArray?[indexPath.row - 1].pintotal ?? 0)" )
            cell.thirdLabel.text = HelperMethods().nuumberFormatting(value:"\(self.portInPortOutTrendArray?[indexPath.row - 1].pinact ?? 0)")
            cell.fourthLabel.text = HelperMethods().nuumberFormatting(value:"\(self.portInPortOutTrendArray?[indexPath.row - 1].pouttotal ?? 0)")
            
            cell.fifthLabel.text = HelperMethods().nuumberFormatting(value:"\(self.portInPortOutTrendArray?[indexPath.row - 1].poutact ?? 0)" )
            cell.backgroundColor = UIColor.white
        }
        else
        {
            
            cell.backgroundColor = UIColor.init(hexString: "F4F4F4")
            let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 12.0) , NSAttributedString.Key.foregroundColor : UIColor.black]
            let boldStringfirstLable = NSMutableAttributedString(string: "Date", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringSecondLable = NSMutableAttributedString(string: "Port In", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringThirdLable = NSMutableAttributedString(string: "Completed", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFourthLable = NSMutableAttributedString(string: "Port out", attributes:attrs3 as [NSAttributedString.Key : Any])
            let boldStringFifthLable = NSMutableAttributedString(string: "Completed", attributes:attrs3 as [NSAttributedString.Key : Any])
            cell.firstLabel.attributedText = boldStringfirstLable
            cell.secondLabel.attributedText = boldStringSecondLable
            cell.thirdLabel.attributedText = boldStringThirdLable
            cell.fourthLabel.attributedText = boldStringFourthLable
            cell.fifthLabel.attributedText = boldStringFifthLable
        }
        return cell
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.portInPortOutTrendArray?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraint.constant = self.journeyTableView.contentSize.height
        self.journeyTableView.layoutIfNeeded()
        
    }
    
    
}

