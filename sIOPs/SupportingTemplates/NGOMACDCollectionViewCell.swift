//
//  NGOMACDCollectionViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 19/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGOMACDCollectionViewCell: UICollectionReusableView {
    
    @IBOutlet weak var lblStatusTitle: UILabel!
    
    @IBOutlet weak var btnSatus: UIButton!
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var thirdBtn: UIButton!
         @IBOutlet weak var thirdLbl: UILabel!
         @IBOutlet weak var descriptionSecondLbl: UILabel!
         @IBOutlet weak var secondBtn: UIButton!
         @IBOutlet weak var secondlbl: UILabel!
         @IBOutlet weak var firstBtn: UIButton!
         @IBOutlet weak var descriptionFirstLbl: UILabel!
         @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var descriptionThirdLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwBackground.layer.borderWidth = 1.0
        self.vwBackground.layer.borderColor = UIColor.clear.cgColor
        self.vwBackground.layer.masksToBounds = true

        self.vwBackground.layer.backgroundColor = UIColor.white.cgColor
        self.vwBackground.layer.shadowColor = UIColor.gray.cgColor
        self.vwBackground.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
        self.vwBackground.layer.shadowRadius = 2.0
        self.vwBackground.layer.shadowOpacity = 1.0
        self.vwBackground.layer.masksToBounds = false
        //self.setUP()
        // Initialization code
    }

}
