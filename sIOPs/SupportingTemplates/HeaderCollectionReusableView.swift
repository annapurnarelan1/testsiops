//
//  CollectionReusableView.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var headerTitleLbl: UILabel!
    @IBOutlet weak var alarmLbl: UILabel!
    @IBOutlet weak var alarmBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
