//
//  ChangeDetailTableViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 09/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import UIKit

class ChangeDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var headerView: UIView!
     @IBOutlet weak var headerViewDescriptionLabel: UILabel!
     @IBOutlet weak var headerViewTitleLabel: UILabel!
     
     @IBOutlet weak var firstViewFirstTitleLabel: UILabel!
     @IBOutlet weak var firstViewFirstDescriptionLabel: UILabel!
     @IBOutlet weak var firstViewSecondTitleLabel: UILabel!
     @IBOutlet weak var firstViewSecondDescriptionLabel: UILabel!
     @IBOutlet weak var firstViewThirdTitleLabel: UILabel!
     @IBOutlet weak var firstViewThirdDescriptionLabel: UILabel!
    
     @IBOutlet weak var impactTitleLabel: UILabel!
     @IBOutlet weak var impactDescriptionLabel: UILabel!
     
     @IBOutlet weak var resolverTitleLabel: UILabel!
     @IBOutlet weak var resolverDescriptionLabel: UILabel!
     
     @IBOutlet weak var SecondViewFirstTitleLabel: UILabel!
     @IBOutlet weak var SecondViewFirstDescriptionLabel: UILabel!
     
     @IBOutlet weak var SecondViewSecondTitleLabel: UILabel!
     @IBOutlet weak var SecondViewSecondDescriptionLabel: UILabel!
     
     @IBOutlet weak var SecondViewThirdTitleLabel: UILabel!
     @IBOutlet weak var SecondViewThirdDescriptionLabel: UILabel!
     
     @IBOutlet weak var SecondViewFourthTitleLabel: UILabel!
     @IBOutlet weak var SecondViewFourthDescriptionLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var textView: UIView!

    @IBOutlet weak var ageingView: UIView!
    @IBOutlet weak var closedDate: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
