//
//  RegionTableViewCell.swift
//  sIOPs
//
//  Created by mac on 12/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class RegionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var separatorLine: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
