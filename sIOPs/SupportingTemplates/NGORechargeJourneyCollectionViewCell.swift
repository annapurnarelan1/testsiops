//
//  JourneyCollectionViewCell.swift
//  sIOPs
//
//  Created by mac on 04/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class NGORechargeJourneyCollectionViewCell: UICollectionReusableView {
    
    @IBOutlet weak var phone: UIButton!
    @IBOutlet weak var btnRechargeJourney: UIButton!
    
     @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var openAtLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var alertBtn: UIButton!
    @IBOutlet weak var applicationLabel: UILabel!
    @IBOutlet weak var textView: UIView!
    
    @IBOutlet weak var ageingHeadingLbl: UILabel!
    @IBOutlet weak var acknoledgeByLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var acknolowledOnLbl: UILabel!
    
    @IBOutlet weak var idLbl: UILabel!
    
    @IBOutlet weak var ranLbl: UILabel!
    @IBOutlet weak var openAttextlbl: UILabel!
    @IBOutlet weak var agingTxtLbl: UILabel!
    @IBOutlet weak var rechargeJourneyTableView: UITableView!
    
    var rechargeTableView: UITableView!
    var activationTableView: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
     
     @IBOutlet weak var btnJourneySatus: UIButton!
    
    var rechargeActivationArray: [NGORechargeStage]?
    
    
    //    var orderActivationArray: [NGO]?
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewheightConstraint: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rechargeJourneyTableView.register(UINib(nibName: "BSDTableViewCell", bundle: nil), forCellReuseIdentifier: "BSDTableViewCell")
        // Initialization code
    }

}


extension NGORechargeJourneyCollectionViewCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell:BSDTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BSDTableViewCell", for: indexPath as IndexPath) as! BSDTableViewCell
    
    let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 10.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#7B7B7B")]
    let attrs2 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 10.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#0078C1")]

    
    if(indexPath.row > 0)
    {
         cell.percentageWidthCOntraint.constant = 40
         //cell.thirdLabelWidtHConstraint.constant = 80
        
        
        let attachment = NSTextAttachment()
         if (self.rechargeActivationArray?[indexPath.row - 1].color == 0) {
                      if (self.rechargeActivationArray?[indexPath.row - 1].status == "UP" ){
                          attachment.image = UIImage(named: "green_arrrow_up")
                          
                      }
                      else if(self.rechargeActivationArray?[indexPath.row - 1].status == "DOWN" ){
                          attachment.image = UIImage(named: "green_arrow_down")
                          
                      }

                                  
                  } else if(self.rechargeActivationArray?[indexPath.row - 1].color == 1) {
                      if (self.rechargeActivationArray?[indexPath.row - 1].status == "UP") {
                          attachment.image = UIImage(named: "red_arrow_up")
                          
                      }
                      else if (self.rechargeActivationArray?[indexPath.row - 1].status == "DOWN"){
                          attachment.image = UIImage(named: "red_arrow_down")
                          
                      }
                      else {
                        attachment.image = nil

            }
                  }
                      
                  else{
                      attachment.image = nil
                      
                  }
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Light", size: 11.0) , NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#7B7B7B")]
        
        let attachmentString = NSAttributedString(attachment: attachment)
        let attributedString1 = NSMutableAttributedString(string:"\(self.rechargeActivationArray?[indexPath.row - 1].perc ?? 0)", attributes: attrs1 )
        
        let yesterdayData =  HelperMethods().nuumberFormatting(value:"\(self.rechargeActivationArray?[indexPath.row - 1].yesCount ?? 0)" )
        
        let attributedString = NSMutableAttributedString(string:"")
        attributedString.append(attachmentString)
        attributedString.append(attributedString1)
        let percentageAttributedString = NSMutableAttributedString(string:"%")

        attributedString.append(percentageAttributedString)
        var attributedString2 = NSMutableAttributedString(string:"\(yesterdayData)")
        attributedString2.append(NSAttributedString(string: "\n"))
        var attributedString3 = NSMutableAttributedString(string:"(\(HelperMethods().nuumberFormatting(value: "\(self.rechargeActivationArray?[indexPath.row - 1].yesTotal ?? 0)")))", attributes: attrs2 )

        if(self.rechargeActivationArray?[indexPath.row - 1].stage == "Pending")
        {
            attributedString2 = NSMutableAttributedString(string:"")
            attributedString2.append(NSAttributedString(string: "\n"))
            attributedString3 =  NSMutableAttributedString(string:"NA", attributes: attrs2 )
        }
        
        
        
        
        
        attributedString2.append(attributedString3)
        cell.firstLblFirstiewLbl.text = "\( self.rechargeActivationArray?[indexPath.row - 1].stage ?? "")"
        cell.secondLblFirstViewLbl.text =  HelperMethods().nuumberFormatting(value: "\(self.rechargeActivationArray?[indexPath.row - 1].todayCount ?? 0)") 
        cell.thirdLblFirstViewLbl.attributedText = attributedString2
        cell.backgroundColor = UIColor.white
        
        if self.rechargeActivationArray?[indexPath.row - 1].perc == 0 {
            cell.percentageLabel.isHidden  = true
        }
        else {
            if(self.rechargeActivationArray?[indexPath.row - 1].stage == "Pending")
                  {
                      cell.percentageLabel.isHidden  = true
                      
                  }
            else
            {
                cell.percentageLabel.isHidden  = false
                cell.percentageLabel.attributedText = attributedString
            }
            

        }
    

    }
    else
    {
        cell.backgroundColor = UIColor.init(hexString:"F4F4F4")
              
        let attrs3 = [NSAttributedString.Key.font : UIFont.init(name:"JioType-Medium", size: 14.0) , NSAttributedString.Key.foregroundColor : UIColor.black]

        let attributedString2 = NSMutableAttributedString(string: "Yesterday", attributes:attrs3 as [NSAttributedString.Key : Any])
        //cell.percentageWidthCOntraint.constant = 0
        //cell.thirdLabelWidtHConstraint.constant = 130

        let boldStringToday = NSMutableAttributedString(string: "Today", attributes:attrs3 as [NSAttributedString.Key : Any])
        
        let boldStringStage = NSMutableAttributedString(string: "Stage", attributes:attrs3 as [NSAttributedString.Key : Any])
        
                   cell.firstLblFirstiewLbl.attributedText =   boldStringStage
                   cell.secondLblFirstViewLbl.attributedText = boldStringToday
                   cell.thirdLblFirstViewLbl.attributedText =  attributedString2

                   //    cell.yesterdayDifferenceLabel .isHidden = true


    }
    
    return cell
        
   }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.rechargeActivationArray?.count ?? 0) + 1

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
    
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
        {
            heightConstraint.constant = self.rechargeJourneyTableView.contentSize.height
            self.rechargeJourneyTableView.layoutIfNeeded()
   
        }
    
    
}
