//
//  SingleButtonCollectionViewCell.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class SingleButtonCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var separatorLine: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()
        // Initialization code
    }

}
