//
//  FilterCategoryTableViewCell.swift
//  sIOPs
//
//  Created by mac on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class BSDTableViewCell: UITableViewCell {

    @IBOutlet weak var yesterdayDifferenceLabel: UILabel!
    @IBOutlet weak var firstLblFirstiewLbl: UILabel!
    @IBOutlet weak var secondLblFirstViewLbl: UILabel!
    @IBOutlet weak var thirdLblFirstViewLbl: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!

    @IBOutlet weak var percentageWidthCOntraint: NSLayoutConstraint!
    
    @IBOutlet weak var thirdLabelWidtHConstraint: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
