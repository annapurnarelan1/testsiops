//
//  IncidentSummaryTableViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 08/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import UIKit

class IncidentSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var headerTitleLabel: UILabel!
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var firstViewButton: UIButton!
    @IBOutlet weak var firstViewDescriptionLabel: UILabel!
    @IBOutlet weak var firstViewTitleLabel: UILabel!
    
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var SecondViewButton: UIButton!
    @IBOutlet weak var SecondViewDescriptionLabel: UILabel!
    @IBOutlet weak var SecondViewTitleLabel: UILabel!
    
    @IBOutlet weak var ThirdView: UIView!
    @IBOutlet weak var ThirdViewButton: UIButton!
    @IBOutlet weak var ThirdViewDescriptionLabel: UILabel!
    @IBOutlet weak var ThirdViewTitleLabel: UILabel!
    
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var FourthView: UIView!
      @IBOutlet weak var FourthViewButton: UIButton!
      @IBOutlet weak var FourthViewDescriptionLabel: UILabel!
      @IBOutlet weak var FourthViewTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwBackground.layer.borderWidth = 0.5
        self.vwBackground.layer.borderColor = UIColor.white.cgColor
        self.vwBackground.layer.masksToBounds = true
        self.vwBackground.layer.cornerRadius = 5
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        //self.layer.cornerRadius = 5
        self.layer.masksToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
