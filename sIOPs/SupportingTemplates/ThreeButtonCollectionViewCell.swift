//
//  ThreeButtonCollectionViewCell.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

import MaterialComponents.MaterialCards

class ThreeButtonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var descriptionThirdLbl: UILabel!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var descriptionSecondLbl: UILabel!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var secondlbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var separatorLine: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUP()
        // Initialization code
    }

}
