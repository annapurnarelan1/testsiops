//
//  SeverityTableViewCell.swift
//  sIOPs
//
//  Created by ASM ESPL on 07/01/20.
//  Copyright © 2020 reliance. All rights reserved.
//

import UIKit

class SeverityTableViewCell: UITableViewCell {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewDescriptionLabel: UILabel!
    @IBOutlet weak var headerViewTitleLabel: UILabel!
    
    @IBOutlet weak var firstViewFirstTitleLabel: UILabel!
    @IBOutlet weak var firstViewFirstDescriptionLabel: UILabel!
    @IBOutlet weak var firstViewSecondTitleLabel: UILabel!
    @IBOutlet weak var firstViewSecondDescriptionLabel: UILabel!
    @IBOutlet weak var firstViewThirdTitleLabel: UILabel!
    @IBOutlet weak var firstViewThirdDescriptionLabel: UILabel!
   
    @IBOutlet weak var impactTitleLabel: UILabel!
    @IBOutlet weak var impactDescriptionLabel: UILabel!
    
    @IBOutlet weak var resolverTitleLabel: UILabel!
    @IBOutlet weak var resolverDescriptionLabel: UILabel!
    
    @IBOutlet weak var SecondViewFirstTitleLabel: UILabel!
    @IBOutlet weak var SecondViewFirstDescriptionLabel: UILabel!
    
    @IBOutlet weak var SecondViewSecondTitleLabel: UILabel!
    @IBOutlet weak var SecondViewSecondDescriptionLabel: UILabel!
    
    @IBOutlet weak var SecondViewThirdTitleLabel: UILabel!
    @IBOutlet weak var SecondViewThirdDescriptionLabel: UILabel!
    
    @IBOutlet weak var SecondViewFourthTitleLabel: UILabel!
    @IBOutlet weak var SecondViewFourthDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        let padding  = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
//        bounds = bounds.inset(by: padding)
//    }
}
