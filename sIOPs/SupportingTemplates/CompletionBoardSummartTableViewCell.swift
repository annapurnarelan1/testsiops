//
//  FilterCategoryTableViewCell.swift
//  sIOPs
//
//  Created by mac on 29/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class CompletionBoardSummartTableViewCell: UICollectionViewCell {
    @IBOutlet weak var descriptionFirstSectionTitleLbl: UILabel!
    @IBOutlet weak var firstSectionTitleLabel: UILabel!
    @IBOutlet weak var descriptionThirdLbl: UILabel!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var descriptionSecondLbl: UILabel!
    @IBOutlet weak var secondlbl: UILabel!
    
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var secondProgressView: UIProgressView!
    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var firstProgressView: UIProgressView!
    @IBOutlet weak var separatorLine: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    @IBOutlet weak var thirdProgressView: UIProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
}
