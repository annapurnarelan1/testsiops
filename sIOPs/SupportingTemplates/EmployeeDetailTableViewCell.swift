//
//  EmployeeDetailTableViewCell.swift
//  sIOPs
//
//  Created by mac on 02/12/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit

class EmployeeDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bellBtn: UIButton!
    @IBOutlet weak var hour1Lbl: UILabel!
    @IBOutlet weak var hour2Lbl: UILabel!
    @IBOutlet weak var hour3Lbl: UILabel!
    @IBOutlet weak var hour4Lbl: UILabel!
    @IBOutlet weak var hour5Lbl: UILabel!
    @IBOutlet weak var hour6Lbl: UILabel!
    @IBOutlet weak var hour7Lbl: UILabel!
    @IBOutlet weak var date1Lbl: UILabel!
    @IBOutlet weak var date2Lbl: UILabel!
    @IBOutlet weak var date3Lbl: UILabel!
    @IBOutlet weak var date4Lbl: UILabel!
    @IBOutlet weak var date5Lbl: UILabel!
    @IBOutlet weak var date6Lbl: UILabel!
    @IBOutlet weak var date7Lbl: UILabel!
    @IBOutlet weak var status1: UILabel!
    @IBOutlet weak var status2: UILabel!
    @IBOutlet weak var status3: UILabel!
    @IBOutlet weak var status4: UILabel!
    @IBOutlet weak var status5: UILabel!
    @IBOutlet weak var status6: UILabel!
    @IBOutlet weak var status7: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
