//
//  SingleStatusCollectionViewCell.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 27/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit


class BSDCollectionViewCell: UICollectionReusableView {
   
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var portIn: UIView!
    @IBOutlet weak var portOut: UIView!
    @IBOutlet weak var lblSectionTitle: UILabel!
    @IBOutlet weak var poDescriotionLabel: UILabel!
    @IBOutlet weak var poTitleLabel: UILabel!
    @IBOutlet weak var poFirstLabel: UILabel!
    @IBOutlet weak var descriptionFirstLbl: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.portIn.layer.cornerRadius = 5.0
        self.portIn.layer.borderWidth = 1.0
        self.portIn.layer.borderColor = UIColor.clear.cgColor
        self.portIn.layer.masksToBounds = true
        
        //self.portOut.layer.cornerRadius = 5.0
        self.portOut.layer.borderWidth = 1.0
        self.portOut.layer.borderColor = UIColor.clear.cgColor
        self.portOut.layer.masksToBounds = true

        self.portIn.layer.backgroundColor = UIColor.white.cgColor
        self.portIn.layer.shadowColor = UIColor.gray.cgColor
        self.portIn.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
        self.portIn.layer.shadowRadius = 2.0
        self.portIn.layer.shadowOpacity = 1.0
        self.portIn.layer.masksToBounds = false
        
        self.portOut.layer.backgroundColor = UIColor.white.cgColor
        self.portOut.layer.shadowColor = UIColor.gray.cgColor
        self.portOut.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
        self.portOut.layer.shadowRadius = 2.0
        self.portOut.layer.shadowOpacity = 1.0
        self.portOut.layer.masksToBounds = false
        //self.setUP()
        // Initialization code
    }

}
