//
//  SceneDelegate.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 26/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import UIKit
import SwiftUI
import IQKeyboardManagerSwift




class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

        // Create the SwiftUI view that provides the window contents.
        let contentView = ContentView()
        
        if let bundleIdentifier = Bundle.main.bundleIdentifier {
            UserDefaults.standard.set(bundleIdentifier, forKey: "bundleIdentifier")
        }
        IQKeyboardManager.shared.enable = true
       
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        UserDefaults.standard.set(appVersion, forKey: Constants.UserDefaultStr.appVersion)
        
        let buildNumber: String = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        UserDefaults.standard.set(buildNumber, forKey: "buildNumber")

        // Use a UIHostingController as window root view controller.
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView)
            self.window = window
            window.makeKeyAndVisible()
        }
        if((LoginModel.sharedInstance.respData) != nil)
        {
            if(LoginModel.sharedInstance.respData[0].respMsg.responsePayload.allowToggleView == "NGO_BUSI")
            {
                Constants.toggle.toggleView = "NGO"
                NGOLeadershipServiceWireFrame.presentNGOLeadershipServiceModule(fromView: self, loginRes: LoginModel.sharedInstance)
//                NGOBusinessServiceWireFrame.presentNGOBusinessServiceModule(fromView: self,loginRes :LoginModel.sharedInstance)
            }
            else
            {
                DashboardWireFrame.presentDashboardModule(fromView: self, loginRes: LoginModel.sharedInstance)
            }
            
        }
        else
        {
        
             LoginWireFrame.presentLoginModule(fromView: self)
        }
        
        
      

       
    }

    
   
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

