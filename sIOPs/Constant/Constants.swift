//
//  Constants.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 26/11/19.
//  Copyright © 2019 reliance. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
     static let headerJourneyRegion = "Order to Activation Journey"
    private struct domains {
        static let  runtime = BuildConfiguration.sharedInstance.runtime()
        static let  imageUrl = BuildConfiguration.sharedInstance.imageUrl()
        
    }
    struct status {
        static let OK = "OK"
        static let NOK = "NOK"
        static let REQUEST_FAIL = "FAIL"
        static let NETWORK_UNAVAILABLE = "NETWORK_UNAVAILABLE"
        static let RESOURCE_UNAVAILABLE = "RESOURCE_UNAVAILABLE"
        static let SUCCESS = "SUCCESS"
        static let FAILURE = "FAILURE"
        
    }
    
    
    
    struct InfraDetail
    {
        static let alarm = "Alarms"
        static let siteDown = "Sites down"
    }
    
    /// Private Struct - API version
    private struct API{
        static let V1 = BuildConfiguration.sharedInstance.API()
    }
    
    struct server {
        static let baseURL = domains.runtime
        static let imageUrl = domains.imageUrl
        
    }
    
    struct APIRoutes {
        
        static let loginApi = "jio-infra-nw-login-service/servlet/Service"
        
    }
    
    struct MESSAGES {
        static let pleaseWaitMessage = "Loading.."
    }
    
    struct COLOR {
        static let loginDisabled = UIColor.init(displayP3Red: 195.0/255.0, green: 195.0/255.0, blue: 195.0/255.0, alpha: 1.0)
        
        static let loginEnabled = UIColor.init(displayP3Red: 33.0/255.0, green: 71.0/255.0, blue: 150.0/255.0, alpha: 1.0)
    }
    
    //MARK: Notification Contants
    struct Notification {
        static var badgeLabel : UILabel?
        static var notificationButton : UIButton?
    }
    
    struct openAlerts {
        static var infra  =  "infra"
        static var application =  "application"
        static var tools = "tools"
    }
    
    struct filterTypes {
        static var Ageing  =  "INFRA_AGING_FILTER"
        static var ImpactedCustomer =  "INFRA_IMPACTED_CUST_FILTER"
        static var WorkOrder = "INFRA_WORK_ORDER_FILTER"
        static var InventoryMasterRegion = "INVENTORY_MASTER_REGION"
        static var InventoryMasterCircle = "INVENTORY_MASTER_CIRCLE"
        static var InventoryMasterMP = "INVENTORY_MASTER_MP"
        static var InventoryMasterJC = "INVENTORY_MASTER_JC"
        static var InventoryMasterSAPID = "INVENTORY_MASTER_SAPID"
    }
    
    struct  rememberUsername {
        static let isUsername = "isUsername"
        static let username = "username"
    }
    
    struct toggle {
        static var toggleView = "NGO"
        static let NGOBuisnessToggle = "NGO_BUSI"
        static let NGO = "NGO"
        static let INFRA = "INFRA"
        static let BOTH = "BOTH"
    }
    
    struct ApplicationCode {
        static let F_NGO_NextGenOPS = "F_NGO_NextGenOPS"
        static let F_RAN = "F_RAN"
    }
    
    
    struct BusiCode {
        static let validateOTP = "ValidateOTP"
        static let resendOTP = "ResendOTP"
        static let login = "Login"
        static let getTransKey = "GetTransKey"
        static let NGODetailSummary = "NGODetailSummary"
        static let NGOOnclickType = "NGOOnclickType"
        static let NGOAlertHistory = "NGOAlertHistory"
        static let NGOOpenAlertHistory = "NGOOpenAlertHistory"
        static let NGOTSPlatform = "NGOTSPlatform"
        static let NGOTSDomain = "NGOTSDomain"
        static let SendReminder = "SendReminder"
        static let NGODelinquentEmployee = "NGODelinquentEmployee"
        static let SiteDownCategory = "SiteDownCategory"
        static let UtilityAlarmCategory = "UtilityAlarmCategory"
        static let InfraPerformanceDetailSummary = "InfraPerformanceDetailSummary"
        static let UtilityAlarmHistory = "UtilityAlarmHistory"
        static let UtilityAlarmDetail = "UtilityAlarmDetail"
        static let Filter = "Filter"
        static let SiteDownHistory = "SiteDownHistory"
        static let SiteDownDetail = "SiteDownDetail"
        static let BCBMainControl = "BCBMainControl"
        static let NGOHistorySummary = "NGOHistorySummary"
        static let NGOSummary = "NGOSummary"
        static let SiteDownSummary = "SiteDownSummary"
        static let UtilityAlarm = "UtilityAlarm"
        static let DashSummary = "DashSummary"
        static let NGOMACDDetails = "NGOMACDDetails"
        static let NGOPortInPortOutCountTrends = "NGOPortInPortOutCountTrends"
        static let NGORechargeActivation = "NGORechargeActivation"
        static let NGOOrderActivation = "NGOOrderActivation"
        static let GraniteDataComplete = "GraniteDataComplete"
        static let DataCompleteOnClick = "DataCompleteOnClick"
        static let PerformanceRAN = "PerformanceRAN"
        static let AvailabilityRAN = "AvailabilityRAN"
        static let NotificationList = "NotificationList"
        static let NGORechargeJourneyDetail = "NGORechargeJourney"
        static let O2ASummary = "O2ASummary"
        static let O2AJourneyCategory = "O2AJourneyCategory"
        static let WorkOrder  = "WorkOrderRAN"
        static let NGOOpenAlerts = "NGOOpenAlerts"
        static let NGOOpenIncident = "NGOOpenIncident"
        static let NGOOpenIncidentHistory = "NGOOpenIncidentHistory"
        static let NGOOpenChange = "NGOOpenChange"
        static let NGOOpenDefect = "NGOOpenDefect"
        static let NGOProblemTicket = "NGOProblemTicket"
        static let Logout = "Logout"
        static let NGOOpenAction = "NGOOpenAction"
        static let TimeClocking  = "NGOEmployeeWiseSelfClocking"
        static let NGOOpenDefectOnClick = "NGOOpenDefectOnClick"
        static let NGOOpenDefectDetail = "NGOOpenDefectDetail"
        static let O2AInProcess = "O2AInProcess"
        static let O2ARejected = "O2ARejected"
    }
    
    struct OpenIncidentType
      {
          static let buisnessImpacting = "Buisness Impacting"
          static let nonBuisnessImpacting = "Non-Buisness Impacting"
      }
    
    struct OpenActionType
    {
        static let pendingAction = "Pending Action"
        static let overdueAction = "Overdue Action"
    }
    
    
    struct ProblemTicketsType
    {
        static let pendingRCA = "Pending RCA"
        static let RCAActionItem = "RCA Action Item"
    }
    
    struct UserDefaultStr {
        static let appVersion = "appVersion"
    }
    
    struct collectIonViewCell {
        static let UserfullLinksCollectionViewCell = "UserfullLinksCollectionViewCell"
        static let HeaderCollectionReusableView = "HeaderCollectionReusableView"
    }
    
}
