//
//  DataPersisterManager.swift
//  sIOPs
//
//  Created by Gunjan Goyal on 28/11/19.
//  Copyright © 2019 Gunjan Goyal. All rights reserved.
//

import Foundation


/// DataPersisterManager handles the persistency in the disk
class DataPersisterManager: NSObject {
    // MARK: variables
    
    /// Singleton instance
    static let sharedInstance = DataPersisterManager()
    
    // MARK: Initializers
    
    /// Private initializer
    override private init() {
    }
    
    // MARK: methods
    
    
    
    
    
    
    /// Saves the User object in the UserDefaults
    func saveLoginResponse(){
        do {
            try UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: LoginModel.sharedInstance, requiringSecureCoding: false), forKey: "Login")
        } catch {
            print("Can't encode data: \(error)")
        }
       
        
    }
    
    func deleteLoginResponse(){
           do {
            try UserDefaults.standard.removeObject(forKey: "Login")
           } catch {
               print("Can't encode data: \(error)")
           }
          
           
       }
    
    
    /// Returns the user in the disk if exists, otherwise will return nil
    func getLoginResponse() -> LoginModel?{
        let ud = UserDefaults.standard
        if let decodedNSData = ud.object(forKey: "Login") as? NSData{
            
            do {
                if let user =  try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decodedNSData as Data) as? LoginModel
                {
                    return user
                }
                
            }
            catch
            {
                fatalError("Can't decode data: \(error)")
            }
        }
        return nil
    }
    
    
    
    
   
}
